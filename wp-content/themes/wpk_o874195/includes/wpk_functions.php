<?php

namespace wpk;

function dump( $var ) {
	ob_start();
	?>
	<pre>
	<?php var_dump( $var ) ?>
</pre>
	<?php
	echo ob_get_clean();
}

function wpk_get_option() {
}

function wpk_update_option() {
}

/**
 * Like get_template_part() put lets you pass args to the template file
 * Args are available in the template as $template_args array
 *
 * @param string $file
 * @param mixed $template_args style argument list
 * @param mixed $cache_args
 *
 * @return mixed
 */
function get_template_part( $file, $template_args = array(), $cache_args = array() ) {
	$template_args = wp_parse_args( $template_args );
	$cache_args    = wp_parse_args( $cache_args );
	if ( $cache_args ) {
		foreach ( $template_args as $key => $value ) {
			if ( is_scalar( $value ) || is_array( $value ) ) {
				$cache_args[ $key ] = $value;
			} else if ( is_object( $value ) && method_exists( $value, 'get_id' ) ) {
				$cache_args[ $key ] = call_user_method( 'get_id', $value );
			}
		}
		if ( ( $cache = wp_cache_get( $file, serialize( $cache_args ) ) ) !== false ) {
			if ( ! empty( $template_args[ 'return' ] ) ) {
				return $cache;
			}
			echo $cache;

			return;
		}
	}
	$file_handle = $file;
	if ( file_exists( get_stylesheet_directory() . '/' . $file . '.php' ) ) {
		$file = get_stylesheet_directory() . '/' . $file . '.php';
	} elseif ( file_exists( get_template_directory() . '/' . $file . '.php' ) ) {
		$file = get_template_directory() . '/' . $file . '.php';
	}
	ob_start();
	$return = require( $file );
	$data   = ob_get_clean();
	if ( $cache_args ) {
		wp_cache_set( $file, $data, serialize( $cache_args ), 3600 );
	}
	if ( ! empty( $template_args[ 'return' ] ) ) {
		if ( $return === false ) {
			return false;
		} else {
			return $data;
		}
	}
	echo $data;
}