<?php
global $conversations, $campaign_query, $admin_conversations;

$type = get_user_meta( get_current_user_id(), 'wpk_user_type', true );
if ( $type === 'company' ) {
	$user           = new \Wpk\User_types\Company\User();
	$campaign_query = $user->getCampaignsQuery( [
		'post_status' => [ 'draft', 'publish', 'started' ],
	] );
} else if ( $type === 'influencer' ) {

	$user     = new \Wpk\User_types\Influencer\User();
	$accepted = $user->getAcceptedCampaigns();
	$to_add   = $user->getToAddCampaigns();

	$collaborated     = $user->getCollaborationCampaigns();
	$invited_to_query = $user->getInvitations( null, 'to' );
	$invited_to       = [];
	if ( $invited_to_query->have_posts() ) {
		/**@var \Wpk\Post_types\Campaign_invitation\Post $item */
		foreach ( $invited_to_query->get_posts() as $item ) {
			$invited_to[] = $item->getCampaignId();
		}
	}
	$offers_sent = $user->getWpkMeta( 'offers_sent' );
	$paged       = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$ids         = array_merge( $accepted, $collaborated, $invited_to + $to_add );
	$args        = [
		'post_status'    => [ 'draft', 'publish', 'started' ],
		'posts_per_page' => 10,
		'paged'          => $paged,
		'post__in'       => $ids,
		'orderby'        => 'meta_value',
		'meta_key'       => 'wpk_start_date',
		'order'          => 'ASC'
	];

	$campaign_query = new \Wpk\Post_types\Campaign\Query( $args );


} else {
	wp_redirect( home_url() );
}
$admin_query         = \Wpk\Post_types\Conversation\Post::getAdminConversationQuery( $user );
$admin_conversations = $admin_query->get_posts();
$campaigns           = $campaign_query->get_posts();
$conversations       = [];
if ( ! empty( $campaign_query ) ) {
	/** @var \Wpk\Post_types\Campaign\Post $campaign */
	foreach ( $campaigns as $campaign ) {
		$conv = $campaign->getConversations();
		if ( ! empty( $conv ) ) {
			if ( $type === 'company' ) {
				$status = $campaign->post->post_status;

				$status = $status === 'draft' || $status === 'publish' ? __( 'Published', 'wpk' ) : $status;
			} else {
				if ( $campaign->haveInfluencer( $user ) || $campaign->willBeAdded( $user ) ) {
					$status = __( 'In campaign', 'wpk' );

				} else if ( $campaign->isInvited( $user ) ) {
					$status = __( 'Invited', 'wpk' );
				} else if ( $campaign->haveSendCollaboration( $user ) ) {
					$status = __( 'Collaborate', 'wpk' );
				} else {
					$status = $campaign->post->post_status;
				}
			}
			$conversations[ $status ][ $campaign->post->ID ] = $conv;
		}
	}
}
get_header();
?>
	<div class="wpk-wrap row wpk-conversations">

		<?php if ( empty( $conversations ) && empty( $admin_conversations ) ): ?>
			<a class="wpk-circle-icon wpk-has-message wpk-full-icon" href="#">
				<div class="wpk-circle">
					<div class="wpk-circle-inner">
						<i class="material-icons">email</i>
					</div>
				</div>
				<div class="wpk-message">
					<?php _e( 'You don\'t have any messages yet.', 'wpk' ) ?>
				</div>
			</a>
		<?php else: ?>

			<?php Wpk\Utility::getSidebarTemplate() ?>

			<div class="col-xl-10 col-md-12 wpk-wrap-inner">
				<?php
				get_template_part( 'templates/post-types/conversation/search-results' );
				global $campaign;
				if ( isset( $_GET[ 'action' ] ) ) {
					$action = $_GET[ 'action' ];
					if ( $action === 'new_message' && ! empty( $_GET[ 'campaign_id' ] ) ) {
						$campaign = new \Wpk\Post_types\Campaign\Post( intval( $_GET[ 'campaign_id' ] ) );
						if ( $campaign->post->post_author == $user->ID ) {
							get_template_part( 'templates/post-types/conversation/new-message' );
						}
					}
				}

				?>

			</div>

		<?php endif; ?>
	</div>
<?php
get_footer();