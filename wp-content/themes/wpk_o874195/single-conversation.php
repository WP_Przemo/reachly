<?php
global $conversations, $post, $user, $influencer, $company, $campaign_query, $admin_query;

$conversation      = new \Wpk\Post_types\Conversation\Post( $post );
$conversation_type = $conversation->getWpkMeta( 'conversation_type' );
$conversation_type = empty( $conversation_type ) ? 'campaign' : $conversation_type;
$user_id           = get_current_user_id();
if ( $conversation_type === 'campaign' ) {
	$campaign   = $conversation->getCampaign( true );
	$influencer = $conversation->getInfluencer( true );
	$company    = new \Wpk\User_types\Company\User( $conversation->getWpkMeta( 'company' ) );
	$type       = get_user_meta( get_current_user_id(), 'wpk_user_type', true );


	if ( $user_id != $influencer->ID && $user_id != $company->ID ) {
		wp_redirect( home_url() );
	}
	if ( $campaign->isSkipped( $influencer ) || ( ! $campaign->haveInfluencer( $influencer ) && ! $campaign->willBeAdded( $influencer ) && ! $campaign->isInvited( $influencer ) && ! $campaign->haveSendCollaboration( $influencer ) ) ) {
		wp_redirect( get_post_type_archive_link( \Wpk\Post_types\Conversation\Post::$post_type ) );
	}
} else if ( $conversation_type === 'admin' ) {
	if ( ! \Wpk\Utility::isAdmin() && intval( $conversation->getWpkMeta( 'user' ) ) != $user_id ) {
		wp_redirect( get_post_type_archive_link( \Wpk\Post_types\Conversation\Post::$post_type ) );
	}
} else {
	wp_redirect( get_post_type_archive_link( \Wpk\Post_types\Conversation\Post::$post_type ) );
}

if ( $conversation_type === 'campaign' ) {

	if ( $company->ID == $user_id ) {
		$user           = $company;
		$campaign_query = $user->getCampaignsQuery();
	} else if ( $influencer->ID = $user_id ) {

		$user     = $influencer;
		$accepted = $user->getAcceptedCampaigns();
		$to_add   = $user->getToAddCampaigns();

		$collaborated     = $user->getCollaborationCampaigns();
		$invited_to_query = $user->getInvitations( null, 'to' );
		$invited_to       = [];
		if ( $invited_to_query->have_posts() ) {
			/**@var \Wpk\Post_types\Campaign_invitation\Post $item */
			foreach ( $invited_to_query->get_posts() as $item ) {
				$invited_to[] = $item->getCampaignId();
			}
		}
		$offers_sent = $user->getWpkMeta( 'offers_sent' );
		$paged       = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$ids         = array_merge( $accepted, $collaborated, $invited_to + $to_add );
		$args        = [
			'post_status'    => 'any',
			'posts_per_page' => 10,
			'paged'          => $paged,
			'post__in'       => $ids,
			'orderby'        => 'meta_value',
			'meta_key'       => 'wpk_start_date',
			'order'          => 'ASC',
		];

		$campaign_query = new \Wpk\Post_types\Campaign\Query( $args );


	} else {
		wp_redirect( home_url() );
	}
} else {
	$user = new \Wpk\Helpers\User();
}

if ( ! empty( $campaign_query ) ) {

	$campaigns = $campaign_query->get_posts();
	$args      = [
		'post_status' => 'any',
		'post_type'   => 'wpk_notification',
		'meta_query'  => [
			[
				'key'   => 'wpk_id',
				'value' => $post->ID,

			],
			[
				'key'   => 'wpk_receiver',
				'value' => get_current_user_id(),
			],
		],
	];
}
/*$notif_query = new \Wpk\Helpers\Query( $args );

if ( $notif_query->have_posts() ) {
	foreach ( $notif_query->get_posts() as $post ) {
		$post->delete();
	}
}*/

$admin_query         = \Wpk\Post_types\Conversation\Post::getAdminConversationQuery( $user );
$admin_conversations = $admin_query->get_posts();

$conversations = [];
if ( ! empty( $campaign_query ) ) {
	/** @var \Wpk\Post_types\Campaign\Post $campaign */
	foreach ( $campaigns as $campaign ) {
		$conv = $campaign->getConversations();
		if ( ! empty( $conv ) ) {
			if ( $type === 'company' ) {
				$status = $campaign->post->post_status;
				$status = $status === 'draft' || $status === 'publish' ? __( 'Published', 'wpk' ) : $status;
			} else {
				if ( $campaign->haveInfluencer( $user ) || $campaign->willBeAdded( $user ) ) {
					$status = __( 'Campaigns that you are in', 'wpk' );

				} else if ( $campaign->isInvited( $user ) ) {
					$status = __( 'Invited', 'wpk' );
				} else if ( $campaign->haveSendCollaboration( $user ) ) {
					$status = __( 'Collaborate', 'wpk' );
				} else {
					$status = $campaign->post->post_status;
				}
			}
			$conversations[ $status ][ $campaign->post->ID ] = $conv;
		}
	}
}
get_header();

?>
	<div class="wpk-wrap row wpk-conversations">

		<?php Wpk\Utility::getSidebarTemplate() ?>

		<div class="col-xl-10 col-md-12 wpk-wrap-inner">
			<?php
			if ( $conversation_type === 'campaign' ) {
				get_template_part( "templates/post-types/conversation/single-conversation" );
			} else if ( $conversation_type === 'admin' ) {
				get_template_part( "templates/post-types/conversation/single-conversation-admin" );
			}
			get_template_part( 'templates/post-types/conversation/search-results' );;
			?>

		</div>
	</div>
	<script>
		(function( $ ) {

			$( document ).ready( function() {
				var $target, $accordion;

				if ( isset( wpk_vars.get.campaign_id ) ) {
					$target = $( 'li[data-campaign-id=' + wpk_vars.get.campaign_id + ']' ).eq( 0 );
					$accordion = $target.parents( '.wpk-accordion-title:not(.wpk-active)' );
					console.log( $accordion );
					setTimeout( function() {
						$accordion.find( '>span' ).click();
					}, 1000 );
				} else {

					$target = $( 'li[data-conversation-id=<?php echo $post->ID ?>]' ).eq( 0 );
					$accordion = $target.parents( '.wpk-accordion-title:not(.wpk-active)' );

					setTimeout( function() {
						$accordion.find( '>span' ).click();
					}, 1000 );
				}
			} )


		})( jQuery );
	</script>
<?php
get_footer();