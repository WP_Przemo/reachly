<?php
/**
 * Template Name: Register
 *
 * @package wpk
 */

$types = [ 'influencer', 'company' ];
if ( ! is_user_logged_in() ) {
	wp_redirect( home_url() );
}
$user = new \Wpk\Helpers\User();
$type = $user->getWpkMeta( 'user_type' );

if($user->getWpkMeta('registered') === 'yes'){
	wp_redirect(get_permalink(\Wpk\Pages::getProfilePage()));
}

/*if ( empty( $type ) && ! isset( $_GET[ 'wpk_type' ] ) ) {
	wp_redirect( \Wpk\Utility::getAuthUrl( [
		'register' => '1'
	] ) );
} */
get_header();
?>

<div class="wpk-wrap wpk-register">
	<div class="col-xl-12 col-md-12 wpk-wrap-inner">

		<?php get_template_part( "templates/register/register-form-$type" ) ?>

	</div>
</div>
<?php get_footer() ?>

