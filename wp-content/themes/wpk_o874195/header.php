<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php
	/**
	 * @see buro_mikado_header_meta() - hooked with 10
	 * @see mkd_user_scalable - hooked with 10
	 */
	?>
	<?php do_action( 'buro_mikado_header_meta' ); ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
<?php buro_mikado_get_side_area(); ?>


<?php
if ( buro_mikado_options()->getOptionValue( 'smooth_page_transitions' ) == "yes" ) {
	$buro_mikado_ajax_class = 'mkd-mimic-ajax';
	?>
	<div class="mkd-smooth-transition-loader <?php echo esc_attr( $buro_mikado_ajax_class ); ?>">
		<div class="mkd-st-loader">
			<div class="mkd-st-loader1">
				<?php buro_mikado_loading_spinners(); ?>
			</div>
		</div>
	</div>
<?php } ?>

<div class="mkd-wrapper">
	<div class="mkd-wrapper-inner">
		<?php buro_mikado_get_header(); ?>

		<?php if ( buro_mikado_options()->getOptionValue( 'show_back_button' ) == "yes" ) { ?>
			<a id='mkd-back-to-top' href='#'>
                <span class="mkd-icon-stack">
                     <?php
                     buro_mikado_icon_collections()->getBackToTopIcon( 'font_elegant' );
                     ?>
                </span>
			</a>
		<?php } ?>
		<?php buro_mikado_get_full_screen_menu(); ?>

		<div class="mkd-content" <?php buro_mikado_content_elem_style_attr(); ?>>
			<div class="mkd-content-inner">