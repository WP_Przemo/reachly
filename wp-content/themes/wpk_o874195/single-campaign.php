<?php
global $post, $campaign;

$campaign  = new \Wpk\Post_types\Campaign\Post( $post );
$user      = new \Wpk\Helpers\User();
$is_author = $campaign->post->post_author == $user->ID;
$type      = $user->getUserType();
if ( $type !== 'influencer' && ! $is_author ) {
	wp_redirect( home_url() );
}
if ( $type === 'influencer' ) {
	$invites = $campaign->getInvitationsQuery( 'from' );
	if ( $invites->have_posts() ) {
		$invite      = $invites->get_posts()[ 0 ];
		$args        = [
			'post_type'  => 'wpk_notification',
			'meta_query' => [
				'key'   => 'id',
				'value' => $invite->post->ID,
			]
		];
		$notif_query = new \Wpk\Helpers\Query( $args );
		if ( $notif_query->have_posts() ) {
			$notification = $notif_query->get_posts()[ 0 ];
			$notification->delete();
		}
	}
}
get_header();
?>
	<div class="wpk-wrap row wpk-my-campaigns">
		<?php
		if ( $is_author ) {
			Wpk\Utility::getSidebarTemplate();
		} ?>
		<div class="<?php echo $is_author ? 'col-xl-10' : 'col-xl-12' ?> col-md-12 wpk-wrap-inner">
			<?php
			get_template_part( "templates/$type/single-campaign" );
			?>
		</div>
	</div>
<?php
get_footer();