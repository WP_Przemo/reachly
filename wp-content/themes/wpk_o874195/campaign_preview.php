<?php
/**
 * Template Name: Campaign preview
 *
 * @package wpk
 */
if ( ! isset( $_COOKIE[ 'wpk_campaign_data' ] ) ) {
	wp_redirect( home_url() );
}

$campaign_data = ( $_COOKIE[ 'wpk_campaign_data' ] );
$campaign_data = maybe_unserialize( wp_unslash( $campaign_data ) );
extract( $campaign_data );
/**
 * @var array $items
 * @var string $website
 * @var array $categories
 * @var array $tags
 * @var string $post_title
 * @var string $post_content
 * @var string $start_date
 * @var int $thumbnail_id
 * @var int $logo_id
 * @var int $id
 */
try {
	$start_date_dt = new DateTime( $start_date );
} catch ( Exception $e ) {
	$start_date_dt = new DateTime();
}
get_header();
?>
	<div class="single-campaign">
		<div class="wpk-wrap row wpk-my-campaigns">

			<div class="col-xl-12 col-md-12 wpk-wrap-inner">
				<div class="wpk-single-campaign wpk-single-campaign-influencer">
					<!--<div class="wpk-navgiation-actions">
					<div class="wpk-item-actions">
						<?php /*if ( $influencer->isVerifed() ) : */ ?>
							<?php /*if ( $campaign->isInvited( $influencer ) ) : */ ?>
								<?php
					/*								$invites = $campaign->getInvitationsQuery( 'from' );
													$invite  = $invites->get_posts()[ 0 ];
													*/ ?>
								<div data-action="accept" class="wpk-influencer-action wpk-button" data-id="<?php /*echo $invite->post->ID */ ?>">
									<i class="material-icons">check</i>
									<span><?php /*_e( 'Accept invite' ) */ ?></span>
								</div>
								<div data-action="decline" class="wpk-influencer-action wpk-button" data-id="<?php /*echo $invite->post->ID */ ?>">
									<i class="material-icons">close</i>
									<span><?php /*_e( 'Decline invite' ) */ ?></span>
								</div>
							<?php /*elseif ( $campaign->haveInfluencer( $influencer ) ): */ ?>
								<div data-action="none" class="wpk-influencer-action wpk-button wpk-disabled">
									<i class="material-icons">today</i>
									<span><?php /*printf( __( 'In campaign until %s', 'wpk' ), date( 'd.m.Y', strtotime( $campaign->getRenewalDate() ) ) ) */ ?></span>
								</div>
							<?php /*elseif ( $campaign->hasDeclined( $influencer ) ) : */ ?>
								<div data-action="none" class="wpk-influencer-action wpk-button wpk-disabled" data-id="<?php /*echo $campaign->post->ID */ ?>">
									<span><?php /*_e( 'Invitation declined' ) */ ?></span>
									<i class="material-icons">block</i>
								</div>
							<?php /*elseif ( ! $campaign->isSkipped( $influencer ) && $campaign->canBeModified() ): */ ?>
								<div data-action="collaborate" class="wpk-influencer-action wpk-button" data-id="<?php /*echo $campaign->post->ID */ ?>">
									<span><?php /*_e( 'Collaborate' ) */ ?></span>
									<i class="material-icons">mail_outline</i>
								</div>
							<?php /*endif; */ ?>

						<?php /*endif; */ ?>
					</div>
				</div>-->
					<article class="wpk-single-campaign-inner" id="<?php echo $id ?>">
						<?php if ( ! empty( $logo_id ) ) : ?>
							<div class="wpk-campaign-logo">
								<img src="<?php echo wp_get_attachment_url( $logo_id ) ?>" alt="">
							</div>
						<?php endif; ?>
						<?php if ( ! empty( $website ) ) : ?>
							<div class="wpk-campaign-website">
								<a href="<?php echo $website ?>"><?php echo $website ?></a>
							</div>
						<?php endif; ?>
						<?php if ( ! empty( $thumbnail_id ) ): ?>
							<div class="wpk-campaign-thumbnail">

								<img src="<?php echo wp_get_attachment_url( $thumbnail_id ) ?>" alt="">
								<div class="wpk-campaign-date">
									<i class="material-icons">date_range</i>
									<span><?php _e( 'Campaign starts: ', 'wpk' ) ?></span>

									<span><?php printf( __( '%sth', 'wpk' ), $start_date_dt->format( 'F d' ) ) ?></span>
								</div>
							</div>
						<?php endif; ?>
						<div class="wpk-campaign-title wpk-section">
							<div class="wpk-section-inner">
								<?php echo $post_title ?>
							</div>
						</div>
						<div class="wpk-campaign-description wpk-section">
							<div class="wpk-section-title">
								<?php _e( 'What we\'re looking for?', 'wpk' ) ?>
							</div>
							<div class="wpk-section-inner">
								<?php echo $post_content ?>
							</div>
						</div>
						<?php if ( ! empty( $tags ) ) : ?>
							<div class="wpk-campaign-tags wpk-section">
								<div class="wpk-section-title">
									<?php _e( 'Preffered tags', 'wpk' ) ?>
								</div>
								<div class="wpk-section-inner">
									<?php foreach ( $tags as $tag ) : ?>
										<div class="wpk-tag">
											<?php echo '#' . $tag ?>
										</div>
									<?php endforeach; ?>

								</div>
							</div>
						<?php endif; ?>
						<?php if ( ! empty( $items ) ): ?>
							<div class="wpk-item-section wpk-section">
								<div class="wpk-section-title">
									<?php _e( 'What you get?', 'wpk' ) ?>
								</div>
								<div class="wpk-section-inner">
									<?php foreach ( $items as $item ) : ?>
										<?php if ( ! empty( $item ) ): ?>
											<div class="wpk-checkbox">
												<input type="checkbox" checked>
												<label><?php echo $item ?></label>
											</div>
										<?php endif; ?>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
					</article>
				</div>

			</div>
		</div>
	</div>
<?php
get_footer();

