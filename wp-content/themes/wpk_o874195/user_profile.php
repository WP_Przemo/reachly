<?php
/**
 * Template Name: User profile
 *
 * @package wpk
 */

if (!is_user_logged_in()){
	auth_redirect();
}

get_header();
$user = new \Wpk\Facebook\Instagram\User();
$type = $user->getUserType();
?>

	<div class="wpk-wrap row" id="wpk_user_profile">
		<div class="wpk-page-title">
			<h2><?php _e('Profile Settings', 'wpk') ?></h2>
		</div>
		<?php \Wpk\Utility::getSidebarTemplate() ?>
		<div class="col-xl-10 col-md-12 wpk-wrap-inner">
			<?php get_template_part( 'templates/user-profile/tabs' ); ?>

		</div>
	</div>
<?php
get_footer();