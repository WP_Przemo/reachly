<?php
/**
 * Template Name: New campaign
 *
 * @package wpk
 */

/**@global \Wpk\Post_types\Campaign\Post $current_campaign */
global $campaign_id, $campaign, $step, $current_user;

if ( ! is_user_logged_in() ) {
	wp_redirect( home_url() );
}
$current_user = new \Wpk\User_types\Company\User();
$type         = $current_user->getUserType();
if ( $type !== 'company' ) {
	wp_redirect( home_url() );
}

$step = isset( $_GET[ 'step' ] ) && ! empty( $_GET[ 'step' ] ) ? intval( $_GET[ 'step' ] ) : 1;
if ( isset( $_GET[ 'key' ] ) ) {
	$order_id = wc_get_order_id_by_order_key( $_GET[ 'key' ] );
	$order    = wc_get_order( $order_id );

	if ( $order->get_item_count() > 1 ) {
		wp_redirect( get_permalink( \Wpk\Pages::getMyCampaignsPage() ) );
	} else {
		$campaign_id = array_shift( $order->get_meta( '_wpk_campaign_ids', true ) );
		$campaign    = new \Wpk\Post_types\Campaign\Post( $campaign_id );
		wp_redirect( $campaign->getCreationUrl() );
	}
}
$campaign_id = isset( $_GET[ 'campaign_id' ] ) ? $_GET[ 'campaign_id' ] : '';
if ( ! empty( $campaign_id ) ) {
	try {
		$campaign = new \Wpk\Post_types\Campaign\Post( $campaign_id );
	} catch ( \Wpk\Exception\WPKException $e ) {
		wp_redirect( \Wpk\Pages::getCampaignCreationUrl() );
	}
}

get_header();


?>

	<div class="wpk-wrap row wpk-my-campaigns">
		<?php if ( ! $current_user->isVerifed() ): ?>
			<div class="col-xl-12 col-md-12 wpk-wrap-inner">
				<a class="wpk-circle-icon wpk-has-message wpk-full-icon" href="<?php echo get_permalink( \Wpk\Pages::getProfilePage() ) ?>">
					<div class="wpk-circle">
						<div class="wpk-circle-inner">
							<i class="material-icons">check</i>
						</div>
					</div>
					<div class="wpk-message">
						<?php _e( 'Verify your account', 'wpk' ) ?>
					</div>
				</a>
			</div>
		<?php else: ?>
			<?php \Wpk\Utility::getSidebarTemplate() ?>
			<div class=" col-sm-12 col-xl-10 col-md-8 wpk-wrap-inner">
				<?php
				get_template_part( 'templates/company/campaign-creation' );
				?>
			</div>
		<?php endif; ?>
	</div>
<?php
get_footer();