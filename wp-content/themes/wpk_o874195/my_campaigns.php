<?php
/**
 * Template Name: My campaigns
 *
 * @package wpk
 */
if ( ! is_user_logged_in() ) {
	auth_redirect();
}
$user = new \Wpk\Helpers\User();
$type = $user->getUserType();
get_header();
?>

	<div class="wpk-wrap row wpk-my-campaigns">
		<?php if ( $type === 'company' ) {
			Wpk\Utility::getSidebarTemplate();
		}
		?>
		<div class="<?php echo $type === 'company' ? 'col-xl-10' : 'col-xl-12' ?> col-md-12 wpk-wrap-inner">
			<?php get_template_part( "templates/$type/campaigns" ); ?>
		</div>
	</div>

<?php
get_footer();