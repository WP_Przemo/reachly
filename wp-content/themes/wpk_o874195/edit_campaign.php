<?php
global $edit_action;

/**
 * Template Name: Edit campaign
 *
 * @package wpk
 */
if ( ! isset( $_GET[ 'id' ] ) || ! is_user_logged_in() ) {
	wp_redirect( home_url() );
}

$edit_action = isset( $_GET[ 'action' ] ) ? $_GET[ 'action' ] : 'edit';

$current_campaign = new \Wpk\Post_types\Campaign\Post( $_GET[ 'id' ] );
$user             = new \Wpk\User_types\Company\User();

if ( $user->getUserType() !== 'company' || $current_campaign->post->post_author != $user->ID || ! $current_campaign->isFullyCreated() ) {
	wp_redirect( home_url() );
}

if ( $edit_action === 'influencers' && ! $current_campaign->canBeModified( 'renewed_date' ) ) {

	wp_redirect( $current_campaign->getEditUrl() );

}

get_header();
?>
	<div class="wpk-wrap row wpk-my-campaigns">

		<?php \Wpk\Utility::getSidebarTemplate() ?>
		<div class="col-sm-12 col-xl-10 col-md-8 wpk-wrap-inner">
			<?php
			if ( $edit_action === 'edit' ) {
				get_template_part( 'templates/post-types/campaign/edit-campaign' );
			} else if ( $edit_action === 'influencers' ) {
				get_template_part( 'templates/post-types/campaign/edit-influencers' );
			}
			?>
		</div>

	</div>
<?php get_footer() ?>