<?php
global $campaigns;
$args = [
	'orderby'     => 'meta_value',
	'meta_key'    => 'wpk_start_date',
	'order'       => 'ASC',
	'post_status' => 'any',
	'meta_query'  => [
		[
			'key'     => 'wpk_creation_step',
			'value'   => '3',
			'compare' => '>'
		]
	]
];
if ( isset( $_GET[ 'wpk_author' ] ) ) {
	$args[ 'author' ] = $_GET[ 'wpk_author' ];
}

$user      = new \Wpk\Helpers\User();
$campaigns = new \Wpk\Post_types\Campaign\Query( $args );
$type      = $user->getUserType();

if ( $type !== 'influencer' ) {
	wp_redirect( home_url() );
}
get_header();

?>
	<div class="wpk-wrap">
		<div class="wpk-wrap-inner">
			<?php
			if ( $campaigns->have_posts() ) {
				get_template_part( "templates/influencer/campaigns-grid" );
			} else{
				?>
				<a class="wpk-circle-icon wpk-has-message wpk-full-icon" href="<?php echo get_permalink(\Wpk\Pages::getBrandsPage()) ?>">
					<div class="wpk-circle">
						<div class="wpk-circle-inner">
							<i class="material-icons">error</i>
						</div>
					</div>
					<div class="wpk-message">
						<?php _e( 'No campaigns found. Return?', 'wpk' ) ?>
					</div>
				</a>
				<?php
			} ?>
		</div>
	</div>

<?php
get_footer();