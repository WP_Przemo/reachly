<?php
/**
 * Template Name: Brands
 *
 * @package wpk
 */

if ( ! is_user_logged_in() ) {
	auth_redirect();
}
$user = new \Wpk\User_types\Influencer\User();
if ( $user->getUserType() !== 'influencer' ) {
	wp_redirect( home_url() );
}
get_header();
?>

	<div class="wpk-wrap row wpk-my-campaigns">
		<div class="col-xl-12 col-md-12 wpk-wrap-inner">
			<?php get_template_part('templates/user-types/company/company-grid') ?>
		</div>
	</div>

<?php
get_footer();
