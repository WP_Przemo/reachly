<?php
ini_set( 'max_execution_time', 500 );
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( ! function_exists( 'child_theme_configurator_css' ) ):
	function child_theme_configurator_css() {
		wp_enqueue_style( 'chld_thm_cfg_child', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', [
			'buro_mikado_default_style',
			'buro_mikado_default_style',
			'buro_mikado_modules_plugins',
			'buro_mikado_modules',
			'mkd_font_awesome',
			'mkd_font_elegant',
			'mkd_ion_icons',
			'mkd_linea_icons',
			'mkd_linear_icons',
			'buro_mikado_blog',
			'buro_mikado_modules_responsive',
			'buro_mikado_blog_responsive',
		] );
	}
endif;
add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css' );

// END ENQUEUE PARENT ACTION

require_once 'includes/wpk_constants.php';
require_once 'includes/wpk_functions.php';
require_once 'vendor/autoload.php';
require_once 'libs/Core.php';
require_once 'libs/Loader.php';

/**
 * Helper function for getting Core
 *
 * @param null $class
 *
 * @return \Wpk\Core|false
 */
function getWpkCore( $class = null ) {

	$core = \Wpk\Core::getInstance();
	if ( empty( $class ) ) {
		return $core;
	}

	$class = $class . '_Core';


	return isset( $core->instances[ $class ] ) ? $core->instances[ $class ] : false;

}

/* Release the kraken! */
$core = getWpkCore();
