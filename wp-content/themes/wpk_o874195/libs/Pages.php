<?php


namespace Wpk;

/**
 * Helper class for getting pages via their templates
 *
 * @package wpk
 */
class Pages {

	public function __construct() {

		$this->setupHooks();

	}

	protected function setupHooks() {

		add_filter( 'pre_get_document_title', [ $this, 'setPageTitle' ] );

	}

	/**
	 * Set page title based on context
	 *
	 * @param string $title
	 *
	 * @return string
	 */
	public function setPageTitle( $title ): string {

		$template = Utility::getPageTemplate();

		switch ( $template ) {

			case 'edit_campaign.php':
				$post_title = get_the_title( $_GET[ 'id' ] );

				return sprintf( __( 'Edit campaign %s', 'wpk' ), $post_title );
				break;

		}

		return $title;

	}

	/**
	 * Get company register page
	 *
	 * @return array|\WP_Post|false
	 */
	public static function getCompanyRegisterPage() {

		return Utility::getPageByTemplate( 'company_register.php' );

	}

	/**
	 * Get "My Campaigns" page
	 *
	 * @return array|false|\WP_Post
	 */
	public static function getMyCampaignsPage() {

		return Utility::getPageByTemplate( 'my_campaigns.php' );

	}

	/**
	 *  Get "Add new campaign" page
	 *
	 * @return array|false|\WP_Post
	 */
	public static function getNewCampaignPage() {

		return Utility::getPageByTemplate( 'new_campaign.php' );

	}

	/**
	 * Get link to campaign creation page. If no campaign page exists, returns home url
	 *
	 * @param int $step Optional, step into which user will be redirected
	 * @param int $campaign_id Optional, campaign id which data will be loaded
	 *
	 * @return string
	 */
	public static function getCampaignCreationUrl( $step = 1, $campaign_id = null ) {

		$page = self::getNewCampaignPage();
		if ( ! empty( $page ) ) {
			$url  = get_permalink( $page->ID );
			$args = [
				'step' => $step,
			];
			if ( ! empty( $campaign_id ) ) {
				$args[ 'campaign_id' ] = $campaign_id;
			}
			$query = add_query_arg( $args, $url );

			return $query;
		}

		return home_url();

	}

	/**
	 * Get "My Profile" page
	 *
	 * @return array|false|\WP_Post
	 */
	public static function getProfilePage() {

		return Utility::getPageByTemplate( 'user_profile.php' );

	}

	/**
	 * Get "Campaign preview" page
	 *
	 * @return array|false|\WP_Post
	 */
	public static function getCampaignPreviewPage() {

		return Utility::getPageByTemplate( 'campaign_preview.php' );

	}

	/**
	 * Get "Statistics" page
	 *
	 * @return array|false|\WP_Post
	 */
	public static function getStatisticsPage() {

		return Utility::getPageByTemplate( 'statictics.php' );

	}

	/**
	 * Get "Messages" page
	 *
	 * @return array|false|\WP_Post
	 */
	public static function getMessagesPage() {

		return Utility::getPageByTemplate( 'messages.php' );

	}

	/**
	 * Get "edit campaign" page
	 *
	 * @return array|false|\WP_Post
	 */
	public static function getEditCampaignPage() {

		return Utility::getPageByTemplate( 'edit_campaign.php' );

	}

	/**
	 * Get "brands" page
	 *
	 * @return array|false|\WP_Post
	 */
	public static function getBrandsPage() {

		return Utility::getPageByTemplate( 'brands.php' );

	}

	/**
	 * Get "Register" page
	 *
	 * @return array|false|\WP_Post
	 */
	public static function getRegisterPage() {

		return Utility::getPageByTemplate( 'register.php' );

	}

}