<?php


namespace Wpk;

/**
 * Core class, used to load other modules.
 * This class is extended by other cores to load their modules
 *
 * @package wpk
 */
class Core {

	/**@var string Prefix of modules files */
	protected $prefix = '';

	/**
	 * @var array Contains a list of modules to load.
	 * @see setModules for arguments
	 */
	protected $modules = [
		'Utility',
		'Admin\\Core',
		'Settings',
		'Enqueue',
		'Pages',
		'UserData',
		'Menu',
		'Facebook\\Core',
		'Post_types\\Core',
		'User_types\\Core',
		'Pages\\Core',
	];

	/**@var string Path to root dir of project */
	protected $dir;

	/**@var string Namespace of module */
	protected $namespace;

	/** @var Core Stores this class instance */
	private static $instance;

	/** @var array Contains instances of all modules */
	public $instances;

	/** @var string Theme slug, used for translations. */
	const SLUG = 'wpk';

	/** @var Loader */
	private $loader;

	/**
	 * Core constructor.
	 *
	 * @param string $namespace Namespace of core class
	 * @param string $dir Directory of core class
	 *
	 */
	public function __construct( $namespace = null, $dir = null ) {

		self::setErrorReporting();

		$this->dir = empty( $dir ) ? $this->getDirectoryPath() : $dir;

		if ( empty( $this->namespace ) ) {
			$this->namespace = empty( $namespace ) ? __NAMESPACE__ : $namespace;
		}

		$this->setupLoader()->setupHooks()->setModules()->registerWidgets();

	}

	private function registerWidgets() {

		add_action( 'widgets_init', function () {
			register_widget( 'Wpk\\Widgets\\UserAvatar' );
		} );

		return $this;

	}

	/**
	 * Setup loader
	 *
	 * @return Core
	 */
	private function setupLoader() {

		if ( empty( $this->loader ) ) {
			$this->loader = new Loader( $this->dir );
		}

		return $this;

	}

	/**
	 * Get directory path of core
	 *
	 * @param string $path
	 *
	 * @return string
	 */
	protected function getDirectoryPath( $path = '' ): string {

		return THEME_PATH . 'libs/' . $path;

	}

	/**
	 * Set error reporting level
	 */
	private static function setErrorReporting() {

		error_reporting( E_ALL ^ E_DEPRECATED );

	}

	/**
	 * Setup class hooks
	 *
	 * @return Core
	 */
	private function setupHooks() {

		add_action( 'setup_theme', [ $this, 'checkDependencies' ] );

		return $this;

	}


	/**
	 * Require and load modules
	 *
	 * @return Core
	 */
	protected function setModules() {

		if ( ! empty( $this->modules ) ) {
			foreach ( $this->modules as $module ) {

				$key                     = str_replace( '\\', '_', $module );
				$class                   = sprintf( '%s\\%s', $this->namespace, $module );
				$this->instances[ $key ] = new $class;

			}
		}

		return $this;
	}


	/**
	 * Hooked into "setup_theme". Checks dependencies before theme activation
	 *
	 * @TODO Include all necessary plugins in vendor dir
	 */
	public function checkDependencies() {

		if ( ! function_exists( 'get_field' ) ) {
			wp_die( __( 'Advanced Custom Fields Pro is required for this child theme in order to work properly', 'wpk' ) );
		}

	}

	/**
	 * Get instance of provided module
	 *
	 * @param string $module_key
	 * @param array  $instances Array of instances (used for recursive calls)
	 *
	 * @return mixed|null
	 */
	public function getModuleInstance( $module_key, array $instances = [] ) {

		if ( empty( $instances ) ) {
			$instances = $this->instances;
		}
		$result = null;
		foreach ( $instances as $key => $instance ) {
			if ( $module_key == $key ) {
				$result = $instance;
			} else if ( $instance instanceof Core ) {
				$result = $this->getModuleInstance( $module_key, $instance->instances );
			}

		}

		return $result;

	}

	/**
	 * Get class instance
	 *
	 * @return Core
	 */
	public static function getInstance() {

		if ( empty( self::$instance ) ) {
			self::$instance = new static();
		}

		return self::$instance;

	}


}