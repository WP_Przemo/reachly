<?php

namespace Wpk;

/**
 * Handles autoloading of classes
 */
class Loader {

	/** @var string Root directory of project */
	protected $dir;

	public function __construct( $dir ) {

		require_once 'Utility.php';

		$this->dir = rtrim( $dir, '/' );
		spl_autoload_register( [ $this, 'loader' ] );

	}

	/**
	 * Autoloader callback
	 *
	 * @param string $class
	 */
	public function loader( $class ) {

		if ( Utility::contains( $class, __NAMESPACE__ ) ) {

			$file = sprintf( '%s/%s', $this->dir, str_replace( __NAMESPACE__ . '\\', '', $class ) );
			$file = str_replace( '\\', '/', $file );

			require_once "$file.php";

		}

	}

}