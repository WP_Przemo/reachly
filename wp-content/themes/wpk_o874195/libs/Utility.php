<?php

namespace Wpk;

use Wpk\Facebook\Actions;
use Wpk\Facebook\Instagram\Media;
use Wpk\Post_types\Campaign\Post;
use Wpk\User_types\Influencer\User;

/**
 * Utility class.
 *
 * @package wpk
 */
class Utility {

	/**@var string Debug option slug. */
	private static $name = 'wpk_debug';

	/**@var mixed Stores debug option. */
	private static $debug;


	/**
	 * Utility constructor
	 */
	public function __construct() {

		self::$debug = get_option( self::$name, [] );
		add_action( 'admin_menu', [ $this, 'addMenu' ] );

	}

	/**
	 * Update debug option
	 *
	 * @param mixed $value
	 */
	public static function updateDebug( $value ) {

		self::$debug = $value;
		update_option( self::$name, self::$debug );

	}

	/**
	 * Get debug option value
	 *
	 * @return mixed
	 */
	public static function getDebug() {

		return self::$debug;

	}

	/**
	 * Add debug menu.
	 */
	public function addMenu() {

		add_menu_page( 'WPK Debug', 'WPK Debug', 'manage_options', 'wpk_debug', [ $this, 'debugMenu' ] );

	}

	/**
	 * Debug menu callback function.
	 */
	public function debugMenu() {

		$url = '/test/true';

		var_dump(add_query_arg(
			[
				'test' => 1
			]
		), $url);


	}

	/**
	 * Check if provided image was uploaded before. If yes, return its id
	 *
	 * @param string $image_src URL to image, or image content base64 encoded
	 *
	 * @return bool|int
	 */
	public static function imageExists( $image_src ) {

		if ( self::contains( $image_src, 'http' ) ) {
			$image = base64_encode( file_get_contents( $image_src ) );
		} else {
			$image = $image_src;
		}
		$query_images_args = [
			'post_type'      => 'attachment',
			'post_mime_type' => 'image',
			'post_status'    => 'inherit',
			'posts_per_page' => - 1,
		];

		$query_images = new \WP_Query( $query_images_args );
		foreach ( $query_images->get_posts() as $post ) {
			$img = wp_get_attachment_url( $post->ID );
			if ( base64_encode( file_get_contents( $img ) ) === $image ) {
				return $post->ID;
			}
		}

		return false;

	}

	/**
	 * Display user first and last name, or login
	 *
	 * @param \WP_User $user
	 *
	 * @deprecated Returns user login for now
	 *
	 * @return string User name
	 */
	public static function getUserName( \WP_User $user ): string {

		return $user->user_login;

		if ( empty( $user->first_name ) || empty( $user->last_name ) ) {
			return $user->user_login;
		}

		return $user->first_name . ' ' . $user->last_name;

	}

	/**
	 * Get date range from provided dates and return them in selected format.
	 *
	 * @param \DateTime     $from
	 * @param \DateTime     $to
	 * @param \DateInterval $interval
	 * @param string        $format
	 *
	 * @return array Array that contain range from provided dates
	 */
	public static function getDateRange( \DateTime $from, \DateTime $to, \DateInterval $interval, $format = 'Y-m-d' ): array {

		$range  = new \DatePeriod( $from, $interval, $to );
		$result = [];


		foreach ( $range as $item ) {
			$result[] = $item->format( $format );
		}
		try {
			$last = new \DateTime( end( $result ) );
			$last->modify( 'tomorrow' );
		} catch ( \Exception $e ) {
			$current_year = date( 'Y' );
			$last         = new \DateTime( $current_year . '-' . end( $result ) );
			$last->modify( 'tomorrow' );
		}
		$result[] = $last->format( $format );


		return $result;

	}

	/**
	 * Check if element is iframe.
	 *
	 * @param $string
	 *
	 * @return bool
	 */
	public static function isIframe( $string ): bool {

		return strpos( $string, '<iframe' ) !== false;

	}

	/**
	 * Get page by provideded template name
	 *
	 * @param string $template Template name (for example page.php)
	 * @param bool   $single Whenever return only first result, or array with all results.
	 *
	 * @return array|\WP_Post|false
	 */
	public static function getPageByTemplate( $template, $single = true ) {

		$args  = [
			'meta_key'   => '_wp_page_template',
			'meta_value' => $template,
		];
		$pages = get_pages( $args );

		if ( ! empty( $pages ) ) {
			if ( $single ) {
				return array_shift( $pages );
			}

			return $pages;
		}

		return false;

	}

	/**
	 * Format input key for display on front end
	 *
	 * @param string $key
	 *
	 * @return string
	 */
	public static function formatInputKey( $key ): string {

		$key = str_replace( [ 'billing_', 'wpk_' ], [ '' ], $key );
		$key = str_replace( [ '_', '-' ], [ ' ' ], $key );


		return ucfirst( $key );

	}

	/**
	 * Check if string contains provided substring
	 *
	 * @param string       $string
	 * @param string|array $substring
	 *
	 * @return bool
	 */
	public static function contains( $string, $substring ) {

		if ( is_array( $substring ) ) {
			foreach ( $substring as $item ) {
				if ( strpos( $string, $item ) !== false ) {
					return true;
				}
			}

			return true;
		} else {
			return strpos( $string, $substring ) !== false;
		}

	}


	/**
	 * Get page template file name
	 *
	 * @return string
	 */
	public static function getPageTemplate() {

		return basename( get_page_template() );

	}

	/**
	 * Filters $_POST values
	 *
	 * @param array $post $_POST array passed as reference
	 *
	 */
	public static function filterPost( &$post ) {

		foreach ( $post as $key => $item ) {

			if ( is_array( $item ) ) {
				Utility::filterPost( $item );

				return;
			}
			$item         = apply_filters( 'wpk/filter_post_value', filter_var( $item ), $key );
			$post[ $key ] = $item;

		}

	}

	/**
	 * Get current page url
	 *
	 * @return string
	 */
	public static function getCurrentUrl() {

		global $wp;

		return add_query_arg( [], home_url( $wp->request ) );

	}

	/**
	 * Creates attachment from upload data
	 *
	 * @param array $upload wp_handle_upload result
	 * @param int   $post_id Optional parent id
	 *
	 * @see wp_handle_upload()
	 *
	 * @return int|\WP_Error ID of created attachment, or WP_Error on failure
	 */
	public static function createAttachmentFromUpload( array $upload, $post_id = null ) {

		$name       = basename( $upload[ 'file' ] );
		$mime       = $upload[ 'type' ];
		$file       = $upload[ 'file' ];
		$attachment = [
			'post_mime_type' => $mime,
			'post_title'     => sanitize_file_name( $name ),
			'post_content'   => '',
			'post_status'    => 'inherit',
		];
		$attach_id  = wp_insert_attachment( $attachment, $file, $post_id );
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

		return $attach_id;


	}

	/**
	 * Round number (thousands to K, millions to M etc.)
	 *
	 * @param mixed $num
	 *
	 * @return string
	 */
	public static function round( $num ) {

		$num = intval( $num );
		if ( $num < 999 ) {
			return $num;
		}
		$x               = round( $num );
		$x_number_format = number_format( $x );
		$x_array         = explode( ',', $x_number_format );
		$x_parts         = [ 'k', 'm', 'b', 't' ];
		$x_count_parts   = count( $x_array ) - 1;
		$x_display       = $x;
		$x_display       = $x_array[ 0 ] . ( (int) $x_array[ 1 ][ 0 ] !== 0 ? '.' . $x_array[ 1 ][ 0 ] : '' );
		$x_display       .= $x_parts[ $x_count_parts - 1 ];

		return $x_display;

	}

	/**
	 * Get link to user edit page
	 *
	 * @param int $user_id
	 *
	 * @return string
	 */
	public static function getUserEditLink( $user_id ): string {

		$url = add_query_arg( [
			'user_id' => $user_id,
		], admin_url( 'profile.php' ) );

		return $url;


	}

	/**
	 * Perform save define of new constant
	 *
	 * @param mixed $name
	 * @param mixed $value
	 *
	 * @return bool
	 */
	public static function define( $name, $value ): bool {

		return defined( $name ) ? false : define( $name, $value );

	}

	/**
	 * Get headers for email
	 *
	 * @return array
	 */
	public static function getEmailHeaders(): array {

		$headers = apply_filters( 'wpk/utility/email_headers', [
			"MIME-Version: 1.0",
			"Content-Type: text/html;charset=UTF-8;",
		] );

		return $headers;

	}

	/**
	 * Handle file upload, create attachment and return its id
	 *
	 * @param array   $file
	 * @param string  $action
	 * @param boolean $only_image Whenever accept only image
	 *
	 * @return bool|int|\WP_Error ID of attachment, or false if upload failed
	 */
	public static function handleUpload( array $file, $action, $only_image = false ) {

		$exists = self::imageExists( $file[ 'tmp_name' ] );
		if ( $exists ) {
			return $exists;
		}
		$size = getimagesize( $file[ 'tmp_name' ] );
		if ( $only_image && ! $size ) {
			return false;
		}
		$upload = wp_handle_upload( $file, [ 'action' => $action ] );
		if ( ! isset( $upload[ 'error' ] ) ) {
			return self::createAttachmentFromUpload( $upload );
		}

		return false;

	}

	/**
	 * Get auth redirect
	 *
	 * @param array $query Additional URL query paired as key -> value
	 * @param bool  $add_redirect Whenever add redirect link to query
	 *
	 * @return string|false
	 */
	public static function getAuthUrl( array $query = [], $add_redirect = true ) {

		// The cookie is no good so force login
		//nocache_headers();

		$redirect = $add_redirect ? ( strpos( $_SERVER[ 'REQUEST_URI' ], '/options.php' ) && wp_get_referer() ) ? wp_get_referer() : set_url_scheme( 'http://' . $_SERVER[ 'HTTP_HOST' ] . $_SERVER[ 'REQUEST_URI' ] ) : '';

		$login_url = wp_login_url( $redirect, true );

		if ( ! empty( $query ) ) {
			$login_url = add_query_arg( $query, $login_url );
		}

		return $login_url;


	}

	public static function getAttachmentIdByUrl( $image_url = null ) {

		global $wpdb;

		if ( empty( $image_url ) ) {
			return false;
		}
		$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ) );

		return isset( $attachment[ 0 ] ) ? $attachment[ 0 ] : false;
	}

	/**
	 * Get HTML content of provided template
	 *
	 * @param $path
	 *
	 * @return string
	 */
	public static function getTemplateHtml( $path ) {

		ob_start();
		get_template_part( $path );

		return ob_get_clean();

	}

	/**
	 * Add workdays to provided date
	 *
	 * @param int       $days How many workdays add to date
	 * @param \DateTime $date
	 *
	 * @return \DateTime
	 */
	public static function addWorkDays( $days, $date = null ) {

		if ( empty( $date ) ) {
			$date = new \DateTime();
		}

		if ( $days == 0 ) {
			return $date;
		}

		$date->add( new \DateInterval( 'P1D' ) );

		if ( ! in_array( $date->format( 'N' ), [ '6', '7' ] ) ) {
			$days --;
		}

		return self::addWorkDays( $days, $date );

	}

	/**
	 * Subtract workdays from provided date
	 *
	 * @param int       $days How many workdays add to date
	 * @param \DateTime $date
	 *
	 * @return \DateTime
	 */
	public static function subtractWorkDays( $days, \DateTime $date = null ) {

		if ( empty( $date ) ) {
			$date = new \DateTime();
		}

		if ( $days == 0 ) {
			return $date;
		}

		$date->modify( '-1 day' );

		if ( ! in_array( $date->format( 'N' ), [ '6', '7' ] ) ) {
			$days --;
		}

		return self::subtractWorkDays( $days, $date );

	}

	public static function dateDiff( $datetime, $full = false ) {
		$now  = new \DateTime;
		$ago  = new \DateTime( $datetime );
		$diff = $now->diff( $ago );

		$diff->w = floor( $diff->d / 7 );
		$diff->d -= $diff->w * 7;

		$string = [
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		];
		foreach ( $string as $k => &$v ) {
			if ( $diff->$k ) {
				$v = $diff->$k . ' ' . $v . ( $diff->$k > 1 ? 's' : '' );
			} else {
				unset( $string[ $k ] );
			}
		}

		if ( ! $full ) {
			$string = array_slice( $string, 0, 1 );
		}

		return $string ? implode( ', ', $string ) . __( ' ago', 'wpk' ) : __( 'just now' );
	}

	public static function highlight( $text, $words ) {

		$highlighted = preg_filter( '/' . preg_quote( $words ) . '/i', '<span class="wpk-message-search-result">$0</span>', $text );
		if ( ! empty( $highlighted ) ) {
			$text = $highlighted;
		}

		return $text;

	}

	/**
	 * Check if provided user has admin role
	 *
	 * @param \WP_User|null
	 *
	 * @return bool
	 */
	public static function isAdmin( $user = null ): bool {

		if ( empty( $user ) ) {
			$user = wp_get_current_user();
		}
		$roles = $user->roles;

		return in_array( 'administrator', $roles );

	}

	/**
	 * Helper function for getting sidebar template
	 */
	public static function getSidebarTemplate() {

		get_template_part( 'templates/general/sidebar' );


	}

	/**
	 * Convert nested object to array
	 *
	 * @param $obj
	 *
	 * @return array
	 */
	public static function objToArray( $obj ): array {

		return json_decode( json_encode( $obj ), true );

	}

	/**
	 *
	 * @param      $log
	 * @param null $title
	 */
	public static function log( $log, $title = null ) {

		if ( ! empty( $title ) ) {
			error_log( $title );
		}
		if ( is_array( $log ) || is_object( $log ) ) {
			error_log( print_r( $log, true ) );
		} else {
			error_log( $log );
		}


	}

	/**
	 * Helper function for formatting data for Highchart
	 *
	 * @param array $values
	 *
	 * @return array
	 */
	public static function preparePieValues( $values = [] ) {

		$result = [];

		foreach ( $values as $index => $value ) {

			$result[] = [ $index, $value ];

		}

		usort( $result, function ( $a, $b ) {
			if ( is_numeric( $a[ 1 ] ) && is_numeric( $b[ 1 ] ) ) {
				return $a[ 1 ] < $b[ 1 ];
			}

			return true;
		} );

		return htmlentities( json_encode( $result ) );

	}

	/**
	 * @return array
	 */
	public static function getAllCountries() {

		return [
			"Afghanistan",
			"Albania",
			"Algeria",
			"American Samoa",
			"Andorra",
			"Angola",
			"Anguilla",
			"Antarctica",
			"Antigua and Barbuda",
			"Argentina",
			"Armenia",
			"Aruba",
			"Australia",
			"Austria",
			"Azerbaijan",
			"Bahamas",
			"Bahrain",
			"Bangladesh",
			"Barbados",
			"Belarus",
			"Belgium",
			"Belize",
			"Benin",
			"Bermuda",
			"Bhutan",
			"Bolivia",
			"Bosnia and Herzegowina",
			"Botswana",
			"Bouvet Island",
			"Brazil",
			"British Indian Ocean Territory",
			"Brunei Darussalam",
			"Bulgaria",
			"Burkina Faso",
			"Burundi",
			"Cambodia",
			"Cameroon",
			"Canada",
			"Cape Verde",
			"Cayman Islands",
			"Central African Republic",
			"Chad",
			"Chile",
			"China",
			"Christmas Island",
			"Cocos (Keeling) Islands",
			"Colombia",
			"Comoros",
			"Congo",
			"Congo, the Democratic Republic of the",
			"Cook Islands",
			"Costa Rica",
			"Cote d'Ivoire",
			"Croatia (Hrvatska)",
			"Cuba",
			"Cyprus",
			"Czech Republic",
			"Denmark",
			"Djibouti",
			"Dominica",
			"Dominican Republic",
			"East Timor",
			"Ecuador",
			"Egypt",
			"El Salvador",
			"Equatorial Guinea",
			"Eritrea",
			"Estonia",
			"Ethiopia",
			"Falkland Islands (Malvinas)",
			"Faroe Islands",
			"Fiji",
			"Finland",
			"France",
			"France Metropolitan",
			"French Guiana",
			"French Polynesia",
			"French Southern Territories",
			"Gabon",
			"Gambia",
			"Georgia",
			"Germany",
			"Ghana",
			"Gibraltar",
			"Greece",
			"Greenland",
			"Grenada",
			"Guadeloupe",
			"Guam",
			"Guatemala",
			"Guinea",
			"Guinea-Bissau",
			"Guyana",
			"Haiti",
			"Heard and Mc Donald Islands",
			"Holy See (Vatican City State)",
			"Honduras",
			"Hong Kong",
			"Hungary",
			"Iceland",
			"India",
			"Indonesia",
			"Iran (Islamic Republic of)",
			"Iraq",
			"Ireland",
			"Israel",
			"Italy",
			"Jamaica",
			"Japan",
			"Jordan",
			"Kazakhstan",
			"Kenya",
			"Kiribati",
			"Korea, Democratic People's Republic of",
			"Korea, Republic of",
			"Kuwait",
			"Kyrgyzstan",
			"Lao, People's Democratic Republic",
			"Latvia",
			"Lebanon",
			"Lesotho",
			"Liberia",
			"Libyan Arab Jamahiriya",
			"Liechtenstein",
			"Lithuania",
			"Luxembourg",
			"Macau",
			"Macedonia, The Former Yugoslav Republic of",
			"Madagascar",
			"Malawi",
			"Malaysia",
			"Maldives",
			"Mali",
			"Malta",
			"Marshall Islands",
			"Martinique",
			"Mauritania",
			"Mauritius",
			"Mayotte",
			"Mexico",
			"Micronesia, Federated States of",
			"Moldova, Republic of",
			"Monaco",
			"Mongolia",
			"Montserrat",
			"Morocco",
			"Mozambique",
			"Myanmar",
			"Namibia",
			"Nauru",
			"Nepal",
			"Netherlands",
			"Netherlands Antilles",
			"New Caledonia",
			"New Zealand",
			"Nicaragua",
			"Niger",
			"Nigeria",
			"Niue",
			"Norfolk Island",
			"Northern Mariana Islands",
			"Norway",
			"Oman",
			"Pakistan",
			"Palau",
			"Panama",
			"Papua New Guinea",
			"Paraguay",
			"Peru",
			"Philippines",
			"Pitcairn",
			"Poland",
			"Portugal",
			"Puerto Rico",
			"Qatar",
			"Reunion",
			"Romania",
			"Russian Federation",
			"Rwanda",
			"Saint Kitts and Nevis",
			"Saint Lucia",
			"Saint Vincent and the Grenadines",
			"Samoa",
			"San Marino",
			"Sao Tome and Principe",
			"Saudi Arabia",
			"Senegal",
			"Seychelles",
			"Sierra Leone",
			"Singapore",
			"Slovakia (Slovak Republic)",
			"Slovenia",
			"Solomon Islands",
			"Somalia",
			"South Africa",
			"South Georgia and the South Sandwich Islands",
			"Spain",
			"Sri Lanka",
			"St. Helena",
			"St. Pierre and Miquelon",
			"Sudan",
			"Suriname",
			"Svalbard and Jan Mayen Islands",
			"Swaziland",
			"Sweden",
			"Switzerland",
			"Syrian Arab Republic",
			"Taiwan, Province of China",
			"Tajikistan",
			"Tanzania, United Republic of",
			"Thailand",
			"Togo",
			"Tokelau",
			"Tonga",
			"Trinidad and Tobago",
			"Tunisia",
			"Turkey",
			"Turkmenistan",
			"Turks and Caicos Islands",
			"Tuvalu",
			"Uganda",
			"Ukraine",
			"United Arab Emirates",
			"United Kingdom",
			"United States",
			"United States Minor Outlying Islands",
			"Uruguay",
			"Uzbekistan",
			"Vanuatu",
			"Venezuela",
			"Vietnam",
			"Virgin Islands (British)",
			"Virgin Islands (U.S.)",
			"Wallis and Futuna Islands",
			"Western Sahara",
			"Yemen",
			"Yugoslavia",
			"Zambia",
			"Zimbabwe",
		];

	}


}