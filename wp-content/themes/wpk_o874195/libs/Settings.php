<?php


namespace Wpk;

/**
 * Stores custom WPK settings
 *
 * @package wpk
 */
class Settings {

	/**@var array Stores settings */
	private static $settings = [];

	public function __construct() {

		self::updateSettings();
		$this->setupHooks();

	}

	protected function setupHooks() {

		add_action( 'init', [ $this, 'addAcfMenu' ] );

	}


	/**
	 * Get settings values from database
	 */
	private static function updateSettings() {

		self::$settings = [
			'campaign_placeholder' => self::getField( 'wpk_campaign_img_placeholder' ),
			'register_influencer'  => self::getField( 'wpk_register_influencer' ),
			'register_company'     => self::getField( 'wpk_register_company' ),
		];

	}

	/**
	 * Helper function for accessing acf fields from options page
	 *
	 * @param string $selector
	 *
	 * @return mixed|null
	 */
	public static function getField( $selector ) {

		return get_field( $selector, 'option' );

	}

	/**
	 * Get settings
	 *
	 * @return array
	 */
	public static function getSettings() {

		return self::$settings;

	}

	/**
	 * Get specific setting
	 *
	 * @param string $key
	 *
	 * @return bool|mixed
	 */
	public static function getSetting( $key ) {

		return isset( self::$settings[ $key ] ) ? self::$settings[ $key ] : false;

	}

	/**
	 * Adds acf menu page
	 */
	public function addAcfMenu() {

		acf_add_options_page( [
			'menu_title' => 'WPK Settings',
			'page_title' => 'WPK Settings',
			'menu_slug'  => 'wpk_settings',
			'capability' => 'manage_options'
		] );



	}

}