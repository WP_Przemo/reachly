<?php


namespace wpk;

use Wpk\Facebook\Instagram\User;
use Wpk\Post_types\Conversation\Post;

/**
 * Handles styles and scripts enqueue.
 *
 * @package wpk
 */
Class Enqueue {

	/** @var string Stores url to assets folder. */
	public static $assets_url;

	/**@var string Stores url to css folder. */
	public static $css_url;

	/** @var string Stores url to js folder. */
	public static $js_url;

	/** @var string Stores URL to vendor folder */
	public static $vendor_url;

	/**
	 * Enqueue constructor.
	 */
	public function __construct() {

		self::$assets_url = THEME_URL . '/assets';
		self::$css_url    = self::$assets_url . '/css';
		self::$js_url     = self::$assets_url . '/js';
		self::$vendor_url = self::$assets_url . '/vendor';

		$this->setupHooks();

	}

	/**
	 * Setup class hooks.
	 */
	protected function setupHooks() {

		add_action( 'wp_enqueue_scripts', [ $this, 'enqueueStyles' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueueScripts' ] );
		add_action( 'wp_print_scripts', [ $this, 'dequeueScripts' ] );

	}

	/**
	 * Enqueue styles.
	 */
	public function enqueueStyles() {

		//wp_enqueue_style( 'chart', 'https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.7/css/highcharts.css' );
		wp_enqueue_style( 'linear-icons', 'https://cdn.linearicons.com/free/1.0.0/icon-font.min.css' );
		wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css', [], '4.0.0' );
		wp_enqueue_style( 'bootstrap-datepicker', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.standalone.min.css' );
		wp_enqueue_style( 'pikaday', 'https://cdnjs.cloudflare.com/ajax/libs/pikaday/1.6.1/css/pikaday.min.css' );
		wp_enqueue_style( 'jquery-ui', self::$vendor_url . '/jquery-ui.min.css' );
		wp_enqueue_style( 'material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', [], '1.0' );
		wp_enqueue_style( 'wpk-font', self::$css_url . '/wpk-fonts.css', [], '1.0' );
		wp_enqueue_style( 'wpk-main', self::$css_url . '/wpk-main.css', [], '1.0' );
		wp_enqueue_style( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css' );
		wp_enqueue_style( 'slick', 'http://cdn.jsdelivr.net/jquery.slick/1.8.0/slick.css' );

	}

	/**
	 * Enqueue scripts.
	 */
	public function enqueueScripts() {

		wp_enqueue_script('google-api', 'https://www.google.com/jsapi');
		wp_enqueue_script( 'chart-js', 'https://www.gstatic.com/charts/loader.js', [ 'jquery' ] );
		wp_enqueue_script( 'bootstrap-datepicker', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js' );
		wp_enqueue_script( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', [ 'jquery' ] );
		wp_enqueue_script( 'moment', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js' );
		wp_enqueue_script( 'pikaday', 'https://cdnjs.cloudflare.com/ajax/libs/pikaday/1.6.1/pikaday.min.js', [ 'moment' ] );
		wp_enqueue_script( 'wpk-utils', self::$js_url . '/wpk-utils.js', [ 'jquery' ], '1.0' );
		wp_enqueue_script( 'jquery-ui', self::$vendor_url . '/jquery-ui.js', [ 'jquery' ] );
		wp_enqueue_script( 'slick', 'http://cdn.jsdelivr.net/jquery.slick/1.8.0/slick.min.js', [ 'jquery' ] );

		wp_enqueue_script( 'wpk-main', self::$js_url . '/wpk-main.js', [
			'jquery-ui',
			'wpk-utils',
			'select2',
			'pikaday',
			'slick',
		], '1.0.1' );
		wp_enqueue_script( 'wpk-chart', self::$js_url . '/wpk-chart.js', [ 'wpk-main' ] );
		wp_enqueue_script( 'wpk-company-register', self::$js_url . '/wpk-company-register.js', [
			'wpk-main',
		], '1.0' );
		wp_enqueue_script( 'wpk-user-profile', self::$js_url . '/wpk-user-profile.js', [
			'wpk-main',
		], '1.0' );
		wp_enqueue_script( 'wpk-my-campaigns', self::$js_url . '/wpk-my-campaigns.js', [ 'wpk-main' ] );
		wp_enqueue_script( 'wpk-influencers-filter', self::$js_url . '/wpk-influencers-filter.js', [
			'wpk-main',
		] );
		wp_enqueue_script( 'wpk-company-filter', self::$js_url . '/wpk-company-filter.js', [
			'wpk-main',
		] );
		wp_enqueue_script( 'wpk-notifications', self::$js_url . '/wpk-notifications.js', [ 'wpk-main' ] );
		wp_enqueue_script( 'wpk-rwd', self::$js_url . '/wpk-rwd.js', [ 'wpk-main' ] );
		wp_enqueue_script( 'wpk-messages', self::$js_url . '/wpk-messages.js', [ 'wpk-main' ], '1.0', true );

		wp_enqueue_script( 'wpk-statistics', self::$js_url . '/wpk-statistics.js', [
			'wpk-main',
			'chart-js',
		], '1.0', true );


		$this->localizeScripts();

	}

	/**
	 * Localize enqueued scripts.
	 */
	private function localizeScripts() {

		$user = new User();

		//wpk-main.js
		wp_localize_script( 'wpk-main', 'wpk_vars', [
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'get'      => $_GET,
			'messages' => [
				'not_enough_impressions' => __( 'You don\t have enough impressions to do that.', 'wpk' ),
			],
		] );

		//wpk-company-register.js
		wp_localize_script( 'wpk-company-register', 'company_register_vars', [
			'not_image' => __( 'Provided file must be an image.', 'wpk' ),
		] );

		//wpk-user-profile.js
		wp_localize_script( 'wpk-user-profile', 'wpk_user_profile_vars', [
			'user_type'                    => $user->getUserType(),
			'instagram_verified_message'   => sprintf( __( 'Successfully connected with instagram account <strong>%s</strong>', 'wpk' ), $user->getInstagramMeta( 'username' ) ),
			'instagram_unverified_message' => sprintf( __( 'You need to connect with your instagram account to continue.', 'wpk' ), $user->getInstagramMeta( 'username' ) ),
			'instagram_id_taken_message'   => __( 'This instagram account is already connected with different user.', 'wpk' ),
		] );

		//wpk-influencers-filter.js
		wp_localize_script( 'wpk-influencers-filter', 'wpk_influencers_filter_vars', [
			'messages' => [
				'uninvite'         => __( 'Cancel invitation', 'wpk' ),
				'invite'           => __( 'Invite', 'wpk' ),
				'unskip'           => __( 'Unskip', 'wpk' ),
				'skip'             => __( 'Skip', 'wpk' ),
				'invite_confirm'   => __( 'Are you sure you want to invite %nickname% to this campaign?', 'wpk' ),
				'skip_confirm'     => __( 'Are you sure you want to skip %nickname%?', 'wpk' ),
				'unskip_confirm'   => __( 'Are you sure you want to unskip %nickname%?', 'wpk' ),
				'uninvite_confirm' => __( 'Are you sure you want to undo invitation for %nickname%?', 'wpk' ),
				'publish_confirm'  => __( 'You need to publish your campaign before you can start inviting influencers. Would you like to publish it now?' ),
				'in_campaign'      => __( 'In campaign', 'wpk' ),
				'remove_confirm'   => __( 'Are you sure you want to remove %nickname%? It cannot be undone, but you will be able to invite him/her again on next renewal.', 'wpk' ),
				'unbookmark'       => __( 'Remove from bookmarks', 'wpk' ),
				'bookmark'         => __( 'Bookmark', 'wpk' ),
			],
			'is_edit'  => intval( Utility::getPageTemplate() === 'edit_campaign.php' ),
		] );

		//wpk-notifications.js
		$notifications = $user->getWpkMeta( 'notification_dashboard' );
		wp_localize_script( 'wpk-notifications', 'wpk_notification_vars', [
			'notification_dashboard' => empty( $notifications ) ? 'on' : $notifications,
		] );
		wp_localize_script( 'wpk-messages', 'wpk_messages_vars', [
			'conversations_url' => get_post_type_archive_link( Post::$post_type ),
		] );

	}

	/**
	 * Dequeues scripts.
	 */
	public function dequeueScripts() {
	}
}