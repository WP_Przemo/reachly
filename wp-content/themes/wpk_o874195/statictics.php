<?php

/**
 * Template Name: Statictics
 *
 * @package wpk
 */

/** @var \Wpk\User_types\Company\User $company */
global $company, $campaigns_query;

if ( ! is_user_logged_in() ) {
	auth_redirect();
}

$company = new \Wpk\User_types\Company\User();
$type = $company->getUserType();
if ( $type !== 'company' ) {
	wp_redirect( home_url() );
}
/*TODO Remove publish param */
$args            = [ 'post_status' => [ 'started', 'stopped', 'ended' ] ];
$campaigns_query = $company->getCampaignsQuery( $args );

get_header();
?>
<div class="wpk-wrap row wpk-statistics">
	<?php if ( ! $campaigns_query->have_posts() ): ?>
		<a class="wpk-circle-icon wpk-has-message wpk-full-icon" href="#">
			<div class="wpk-circle">
				<div class="wpk-circle-inner">
					<i class="material-icons">email</i>
				</div>
			</div>
			<div class="wpk-message">
				<?php _e( 'You don\'t have any started campaigns yet.', 'wpk' ) ?>
			</div>
		</a>
	<?php else:
		Wpk\Utility::getSidebarTemplate(); ?>
		<div class="col-xl-10 col-md-12 wpk-wrap-inner">
			<?php get_template_part( 'templates/company/statistics' ) ?>
		</div>
	<?php endif; ?>
</div>
<?php get_footer() ?>
