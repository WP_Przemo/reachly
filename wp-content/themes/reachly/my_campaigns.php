<?php

/**
 * Template Name: My campaigns
 *
 * @package wpk
 */

get_header();

do_action( 'wpk/myCampaigns' );

get_footer();