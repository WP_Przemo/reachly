import { $ } from '../constants/jquery';
import { TabletSmall } from '../constants/responsive';
import { $Window } from '../constants/selectors';
import { $Accordion } from '../constants/accordion';


if ( $Accordion.length && $Window.width() < TabletSmall ) {

	let $sidebar   = $Accordion.parents( '.wpk-sidebar' ),
		$container = $( '.mkd-position-left-inner' ),
		activator  = '<div class="wpk-sidebar-activator"><i class="icon-ellipsis linear-icon"></i></div>';

	$container.prepend( activator );

	$container.parent().addClass( 'wpk-has-accordion' );

	$sidebar.addClass( 'wpk-absolute' );

	$( '.wpk-sidebar-activator' ).on( 'click', function() {

		$sidebar.toggleClass( 'wpk-visible' );
		$( this ).toggleClass( 'wpk-active' );

	} );

}