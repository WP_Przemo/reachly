import { $Filter } from '../constants/influencers';
import { $Window } from '../constants/selectors';
import { TabletSmall } from '../constants/responsive';
import { $ } from '../constants/jquery';

if ( $Filter.length && $Window.width() < TabletSmall ) {
	$( '.wpk-campaign-impressions .wpk-button' ).insertAfter( '.wpk-campaign-impressions' );
}