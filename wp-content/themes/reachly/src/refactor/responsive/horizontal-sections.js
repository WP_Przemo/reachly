import { TabletSmall, $Sections } from '../constants/responsive';
import { $Window } from '../constants/selectors';

function handleScrolling() {

	let scroll = $Window.scrollTop();

	if ( scroll > 104 ) {
		$Sections.addClass( 'wpk-adjusted' );
	} else {
		$Sections.removeClass( 'wpk-adjusted' );
	}

}

if ( $Window.width() < TabletSmall ) {
	$Window.on( 'scroll', handleScrolling );
}
