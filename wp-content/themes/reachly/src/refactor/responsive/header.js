import { $ } from '../constants/jquery';
import { $MobileProfile, TabletSmall } from '../constants/responsive';
import { $Window } from '../constants/selectors';


if ( $Window.width() < TabletSmall ) {

	$( '.wpk-header-avatar-inner' ).on( 'click', function() {
		$MobileProfile.addClass( 'wpk-active' );
	} )
		.find( 'a' ).removeAttr( 'href' ).on( 'click', function( e ) {

		e.preventDefault();
		$MobileProfile.addClass( 'wpk-active' );
		return false;

	} );

	$( '.wpk-mobile-profile .wpk-modal-inner' ).on( 'click', function() {

		$( this ).parent().removeClass( 'wpk-active' );

	} );

	$MobileProfile.find( '*' ).on( 'click', function( e ) {
		e.stopPropagation();
	} );

} else {
	$MobileProfile.remove();
}



