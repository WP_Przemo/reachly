import { $ } from '../constants/jquery';
import { TabletSmall } from '../constants/responsive';
import { $Window } from '../constants/selectors';


let $container   = $( '.wpk-my-campaigns, .wpk-edit-campaign' ),
	$plans       = $( '.wpk-campaign-plans' ),
	$filterInner = $( '.wpk-influencers-filter-inner' ),
	width        = $Window.width(),
	settings     = {
		arrows:        false,
		centerMode:    true,
		centerPadding: '20px',
		slidesToShow:  3,
		responsive:    [
			{
				breakpoint: 769,
				settings:   {
					arrows:        false,
					centerMode:    true,
					centerPadding: '40px',
					slidesToShow:  1
				}
			},
			{
				breakpoint: 480,
				settings:   {
					arrows:        false,
					centerMode:    true,
					centerPadding: '40px',
					slidesToShow:  1
				}
			}
		]
	};

if ( $container.length && width < TabletSmall ) {
	let $steps = $( '.wpk-steps-wrapper' );
	$steps.prependTo( '.wpk-wrap' );
}

if ( $plans.length && width < TabletSmall ) {

	let $slick = $plans.slick( settings );

	$( '.wpk-durations .wpk-button' ).on( 'click', function() {

		$slick.addClass( 'wpk-opacity' );

		setTimeout( () => $slick.slick( 'unslick' ).slick( settings ), 400 );
		setTimeout( () => $slick.removeClass( 'wpk-opacity' ), 800 );

	} );
}


if ( width < TabletSmall ) {

	$( '.wpk-filter-mobile-activator' ).on( 'click', function() {
		$filterInner.slideToggle();

		$( this ).find( 'i' ).toggleClass( 'wpk-open' );
	} ).click();

}
