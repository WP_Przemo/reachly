import { $ } from '../../constants/jquery';
import { $CreationContainer, Actions } from '../../constants/campaign';
import { ajax, handleResponse } from '../../helpers/ajax';
import { appendContainerLoader, removeContainerLoader } from './loader';
import { isset } from '../../helpers/checkers';
import { redirect } from '../../helpers/url';

const $CampaignDuration = $CreationContainer.find( '#campaign_duration' ),
	  $Button           = $CreationContainer.find( '.wpk-durations .wpk-button' ),
	  $PlanTabs         = $CreationContainer.find( '.wpk-campaign-plan-tab' );

/**
 * Handle first step of campaign creation
 *
 * @return void
 * */
function handleClick() {

	const $This  = $( this ),
		  PlanID = $This.data( 'plan-id' ),
		  Fd     = new FormData();

	Fd.append( 'action', Actions.firstStep );
	Fd.append( 'plan_id', PlanID );
	Fd.append( 'campaign_length', $CreationContainer.find( '#campaign_length' ).val() );

	ajax( {

		beforeSend() {
			appendContainerLoader();
		},
		data: Fd,
		success( data ) {

			handleResponse( data );

			if ( isset( data.redirect_url ) && data.redirect_url !== '' ) {
				redirect( data.redirect_url );
			}

		},
		complete() {
			removeContainerLoader()
		}

	} );

}

function handleDurationChange() {

	const $This  = $( this ),
		  Length = parseInt( $This.data( 'length' ) );

	$CreationContainer.find( '.wpk-button.wpk-active' ).removeClass( 'wpk-active' );

	$This.addClass( 'wpk-active' );

	$PlanTabs.each( function() {

		let $that  = $( this ),
			prices = $that.data( 'prices' );

		if ( isset( prices[ Length ] ) ) {

			let price    = parseInt( prices[ Length ] ),
				oldPrice = parseInt( $that.find( '.wpk-price' ).text() );

			$that.find( '.wpk-price' ).prop( 'Counter', oldPrice ).animate( {
				complete: function() {

				},
				Counter:  price,
			}, {
				duration: 500,
				easing:   'swing',
				step:     function( now ) {
					$that.find( '.wpk-price' ).text( Math.ceil( now ) );
				}
			} );

			let $campaignPrice = $CreationContainer.find( '#campaign_price' );

			if ( $campaignPrice.length &&
				$CampaignDuration.length &&
				$campaignPrice.val() === price &&
				$CampaignDuration.val() === Length ) {

				$that.addClass( 'wpk-active' );

			} else {

				$that.removeClass( 'wpk-active' );

			}

			$CreationContainer.find( '#campaign_length' ).val( Length );

		}

	} );
}

//Advance step on plan click
$CreationContainer.find( '.wpk-campaign-plan-tab' ).on( 'click', handleClick );

$Button.on( 'click', handleDurationChange );

if ( $CampaignDuration.length ) {
	$CreationContainer.find( `.wpk-durations .wpk-button[data-length=${$CampaignDuration.val()}]` ).click();
} else {
	$Button.eq( 0 ).trigger( 'click' );
}