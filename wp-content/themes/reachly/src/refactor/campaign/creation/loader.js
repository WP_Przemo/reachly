import { $CreationContainer } from '../../constants/campaign';
import { appendLoader, removeLoader } from '../../helpers/loader';

export function appendContainerLoader() {
	appendLoader( $CreationContainer, true );
}

export function removeContainerLoader() {
	removeLoader( $CreationContainer );
}
