/*global tinyMCE*/
import { $ } from '../../constants/jquery';
import { $CreationContainer, Actions } from '../../constants/campaign';
import { ajax, handleResponse } from '../../helpers/ajax';
import { appendContainerLoader, removeContainerLoader } from './loader';
import { redirect } from '../../helpers/url';

const $Form       = $CreationContainer.find( '.wpk-new-campaign-form' ),
	  $StartDate  = $( '#start_date' ),
	  $Tags       = $( '#tags' ),
	  $Categories = $( '#categories' );

/**
 * Get values from custom selection
 *
 * @param {jQuery} $item
 *
 * @return Array
 * */
function getSelectionValues( $item ) {

	let result = [];

	$item.parent().find( '.wpk-selection' ).each( function() {

		let $this = $( this ),
			value = $this.data( 'value' );

		result.push( value );

	} );

	return result;

}

/**
 * Handle third step form submission
 *
 * @return void
 * */
function handleSubmit( e ) {

	e.preventDefault();

	tinyMCE.triggerSave();

	const Fd         = new FormData( this ),
		  Tags       = getSelectionValues( $Tags ),
		  Categories = getSelectionValues( $Categories );

	Fd.append( 'action', Actions.thirdStep );
	//We need to append data from sidebar which is outside of the form

	Fd.append( 'start_date', $StartDate.val() );
	Fd.append( 'categories', Categories );
	Fd.append( 'tags', Tags );

	ajax( {
		data: Fd,
		beforeSend() {
			appendContainerLoader();
		},
		success( data ) {

			handleResponse( data );

			if ( data.result && data.redirect_url ) {
				redirect( data.redirect_url );
			}

		},
		complete() {
			removeContainerLoader();
		}
	} );

}