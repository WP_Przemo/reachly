import { $ } from '../../constants/jquery';
import { ajax, handleResponse } from '../../helpers/ajax';
import { appendLoader, removeLoader } from '../../helpers/loader';
import { redirect } from '../../helpers/url';
import { $Document } from '../../constants/selectors';
import { Actions } from '../../constants/campaign';

/**
 * @return void
 * */
function handlePublishCampaign() {

	const $This      = $( this ),
		  CampaignID = $This.data( 'campaign-id' ),
		  Fd         = new FormData();

	Fd.append( 'action', Actions.publishCampaign );
	Fd.append( 'wpk_nonce', $This.next().val() );
	Fd.append( 'campaign_id', CampaignID );

	ajax( {
		data:     Fd,
		beforeSend() {
			appendLoader( $This );
		},
		success( data ) {

			handleResponse( data );

			if ( data.redirect_url ) {
				redirect( data.redirect_url );
			}

		},
		complete: () => {
			removeLoader( $This );
		}
	} );

}

$Document.on( 'click', '.wpk-publish-campaign:not(.wpk-disabled)', handlePublishCampaign );