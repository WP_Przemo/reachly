import { $ } from '../../constants/jquery';
import { $ImpressionsSlider } from '../../constants/campaign';
import { $Window } from '../../constants/selectors';
import 'jquery-ui-dist/jquery-ui';

/**
 * Creates impressions slider
 *
 * @return void
 * */
function createSlider() {

	let $slider = this.$slider,
		value   = $slider.data( 'value' ),
		min     = $slider.data( 'min' ),
		max     = $slider.data( 'max' ),
		$parent = $slider.parents( '.wpk-campaign-impressions' );

	$slider.slider( {
		min:      min,
		max:      max,
		range:    true,
		values:   [ 0, value ],
		disabled: true,
		change( event, ui ) {

			let values = ui.values;
			$slider.next().find( '.wpk-impressions-value' ).text( values[ 1 ] );

		}
	} );

	if ( $parent.hasClass( 'wpk-fixable' ) && $( window ).width() > 769 ) {

		let position = $parent.offset().top,
			clone    = '<div class="wpk-placeholder" style="display: none; height:' + $slider.parent().height() + 'px"></div>';

		$parent.after( clone );

		let $placeholder = $( '.wpk-placeholder' ),
			oldScroll    = 0;

		$Window.on( 'scroll', () => {

			let scroll = $Window.scrollTop();

			if ( scroll >= position ) {
				$parent.addClass( 'wpk-fixed-impressions' );
				$placeholder.show();
			} else {
				$parent.removeClass( 'wpk-fixed-impressions' );
				$placeholder.hide();
			}

			oldScroll = scroll;
		} );
	}

}

/**
 * Set value of impressions slider
 *
 * @param {int} value
 *
 * @return void
 * */
export function setValue( value ) {
	$ImpressionsSlider.slider( 'values', [ 0, value ] );
}

/**
 * Get remaining impressions from impressions slider
 *
 * @return {int}
 * */
export function getValue() {
	return $ImpressionsSlider.slider( 'values' )[ 1 ];
}

if ( $ImpressionsSlider.length ) {
	createSlider();
}