import { $ } from '../constants/jquery';

/**
 * Create event that launches when user finishes typing
 *
 * @param {jQuery} $element
 * @param {Function} callback
 * @param {Number} timeoutAmount
 *
 * @return void
 * */
export function onFinishTyping( $element, callback, timeoutAmount = 1000 ) {

	let timeout,
		currentValue = $element.val();

	$element.on( 'keyup', function() {

		let value = $( this ).val();

		clearTimeout( timeout );

		//Don't launch event if the value haven't changed
		if ( value === currentValue ) {
			return;
		}

		timeout = setTimeout( () => {
			callback.call( this );

			currentValue = value;
		}, timeoutAmount );

	} ).on( 'keydown', function() {

		clearTimeout( timeout );

	} );

}