import { $ } from '../constants/jquery';
import { $Area } from '../constants/popups';

/**
 * Displays customized popup
 *
 * @param {String} type Type of popup (error, warning, success)
 * @param {String} message Text that will be displayed in popup
 * @param {boolean|Number} clear
 * */
export function displayPopup( type, message, clear = 3000 ) {

	let el = `<div class="wpk-popup alert alert-${type}">${message}</div>`,
		$popup;

	$Area.prepend( el );
	$popup = $( '.wpk-popup' ).first();

	$popup.fadeIn( 400, function() {
		$( this ).addClass( 'wpk-popup-ready' );
	} );

	if ( clear ) {
		setTimeout( () => this.clearPopup( $popup ), clear );
	}

	$popup.on( 'click', function() {
		clearPopup( $( this ) );
	} );

}

/**
 * @return void
 * */
export function clearPopup( $popup ) {

	$popup.removeClass( 'wpk-popup-ready' ).fadeOut( function() {
		$( this ).remove();
	} );

}