import { $ } from '../constants/jquery';
import { $Document } from '../constants/selectors';

/**
 *  Displays modal
 *
 *  @param {String} content Inner content of modal
 *  @param {Object} options Additional options for modal Type of modal (full, full_under_menu, half)
 *
 *  @return {Promise}
 *
 * */
export function showModal( content, options = { full: true, underMenu: false, transparent: false } ) {

	return new Promise( resolve => {

		let modal, appendTarget, $modal = $( '.wpk-modal:not(.wpk-mobile-profile)' );

		let { full, underMenu, transparent } = options;

		if ( full && underMenu && transparent ) {

			modal = `<div data-type="full_under_menu_transparent" class="wpk-modal wpk-modal-absolute wpk-modal-hidden wpk-modal-transparent"><div class="wpk-modal-inner">${content}</div></div>`;

		} else if ( full && underMenu ) {
			modal = `<div data-type="full_under_menu" class="wpk-modal wpk-modal-absolute wpk-modal-hidden"><div class="wpk-modal-inner">${content}</div></div>`;
		} else if ( full ) {
			modal = `<div data-type="full" class="wpk-modal wpk-modal-hidden"><div class="wpk-modal-inner">${content}</div></div>`;
		} else {
			modal = `<div class="wpk-modal wpk-half-modal wpk-modal-hidden"><div class="wpk-close-modal"><i class="material-icons">close</i></div><div class="wpk-modal-inner">${content}</div></div><div class="wpk-modal-overlay"></div>`;
		}

		appendTarget = full && underMenu ? '.mkd-content' : 'body';


		if ( $modal.length ) {

			$modal.replaceWith( modal );
			$( '.wpk-modal' ).removeClass( 'wpk-modal-hidden' ).removeClass( 'wpk-modal-absolute' ).addClass( 'wpk-modal-relative' );

			resolve( $modal );

		} else {

			$( appendTarget ).append( modal );

			$modal = $( '.wpk-modal' );

			setTimeout( function() {

				if ( !transparent ) {
					window.scrollTo( 0, 0 );
				}

				$( '.wpk-modal:not(.wpk-mobile-profile)' ).removeClass( 'wpk-modal-hidden' );

				if ( full && underMenu ) {
					$( '.wpk-modal' ).removeClass( 'wpk-modal-absolute' ).addClass( 'wpk-modal-relative' );
					$( '.mkd-content-inner' ).hide();
				}

				resolve( $modal );

			}, 500 );
		}

	} );
}

/**
 * @return void
 * */
export function closeModal() {

	let $modal = $( '.wpk-modal:not(.wpk-mobile-profile)' ),
		type   = $modal.data( 'type' );

	$modal.addClass( 'wpk-modal-hidden' );

	if ( type === 'full_under_menu' || type === 'full' ) {
		setTimeout( () => $( '.mkd-content-inner, .wpk-wrap' ).fadeIn(), 400 );
	}

	setTimeout( () => {
		$modal.trigger( 'modal:close' ).remove();

		$( '.wpk-modal-overlay' ).remove();
	}, 400 );


}

$Document.on( 'click', '.wpk-modal-overlay, .wpk-close-modal', closeModal );

$Document.on( 'keyup', e => {
	//Close modal on esc press
	if ( e.keyCode === 27 ) {
		closeModal();
	}
} );