import { pageLoader } from './loader';

/**
 * Performs redirect to provided url
 *
 * @param {String} url
 *
 * @return void
 *
 * */
export function redirect( url ) {
	let callback = () => window.location.href = url;

	pageLoader( 'show', callback );
}

/**
 * Add new query param to url
 *
 * @return void
 * */
export function addQueryParam( name, value, state = {} ) {

	const params = new URLSearchParams( location.search );
	params.set( name, value );
	window.history.pushState( state, '', decodeURIComponent( `${location.pathname}?${params}` ) );

}

/**
 * Removes query param from url
 *
 * @return void
 * */
export function removeQueryParam( name, state = {} ) {

	const params = new URLSearchParams( location.search );

	params.delete( name );
	window.history.pushState( state, '', decodeURIComponent( `${location.pathname}?${params}` ) );

}