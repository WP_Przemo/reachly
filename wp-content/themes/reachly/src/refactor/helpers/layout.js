import { $ } from '../constants/jquery';
import { $Window } from '../constants/selectors';

/**
 * Create horizontal section pagination that shows current section
 *
 * @param {jQuery} $container
 *
 * @return void
 * */
export function horizontalSections( $container ) {

	$Window.on( 'scroll', function() {
		let scroll = window.scrollY;

		$container.find( '.wpk-section' ).each( function() {

			let $that   = $( this ),
				$target = $( $that.data( 'target' ) );
			if ( $target.position().top - 5 <= scroll ) {
				$container.find( '.wpk-active' ).removeClass( 'wpk-active' );
				$that.addClass( 'wpk-active' );
			}
		} );

	} ).trigger( 'scroll' );

	$container.find( '.wpk-section' ).on( 'click', function() {

		let $that   = $( this ),
			$target = $( $that.data( 'target' ) );
		$( 'html,body' ).animate( {
			scrollTop: $target.position().top + 'px'
		}, 500 );
		$Window.trigger( 'scroll' );

	} );

}

/**
 * Perform scroll to middle of provived element
 *
 * @return {void}
 * */
export function scrollToMiddle( $element ) {

	let elementTop     = $element.offset().top,
		elementHeight  = $element.height(),
		viewportHeight = $Window.height(),
		scrollIt       = elementTop - ((viewportHeight - elementHeight) / 2);

	$( 'html,body' ).animate( {
		scrollTop: scrollIt
	}, 500 );

}