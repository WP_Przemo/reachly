import { $Loader } from '../constants/loader';
import { $ } from '../constants/jquery';

/**
 * Show/hide page loader
 *
 * @param {String} action Loader action (show, hide)
 * @param {Function|Boolean) callback Function that will execute after loader will be shown/hiden
	 *
 * @return void
 *
 */
export function pageLoader( action, callback = false ) {

	if ( action === 'show' ) {
		$Loader.fadeIn( 'fast', function() {
			if ( typeof callback === 'function' ) {
				callback();
			}
		} );
	}
}

/**
 * Append loader to element
 *
 * @param {jQuery} $element
 * @param {Boolean} fade
 * @param {String} message
 *
 * */
export function appendLoader( $element, fade = false, message = '' ) {

	let $loader = $Loader.clone(),
		$message;

	if ( $element.find( ' > .wpk-loader' ).length ) {

		if ( message ) {

			$message = $( `<div class="wpk-loader-message">${message}</div>` );
			$element
				.find( '.wpk-loader-message' )
				.remove()
				.end()
				.find( '.wpk-loader' )
				.append( $message );

		}

		return;
	}

	$loader.addClass( 'wpk-loader' ).removeClass( 'mkd-smooth-transition-loader' );
	$element.addClass( 'wpk-relative' ).append( $loader );
	$loader.fadeIn();

	if ( fade ) {
		$element.addClass( 'wpk-loader-fade' );
	}

	if ( message ) {
		$message = $( `<div class="wpk-loader-message">${message}</div>` );
		$loader.append( $message );
	}

}

/**
 * Remove ajax loader from page.
 *
 * @param {jQuery} $element
 * @param {Function|null) callback
	 *
	 * */
export function removeLoader( $element, callback = null ) {

	$element.find( '.wpk-loader' ).fadeOut( function() {

		$( this ).remove();

		$element.removeClass( 'wpk-loader-fade' ).removeClass( 'wpk-relative' );
		if ( typeof callback === 'function' ) {
			setTimeout( callback, 1000 );
		}
	} );
}