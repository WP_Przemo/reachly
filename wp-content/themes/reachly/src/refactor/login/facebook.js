/*global FB*/
import { $ } from '../constants/jquery';
import { $Body, $Document } from '../constants/selectors';
import { appendLoader, removeLoader } from '../helpers/loader';
import { handleResponse, ajax } from '../helpers/ajax';
import { isset } from '../helpers/checkers';
import { Actions } from '../constants/login';
import { showModal, closeModal } from '../helpers/modal';
import { redirect } from '../helpers/url';
import { getMessage } from '../helpers/vars';

let token;

const checkLoginState = () => {

	appendLoader( $Body );

	FB.getLoginStatus( function( response ) {

		if ( isset( response.authResponse.accessToken ) ) {

			const Fd = new FormData();

			token = response.authResponse.accessToken;

			Fd.append( 'token', token );
			Fd.append( 'action', Actions.getFBAccounts );

			ajax( {
				data:     Fd,
				success( data ) {

					handleResponse( data );

					if ( isset( data.result.accounts ) ) {

						let igAccounts = data.result.accounts;
						token = data.result.token;

						showModal( igAccounts, { full: false } );

					}
				},
				complete: () => {
					removeLoader( $Body );
				}
			} );
		}
	} );

};

/**
 * Create facebook embed script
 *
 * @return void
 * */
function createScript() {

	(function( d, s, id ) {
		let js, fjs = d.getElementsByTagName( s )[ 0 ];
		if ( d.getElementById( id ) ) {
			return;
		}
		js = d.createElement( s );
		js.id = id;
		js.src = 'https://connect.facebook.net/en_US/sdk.js';
		fjs.parentNode.insertBefore( js, fjs );
	}( document, 'script', 'facebook-jssdk' ));

}

/**
 * Login or register after clicking instagram accountl
 *
 * @return void
 * */
function handleIGAccountClick( e ) {

	const $This  = $( e.currentTarget ),
		  Id     = $This.data( 'id' ),
		  Fd     = new FormData(),
		  $Modal = $( '.wpk-modal' );

	Fd.append( 'action', Actions.instagramLogin );
	Fd.append( 'token', token );
	Fd.append( 'id', Id );

	ajax( {
		data: Fd,
		beforeSend() {
			appendLoader( $Modal, false, getMessage( 'fetching_instagram_data' ) );
		},
		success( data ) {

			handleResponse( data );

			if ( data.redirect_url ) {
				closeModal();

				setTimeout( () => redirect( data.redirect_url ), 500 );
			}

		},
		complete() {
			removeLoader( $Modal );
		}
	} );

}

//Create FB embed script
createScript();

//Bind global handler to window
window.checkLoginState = checkLoginState;

//Setup global click event for instagram account
$Document.on( 'click', '.wpk-ig-account', handleIGAccountClick );

