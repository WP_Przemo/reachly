import { onFinishTyping } from '../helpers/events';
import { $Body } from '../constants/selectors';
import { $Input, Actions, $CompanyGrid, $Suggestions, $Filter } from '../constants/brands';
import { $ } from '../constants/jquery';
import { ajax } from '../helpers/ajax';
import { appendLoader, removeLoader } from '../helpers/loader';
import { getMessage } from '../helpers/vars';

const AjaxCalls = [];

/**
 * Create new list with brands suggestions
 *
 * @param {Array} items
 *
 * @return void
 * */
function createSuggestions( items ) {

	let listItems = '';

	for ( let item of items ) {

		let { id, avatar, link, logo, name } = item;

		listItems += `
			<li data-id="${id}" data-avatar="${avatar}" data-link="${link}" data-logo="${logo}" data-name="${name}">
				<a class="wpk-link" href="${item.link}">${item.name}</a>
			</li>`;

	}

	$Suggestions.html( listItems );

}

/**
 * Clears suggestions list
 *
 * @return void
 * */
function clearSuggestions() {
	$Suggestions.html( '' );
}

/**
 * Callback for finish typing event
 *
 * @return void
 * */
function filter() {

	const Fd            = new FormData(),
		  $This         = $( this ),
		  $LoaderTarget = $This.parent(),
		  Nonce         = $This.next().val();

	Fd.append( 'search', $This.val() );
	Fd.append( 'action', Actions.getBrands );
	Fd.append( 'wpk_nonce', Nonce );

	AjaxCalls.push(
		ajax( {
			data: Fd,
			beforeSend() {
				appendLoader( $LoaderTarget );
				appendLoader( $CompanyGrid );
			},
			success( data ) {

				if ( data && data.result ) {

					createSuggestions( data.result.suggestions );

					let $data = $( data.result.companies );

					$CompanyGrid.html( $data.get( 2 ).html() );

				}
			},
			complete() {
				AjaxCalls.shift();

				if ( AjaxCalls.length ) {
					removeLoader( $LoaderTarget );
					removeLoader( $CompanyGrid );
				}

			}
		} )
	);

}

/**
 * Show brands preview on mouse enter
 *
 * @return void
 * */
function showPreview() {

	let $this                            = $( this ),
		{ id, avatar, link, logo, name } = $this.data(),
		companyElement                   = `
				<div class="wpk-company wpk-suggestion-element wpk-element col-xl-3 col-md-6 col-xs-12" data-company-id="${id}">
   					<div class="wpk-company-inner wpk-element-inner">
        				<div class="wpk-company-image wpk-element-image">
           					 <img src="${avatar}" alt="">
       					 </div>
      					 <div class="wpk-company-data wpk-element-data">                
            		<div class="wpk-element-actions wpk-company-actions">
               			 <a href="${link}" class="wpk-element-action wpk-company-action">
                   		 	<span class="wpk-action-label">${getMessage( 'view_campaigns' )}</span>
               			 </a>
           			 	</div>
       				 </div>
    			</div>
			</div>
			`,
		$companyElement                  = $( companyElement );

	if ( !logo ) {

		$companyElement.find( '.wpk-element-data' ).prepend( `<div class="wpk-company-title wpk-element-title"><span class="">${name}</span></div>` );

	} else {

		$companyElement.find( '.wpk-element-data' ).prepend( ` <div class="wpk-element-logo wpk-company-logo"><img src="${logo}" alt=""></div>` );

	}

	$CompanyGrid
		.find( '.wpk-company' )
		.addClass( 'wpk-hidden' )
		.end()
		.find( '.wpk-pagination' )
		.before( $companyElement );

}

/**
 * Hide brands preview on mouse leave
 *
 * @return void
 * */
function hidePreview() {

	$CompanyGrid
		.find( '.wpk-suggestion-element ' )
		.remove()
		.end()
		.find( '.wpk-company' )
		.removeClass( 'wpk-hidden' );

}

//Setup event that launches when user finishes typing
onFinishTyping( $Input, filter, 300 );

//Close suggestion on body click
$Body.on( 'click', clearSuggestions );
$Filter.on( 'click', e => e.stopPropagation() );

//Show brands preview on suggestion hover
$Suggestions.on( 'mouseenter', 'li', showPreview ).on( 'mouseleave', hidePreview );