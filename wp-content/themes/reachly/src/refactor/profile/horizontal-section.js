import { horizontalSections } from '../helpers/layout';
import { $Container } from '../constants/profile';

horizontalSections( $Container.find( '.wpk-horizontal-sections' ) );