import { $ } from '../constants/jquery';
import { $Form, Actions } from '../constants/profile';
import { ajax, handleResponse } from '../helpers/ajax';
import { appendLoader, removeLoader } from '../helpers/loader';
import { redirect } from '../helpers/url';

/**
 * Handle for submit on profile or regsiter page
 *
 * @return void
 * */
function handleSubmit( e ) {

	e.preventDefault();

	const Fd    = new FormData( this ),
		  $This = $( this );

	Fd.append( 'action', $This.attr( 'id' ) === 'wpk_register_form' ? Actions.register : Actions.saveProfile );

	ajax( {
		beforeSend() {
			appendLoader( $This, true );
		},
		data: Fd,
		success( data ) {

			handleResponse( data );

			if ( data.redirect_url ) {
				redirect( data.redirect_url );
			}

		},
		complete() {
			removeLoader( $This );
		}
	} );

}

$Form.on( 'submit', handleSubmit );
