import { $ } from '../constants/jquery';
import { ajax } from '../helpers/ajax';
import { appendLoader, removeLoader } from '../helpers/loader';
import { $Grid, $Filter } from '../constants/influencers';

const AjaxCalls = [];

/**
 * @return void
 * */
function handleFilter() {

	const Fd = new FormData( this );

	AjaxCalls.push(
		ajax( {
			url:  document.location.href,
			data: Fd,
			beforeSend() {
				appendLoader( $Grid.parent(), true );
			},
			success( data ) {

				let $data    = $( data ),
					$newGrid = $data.find( '.wpk-influencers-grid' );

				if ( $newGrid.length ) {
					$Grid.html( $newGrid.html() );
				}

			},
			complete() {

				AjaxCalls.shift();

				if ( !AjaxCalls.length ) {
					removeLoader( $Grid.parent() );
				}

			}

		} )
	);

}

$Filter.on( 'change', handleFilter );