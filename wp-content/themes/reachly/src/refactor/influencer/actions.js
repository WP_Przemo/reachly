import { $ } from '../constants/jquery';
import Influencers from '../../js/campaign/Influencers';
import { ajax, handleResponse } from '../helpers/ajax';
import { appendLoader, removeLoader } from '../helpers/loader';
import { Vars } from '../constants/vars';
import InfluencerModal from '../../js/influencer/Modal';
import { showModal } from '../helpers/modal';
import { scrollToMiddle } from '../helpers/layout';
import { showInfluencerModal } from './modal';
import { setValue } from '../campaign/impressions/slider';
import { $ImpressionsSlider } from '../constants/campaign';

const AjaxCalls         = [],
	  InfluencerActions = [ 'influencer_accept', 'influencer_decline', 'collaborate' ];

/**
 * Global. Handles click on influencer actions
 *
 * @return void
 * */
function handleInfluencerActions() {

	let $clicked     = $( this ),
		campaignID   = $clicked.data( 'campaign-id' ),
		influencerID = $clicked.data( 'influencer-id' ),
		action       = $clicked.data( 'action' );

	if ( !$clicked.hasClass( 'wpk-action-disabled' ) ) {
		handleAction( { action, $clicked, influencerID, campaignID } );
	}


}

/**
 * Handle influencer related action
 *
 * @param {String} action
 * @param {jQuery|null} $clicked Clicked object, to which parent loader will be appended
 * @param {int} campaignID
 * @param {int} influencerID
 *
 * @return {void}
 *
 * */
export function handleAction( { action, $clicked = null, campaignID, influencerID } ) {

	const Fd = new FormData();

	Fd.append( 'influencer_action', action );
	Fd.append( 'action', 'wpk_influencer_action' );
	Fd.append( 'campaign_id', campaignID );
	Fd.append( 'influencer_id', influencerID );
	Fd.append( 'wpk_nonce', Vars.nonces.influencer_action );

	ajax( {
		data: Fd,
		beforeSend() {

			if ( $clicked ) {
				appendLoader( $clicked.parent() );
			}

		},
		success( data ) {
			handleResponse( data );

			handleActionResponse( action, data );
		},
		complete() {

			if ( $clicked ) {
				removeLoader( $clicked.parent() );
			}

		}
	} );

}

/**
 * Handle afterwards of influencer actions, based on where clicked button was located
 *
 * @return {void}
 * */
function handleActionResponse( action, data ) {

	let $form  = $( '.wpk-influencers-filters' ),
		$modal = $( '.wpk-modal' );

	if ( $form.length ) {
		//Trigger change on form to reload grid elements
		$form.change();
	}

	//We have slider, let's set its value
	if ( parseInt( data.result ) > 0 && $ImpressionsSlider.length ) {
		setValue( data.result );
	}

	//Influencer modal is opened
	if ( $modal.length ) {

		let $influencer = $modal.find( '.wpk-influencer-modal' );

		if ( $influencer.length ) {

			let campaignID   = $influencer.data( 'campaign-id' ),
				influencerID = $influencer.data( 'influencer-id' );

			showInfluencerModal( influencerID, campaignID );

		}

	}

	if ( InfluencerActions.indexOf( action ) > -1 ) {
		showModal( data.result, {
			full:        true,
			underMenu:   true,
			transparent: true
		} ).then( $modal => scrollToMiddle( $modal.find( '.linear-icon' ) ) );
	}

}

$( document ).on( 'click', '.wpk-influencer-action', handleInfluencerActions );