import { $ } from '../constants/jquery';
import Charts from '../Charts';
import { showModal } from '../helpers/modal';
import { appendLoader, removeLoader } from '../helpers/loader';
import { ajax, handleResponse } from '../helpers/ajax';
import { $Document } from '../constants/selectors';

/**
 * Open modal after clicking on influencer element
 *
 * @return void
 * */
function handleClick() {

	let $target = $( this );

	//Don't conflict with element actions
	if ( $target.parents( '.wpk-element-actions' ).length ) {
		return;
	}

	let $this        = $( this ),
		campaignID   = $this.data( 'campaign-id' ),
		influencerID = $this.data( 'influencer-id' );

	showInfluencerModal( influencerID, campaignID );

}

/**
 * Displays modal
 *
 * @param {int} influencerID
 * @param {int|null} campaignID Optional campaign context
 *
 * @return void
 *
 * */
export function showInfluencerModal( influencerID, campaignID = null ) {

	const Fd = new FormData();

	let $modal     = $( '.wpk-modal:not(.wpk-mobile-profile)' ),
		$loaderObj = $modal.length ? $modal : $( '.wpk-wrap' );

	Fd.append( 'influencer_id', influencerID );
	Fd.append( 'campaign_id', campaignID );
	Fd.append( 'action', 'wpk_influencer_modal' );

	ajax( {
		data: Fd,
		beforeSend() {
			appendLoader( $loaderObj, true );
		},
		success( data ) {

			handleResponse( data );

			if ( data.result ) {
				let $data = $( data.result );

				showModal( $data.get( 0 ).outerHTML, {
					full:      true,
					underMenu: true
				} );

				showInsights();

			}

		},
		complete() {
			removeLoader( $loaderObj );
		}

	} );

}

/**
 * Show insights graphs in modal
 *
 * @return void
 * */
function showInsights() {

	let $insights = $( '.wpk-influencer-insights' );

	appendLoader( $insights );

	setTimeout( () => {

		$( '.wpk-pie' ).each( function() {
			Charts.createPie( $( this ) );
		} );

		$( '.wpk-bar' ).each( function() {
			Charts.createBar( $( this ) );
		} );


		removeLoader( $insights );
	} );

}

$Document.on( 'click', '.wpk-influencer', handleClick );