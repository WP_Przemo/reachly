import { $ } from './jquery';

export const $Container = $( '#wpk_user_profile, .wpk-register' );
export const $Form = $Container.find( 'form' );
export const Actions = {
	saveProfile: 'wpk_user_profile',
	register:    'wpk_register'
}