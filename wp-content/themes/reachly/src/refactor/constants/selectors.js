import { $ } from './jquery';

export const $Body = $( '.body' );
export const $Window = $( window );
export const $Document = $( document );