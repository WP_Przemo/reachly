import { $ } from './jquery';

export const $Brands = $( '.wpk-brands' );
export const $Suggestions = $Brands.find( '.wpk-suggestions' );
export const $CompanyGrid = $Brands.find( '.wpk-company-grid' );
export const $Input = $Brands.find( '.wpk-autocomplete' );
export const $Filter = $( '.wpk-filter' );
export const Actions = {
	getBrands: 'wpk_get_brands'
};