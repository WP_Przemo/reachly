import { $ } from './jquery';

export const $CreationContainer = $( '.wpk-my-campaigns' );
export const Actions = {
	firstStep:       'wpk_campaign_creation_first_step',
	thirdStep:       'wpk_campaign_creation_third_step',
	publishCampaign: 'wpk_publish_campaign'
};
export const $ImpressionsSlider = $( '.wpk-impressions-slider' );