import Utils from './Utils';

const $ = jQuery.noConflict();

/**
 * Handles brands page
 *
 * @author Przemysław Żydek
 * */
class Brands {

	constructor( $container ) {

		this.$container = $container;
		this.$suggestions = $container.find( '.wpk-suggestions' );
		this.$companyGrid = $container.find( '.wpk-company-grid' );

		this.ajaxCalls = [];

		this.setEvent().autocompletePreview();
	}

	/**
	 * @return Brands
	 * */
	autocompletePreview() {

		const Instance = this;

		this.$suggestions.on( 'mouseenter', 'li', function( e ) {

			let $this                            = $( this ),
				{ id, avatar, link, logo, name } = $this.data(),
				companyElement                   = `
				<div class="wpk-company wpk-suggestion-element wpk-element col-xl-3 col-md-6 col-xs-12" data-company-id="${id}">
   					<div class="wpk-company-inner wpk-element-inner">
        				<div class="wpk-company-image wpk-element-image">
           					 <img src="${avatar}" alt="">
       					 </div>
      					 <div class="wpk-company-data wpk-element-data">                
            		<div class="wpk-element-actions wpk-company-actions">
               			 <a href="${link}" class="wpk-element-action wpk-company-action">
                   		 	<span class="wpk-action-label">${Utils.getMessage( 'view_campaigns' )}</span>
               			 </a>
           			 	</div>
       				 </div>
    			</div>
			</div>
			`,
				$companyElement                  = $( companyElement );

			if ( !logo ) {

				$companyElement.find( '.wpk-element-data' ).prepend( `<div class="wpk-company-title wpk-element-title"><span class="">${name}</span></div>` );

			} else {

				$companyElement.find( '.wpk-element-data' ).prepend( ` <div class="wpk-element-logo wpk-company-logo"><img src="${logo}" alt=""></div>` );

			}

			Instance.$companyGrid
				.find( '.wpk-company' )
				.addClass( 'wpk-hidden' )
				.end()
				.find('.wpk-pagination')
				.before( $companyElement );

		} ).on( 'mouseleave', 'li', function() {

			Instance.$companyGrid
				.find( '.wpk-suggestion-element ' )
				.remove()
				.end()
				.find( '.wpk-company' )
				.removeClass( 'wpk-hidden' );


		} );

		return this;

	}

	/**
	 * @return Brands
	 * */
	setEvent() {

		const $Input = this.$container.find( '.wpk-autocomplete' );

		Utils.onFinishTyping( $Input, () => this.filter( $Input.val(), $Input.next().val(), $Input.parent() ), 300 );

		//Close suggestions when clicked outside them
		$( 'body' ).on( 'click', () => this.clearSuggestions() );
		$( '.wpk-filter' ).on( 'click', 'li', e => e.stopPropagation() );

		return this;

	}

	/**
	 * @param {String} search
	 * @param {String} nonce
	 * @param {jQuery} $loaderTarget
	 *
	 * @return void
	 * */
	filter( search, nonce, $loaderTarget ) {

		let fd       = new FormData(),
			instance = this;

		fd.append( 'search', search );
		fd.append( 'action', 'wpk_get_brands' );
		fd.append( 'wpk_nonce', nonce );

		this.clearSuggestions();

		this.ajaxCalls.push(
			Utils.ajax( {
				data: fd,
				beforeSend() {
					Utils.appendLoader( $loaderTarget );
					Utils.appendLoader( instance.$companyGrid );
				},
				success( data ) {
					if ( data && data.result ) {

						console.log( data );

						instance.createSuggestions( data.result.suggestions );

						let $data = $( data.result.companies );
						instance.replaceGrid( $data.get( 2 ) );

					}
				},
				complete() {
					instance.ajaxCalls.shift();

					if ( !instance.ajaxCalls.length ) {
						Utils.removeLoader( $loaderTarget );
						Utils.removeLoader( instance.$companyGrid );
					}

				}
			} )
		);


	}

	replaceGrid( $newGrid ) {

		this.$companyGrid.replaceWith( $newGrid );
		this.$companyGrid = this.$container.find( '.wpk-company-grid' );

	}

	/**
	 * Clears suggestions list
	 *
	 * @return Brands
	 * */
	clearSuggestions() {
		this.$suggestions.html( '' );

		return this;
	}

	/**
	 * @param {Array} items
	 *
	 * @return void
	 * */
	createSuggestions( items ) {

		let listItems = '';

		for ( let item of items ) {

			let { id, avatar, link, logo, name } = item;

			listItems += `
			<li data-id="${id}" data-avatar="${avatar}" data-link="${link}" data-logo="${logo}" data-name="${name}">
				<a class="wpk-link" href="${item.link}">${item.name}</a>
			</li>`;

		}

		this.$suggestions.html( listItems );

	}

}

export default Brands;