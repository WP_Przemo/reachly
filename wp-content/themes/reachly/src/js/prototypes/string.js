/**
 * Check if string contains provided substring.
 *
 * @param {String} value
 * */
String.prototype.includes = function( value ) {

	let that = this.toLowerCase(),
		val  = value.toLowerCase();

	return that.indexOf( val ) !== -1;

};