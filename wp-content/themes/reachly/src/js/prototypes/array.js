/**
 * Removes provided item from array
 *
 * @param {mixed} item
 *
 * @return {Array} Array with removed item
 *
 * */
Array.prototype.removeItem = function( item ) {

	let index = this.indexOf( item );
	
	if ( index !== -1 ) {
		return this.splice( index, 1 );
	}

};