import './prototypes/array';
import './prototypes/string';

import Utils from './Utils';
import Campaign from './campaign/Campaign';
import Profile from './Profile';
import Login from './Login';
import Rwd from './Rwd';
import InfluencerModal from './influencer/Modal';
import Charts from './Charts';
import Brands from './Brands';

import './partials/file-inputs';
import './partials/float-label';
import './partials/radio-activators';
import './partials/date-picker';
import './partials/select';
import './partials/list-item';
import './partials/checkbox-buttons';
import './partials/accordion';
import './partials/range-sliders';

const $ = jQuery.noConflict();

Utils.addInstance( 'utils', new Utils() );

$( document ).ready( () => {

	Utils.addInstance( 'campaign', Campaign );

	Utils.addInstance( 'profile', new Profile( $( '#wpk_user_profile, .wpk-register' ) ) );

	Utils.addInstance( 'rwd', new Rwd() );

	const $Login = $( '.wpk-login' );

	if ( $Login.length ) {
		Utils.addInstance( 'login', new Login( $Login ) );
	}

	Utils.addInstance( 'influencerModal', new InfluencerModal() );

	const $Brands = $( '.wpk-brands' );

	if ( $Brands.length ) {
		Utils.addInstance( 'brands', new Brands( $Brands ) );
	}

} );

