import 'jquery-ui-dist/jquery-ui';

let $ = jQuery.noConflict();

/**
 * @author Przemysław Żydek
 * */
class ImpressionsSlider {

	constructor( $slider ) {

		this.$slider = $slider;

		if ( this.$slider.length ) {
			this.createSlider();
		}

	}

	/**
	 * Creates impressions slider
	 *
	 * @return {ImpressionsSlider}
	 *
	 * */
	createSlider() {

		let $slider = this.$slider,
			value   = $slider.data( 'value' ),
			min     = $slider.data( 'min' ),
			max     = $slider.data( 'max' ),
			$parent = $slider.parents( '.wpk-campaign-impressions' );

		$slider.slider( {
			min:      min,
			max:      max,
			range:    true,
			values:   [ 0, value ],
			disabled: true,
			change:   function( event, ui ) {

				let values = ui.values;
				$slider.next().find( '.wpk-impressions-value' ).text( values[ 1 ] );

			}
		} );

		if ( $parent.hasClass( 'wpk-fixable' ) && $( window ).width() > 769 ) {

			let position = $parent.offset().top,
				clone    = '<div class="wpk-placeholder" style="display: none; height:' + $slider.parent().height() + 'px"></div>';

			$parent.after( clone );

			let $placeholder = $( '.wpk-placeholder' ),
				oldScroll    = 0;

			$( window ).on( 'scroll', function() {

				let scroll = $( this ).scrollTop();

				if ( scroll >= position ) {
					$parent.addClass( 'wpk-fixed-impressions' );
					$placeholder.show();
				} else {
					$parent.removeClass( 'wpk-fixed-impressions' );
					$placeholder.hide();
				}

				oldScroll = scroll;
			} );
		}

		return this;

	}

	/**
	 * Set value of impressions slider
	 *
	 * @param {int} value
	 * */
	setValue( value ) {

		this.$slider.slider( 'values', [ 0, value ] );

	}

	/**
	 * Get remaining impressions from impressions slider
	 *
	 * @return {int}
	 * */
	getValue() {

		return this.$slider.slider( 'values' )[ 1 ];

	}

}

export default ImpressionsSlider;