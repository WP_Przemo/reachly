import Utils from '../Utils';
import Campaign from './Campaign';
import InfluencerModal from '../influencer/Modal';

let $ = jQuery.noConflict();


/**
 * Handles influencers related actions for campaign
 *
 * @author Przemysław Żydek
 * */
class Influencers {

	constructor() {

		/**
		 * @property {Array}
		 *
		 * Stores all ajax calls that were made using filter
		 * */
		this.filterCalls = [];

		$( document ).on( 'click', '.wpk-publish-campaign:not(.wpk-disabled)', e => this.handleInfluencerActions( e ) );

		$( '.wpk-influencers-filters' ).on( 'change', e => {
			setTimeout( () => this.handleFilter( e ), 300 );
		} );

		$( document ).on( 'click', '.wpk-influencer-action', e => this.handleInfluencerActions( e ) );

	}

	/**
	 * Global. Handles click on influencer actions
	 *
	 * @return void
	 * */
	handleInfluencerActions( e ) {

		const $clicked     = $( e.currentTarget ),
			  campaignID   = $clicked.data( 'campaign-id' ),
			  influencerID = $clicked.data( 'influencer-id' ),
			  action       = $clicked.data( 'action' );

		if ( !$clicked.hasClass( 'wpk-action-disabled' ) ) {
			Influencers.handleAction( { action, $clicked, influencerID, campaignID } );
		}


	}

	/**
	 * Handle filtering
	 *
	 * @return void
	 * */
	handleFilter( e ) {

		const Fd = new FormData( e.currentTarget );

		this.filterCalls.push(
			Utils.ajax( {
				url:        document.location.href,
				data:       Fd,
				beforeSend: () => {
					Utils.appendLoader( Influencers.$grid.parent(), true );
				},
				success:    data => {

					let $data    = $( data ),
						$newGrid = $data.find( '.wpk-influencers-grid' );

					if ( $newGrid.length ) {
						Influencers.replaceGrid( $newGrid );
					}

				},
				complete:   () => {

					this.filterCalls.shift();

					if ( !this.filterCalls.length ) {
						Utils.removeLoader( Influencers.$grid.parent() );
					}

				}

			} )
		);


	}

	/**
	 * Handle influencer related action
	 *
	 * @param {String} action
	 * @param {jQuery|null} $clicked Clicked object, to which parent loader will be appended
	 * @param {int} campaignID
	 * @param {int} influencerID
	 *
	 * @return {void}
	 *
	 * */
	static handleAction( { action, $clicked = null, campaignID, influencerID } ) {

		const Fd = new FormData();

		Fd.append( 'influencer_action', action );
		Fd.append( 'action', 'wpk_influencer_action' );
		Fd.append( 'campaign_id', campaignID );
		Fd.append( 'influencer_id', influencerID );
		Fd.append( 'wpk_nonce', Utils.vars.nonces.influencer_action );

		Utils.ajax( {
			data: Fd,
			beforeSend() {

				if ( $clicked ) {
					Utils.appendLoader( $clicked.parent() );
				}

			},
			success( data ) {

				Utils.handleResponse( data );

				Influencers.handleActionResponse( action, data, $clicked );

			},
			complete() {

				if ( $clicked ) {
					Utils.removeLoader( $clicked.parent() );
				}

			}
		} );

	}

	/**
	 * Replace influencers grid with new one
	 *
	 * @param {jQuery} $newGrid
	 *
	 * @return void
	 * */
	static replaceGrid( $newGrid ) {

		Influencers.$grid.replaceWith( $newGrid );

		Influencers.$grid = $newGrid;

	}

	/**
	 * Refleshes influencers grid via ajax call
	 *
	 * @return void
	 * */
	static refreshGrid() {

		let $grid = Influencers.$grid;

		Utils.ajax( {
			type: 'GET',
			url:  document.location.href,
			beforeSend() {
				Utils.appendLoader( $grid, true );
			},
			success( data ) {

				let $data    = $( data ),
					$newGrid = $data.find( '.wpk-influencers-grid' );

				if ( $newGrid.length ) {
					Influencers.replaceGrid( $newGrid );
				}

			},
			complete() {
				Utils.removeLoader( $grid );
			}
		} );

	}

	/**
	 * Handle afterwards of influencer actions, based on where clicked button was located
	 *
	 * @return {void}
	 * */
	static handleActionResponse( action, data, $clicked ) {

		let $form  = $( '.wpk-influencers-filters' ),
			$modal = $( '.wpk-modal' );

		if ( $form.length ) {
			//Trigger change on form to reload grid elements
			$form.change();
		}

		//We have slider, let's set its value
		if ( parseInt( data.result ) > 0 && Campaign.impressionsSlider.$slider.length ) {
			Campaign.impressionsSlider.setValue( data.result );
		}

		//Influencer modal is opened
		if ( $modal.length ) {

			let $influencer = $modal.find( '.wpk-influencer-modal' );

			if ( $influencer.length ) {

				let campaignID   = $influencer.data( 'campaign-id' ),
					influencerID = $influencer.data( 'influencer-id' );

				InfluencerModal.show( influencerID, campaignID );


			}

		}

		let influencerActions = [ 'influencer_accept', 'influencer_decline', 'collaborate' ];

		if ( influencerActions.find( item => item === action ) ) {
			Utils.modal( data.result, {
				full:        true,
				underMenu:   true,
				transparent: true
			} ).then( $modal => Utils.scrollToMiddle( $modal.find( '.linear-icon' ) ) );
		}

	}

}

Influencers.$grid = $( '.wpk-influencers-grid' );

export default Influencers;