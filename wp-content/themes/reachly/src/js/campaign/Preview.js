import Utils from '../Utils';

const $ = jQuery.noConflict();

/**
 * @author Przemysław Żydek
 * */
class Preview {

	constructor() {
		$( document ).on( 'click', '.wpk-campaign-preview', e => this.handle( e ) );

		window.addEventListener( 'popstate', e => this.handlePopState( e ) );
	}

	/**
	 * @return void
	 * */
	handlePopState( e ) {

		let state = e.state;

		if ( (!state && $( '.wpk-modal .wpk-single-campaign' ).length) || state.closeModal ) {
			Utils.closeModal();

			this.close();
		} else if ( state.modal ) {
			this.open( state.modal );
		}

	}

	/**
	 * @return void
	 * */
	handle( e ) {

		e.preventDefault();

		tinyMCE.triggerSave();

		let $this         = $( e.currentTarget ),
			$form         = $this.parents( 'form' ),
			fd            = new FormData( $form.get( 0 ) ),
			$loaderTarget = $this.parents( '.wpk-wrap' );

		fd.append( 'action', 'wpk_campaign_preview' );

		//Start date is outside form, so we append it manually
		fd.append( 'start_date', $( '#start_date' ).val() );

		Utils.ajax( {
			data:       fd,
			beforeSend: () => {
				Utils.appendLoader( $loaderTarget, true );
			},
			success:    data => {

				if ( data.result ) {

					let html = `<div class="single-campaign">${data.result}</div>`;

					this.open( html );

				}

			},
			complete:   () => {
				Utils.removeLoader( $loaderTarget );
			}
		} );

	}

	/**
	 * Opens campaign preview
	 *
	 * @return void
	 * */
	open( html ) {

		Utils.modal( html, {
			full:        true,
			transparent: false,
			underMenu:   true
		} ).then( $modal => {
			Utils.addQueryParam( 'is_preview', 1, { modal: $modal.html() } );

			$modal.on( 'modal:close', this.close );
		} );

	}

	/**
	 * Close campaign preview
	 *
	 * @return void
	 * */
	close() {
		Utils.removeQueryParam( 'is_preview', { closeModal: true } );
	}

}

export default Preview;