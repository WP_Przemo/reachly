import Utils from '../Utils';

const $ = jQuery.noConflict();

/**
 * Handles campaign creation operations
 * */
class Creation {

	/**
	 * @param {jQuery} $container
	 * */
	constructor( $container ) {

		this.$container = $container;

		this.$container.find( '.wpk-campaign-plan-tab' ).on( 'click', e => this.handleCampaignPlanClick( e ) );

		$( document ).on( 'click', '.wpk-publish-campaign:not(.wpk-disabled)', e => this.publishCampaign( e ) );

		this.priceChange().thirdStep();

	}

	/**
	 * Handle first step of campaign creation
	 *
	 * @return void
	 * */
	handleCampaignPlanClick( e ) {

		const $This  = $( e.currentTarget ),
			  PlanID = $This.data( 'plan-id' ),
			  Fd     = new FormData();

		Fd.append( 'action', 'wpk_campaign_creation_first_step' );
		Fd.append( 'plan_id', PlanID );
		Fd.append( 'campaign_length', this.$container.find( '#campaign_length' ).val() );

		Utils.ajax( {

			beforeSend: () => {
				Utils.appendLoader( this.$container, true );
			},
			data:       Fd,
			success:    data => {

				Utils.handleResponse( data );

				if ( Utils.isset( data.redirect_url ) && data.redirect_url !== '' ) {
					Utils.redirect( data.redirect_url );
				}

			},
			complete:   () => {
				Utils.removeLoader( this.$container );
			}

		} );


	}

	/**
	 * Changes price value after clicking button with period
	 *
	 * @return {Creation}
	 *
	 * */
	priceChange() {

		let $target           = this.$container.find( '.wpk-durations .wpk-button' ),
			instance          = this,
			$campaignDuration = instance.$container.find( '#campaign_duration' );

		$target.on( 'click', function() {

			let $this  = $( this ),
				length = parseInt( $this.data( 'length' ) );

			instance.$container.find( '.wpk-button.wpk-active' ).removeClass( 'wpk-active' );

			$this.addClass( 'wpk-active' );

			instance.$container.find( '.wpk-campaign-plan-tab' ).each( function() {
				let $that  = $( this ),
					prices = $that.data( 'prices' );

				if ( Utils.isset( prices[ length ] ) ) {

					let price    = parseInt( prices[ length ] ),
						oldPrice = parseInt( $that.find( '.wpk-price' ).text() );

					$that.find( '.wpk-price' ).prop( 'Counter', oldPrice ).animate( {
						complete: function() {

						},
						Counter:  price,
					}, {
						duration: 500,
						easing:   'swing',
						step:     function( now ) {
							$that.find( '.wpk-price' ).text( Math.ceil( now ) );
						}
					} );

					let $campaignPrice = instance.$container.find( '#campaign_price' );

					if ( $campaignPrice.length &&
						$campaignDuration.length &&
						$campaignPrice.val() === price &&
						$campaignDuration.val() === length ) {
						$that.addClass( 'wpk-active' );
					} else {
						$that.removeClass( 'wpk-active' );
					}

					instance.$container.find( '#campaign_length' ).val( length );

				}
			} );

		} );

		if ( $campaignDuration.length ) {
			this.$container.find( `.wpk-durations .wpk-button[data-length=${$campaignDuration.val()}]` ).click();
		} else {
			$target.eq( 0 ).click();
		}

		return this;

	}

	/**
	 * Handle third step of campaign creation
	 *
	 * @return {Creation}
	 * */
	thirdStep() {

		const getSelectionValues = ( $item ) => {

			let result = [];

			$item.parent().find( '.wpk-selection' ).each( function() {

				let $this = $( this ),
					value = $this.data( 'value' );

				result.push( value );

			} );

			return result;

		};

		let $container  = this.$container.find( '.wpk-step-3' ),
			$form       = $container.find( '.wpk-new-campaign-form' ),
			$tags       = $( '#tags' ),
			$categories = $( '#categories' ),
			instance    = this;

		$form.on( 'submit', function( e ) {

			e.preventDefault();

			tinyMCE.triggerSave();

			let fd         = new FormData( this ),
				tags       = getSelectionValues( $tags ),
				categories = getSelectionValues( $categories );

			fd.append( 'action', 'wpk_campaign_creation_third_step' );
			//We need to append data from sidebar which is outside of the form

			fd.append( 'start_date', $( '#start_date' ).val() );
			fd.append( 'categories', categories );
			fd.append( 'tags', tags );


			Utils.ajax( {
				data: fd,
				beforeSend() {
					Utils.appendLoader( instance.$container, true );
				},
				success( data ) {

					Utils.handleResponse( data );

					if ( data.result && data.redirect_url ) {
						Utils.redirect( data.redirect_url );
					}

				},
				complete() {
					Utils.removeLoader( instance.$container );
				}
			} );


		} );

		return this;

	}

	/**
	 * @return void
	 * */
	publishCampaign( e ) {

		const $This      = $( e.currentTarget ),
			  CampaignID = $This.data( 'campaign-id' ),
			  Fd         = new FormData();

		Fd.append( 'action', 'wpk_publish_campaign' );
		Fd.append( 'wpk_nonce', $This.next().val() );
		Fd.append( 'campaign_id', CampaignID );

		Utils.ajax( {
			data:       Fd,
			beforeSend: () => {
				Utils.appendLoader( $This );
			},
			success:    data => {

				Utils.handleResponse( data );

				if ( data.redirect_url ) {
					Utils.redirect( data.redirect_url );
				}

			},
			complete:   () => {
				Utils.removeLoader( $This );
			}
		} );

	}

}

export default Creation;

