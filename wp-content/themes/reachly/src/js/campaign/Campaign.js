import Creation from './Creation';
import Influencers from './Influencers';
import ImpressionsSlider from './ImpressionsSlider';
import Preview from './Preview';

let $ = jQuery.noConflict();

const Campaign = {
	creation:          new Creation( $( '.wpk-my-campaigns' ) ),
	influencers:       new Influencers(),
	impressionsSlider: new ImpressionsSlider( $( '.wpk-impressions-slider' ) ),
	preview:           new Preview()
};

export default Campaign;