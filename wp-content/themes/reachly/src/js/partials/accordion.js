
const $          = jQuery.noConflict(),
	$accordion = $( '.wpk-accordion-title span' );

$accordion.parent().find( ' .wpk-accordion-list' ).hide();

$accordion.on( 'click', function() {

	let $this = $( this ).parent();

	if ( !$this.parents( '.wpk-conversations-sidebar' ).length ) {

		$( '.wpk-accordion' ).find( '.wpk-active' ).not( $this ).removeClass( 'wpk-active' ).find( '.wpk-accordion-list' ).slideUp();
		$this.toggleClass( 'wpk-active' ).find( '.wpk-accordion-list' ).slideToggle();

	} else {

		$this.toggleClass( 'wpk-active' ).find( '>.wpk-accordion-list' ).slideToggle();

	}

} );