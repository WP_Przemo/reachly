import Utils from '../Utils';
import 'select2';

const $ = jQuery.noConflict();

const appendSelection = ( $input, label, value ) => {

	let el          = `<div class="wpk-selection" data-value="${value}">${label}</div>`,
		$selections = $input.parent().find( '.wpk-selections' );

	$selections.find( '.wpk-selection' ).each( function() {
		let $this     = $( this ),
			dataValue = $this.data( 'value' );


		if ( dataValue == value ) {
			$this.remove();
			return false;
		}

	} );

	$selections.append( el );

};

$( '.wpk-select2' ).each( function() {

	let $this       = $( this ),
		placeholder = $this.attr( 'placeholder' );
	$this.select2( {
		placeholder: placeholder
	} );

} );

$( '.wpk-select2-under' ).each( function() {

	let $that       = $( this ),
		placeholder = $that.attr( 'placeholder' ),
		settings    = {
			placeholder: placeholder
		};

	if ( Utils.isset( $that.data( 'noresults' ) ) ) {

		settings.language = {};
		settings.language.noResults = function() {
			return $that.data( 'noresults' );
		};

	}

	if ( $that.data( 'allow-new' ) == 1 ) {

		settings.tags = true;
		settings.createTag = function( params ) {

			return {
				id:     params.term,
				text:   params.term,
				newTag: true
			};

		};

		settings.maximumInputLength = 20; // only allow terms up to 20 characters long
	}

	$that.select2( settings ).on( 'select2:select', function( e ) {

		let data   = e.params.data,
			id     = data.id,
			label  = data.text,
			$input = $that.next().find( '.select2-search__field' ),
			$this  = $( this );

		//Tags
		if ( $this.data( 'allowNew' ) == 1 ) {
			label = '#' + label.replace( '#', '' ).replace( /\s/g, '' );
			id = '#' + id.replace( '#', '' ).replace( /\s/g, '' );
		}

		appendSelection( $this, label, id );

		//Select2 removes placeholder if we pick value, so we have to add it again
		$input.attr( 'placeholder', placeholder );

	} );
} );

$( document ).on( 'click', '.wpk-selection', function() {

	let $this   = $( this ),
		value   = $this.data( 'value' ),
		$select = $this.parents( '.wpk-input-container' ).find( '.wpk-select2-under-save, .wpk-select2-under' );

	if ( $this.parents( '.wpk-selection-disabled' ).length ) {
		return;
	}

	if ( $select.length ) {

		let values = $select.val();

		if ( values ) {
			values.removeItem( value );
			$select.val( values ).change();
		}

		$this.remove();

	}

} );