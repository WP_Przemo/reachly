const $ = jQuery.noConflict();

//Checkout buttons

$( '.wpk-radio-activators' ).find( '.wpk-button' ).on( 'click', function() {

	let $this  = $( this ),
		target = $this.data( 'target' );

	$( target ).click();

	$this.parent().find( '.wpk-active' ).removeClass( 'wpk-active' );
	$this.addClass( 'wpk-active' );

} ).eq( 0 ).click();