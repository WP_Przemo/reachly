const $ = jQuery.noConflict();

$( '.wpk-float-label input' ).on( 'keyup change', function() {
	let $this = $( this ),
		val   = $this.val();
	if ( val !== '' ) {
		$this.parent().addClass( 'wpk-not-empty' );
	} else {
		$this.parent().removeClass( 'wpk-not-empty' );
	}
} ).change();