const $ = jQuery.noConflict();


$( '.wpk-checkbox-button' ).on( 'click', function() {
	let $this         = $( this ),
		$target       = $this.next(),
		$parent       = $this.parent(),
		type          = $target.attr( 'type' ),
		allowMultiple = $this.data( 'allow-multiple' );

	if ( type === 'radio' ) {
		$parent.find( '.wpk-active' ).removeClass( 'wpk-active' );
		$this.addClass( 'wpk-active' );
		$target.click();
	} else if ( type === 'checkbox' ) {

		if ( !allowMultiple ) {
			if ( $this.hasClass( 'wpk-active' ) ) {

				$this.removeClass( 'wpk-active' );
				$target.click().change();

			} else {

				$parent.find( '.wpk-active' ).removeClass( 'wpk-active' ).next().click();
				$this.addClass( 'wpk-active' );
				$target.click().change();

			}
		} else {

			$this.toggleClass( 'wpk-active' );
			$target.click();

		}


	}

} );