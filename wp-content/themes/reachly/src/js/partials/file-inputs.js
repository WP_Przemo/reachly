import Utils from '../Utils';

const $ = jQuery.noConflict();


/**
 * Handles file inputs actions
 * */
const Upload = {

	/**
	 * @param {File} file File to upload
	 * @param {String} action Ajax action
	 * @param {String} nonce
	 * @param {jQuery} $container
	 *
	 * @return {Promise}
	 * */
	handle( file, action, nonce, $container ) {

		let fd            = new FormData(),
			$loaderTarget = $container.find( '.wpk-file-input-target' ),
			id            = $container.data( 'id' );

		fd.append( 'file', file );
		fd.append( 'action', action );
		fd.append( 'wpk_file_nonce', nonce );
		fd.append( 'id', id );

		return new Promise( ( resolve ) => {

			$container.parent().removeClass( 'wpk-disabled' );

			Utils.ajax( {
				beforeSend() {
					Utils.appendLoader( $loaderTarget );
				},
				data: fd,
				success( data ) {
					resolve( data );
				},
				complete() {
					Utils.removeLoader( $loaderTarget );
				},
			} );

		} );

	},

	/**
	 * Add image to input preview
	 *
	 * @param {Array} images
	 * @param {jQuery} $container
	 *
	 * @return void
	 * */
	addImages( images, $container ) {

		$container.addClass( 'wpk-has-image' ).find( 'img' ).remove();

		let $target = $container.find( '.wpk-file-input-target' ),
			img;

		for ( let image of images ) {

			img = new Image();

			img.onload = function() {
				$target.append( this );
			};

			img.src = image;

		}

	},

	/**
	 * @param {String} action
	 * @param {String} nonce
	 * @param {jQuery} $container
	 *
	 * @return void
	 *
	 * */
	removeImages( action, nonce, $container ) {

		let fd            = new FormData(),
			$loaderTarget = $container.find( '.wpk-file-input-target' ),
			id            = $container.data( 'id' );

		fd.append( 'action', action );
		fd.append( 'wpk_file_nonce', nonce );
		fd.append( 'id', id );

		Utils.ajax( {
			data: fd,
			beforeSend() {
				Utils.appendLoader( $loaderTarget, true );
			},
			success( data ) {

				Utils.handleResponse( data );

				if ( data.result ) {
					$container
						.removeClass( 'wpk-has-image' )
						.find( 'img' )
						.remove()
						.end()
						.find( 'input[type="file"]' ).val( '' );
				}

			},
			complete() {
				Utils.removeLoader( $loaderTarget );
			}
		} );

	}

};

$( '.wpk-image-upload' ).each( function() {

	let $container   = $( this ),
		$input       = $container.find( 'input[type=file]' ),
		action       = $container.data( 'action' ),
		removeAction = $container.data( 'remove-action' ),
		nonce        = $container.find( '.wpk-file-nonce' ).val(),
		images       = $container.data( 'images' );

	$container.find( '.wpk-file-input-target' ).on( 'click', function() {

		if ( $container.find( '.wpk-loader' ).length ) {
			return;
		}

		$input.click();
	} );

	images = images.filter( item => item !== '' );

	if ( images.length ) {
		Upload.addImages( images, $container );
	}

	$input.on( 'change', function() {

		if ( !this.files ) {
			return;
		}

		let files = this.files;

		for ( let file of files ) {
			Upload.handle( file, action, nonce, $container ).then( data => Upload.addImages( data.result, $container ) );
		}

	} );

	$container.find( '.wpk-image-actions > span' ).on( 'click', function() {

		let $this  = $( this ),
			action = $this.data( 'action' );

		switch ( action ) {

			case 'remove':
				Upload.removeImages( removeAction, nonce, $container );
				break;

		}

	} );


} );