import 'jquery-ui-dist/jquery-ui';

const $ = jQuery.noConflict();

$( '.wpk-range-slider' ).each( function() {

	let $this           = $( this ),
		min             = $this.data( 'min' ),
		max             = $this.data( 'max' ),
		$inputs         = $this.find( '.wpk-range-input' ),
		$values_targets = $this.find( '.wpk-value-target' );

	$this.find( '.wpk-range-container' ).slider( {
		range:  true,
		min:    min,
		max:    max,
		values: [ min, max ],
		stop( event, ui ) {

			let values = ui.values,
				index  = ui.handle.nextSibling ? 0 : 1;

			$inputs.eq( 0 ).val( values[ 0 ] );
			$inputs.eq( 1 ).val( values[ 1 ] );
			$inputs.eq( index ).change();

		},
		slide( event, ui ) {

			let values = ui.values;

			$values_targets.eq( 0 ).html( '' ).append( values[ 0 ] );
			$values_targets.eq( 1 ).html( '' ).append( values[ 1 ] );

		},
		change( event, ui ) {

			let values = ui.values;

			$values_targets.eq( 0 ).html( '' ).append( values[ 0 ] );
			$values_targets.eq( 1 ).html( '' ).append( values[ 1 ] );

		}
	} );

} );