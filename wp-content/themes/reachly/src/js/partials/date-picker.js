import 'bootstrap-datepicker';

const $ = jQuery.noConflict();



$( '.wpk-date-picker' ).each( function() {
	let $this       = $( this ),
		id          = $this.attr( 'id' ),
		orientation = $this.data( 'orientation' ) ? $this.data( 'orientation' ) : 'right',
		startDate   = $this.data( 'allow_past' ) ? '' : new Date(),
		settings    = {
			container:   `#${id}_calendar`,
			format:      'dd.mm.yyyy',
			orientation: orientation,
			startDate:   startDate,
			autoclose:   true,
			templates:   {
				leftArrow:  '<i class="material-icons">keyboard_arrow_left</i>',
				rightArrow: '<i class="material-icons">keyboard_arrow_right</i>',
			}
		};


	if ( $this.hasClass( 'wpk-campaign-date' ) ) {

		settings.beforeShowDay = function( date ) {
			let day = date.getDate();

			return day === 1 || day === 15;
		};
	}


	try{
		$this.datepicker( settings );
	} catch(e){
		console.log( e );
	}
} );