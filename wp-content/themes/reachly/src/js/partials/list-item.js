const $         = jQuery.noConflict(),
	$listItem = $( '.wpk-list-item' );

$listItem.find( '.wpk-add-more' ).on( 'click', function( e ) {

	e.preventDefault();

	let $parent = $( this ).parent(),
		$clone  = $parent.clone();

	$clone
		.find( '.wpk-add-more' )
		.text( '-' )
		.removeClass( 'wpk-add-more' )
		.addClass( 'wpk-remove-item' )
		.parent()
		.find( '.wpk-campaign-items' )
		.val( '' );

	$parent.parent().find( '.wpk-list-item' ).last().after( $clone );

} );

$( document ).on( 'click', '.wpk-remove-item', function( e ) {
	e.preventDefault();

	$( this ).parent().remove();
} );