import Utils from './Utils';

/**
 * Handles registering process
 *
 * @author Przemysław Żydek
 * */
class Register {

	/**
	 * @param {jQuery} $container
	 * */
	constructor( $container ) {

		this.$container = $container;

	}

	/**
	 * @return {Register}
	 * */
	handleForm(){

		let $form = this.$container.find('#wpk_register_form');

		$form.on('submit', function(){

		});

		return this;
	}

}