const $ = jQuery.noConflict();

class Utils {

	constructor() {
		this.setupModalEvents();
	}

	/**
	 * @return {Utils}
	 * */
	setupModalEvents() {
		$( document ).on( 'click', '.wpk-modal-overlay, .wpk-close-modal', Utils.closeModal );

		$( document ).on( 'keyup', e => {
			//Close modal on esc press
			if ( e.keyCode === 27 ) {
				Utils.closeModal();
			}
		} );

		return this;
	}

	/**
	 * Check if value exists
	 *
	 * @param value Value to check
	 * @return {Boolean}
	 */
	static isset( value ) {

		return 'undefined' !== typeof value;

	}

	/**
	 *
	 * Add instance from job to global storage
	 *
	 * @param {String} key Instance key
	 * @param instance
	 *
	 * @return void
	 *
	 * */
	static addInstance( key, instance ) {

		if ( !this.isset( window.wpk ) ) {
			window.wpk = {};
		}

		window.wpk[ key ] = instance;

	}

	/**
	 *  Displays modal
	 *
	 *  @param {String} content Inner content of modal
	 *  @param {Object} options Additional options for modal Type of modal (full, full_under_menu, half)
	 *
	 *  @return {Promise}
	 *
	 * */
	static modal( content, options = { full: true, underMenu: false, transparent: false } ) {

		return new Promise( resolve => {

			let modal, appendTarget, $modal = $( '.wpk-modal:not(.wpk-mobile-profile)' );

			let { full, underMenu, transparent } = options;

			if ( full && underMenu && transparent ) {

				modal = `<div data-type="full_under_menu_transparent" class="wpk-modal wpk-modal-absolute wpk-modal-hidden wpk-modal-transparent"><div class="wpk-modal-inner">${content}</div></div>`;

			} else if ( full && underMenu ) {
				modal = `<div data-type="full_under_menu" class="wpk-modal wpk-modal-absolute wpk-modal-hidden"><div class="wpk-modal-inner">${content}</div></div>`;
			} else if ( full ) {
				modal = `<div data-type="full" class="wpk-modal wpk-modal-hidden"><div class="wpk-modal-inner">${content}</div></div>`;
			} else {
				modal = `<div class="wpk-modal wpk-half-modal wpk-modal-hidden"><div class="wpk-close-modal"><i class="material-icons">close</i></div><div class="wpk-modal-inner">${content}</div></div><div class="wpk-modal-overlay"></div>`;
			}

			appendTarget = full && underMenu ? '.mkd-content' : 'body';


			if ( $modal.length ) {

				$modal.replaceWith( modal );
				$( '.wpk-modal' ).removeClass( 'wpk-modal-hidden' ).removeClass( 'wpk-modal-absolute' ).addClass( 'wpk-modal-relative' );

				resolve( $modal );

			} else {

				$( appendTarget ).append( modal );

				$modal = $( '.wpk-modal' );

				setTimeout( function() {

					if ( !transparent ) {
						window.scrollTo( 0, 0 );
					}

					$( '.wpk-modal:not(.wpk-mobile-profile)' ).removeClass( 'wpk-modal-hidden' );

					if ( full && underMenu ) {
						$( '.wpk-modal' ).removeClass( 'wpk-modal-absolute' ).addClass( 'wpk-modal-relative' );
						$( '.mkd-content-inner' ).hide();
					}

					resolve( $modal );

				}, 500 );
			}

		} );
	}

	/**
	 * @return void
	 * */
	static closeModal() {

		let $modal = $( '.wpk-modal:not(.wpk-mobile-profile)' ),
			type   = $modal.data( 'type' );

		$modal.addClass( 'wpk-modal-hidden' );

		if ( type === 'full_under_menu' || type === 'full' ) {
			setTimeout( function() {
				$( '.mkd-content-inner, .wpk-wrap' ).fadeIn();
			}, 400 );
		}

		setTimeout( function() {
			$modal.trigger( 'modal:close' ).remove();

			$( '.wpk-modal-overlay' ).remove();
		}, 400 );


	}

	/**
	 * Append loader to element
	 *
	 * @param {jQuery} $element
	 * @param {Boolean} fade
	 * @param {String} message
	 *
	 * */
	static appendLoader( $element, fade = false, message = '' ) {

		let $loader = $( '.mkd-smooth-transition-loader' ).clone(),
			$message;

		if ( $element.find( ' > .wpk-loader' ).length ) {

			if ( message ) {

				$message = $( `<div class="wpk-loader-message">${message}</div>` );
				$element
					.find( '.wpk-loader-message' )
					.remove()
					.end()
					.find( '.wpk-loader' )
					.append( $message );

			}

			return;
		}

		$loader.addClass( 'wpk-loader' ).removeClass( 'mkd-smooth-transition-loader' );
		$element.addClass( 'wpk-relative' ).append( $loader );
		$loader.fadeIn();

		if ( fade ) {
			$element.addClass( 'wpk-loader-fade' );
		}

		if ( message ) {
			$message = $( `<div class="wpk-loader-message">${message}</div>` );
			$loader.append( $message );
		}

	}

	/**
	 * Remove ajax loader from page.
	 *
	 * @param {jQuery} $element
	 * @param {Function|null) callback
	 *
	 * */
	static removeLoader( $element, callback = null ) {

		$element.find( '.wpk-loader' ).fadeOut( function() {

			$( this ).remove();

			$element.removeClass( 'wpk-loader-fade' ).removeClass( 'wpk-relative' );
			if ( typeof callback === 'function' ) {
				setTimeout( callback, 1000 );
			}
		} );
	}

	/**
	 * Perform ajax call.
	 *
	 * @param {Object} args Additional settings for ajax
	 *
	 * */
	static ajax( args = [] ) {

		let settings = {
			type:        'POST',
			url:         this.vars.ajax_url,
			processData: false,
			contentType: false,
			error( xhr, ajaxOptions, thrownError ) {
				Utils.displayPopup( 'error', 'Server error' );
				console.error( thrownError );
			},
		};

		settings = $.extend( settings, args );

		return $.ajax( settings );

	}

	/**
	 * Show/hide page loader
	 *
	 * @param {String} action Loader action (show, hide)
	 * @param {Function|Boolean) callback Function that will execute after loader will be shown/hiden
	 *
	 * @return void
	 *
	 */
	static pageLoader( action, callback = false ) {

		let $loader = $( '.mkd-smooth-transition-loader' );

		if ( action === 'show' ) {
			$loader.fadeIn( 'fast', function() {
				if ( typeof callback === 'function' ) {
					callback();
				}
			} );
		}
	}

	/**
	 * Performs redirect to provided url
	 *
	 * @param {String} url
	 *
	 * @return void
	 *
	 * */
	static redirect( url ) {

		let callback = () => window.location.href = url;

		this.pageLoader( 'show', callback );

	}

	/**
	 * Parse AJAX response
	 *
	 * @param {Object} response
	 *
	 * @return void
	 *
	 * */
	static handleResponse( response ) {

		//Remove all errors first
		$( '.wpk-has-error' ).removeClass( 'wpk-has-error' ).find( '.wpk-error-message' ).remove();

		if ( response == 0 ) {
			Utils.displayPopup( 'error', 'Invalid server response' );
		} else {

			if ( response.messages.length ) {

				for ( let message of response.messages ) {

					let target = message.target;

					if ( target === 'alert' ) {
						window.alert( message.message );
					} else if ( target === 'popup' ) {
						Utils.displayPopup( message.type, message.message );

					} else {
						let $target = $( target ).parent();

						$target.addClass( 'wpk-has-error' ).append( `<span class="wpk-error-message">${message.message}</span>` );

					}

				}

			}

			//Scroll to first input error, if any exists
			let $errors = $( '.wpk-has-error' );

			if ( $errors.length ) {
				let offset = $errors.eq( 0 ).offset();
				$( 'html,body' ).animate( {
					scrollTop: offset.top - 200
				}, 500 );

			}


		}

	}

	/**
	 * Displays customized popup
	 *
	 * @param {String} type Type of popup (error, warning, success)
	 * @param {String} message Text that will be displayed in popup
	 * @param {boolean|Number} clear
	 * */
	static displayPopup( type, message, clear = 3000 ) {

		let el    = `<div class="wpk-popup alert alert-${type}">${message}</div>`,
			$popup,
			$area = $( '.wpk-notification-area' );

		$area.prepend( el );
		$popup = $( '.wpk-popup' ).first();

		$popup.fadeIn( 400, function() {
			$( this ).addClass( 'wpk-popup-ready' );
		} );

		if ( clear ) {
			setTimeout( () => this.clearPopup( $popup ), clear );
		}

		$popup.on( 'click', function() {
			Utils.clearPopup( $( this ) );
		} );
	}

	/**
	 * @return void
	 * */
	static clearPopup( $popup ) {

		$popup.removeClass( 'wpk-popup-ready' ).fadeOut( function() {
			$( this ).remove();
		} );

	}

	/**
	 * Create horizontal section pagination that shows current section
	 *
	 * @param {jQuery} $container
	 *
	 * @return void
	 * */
	static horizontalSections( $container ) {

		let $window = $( window );

		$window.on( 'scroll', function() {
			let scroll = window.scrollY;

			$container.find( '.wpk-section' ).each( function() {

				let $that   = $( this ),
					$target = $( $that.data( 'target' ) );
				if ( $target.position().top - 5 <= scroll ) {
					$container.find( '.wpk-active' ).removeClass( 'wpk-active' );
					$that.addClass( 'wpk-active' );
				}
			} );

		} ).trigger( 'scroll' );

		$container.find( '.wpk-section' ).on( 'click', function() {

			let $that   = $( this ),
				$target = $( $that.data( 'target' ) );
			$( 'html,body' ).animate( {
				scrollTop: $target.position().top + 'px'
			}, 500 );
			$window.trigger( 'scroll' );

		} );

	}

	/**
	 * @return {String}
	 * */
	static getMessage( message ) {

		let vars = Utils.vars.messages;

		return Utils.isset( vars[ message ] ) ? vars[ message ] : '';

	}

	/**
	 * Perform scroll to middle of provived element
	 *
	 * @return {void}
	 * */
	static scrollToMiddle( $element ) {

		let $window        = $( window ),
			elementTop     = $element.offset().top,
			elementHeight  = $element.height(),
			viewportHeight = $window.height(),
			scrollIt       = elementTop - ((viewportHeight - elementHeight) / 2);

		$( 'html,body' ).animate( {
			scrollTop: scrollIt
		}, 500 );

	}

	/**
	 * Add new query param to url
	 *
	 * @return void
	 * */
	static addQueryParam( name, value, state = {} ) {

		const params = new URLSearchParams( location.search );
		params.set( name, value );
		window.history.pushState( state, '', decodeURIComponent( `${location.pathname}?${params}` ) );

	}

	/**
	 * Removes query param from url
	 *
	 * @return void
	 * */
	static removeQueryParam( name, state = {} ) {

		const params = new URLSearchParams( location.search );

		params.delete( name );
		window.history.pushState( state, '', decodeURIComponent( `${location.pathname}?${params}` ) );

	}

	/**
	 * Create event that launches when user finishes typing
	 *
	 * @param {jQuery} $element
	 * @param {Function} callback
	 * @param {Number} timeoutAmount
	 *
	 * @return void
	 * */
	static onFinishTyping( $element, callback, timeoutAmount = 1000 ) {

		let timeout,
			currentValue = $element.val();

		$element.on( 'keyup', function() {

			let value = $( this ).val();

			clearTimeout( timeout );

			//Don't launch event if the value haven't changed
			if ( value === currentValue ) {
				return;
			}

			timeout = setTimeout( () => {
				callback.call( this );

				currentValue = value;
			}, timeoutAmount );

		} ).on( 'keydown', function() {

			clearTimeout( timeout );

		} );

	}

}

Utils.vars = wpk_reachly_vars;

export default Utils;


