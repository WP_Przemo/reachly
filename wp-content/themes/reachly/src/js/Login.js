/*global FB*/
const $ = jQuery.noConflict();

import Utils from './Utils';

class Login {

	/**
	 * @param {jQuery} $container
	 * */
	constructor( $container ) {

		this.$container = $container;

		this.createScript().FBHandler().facebookInit();

		this.$container.find( '#wpk_login' ).on( 'submit', e => this.handleLogin( e ) );

	}

	/**
	 * Handles regular login
	 *
	 * @return void
	 * */
	handleLogin( e ) {

		e.preventDefault();

		const $Submit = this.$container.find( '.wpk-submit' ),
			  Fd      = new FormData( e.currentTarget );

		Fd.append( 'action', 'wpk_login' );

		Utils.ajax( {
			data:       Fd,
			beforeSend: () => {
				Utils.appendLoader( $Submit );
			},
			success:    data => {

				Utils.handleResponse( data );

				if ( data.result && data.redirect_url ) {
					Utils.redirect( data.redirect_url );
				}

			},
			complete:   () => {
				Utils.removeLoader( $Submit );
			}
		} );

	}

	/**
	 * @return {Login}
	 * */
	createScript() {

		(function( d, s, id ) {
			let js, fjs = d.getElementsByTagName( s )[ 0 ];
			if ( d.getElementById( id ) ) {
				return;
			}
			js = d.createElement( s );
			js.id = id;
			js.src = 'https://connect.facebook.net/en_US/sdk.js';
			fjs.parentNode.insertBefore( js, fjs );
		}( document, 'script', 'facebook-jssdk' ));

		return this;

	}

	/**
	 * @return {Login}
	 * */
	facebookInit() {

		window.fbAsyncInit = function() {

			FB.init( {
				appId:   Utils.vars.fb_app_id,
				cookie:  true,
				xfbml:   true,
				version: 'v3.0'
			} );

			FB.AppEvents.logPageView();

		};

	}

	/**
	 * Handle login through FB or instagram
	 *
	 * @return {Login}
	 *
	 * */
	FBHandler() {

		let $body = $( 'body' ),
			token;

		window.checkLoginState = () => {

			Utils.appendLoader( $body );

			FB.getLoginStatus( function( response ) {

				if ( Utils.isset( response.authResponse.accessToken ) ) {

					const Fd = new FormData();

					token = response.authResponse.accessToken;

					Fd.append( 'token', token );
					Fd.append( 'action', 'wpk_get_fb_accounts' );

					Utils.ajax( {
						data:     Fd,
						success:  data => {

							Utils.handleResponse( data );

							if ( Utils.isset( data.result.accounts ) ) {

								let igAccounts = data.result.accounts;
								token = data.result.token;

								Utils.modal( igAccounts, { full: false } );

							}
						},
						complete: () => {
							Utils.removeLoader( $body );
						}
					} );
				}
			} );

		};

		$( document ).on( 'click', '.wpk-ig-account', e => this.handleIGAccountClick( e, token ) );

		return this;

	}

	/**
	 * @param {String} token
	 *
	 * @return void
	 * */
	handleIGAccountClick( e, token ) {

		const $This  = $( e.currentTarget ),
			  Id     = $This.data( 'id' ),
			  Fd     = new FormData(),
			  $Modal = $( '.wpk-modal' );

		Fd.append( 'action', 'wpk_instagram_login' );
		Fd.append( 'token', token );
		Fd.append( 'id', Id );

		Utils.ajax( {
			data:       Fd,
			beforeSend: () => {
				Utils.appendLoader( $Modal, false, Utils.getMessage( 'fetching_instagram_data' ) );
			},
			success:    data => {

				Utils.handleResponse( data );

				if ( data.redirect_url ) {
					Utils.closeModal();

					setTimeout( () => Utils.redirect( data.redirect_url ), 500 );
				}

			},
			complete:   () => {
				Utils.removeLoader( $Modal );
			}
		} );

	}

}

export default Login;