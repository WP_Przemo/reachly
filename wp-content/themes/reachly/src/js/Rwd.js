let $ = jQuery.noConflict();

/**
 * Handles RWD adjustments
 *
 * @author Przemysław Żydek
 * */
class Rwd {

	constructor() {

		this.$window = $( window );
		this.width = this.$window.width();
		this.tabletSmall = 769;

		this
			.header()
			.horizontalSections()
			.editCampaignAdjustments()
			.editInfluencersAdjustments()
			.accordionSidebar();

	}

	/**
	 * @return {Rwd}
	 * */
	header() {

		let $target = $( '.wpk-mobile-profile' );

		if ( this.width < this.tabletSmall ) {

			$( '.wpk-header-avatar-inner' ).on( 'click', function() {
				$target.addClass( 'wpk-active' );
			} )
				.find( 'a' ).removeAttr( 'href' ).on( 'click', function( e ) {

				e.preventDefault();
				$target.addClass( 'wpk-active' );
				return false;

			} );

			$( '.wpk-mobile-profile .wpk-modal-inner' ).on( 'click', function() {

				$( this ).parent().removeClass( 'wpk-active' );

			} );

			$target.find( '*' ).on( 'click', function( e ) {
				e.stopPropagation();
			} );

		} else {
			$target.remove();
		}

		return this;

	}

	/**
	 * @return {Rwd}
	 * */
	horizontalSections() {

		if ( this.width < 769 ) {
			let $sections = $( '.wpk-horizontal-sections' );

			this.$window.on( 'scroll', function() {

				let $this  = $( this ),
					scroll = $this.scrollTop();

				if ( scroll > 104 ) {
					$sections.addClass( 'wpk-adjusted' );
				} else {
					$sections.removeClass( 'wpk-adjusted' );
				}

			} );

		}

		return this;

	}

	/**
	 * @return {Rwd}
	 * */
	editCampaignAdjustments() {

		let $container   = $( '.wpk-my-campaigns, .wpk-edit-campaign' ),
			$plans       = $( '.wpk-campaign-plans' ),
			$filterInner = $( '.wpk-influencers-filter-inner' ),
			instance     = this,
			settings     = {
				arrows:        false,
				centerMode:    true,
				centerPadding: '20px',
				slidesToShow:  3,
				responsive:    [
					{
						breakpoint: 769,
						settings:   {
							arrows:        false,
							centerMode:    true,
							centerPadding: '40px',
							slidesToShow:  1
						}
					},
					{
						breakpoint: 480,
						settings:   {
							arrows:        false,
							centerMode:    true,
							centerPadding: '40px',
							slidesToShow:  1
						}
					}
				]
			};

		if ( $container.length && this.width < this.tabletSmall ) {
			let $steps = $( '.wpk-steps-wrapper' );
			$steps.prependTo( '.wpk-wrap' );
		}

		if ( $plans.length && this.width < this.tabletSmall ) {

			let $slick = $plans.slick( settings );

			$( '.wpk-durations .wpk-button' ).on( 'click', function() {

				$slick.addClass( 'wpk-opacity' );
				setTimeout( () => $slick.slick( 'unslick' ).slick( settings ), 400 );
				setTimeout( () => $slick.removeClass( 'wpk-opacity' ), 800 );

			} );
		}


		if ( this.width < this.tabletSmall ) {

			$( '.wpk-filter-mobile-activator' ).on( 'click', function() {
				$filterInner.slideToggle();

				$( this ).find( 'i' ).toggleClass( 'wpk-open' );
			} ).click();

		}

		return this;

	}

	/**
	 * @return {Rwd}
	 * */
	editInfluencersAdjustments() {

		let $filter = $( '.wpk-influencers-filters' );

		if ( $filter.length && this.width < this.tabletSmall ) {
			$( '.wpk-campaign-impressions .wpk-button' ).insertAfter( '.wpk-campaign-impressions' );
		}

		return this;

	}

	/**
	 * @return {Rwd}
	 * */
	accordionSidebar() {

		let $accordion = $( '.wpk-accordion' ),
			$container = $( '.mkd-position-left-inner' );

		if ( $accordion.length && this.width < this.tabletSmall ) {

			let $sidebar  = $accordion.parents( '.wpk-sidebar' ),
				activator = '<div class="wpk-sidebar-activator"><i class="icon-ellipsis linear-icon"></i></div>';

			$container.prepend( activator );

			$container.parent().addClass( 'wpk-has-accordion' );

			$sidebar.addClass( 'wpk-absolute' );

			$( '.wpk-sidebar-activator' ).on( 'click', function() {

				$sidebar.toggleClass( 'wpk-visible' );
				$( this ).toggleClass( 'wpk-active' );

			} );

		}

	}

}

export default Rwd;


