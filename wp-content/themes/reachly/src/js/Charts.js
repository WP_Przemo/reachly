import { GoogleCharts } from 'google-charts';

GoogleCharts.load( 'current', { packages: [ 'corechart' ] } );

let $ = jQuery.noConflict();

/**
 * Manages charts used on site
 *
 * @author Przemysław Żydek
 * */
class Charts {

	/**
	 * @return {void}
	 * */
	static addChart( chart ) {
		Charts.charts.push( chart );
	}

	/**
	 * @param {jQuery} $container
	 *
	 * @return void
	 * */
	static createPie( $container ) {

		//var data = $container.data( 'values' );

		let dataTable = new GoogleCharts.api.visualization.DataTable(),
			columns   = $container.data( 'columns' ),
			values    = $container.data( 'values' );

		if ( !values.length ) {
			return;
		}

		for ( let i in columns ) {
			if ( columns.hasOwnProperty( i ) ) {
				dataTable.addColumn( i, columns[ i ] );
			}
		}

		dataTable.addColumn( { type: 'string', role: 'tooltip' } );

		values = Charts.createPieTooltip( values );

		dataTable.addRows( values );

		// Instantiate and draw the chart.
		let chart = new GoogleCharts.api.visualization.PieChart( $container.get( 0 ) );

		chart.draw( dataTable, Charts.options );

		Charts.createPieLegend( $container );

		Charts.addChart( chart );

	}

	/**
	 * Create new bar chart
	 *
	 * @param {jQuery} $container
	 * @param {Array} values Optional values
	 *
	 * @return void
	 * */
	static createBar( $container, values = Charts.objectValuesToArray( $container.data( 'values' ) ) ) {

		let columns = $container.data( 'columns' );

		if ( !values.length ) {
			return;
		}

		//For styling
		columns.push( { role: 'style' } );

		values.unshift( columns );

		let dataTable = GoogleCharts.api.visualization.arrayToDataTable( values ),
			chart     = new GoogleCharts.api.visualization.BarChart( $container.get( 0 ) );

		//Resort values again, as they were before
		dataTable.sort( 0 );

		chart.draw( dataTable, Charts.options );

		$container
			.parents( '.wpk-bar-container' )
			.find( '.wpk-bar-value' )
			.on( 'click', function() {

				let $this  = $( this ),
					values = Charts.objectValuesToArray( $this.data( 'values' ) );

				values.unshift( columns );

				let dataTable = GoogleCharts.api.visualization.arrayToDataTable( values );

				$container.parents( '.wpk-bar-container' ).find( '.wpk-bar-value.wpk-active' ).removeClass( 'wpk-active' );
				$this.addClass( 'wpk-active' );

				dataTable.sort( 0 );
				chart.draw( dataTable, Charts.options );


			} );

	}

	/**
	 * @param {Array} values
	 *
	 * @return {Array}
	 *
	 * */
	static createPieTooltip( values ) {

		let full = 0;

		values.forEach( ( item ) => full += item[ 1 ] );

		values.forEach( ( value ) => {

			let percent = (value[ 1 ] / full) * 100;

			value.push( value[ 0 ] + ':' + percent.toFixed( 1 ) + '%' );

		} );

		return values;

	}

	/**
	 * Create legend for pie
	 *
	 * @param {jQuery} $pieContainer
	 *
	 * @return void
	 * */
	static createPieLegend( $pieContainer ) {

		let values  = $pieContainer.data( 'values' ),
			colors  = Charts.options.colors,
			full    = 0,
			$legend = $pieContainer.parent().find( '.wpk-pie-legend' );

		values.forEach( item => full += item[ 1 ] );

		values.forEach( ( item, index ) => {

			let color  = colors[ index ],
				value  = (item[ 1 ] / full) * 100,
				legend = `<div class="wpk-legend-item"><span class="wpk-legend-color" style="background-color: ${color}"></span><span class="wpk-legend-value"><strong>${value.toFixed( 1 )}% </strong>${item[ 0 ]}</span></div>`;

			$legend.append( legend );
		} );
	}

	/**
	 * Parse object values to array for google datatable
	 *
	 * @param {Object} values
	 *
	 * @return {Array}
	 * */
	static objectValuesToArray( values ) {

		let result = [];

		if ( typeof values === 'string' ) {
			values = JSON.parse( JSON.parse( values ) );
		}

		for ( let prop in values ) {

			if ( values.hasOwnProperty( prop ) ) {
				result.push( [ prop, values[ prop ] ] );
			}

		}

		result = result.slice( 0, 11 );

		result.sort( ( a, b ) => a[ 1 ] < b[ 1 ] );

		result.forEach( ( value, index ) => {
			//Column style
			value.push( 'color: ' + Charts.options.colors[ index ] );
		} );

		return result;

	}

}

Charts.charts = [];

Charts.options = {
	tooltip:           { isHtml: true },
	colors:            [ 'rgb(155,0,0)', 'rgb(195,0,0)', 'rgb(205,21,21)', 'rgb(241,57,57)', 'rgb(255,93,93)', 'rgb(255,129,129)', 'rgb(255,166,166)', 'rgb(255,202,202)', 'rgb(255,239,239)', 'rgb(255,255,255)' ],
	defaultFontFamily: 'Prompt',
	animation:         {
		duration: 500,
		easing:   'in',
	},
	legend:            { position: "none" },
	hAxis:             {
		textPosition: 'none',
		gridlines:    {
			color: 'transparent'
		}
	},
	vAxis:             {
		gridlines: {
			color: 'transparent'
		},
		textStyle: {
			fontSize: 15,
			fontName: 'Prompt',
		}
	}
};

export default Charts;