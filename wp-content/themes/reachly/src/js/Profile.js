import Utils from './Utils';

const $ = jQuery.noConflict();

/**
 * Handles profile related operations
 *
 * @author Przemysław Żydek
 * */
class Profile {

	constructor( $container ) {

		this.$container = $container;

		this.horizontalSection();

		this.$container.find( 'form' ).on( 'submit', e => this.handleSubmit( e ) );

	}

	/**
	 * @return void
	 * */
	handleSubmit( e ) {

		e.preventDefault();

		const Fd    = new FormData( e.currentTarget ),
			  $This = $( e.currentTarget );

		Fd.append( 'action', $This.attr( 'id' ) === 'wpk_register_form' ? 'wpk_register' : 'wpk_user_profile' );

		Utils.ajax( {
			beforeSend: () => {
				Utils.appendLoader( $This, true );
			},
			data:       Fd,
			success:    ( data ) => {

				Utils.handleResponse( data );

				if ( data.redirect_url ) {
					Utils.redirect( data.redirect_url );
				}

			},
			complete:   () => {
				Utils.removeLoader( $This );
			}
		} );

	}

	/**
	 * @return {Profile}
	 * */
	horizontalSection() {

		let $section = this.$container.find( '.wpk-horizontal-sections' );

		Utils.horizontalSections( $section );

		return this;

	}

}

export default Profile;