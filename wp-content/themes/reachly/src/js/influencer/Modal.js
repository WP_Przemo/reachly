import Utils from '../Utils';
import Charts from '../Charts';

let $ = jQuery.noConflict();

/**
 * Handles influencer modal
 *
 * @author Przemysław Żydek
 * */
class Modal {

	constructor() {
		$( document ).on( 'click', '.wpk-influencer', e => this.handleClick( e ) );
	}

	/**
	 * Open modal after clicking on influencer element
	 *
	 * @return void
	 * */
	handleClick( e ) {

		let $target = $( e.target );

		//Don't conflict with element actions
		if ( $target.parents( '.wpk-element-actions' ).length ) {
			return;
		}

		let $this        = $( this ),
			campaignID   = $this.data( 'campaign-id' ),
			influencerID = $this.data( 'influencer-id' );

		Modal.show( influencerID, campaignID );

	}

	/**
	 * Displays modal
	 *
	 * @param {int} influencerID
	 * @param {int|null} campaignID Optional campaign context
	 *
	 * @return void
	 *
	 * */
	static show( influencerID, campaignID = null ) {

		const Fd = new FormData();

		let $modal     = $( '.wpk-modal:not(.wpk-mobile-profile)' ),
			$loaderObj = $modal.length ? $modal : $( '.wpk-wrap' );

		Fd.append( 'influencer_id', influencerID );
		Fd.append( 'campaign_id', campaignID );
		Fd.append( 'action', 'wpk_influencer_modal' );

		Utils.ajax( {
			data: Fd,
			beforeSend() {
				Utils.appendLoader( $loaderObj, true );
			},
			success( data ) {

				Utils.handleResponse( data );

				if ( data.result ) {
					let $data = $( data.result );

					Utils.modal( $data.get( 0 ).outerHTML, {
						full:      true,
						underMenu: true
					} );

					Modal.showInsights();

				}

			},
			complete() {
				Utils.removeLoader( $loaderObj );
			}

		} );


	}

	/**
	 * Show insights graphs in modal
	 *
	 * @return void
	 * */
	static showInsights() {

		let $insights = $( '.wpk-modal' ).find( '.wpk-influencer-insights' );

		Utils.appendLoader( $insights );

		setTimeout( () => {

			$( '.wpk-pie' ).each( function() {
				Charts.createPie( $( this ) );
			} );

			$( '.wpk-bar' ).each( function() {
				Charts.createBar( $( this ) );
			} );


			Utils.removeLoader( $insights );
		} );

	}

}

export default Modal;