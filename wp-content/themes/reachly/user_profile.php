<?php
/**
 * Template Name: User profile
 *
 * @package wpk
 */

get_header();

do_action('wpk/userProfilePage');

get_footer();