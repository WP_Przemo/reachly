<?php

add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_style( 'chld_thm_cfg_child', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', [
		'buro_mikado_default_style',
		'buro_mikado_default_style',
		'buro_mikado_modules_plugins',
		'buro_mikado_modules',
		'mkd_font_awesome',
		'mkd_font_elegant',
		'mkd_ion_icons',
		'mkd_linea_icons',
		'mkd_linear_icons',
		'buro_mikado_blog',
		'buro_mikado_modules_responsive',
		'buro_mikado_blog_responsive',
	] );
} );

require_once 'includes/wpk_constants.php';
require_once 'wpk.php';