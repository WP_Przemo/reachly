<?php


namespace Wpk\Traits;

/**
 * Trait with helper request methods
 */
trait Request {

	/**
	 * Filters request param
	 *
	 * @param mixed $param
	 *
	 * @return array|mixed|string
	 */
	protected static function filter( $param ) {

		if ( is_array( $param ) ) {
			$param = array_filter( $param );
		}

		return stripslashes_deep( $param );

	}

	/**
	 * Helper function for getting post param
	 *
	 * @param      $key
	 * @param null $default
	 *
	 * @return mixed|null
	 */
	public function getPostParam( $key, $default = null ) {

		if ( ! isset( $_POST[ $key ] ) ) {
			return $default;
		}

		return self::filter( $_POST[ $key ] );
	}

	/**
	 * Get file from request
	 *
	 * @param string $key
	 *
	 * @return bool|array
	 */
	public function getFile( $key ) {

		$file = false;

		if ( isset( $_FILES[ $key ] ) ) {

			$file = $_FILES[ $key ];

			if ( $file[ 'error' ] ) {
				$file = false;
			}

		}

		return $file;
	}

	/**
	 * Helper function for getting query param
	 *
	 * @param mixed $key
	 * @param null  $default
	 *
	 * @return mixed|null
	 */
	public function getQueryParam( $key, $default = null ) {

		if ( ! isset( $_GET[ $key ] ) ) {
			return $default;
		}

		return self::filter( $_GET[ $key ] );

	}

	/**
	 * @param mixed $key
	 * @param null  $default
	 *
	 * @return mixed|null
	 */
	public function getCookie( $key, $default = null ) {

		if ( ! isset( $_COOKIE[ $key ] ) ) {
			return $default;
		}

		return self::filter( $_COOKIE[ $key ] );

	}

}