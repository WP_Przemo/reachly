<?php


namespace Wpk\Campaign;

use Wpk\Models\Campaign;
use Wpk\Models\User;

/**
 * Handle grid actions for campaign element
 *
 * @author Przemysław Żydek
 */
class GridActions {

	/**
	 * @param Campaign $campaign
	 * @param User     $user
	 *
	 * @return array
	 */
	public static function campaignElement( Campaign $campaign, User $user ): array {

		$actions = [];


		if ( $campaign->creationStep() > Campaign::DETAILS_STEP ) {
			$actions[] = [
				'link'   => $campaign->getPermalink(),
				'action' => 'view',
				'label'  => __( 'View campaign', 'wpk' ),
			];
		}

		if ( $campaign->post_author == $user->ID ) {

			$action = [
				'action' => 'edit',
			];

			if ( $campaign->fullyCreated() ) {

				/*TODO Get edit url*/
				$action[ 'link' ]  = 'editurl';
				$action[ 'label' ] = __( 'Edit campaign', 'wpk' );

			} else {
				$action[ 'link' ]  = $campaign->getUrl();
				$action[ 'label' ] = __( 'Edit details', 'wpk' );
			}

			$actions[] = $action;

		}

		return $actions;

	}

	/**
	 * Get influencer element action basing on relation between campaign
	 *
	 * TODO
	 *
	 * @param User     $influencer
	 * @param Campaign $campaign
	 *
	 * @return array
	 */
	public static function influencerActions( User $influencer, Campaign $campaign ) {

		$actions = [];

		if ( $campaign->willBeAdded( $influencer ) ) {

			$date = $campaign->hasStarted() ? $campaign->renewalDate() : $campaign->startDate();

			$actions[] = [
				'action'   => 'will_be_added',
				'label'    => sprintf( esc_html__( 'Will be added on %s', 'wpk' ), $date ),
				'disabled' => true,
			];

		} else if ( $campaign->hasDeclined( $influencer ) ) {


			$actions[] = [
				'action'   => 'declined',
				'label'    => esc_html__( 'This influencer has declined invitation', 'wpk' ),
				'disabled' => true,
			];

		} else if ( $campaign->inCampaign( $influencer ) ) {

			if ( $campaign->canBeModified( 'renewed_date' ) ) {

				$actions[] = [
					'action' => 'remove',
					'label'  => __( 'Remove from campaign', 'wpk' ),
				];

			} else {
				$actions[] = [
					'action'   => 'in_campaign',
					'label'    => __( 'In campaign', 'wpk' ),
					'disabled' => true,
				];
			}

		} else {

			if ( $campaign->isInvited( $influencer ) ) {
				$actions[] = [
					'action' => 'uninvite',
					'label'  => __( 'Cancel invite', 'wk' ),
				];
			} else if ( $campaign->haveSendCollaboration( $influencer ) ) {
				$actions[] = [
					'action' => 'accept',
					'label'  => __( 'Accept offer', 'wk' ),
				];

			} else {
				$actions[] = [
					'action' => 'invite',
					'label'  => __( 'Invite', 'wk' ),
				];
			}

			if ( $campaign->isSkipped( $influencer ) ) {
				$actions[] = [
					'action' => 'unskip',
					'label'  => __( 'Unskip', 'wk' ),
				];
			} else {
				$actions[] = [
					'action' => 'skip',
					'label'  => __( 'Skip', 'wpk' ),
				];
			}

		}

		return $actions;

	}

}