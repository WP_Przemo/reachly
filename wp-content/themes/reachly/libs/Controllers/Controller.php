<?php


namespace Wpk\Controllers;

use Wpk\Helpers\Response;
use Wpk\Models\User;
use Wpk\Traits\Request;

/**
 * @author Przemysław Żydek
 */
abstract class Controller {

	use Request;

	/** @var Response */
	protected $response;

	/** @var array Array with middleware classes to use */
	protected $middleware = [];

	/** @var User Stores current user */
	protected $user;

	/**
	 * Controller constructor.
	 */
	public function __construct() {

		$this->loadMiddleware();

		$this->user     = User::current();
		$this->response = new Response();

	}

	/**
	 * Perform load of middleware modules
	 *
	 * @return void
	 */
	protected function loadMiddleware() {

		foreach ( $this->middleware as $key => $middleware ) {
			$this->middleware[ $key ] = new $middleware();
		}

	}

	/**
	 * Get controller middleware
	 *
	 * @param string $middleware
	 *
	 * @return bool|mixed
	 */
	protected function middleware( string $middleware ) {
		return isset( $this->middleware[ $middleware ] ) ? $this->middleware[ $middleware ] : false;
	}

}