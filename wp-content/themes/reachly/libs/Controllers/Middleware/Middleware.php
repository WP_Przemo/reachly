<?php

namespace Wpk\Controllers\Middleware;

use Wpk\Traits\Request;

/**
 * @author Przemysław Żydek
 */
abstract class Middleware {

	use Request;

}