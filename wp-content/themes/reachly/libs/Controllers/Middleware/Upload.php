<?php


namespace Wpk\Controllers\Middleware;

/**
 * Helper upload class
 */
class Upload extends Middleware {

	/**
	 * @return bool
	 */
	public function checkNonce(): bool {
		return check_ajax_referer( 'wpk_file_nonce', 'wpk_file_nonce', false );
	}

	/**
	 * Create attachment from uploaded file
	 *
	 * @param array $file
	 *
	 * @return bool|int
	 */
	public function createAttachment( array $file = [] ) {

		$upload = wp_handle_upload( $file, [ 'test_form' => false ] );

		if ( ! isset( $upload[ 'error' ] ) ) {

			$attachment = [
				'post_mime_type' => $upload[ 'type' ],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $upload[ 'url' ] ) ),
				'post_content'   => '',
				'post_status'    => 'inherit',
				'guid'           => $upload[ 'url' ],
			];

			$attachID = wp_insert_attachment( $attachment, $upload[ 'file' ] );

			if ( is_wp_error( $attachID ) ) {
				return false;
			}

			require_once( ABSPATH . 'wp-admin/includes/image.php' );

			$attachmentData = wp_generate_attachment_metadata( $attachID, $upload[ 'file' ] );

			wp_update_attachment_metadata( $attachID, $attachmentData );

			return $attachID;

		}

		return false;

	}

}