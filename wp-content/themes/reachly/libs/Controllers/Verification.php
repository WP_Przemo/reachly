<?php


namespace Wpk\Controllers;

use Wpk\Pages;
use Wpk\Utility;

/**
 * Handles user verification
 *
 * @author Przemysław Żydek
 */
class Verification extends Controller {

	/**
	 * Verification constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wp', [ $this, 'check' ] );
	}

	/**
	 * Check user verification status on site load
	 *
	 * @return void
	 */
	public function check() {

		if ( is_user_logged_in() ) {

			if ( ! Utility::isAdmin() && ! $this->user->hasValidToken() && Utility::getPageTemplate() !== 'register.php' ) {
				wp_redirect( Pages::getRegisterPageUrl() );
			}

		}

	}

}