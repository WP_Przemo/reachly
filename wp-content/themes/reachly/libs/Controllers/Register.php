<?php


namespace Wpk\Controllers;

use Wpk\Helpers\Response;
use Wpk\Models\Campaign;
use Wpk\Models\User;
use Wpk\Pages;
use Wpk\Traits\Request;

use function Wpk\Core as Corefc;
use Wpk\Utility;

/**
 * Handles register page
 *
 * @author Przemysław Żydek
 */
class Register extends Controller {

	/**
	 * Register constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wpk/registerPage', [ $this, 'handle' ] );
		add_action( 'wp', [ $this, 'redirect' ], 1 );

		add_action( 'wp_ajax_wpk_register', [ $this, 'handleForm' ] );
	}

	/**
	 * Redirect user to register page if he haven't completed the register form
	 *
	 * @return void
	 */
	public function redirect() {

		if ( is_user_logged_in() ) {

			$user = $this->user;

			if ( ! Utility::isAdmin( $user ) && Utility::getPageTemplate() !== 'register.php' && ! $user->meta( 'registered' ) ) {
				wp_redirect( Pages::getRegisterPageUrl() );
			}

		}

	}

	/**
	 * @return void
	 */
	public function handle() {

		if ( ! is_user_logged_in() ) {
			$this->response->setRedirectUrl( home_url() )->redirect();
		}

		$user = $this->user;

		if ( $user->meta( 'registered' ) ) {
			$this->response->render404();

			return;
		}

		$type = $user->meta( 'type' );

		$data = [
			'user' => $user,
			'type' => $user->meta( 'type' ),
		];

		if ( empty( $type ) ) {

			$user->delete();
			$this->response->render404();

			return;
		}

		if ( $user->meta( 'type' ) === 'influencer' ) {
			$data[ 'categories' ] = Campaign::getCategories();
			$data[ 'genders' ]    = User::$genders;
			$data[ 'userGender' ] = $user->meta( 'gender' );
		}

		$this->response->render( 'register.register', $data );

	}

	/**
	 * @return void
	 */
	public function handleForm() {

		Utility::filterPost( $_POST );

		$response = $this->response;

		$response->checkNonce( 'wpk_register', 'wpk_nonce' );
		$response->checkUserLoggedIn();

		$user = $this->user;
		$type = $user->meta( 'type' );

		$requiredFields = [];

		if ( $type === 'company' ) {
			$requiredFields = [
				'email',
				'phone',
				'first_name',
				'last_name',
				'company',
				'city',
				'zip',
				'address',
				'accept_terms',
			];
		} else if ( $type === 'influencer' ) {
			$requiredFields = [
				'email',
				'phone',
				'first_name',
				'last_name',
				'city',
				'birth_date',
				'zip',
				'address',
				'accept_terms',
			];
		} else {
			$response->addError( esc_html__( 'Invalid user type', 'wpk' ), 'popup', true );
		}

		foreach ( $requiredFields as $requiredField ) {

			if ( empty( $this->getPostParam( $requiredField ) ) ) {

				$label = Utility::formatInputKey( $requiredField );
				if ( $label === 'Accept terms' ) {
					$response->addError( sprintf( __( 'You must accept terms', 'wpk' ), $label ), "#{$requiredField}" );
				} else {
					$response->addError( sprintf( __( '%s is required', 'wpk' ), $label ), "#{$requiredField}" );
				}

			}
		}


		if ( $response->hasErrors() ) {
			$response->sendJson();
		}

		if ( $type === 'influencer' ) {
			$birthDate = $this->getPostParam( 'birth_date' );

			try {
				$birthDate = new \DateTime( $birthDate );
			} catch ( \Exception $e ) {
				$response->addError( esc_html__( 'Invalid birth date format', 'wpk' ), '#birth_date', true );
			}

			$today     = (int) date( 'Y' );
			$birthYear = (int) $birthDate->format( 'Y' );
			$age       = $today - $birthYear;

			if ( $age < 18 ) {
				$response->addError( esc_html__( 'You must be at least 18 years old.', 'wpk' ), '#birth_date', true );
			}

		}

		$email = $this->getPostParam( 'email' );

		if ( ! Utility::validateEmail( $email ) ) {
			$response->addError( esc_html__( 'Invalid e-mail format.', 'wpk' ), '#email', true );
		}

		$user = $user->firstName( $this->getPostParam( 'first_name' ) )
		             ->lastName( $this->getPostParam( 'last_name' ) )
		             ->email( $this->getPostParam( 'email' ) )
		             ->update();

		if ( is_wp_error( $user ) ) {
			$response->handleWpError( $user )->sendJson();
		}

		$gender = $this->getPostParam( 'gender' );

		if ( ! empty( $gender ) && ! in_array( $gender, array_keys( User::$genders ) ) ) {
			$response->addError( esc_html__( 'Invalid gender selected', 'wpk' ), 'popup', true );
		}

		$meta = [
			'address'                => $this->getPostParam( 'address' ),
			'city'                   => $this->getPostParam( 'city' ),
			'zip'                    => $this->getPostParam( 'zip' ),
			'phone'                  => $this->getPostParam( 'phone' ),
			'bio'                    => $this->getPostParam( 'bio' ),
			'accept_terms'           => true,
			'registered'             => true,
			//Enable notifications by default
			'notification_email'     => true,
			'notification_dashboard' => true,
		];

		if ( $type === 'influencer' ) {

			$meta[ 'interests' ]  = implode( ',', $this->getPostParam( 'interests' ) );
			$meta[ 'birth_date' ] = $birthDate->format( MYSQL_DATE_FORMAT );
			$meta[ 'age' ]        = $age;
			$meta[ 'gender' ]     = $gender;

		} else if ( $type = 'company' ) {

			$meta[ 'company' ] = $this->getPostParam( 'company' );
			$meta[ 'website' ] = $this->getPostParam( 'website' );

		}

		$user->updateMetas( $meta );

		$response->setRedirectUrl( Pages::getProfileUrl() )->sendJson();

	}

}