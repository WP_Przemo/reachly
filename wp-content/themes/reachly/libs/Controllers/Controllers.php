<?php


namespace Wpk\Controllers;

use Wpk\Controllers\Campaign;
use Wpk\Controllers\CampaignInvitation;
use Wpk\Controllers\Influencer;
use Wpk\Controllers\Login\Login;

/**
 * Stores instances of all controllers
 *
 * @author Przemysław Żydek
 */
class Controllers {

	/** @var array */
	protected static $controllers = [
		CampaignPlan\Acf::class,
		Campaign\Creation::class,
		Campaign\InfluencersActions::class,
		Campaign\Single::class,
		Campaign\AddInfluencers::class,
		Campaign\Publish::class,
		Campaign\Preview::class,
		Campaign\Archive::class,
		CampaignInvitation\Actions::class,
		Influencer\Modal::class,
		Admin\CampaignNotification::class,
		Menu::class,
		Login::class,
		Register::class,
		Profile::class,
		MyCampaigns::class,
		Brands::class,
		Verification::class,
	];

	/**
	 * Controllers constructor.
	 */
	public static function load() {

		foreach ( self::$controllers as $controller ) {
			new $controller();
		}

	}


}