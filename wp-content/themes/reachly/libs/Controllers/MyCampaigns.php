<?php


namespace Wpk\Controllers;

use function Wpk\Core as CoreFc;
use Wpk\Models\Campaign;
use Wpk\Models\User;

/**
 * Handles my campaigns page
 *
 * @author Przemysław Żydek
 */
class MyCampaigns extends Controller {

	/**
	 * MyCampaigns constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wpk/myCampaigns', [ $this, 'view' ] );
	}

	/**
	 * @return void
	 */
	public function view() {

		if ( ! is_user_logged_in() ) {
			$this->response->render404();

			return;
		}

		$user = $this->user;
		$type = $user->meta( 'type' );
		$data = [
			'type' => $type,
			'user' => $user,
		];

		if ( $type === 'company' ) {
			$data = array_merge( $data, $this->getCompanyData( $user ) );
		} else if ( $type === 'influencer' ) {
			$data = [];
		}

		$this->response->render( 'campaign.grid', $data );

	}

	/**
	 * Get campaigns from company
	 *
	 * @param User $user
	 *
	 * @return array
	 */
	protected function getCompanyData( User $user ): array {

		$paged     = $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$campaigns = Campaign::init()
		                     ->author( $user->ID )
		                     ->perPage( 10 )
		                     ->status( 'any' )
		                     ->paged( $paged )
		                     ->get();

		$data = [
			'campaigns' => $campaigns,
			'paged'     => $paged,
			'published' => $campaigns->find( 'post_status', 'publish' ),
			'drafts'    => $campaigns->find( 'post_status', 'draft' ),
			'online'    => $campaigns->find( 'post_status', 'online' ),
			'started'   => $campaigns->find( 'post_status', 'started' ),
			'ended'     => $campaigns->find( 'post_status', 'ended' ),
		];

		return $data;

	}

}