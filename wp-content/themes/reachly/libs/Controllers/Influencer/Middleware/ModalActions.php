<?php


namespace Wpk\Controllers\Influencer\Middleware;

use Wpk\Controllers\Middleware\Middleware;
use Wpk\Models\Campaign;
use Wpk\Models\User;

/**
 * @author Przemysław Żydek
 */
class ModalActions extends Middleware {

	/**
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return array
	 */
	public function get( Campaign $campaign, User $influencer ): array {

		$actions = [];

		if ( $campaign->hasDeclined( $influencer ) ) {

			$actions[] = [
				'action' => 'declined',
				'text'   => esc_html__( 'This influencer has declined invitation', 'wpk' ),
			];

		} else if ( $campaign->willBeAdded( $influencer ) ) {

			$date = $campaign->hasStarted() ? $campaign->renewalDate() : $campaign->startDate();

			$actions[] = [
				'action' => 'will_be_added',
				'text'   => sprintf( esc_html__( 'Will be added on %s', 'wpk' ), $date ),
			];

		} else {

			if ( $campaign->isInvited( $influencer ) ) {
				$actions[] = [
					'action' => 'uninvite',
					'text'   => esc_html__( 'Cancel invitation', 'wpk' ),
				];
			} else if ( $campaign->haveSendCollaboration( $influencer ) ) {
				$actions[] = [
					'action' => 'accept',
					'text'   => esc_html__( 'Accept offer', 'wpk' ),
				];
			} else {

				$actions[] = [
					'action' => 'invite',
					'text'   => esc_html__( 'Invite', 'wpk' ),
				];

			}

			if ( $campaign->isSkipped( $influencer ) ) {

				$actions[] = [
					'action' => 'unskip',
					'text'   => esc_html__( 'Unskip', 'wpk' ),
				];

			} else {

				$actions[] = [
					'action' => 'skip',
					'text'   => esc_html__( 'Skip', 'wpk' ),
				];

			}

			if ( $campaign->isBookmarked( $influencer ) ) {

				$actions[] = [
					'action' => 'unbookmark',
					'text'   => esc_html__( 'Remove bookmark', 'wpk' ),
				];

			} else {

				$actions[] = [
					'action' => 'bookmark',
					'text'   => esc_html__( 'Bookmark', 'wpk' ),
				];

			}

		}

		return $actions;

	}

}