<?php


namespace Wpk\Controllers\Influencer;

use Wpk\Api\Instagram\Api;
use Wpk\Controllers\Controller;
use Wpk\Controllers\Influencer\Middleware\ModalActions;
use Wpk\Api\Helpers\Insight;
use Wpk\Helpers\Chart;
use Wpk\Helpers\Response;
use Wpk\Models\Campaign;
use Wpk\Models\User;
use Wpk\Utility;


use function Wpk\Core;

/**
 * @author Przemysław Żydek
 */
class Modal extends Controller {

	protected $middleware = [
		'modal_actions' => ModalActions::class,
	];

	/**
	 * Modal constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wp_ajax_wpk_influencer_modal', [ $this, 'display' ] );
	}

	/**
	 * Displays influencer modal
	 *
	 * @return void
	 */
	public function display() {

		$response = $this->response;

		$influencerID = (int) $this->getPostParam( 'influencer_id' );
		$campaignID   = (int) $this->getPostParam( 'campaign_id' );

		if ( empty( $influencerID ) ) {
			$response->addMessage( esc_html__( 'Invalid data', 'wpk' ), 'popup', true );
		}

		$influencer = User::find( $influencerID );

		$insights = $influencer->socialMeta( 'instagram', 'insights' );

		$data = [
			'influencer'     => $influencer,
			'interests'      => $influencer->getInterests(),
			'followersCount' => Utility::round( $influencer->socialMeta( 'instagram', 'followers_count' ) ),
			'likesPerPost'   => Utility::round( $influencer->socialMeta( 'instagram', 'likes_per_post' ) ),
			'reach'          => Utility::round( $insights[ 'reach' ]->value ),
			'impressions'    => Utility::round( $influencer->socialMeta( 'instagram', 'impressions' ) ),
			'engagmentRate'  => round( $influencer->socialMeta( 'instagram', 'engagment_rate' ), 2 ),
			'medias'          => array_slice( $influencer->socialMeta( 'instagram', 'media' ), 0, 12 ),
		];

		$this->prepareInsights( $insights, $data );

		if ( ! empty( $campaignID ) ) {

			$campaign = Campaign::find( $campaignID );

			$response->validateAuthor( User::current(), $campaign );

			$data[ 'campaign' ] = $campaign;
			$data[ 'actions' ]  = $this->middleware( 'modal_actions' )->get( $campaign, $influencer );

		}

		$modal = Core()->view->render( 'influencer.modal.modal', $data );

		$response->setResult( $modal )->sendJson();

	}

	/**
	 * Prepare insights and store them into view data
	 *
	 * @param array $insights
	 * @param array $data Passed by reference
	 *
	 * @return void
	 */
	protected function prepareInsights( array $insights, array &$data ) {

		/**
		 * @var Insight $ageRange
		 * @var Insight $audienceCity
		 * @var Insight $audienceCountry
		 */
		$ageRange = $insights[ 'audience_gender_age' ];

		$genderRange = [
			'Men'   => 0,
			'Woman' => 0,
		];
		$ages        = Insight::parseAgeRange( $ageRange->value );

		foreach ( $ageRange->value as $key => $item ) {

			if ( Utility::contains( $key, 'F' ) ) {
				$genderRange[ 'Woman' ] ++;
			} else if ( Utility::contains( $key, 'M' ) ) {
				$genderRange[ 'Men' ] ++;
			}

		}

		$data[ 'ageRange' ]        = $ageRange;
		$data[ 'audienceCity' ]    = json_encode( $insights[ 'audience_city' ]->value );
		$data[ 'audienceCountry' ] = json_encode( $insights[ 'audience_country' ]->value );
		$data[ 'ages' ]            = [
			'all'    => json_encode( $ages[ 'all' ] ),
			'male'   => json_encode( $ages[ 'male' ] ),
			'female' => json_encode( $ages[ 'female' ] ),
		];
		$data[ 'genderRange' ]     = Chart::preparePieValues( $genderRange );

	}

}