<?php


namespace Wpk\Controllers\CampaignInvitation;

use Wpk\Controllers\Controller;
use Wpk\Core;
use Wpk\Models\Campaign;
use Wpk\Models\CampaignInvitation;

/**
 * Handle invitation actions
 *
 * @author Przemysław Żydek
 */
class Actions extends Controller {

	/**
	 * Actions constructor.
	 */
	public function __construct() {
		add_action( 'before_delete_post', [ $this, 'restoreImpressions' ] );
		add_action( 'before_delete_post', [ $this, 'removeInvites' ] );

		add_action( 'wpk/campaign/published', [ $this, 'removeOnPublish' ] );

		parent::__construct();
	}

	/**
	 * Remove all remaining invites when campaign has been published
	 *
	 * @param Campaign $campaign
	 */
	public function removeOnPublish( Campaign $campaign ) {

		$invites = CampaignInvitation::getInvitesAndCollaborations( $campaign );

		/**
		 * @var CampaignInvitation $invite
		 */
		foreach ( $invites->all() as $invite ) {
			$invite->delete();
		}

	}

	/**
	 * On campaign removal delete all invites
	 *
	 * @param int $postID
	 *
	 * @return void
	 */
	public function removeInvites( int $postID ) {

		if ( get_post_type( $postID ) !== Core::CAMPAIGN ) {
			return;
		}

		$campaign     = Campaign::find( $postID );
		$invites      = CampaignInvitation::getInvite( $campaign );
		$collaborates = CampaignInvitation::getCollaboration( $campaign );

		/**
		 * @var CampaignInvitation $invite
		 * @var CampaignInvitation $item
		 */
		foreach ( $invites->all() as $invite ) {
			$invite->delete();
		}

		foreach ( $collaborates->all() as $item ) {
			$item->delete();
		}

	}

	/**
	 * On invitation removal restore impressions to campaign
	 *
	 * @param int $postID
	 *
	 * @return void
	 */
	public function restoreImpressions( int $postID ) {

		if ( get_post_type( $postID ) !== Core::CAMPAIGN_INVITATION ) {
			return;
		}

		$invitation = CampaignInvitation::find( $postID );

		//Invitation is in fact collaboration offer or the invitation has been accepted, no need to restore impressions,
		if ( $invitation->post_author != $invitation->getCampaign()->post_author || $invitation->meta( 'status' ) !== 'pending' ) {
			return;
		}

		$impressions = $invitation->getImpressions();

		$invitation->getCampaign()->addImpressions( $impressions );

	}

}