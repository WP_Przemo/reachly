<?php


namespace Wpk\Controllers;

use Wpk\Core;
use Wpk\Models\Campaign;
use Wpk\Models\Collection;
use Wpk\Models\User;

use function Wpk\Core as CoreFc;

/**
 * Handles brands page
 *
 * @author Przemysław Żydek
 */
class Brands extends Controller {

	/**
	 * Brands constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wpk/brands', [ $this, 'view' ] );
		add_action( 'wp_ajax_wpk_get_brands', [ $this, 'filterBrands' ] );
	}

	/**
	 * @param int    $page
	 * @param string $companyName Optional company name to search for
	 *
	 * @return Collection
	 */
	protected function getCompanies( int $page, string $companyName = '' ): Collection {

		$companies = User::init()
		                 ->hasMetaValue( 'type', 'company' )
		                 ->hasMetaValue( 'registered', true )
		                 ->perPage( 10 )
		                 ->page( $page );

		if ( ! empty( $companyName ) ) {
			$companies->hasMetaValue( 'company', $companyName, 'LIKE' );;
		}

		/** @var User $company */
		return $companies->get()->filter( function ( $company ) {

			$campaigns = Campaign::init()
			                     ->author( $company->ID )
			                     ->status( [
				                     'publish',
				                     'draft',
				                     'started',
			                     ] )->hasMetaValue( 'creation_step', 3, '>' )
			                     ->get();

			return ! $campaigns->empty();

		} );

	}

	/**
	 * @return void
	 */
	public function view() {

		if ( ! is_user_logged_in() ) {
			$this->response->render404();

			return;
		}

		$page = (int) $this->getQueryParam( 'page', 1 );

		$companies = $this->getCompanies( $page );

		$data = [
			'companies'   => $companies,
			'archiveLink' => get_post_type_archive_link( Core::CAMPAIGN ),
			'totalPages'  => $companies->query->max_num_pages,
			'page'        => $page,
		];

		$this->response->render( 'brands.brands', $data );

	}

	/**
	 * @return void
	 */
	public function filterBrands() {

		$search  = $this->getPostParam( 'search' );
		$archive = get_post_type_archive_link( Core::CAMPAIGN );

		$page = (int) $this->getQueryParam( 'page', 1 );

		$this->response->checkNonce( 'wpk_company_filter_autocomplete', 'wpk_nonce' );

		$result = [];

		if ( ! empty( $search ) ) {

			$companies = User::init()
			                 ->hasMetaValue( 'company', $search, 'LIKE' )
			                 ->hasMetaValue( 'type', 'company' )
			                 ->hasMetaValue( 'registered', true )
			                 ->get();


			/** @var User $company */
			foreach ( $companies->all() as $company ) {

				$name = $company->meta( 'company' );

				if ( empty( $name ) ) {
					$name = $company->user_login;
				}

				$campaigns = Campaign::init()
				                     ->author( $company->ID )
				                     ->status( [
					                     'publish',
					                     'draft',
					                     'started',
				                     ] )->hasMetaValue( 'creation_step', 3, '>' )
				                     ->get();

				if ( ! $campaigns->empty() ) {

					$result[] = [
						'id'     => $company->ID,
						'link'   => add_query_arg( 'author', $company->ID, $archive ),
						'name'   => $name,
						'avatar' => $company->getAvatar(),
						'logo'   => $company->getLogo(),
					];

				}

			}

		}

		$companiesForGrid = $this->getCompanies( $page, $search );

		$data = [
			'companies'   => $companiesForGrid,
			'archiveLink' => get_post_type_archive_link( Core::CAMPAIGN ),
			'totalPages'  => $companiesForGrid->query->max_num_pages,
			'page'        => $page,
		];

		$this->response->setResult( [
			'suggestions' => $result,
			'companies'   => CoreFc()->view->render( 'company.grid', $data ),
		] )->sendJson();

	}


}