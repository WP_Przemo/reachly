<?php


namespace Wpk\Controllers\Login\Middleware;

use Wpk\Controllers\Middleware\Middleware;
use Facebook\Authentication\AccessToken;
use Wpk\Api\Instagram\Api;
use Wpk\Helpers\Response;
use Wpk\Models\User;
use Wpk\Pages;

use function Wpk\Core;

/**
 * Middleware for instagram login and register
 *
 * @author Przemysław Żydek
 */
class Instagram extends Middleware {

	/**
	 * @return void
	 */
	public function getAccounts() {

		$response = new Response();

		$token = $this->getCookie( 'wpk_facebook_token' );

		if ( ! empty( $token ) ) {
			$token = new AccessToken( $token );
		}

		//Token from cookie is empty or expired
		if ( empty( $token ) || $token->isExpired() ) {
			$token = $this->getPostParam( 'token' );
			$token = new AccessToken( $token );

			//Get long lived token
			if ( ! $token->isLongLived() ) {
				$token = Api::getLongLivedToken( $token );
			}

		}

		//No token in cookie or post request
		if ( empty( $token ) ) {
			$response->addError( __( 'No token provided', 'wpk' ), 'popup', true );
		};

		setcookie( 'wpk_facebook_token', $token->getValue(), time() + 3600 * 48 );

		$instagramAccounts = Api::getInstagramAccounts( $token );
		$result            = [];

		foreach ( $instagramAccounts as $id ) {
			$result[] = Api::getAccountData( $id, $token );
		}

		if ( empty( $instagramAccounts ) ) {
			$response->addError( __( 'No instagram accounts found', 'wpk' ), 'popup', true );
		}

		$view = Core()->view->render( 'login.ig-accounts', [ 'accounts' => $result ] );

		$response->setResult( [
			'accounts' => $view,
			'token'    => $token->getValue(),
		] );

		$response->sendJson();

	}

	/**
	 * @return void
	 */
	public function loginRegister() {

		$response = new Response();

		$accountID = $this->getPostParam( 'id' );
		$token     = $this->getPostParam( 'token' );

		if ( empty( $accountID ) || empty( $token ) ) {
			$response->addError( esc_html__( 'Invalid parameters', 'wpk' ), 'popup', true );
		}

		$token = new AccessToken( $token );

		if ( ! $token ) {
			$response->addError( esc_html__( 'Invalid token', 'wpk' ), 'popup', true );
		}

		$data = Api::getAccountData( $accountID, $token );

		if ( empty( $data ) ) {
			$response->addError( esc_html__( 'Token validation error', 'wpk' ), 'popup', true );
		}

		$user = User::init()->hasMetaValue( 'instagram_id', $accountID )->get()->first();

		//Register
		if ( empty( $user ) ) {

			/*TODO Set proper type */
			$type = 'influencer';

			$user = User::init();
			$meta = [];

			$meta[ 'facebook_token' ] = $token;
			$meta[ 'social_media' ]   = [ 'instagram' ];
			$meta[ 'type' ]           = $type;
			$meta[ 'instagram_id' ]   = $data[ 'id' ];
			$meta[ 'registered' ]     = false;

			$user = $user
				->addMetas( $meta )
				->login( $data[ 'username' ] )
				->password( wp_generate_password() )
				->create();

			//Probably user didn't provided necessary permissions, delete account and show notice
			if ( ! $user->updateInstagramData( true ) ) {
				$user->delete();

				$response->addError( esc_html__( 'Error while creating account', 'wpk' ), 'popup', true );
			}

			$response->setRedirectUrl( Pages::getRegisterPageUrl() );


		} else {
			$response->setRedirectUrl( home_url() );
		}

		wp_set_auth_cookie( $user->ID, true );

		$response->sendJson();

	}

}