<?php


namespace Wpk\Controllers\Login;

use Wpk\Controllers\Controller;
use Wpk\Controllers\Login\Middleware\Instagram;
use Wpk\Helpers\Response;
use Wpk\Models\User;
use Wpk\Pages;

use function Wpk\Core;

/**
 * @author Przemysław Żydek
 */
class Login extends Controller {

	protected $middleware = [
		'instagram' => Instagram::class,
	];

	/**
	 * Login constructor.
	 */
	public function __construct() {

		parent::__construct();

		add_action( 'wpk/loginPage', [ $this, 'view' ] );
		add_action( 'wp_ajax_nopriv_wpk_get_fb_accounts', [ $this->middleware( 'instagram' ), 'getAccounts' ] );
		add_action( 'wp_ajax_nopriv_wpk_instagram_login', [ $this->middleware( 'instagram' ), 'loginRegister' ] );
		add_action( 'wp_ajax_nopriv_wpk_login', [ $this, 'handleLogin' ] );

		add_filter( 'logout_redirect', [ $this, 'logoutRedirect' ], 10, 3 );

	}

	/**
	 * Handles ajax login
	 *
	 * @return void
	 */
	public function handleLogin() {

		$response = $this->response;

		$response->checkNonce( 'wpk_login', 'wpk_nonce' );

		if ( ! is_user_logged_in() ) {

			$login    = $this->getPostParam( 'login' );
			$password = $this->getPostParam( 'password' );

			if ( empty( $login ) ) {
				$response->addError( esc_html__( 'No login provided', 'wpk' ), '#login', true );
			}

			if ( empty( $password ) ) {
				$response->addError( esc_html__( 'No password provided', 'wpk' ), '#password', true );
			}

			$result = wp_signon( [
				'user_login'    => $login,
				'user_password' => $password,
				'remember'      => true,
			] );

			if ( is_wp_error( $result ) ) {
				$response->handleWpError( $result )->sendJson();
			}
		}

		$response->setResult( true )->setRedirectUrl( home_url() )->sendJson();

	}

	/**
	 * Redirect to our custom login page rather to WordPress one
	 *
	 * @param string   $redirect
	 * @param string   $requestRedirect
	 * @param \WP_User $user
	 *
	 * @return string
	 */
	public function logoutRedirect( string $redirect, string $requestRedirect, \WP_User $user ): string {

		$user = new User( $user );

		if ( in_array( $user->meta( 'type' ), User::TYPES ) ) {
			return add_query_arg( 'logged_out', '1', Pages::getLoginPageUrl() );
		}

		return $redirect;

	}

	/**
	 * @return void
	 */
	public function view() {

		if ( is_user_logged_in() ) {
			$this->response->render404();

			return;
		}

		$this->response->render( 'login.form' );

	}

}