<?php

namespace Wpk\Controllers;

use Wpk\Core;
use Wpk\Helpers\PostType;
use Wpk\Models\CampaignInvitation;
use Wpk\Models\CampaignPlan;

/**
 * Registers new post types
 *
 * @author Przemysław Żydek
 */
class PostTypes {

	/**
	 * @return void
	 */
	public static function register() {

		self::registerCampaign();
		self::registerCampaignPlan();
		self::registerInvitations();

	}

	/**
	 * @return void
	 */
	protected static function registerCampaignPlan() {

		$postType = new PostType( Core::CAMPAIGN_PLAN, [
			'labels'             => [
				'name'               => _x( 'Campaign Plans', 'post type general name', 'wpk' ),
				'singular_name'      => _x( 'Campaign Plan', 'post type singular name', 'wpk' ),
				'menu_name'          => _x( 'Campaign Plans', 'admin menu', 'wpk' ),
				'name_admin_bar'     => _x( 'Campaign Plan', 'add new on admin bar', 'wpk' ),
				'add_new'            => _x( 'Add New', 'Campaign Plan', 'wpk' ),
				'add_new_item'       => __( 'Add New Campaign Plan', 'wpk' ),
				'new_item'           => __( 'New Campaign Plan', 'wpk' ),
				'edit_item'          => __( 'Edit Campaign Plan', 'wpk' ),
				'view_item'          => __( 'View Campaign Plan', 'wpk' ),
				'all_items'          => __( 'All Campaign Plans', 'wpk' ),
				'search_items'       => __( 'Search Campaign Plans', 'wpk' ),
				'parent_item_colon'  => __( 'Parent Campaign Plans:', 'wpk' ),
				'not_found'          => __( 'No Campaign Plans found.', 'wpk' ),
				'not_found_in_trash' => __( 'No Campaign Plans found in Trash.', 'wpk' ),
			],
			'description'        => __( 'Description.', 'wpk' ),
			'public'             => true,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => [ 'slug' => Core::CAMPAIGN_PLAN ],
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => [ 'title', 'editor', 'thumbnail', 'custom-fields' ],
		] );

		$postType->addAdminColumn(
			'impressions',
			'Impressions',
			function ( $postId ) {
				echo CampaignPlan::find( $postId )->meta( 'impressions' );
			} );
	}

	/**
	 * @return void
	 */
	protected static function registerCampaign() {

		$postType = new PostType( Core::CAMPAIGN, [
			'labels'             => [
				'name'               => _x( 'Campaigns', 'post type general name', 'wpk' ),
				'singular_name'      => _x( 'Campaign ', 'post type singular name', 'wpk' ),
				'menu_name'          => _x( 'Campaigns', 'admin menu', 'wpk' ),
				'name_admin_bar'     => _x( 'Campaign ', 'add new on admin bar', 'wpk' ),
				'add_new'            => _x( 'Add New', 'Campaign ', 'wpk' ),
				'add_new_item'       => __( 'Add New Campaign ', 'wpk' ),
				'new_item'           => __( 'New Campaign ', 'wpk' ),
				'edit_item'          => __( 'Edit Campaign ', 'wpk' ),
				'view_item'          => __( 'View Campaign ', 'wpk' ),
				'all_items'          => __( 'All Campaigns', 'wpk' ),
				'search_items'       => __( 'Search Campaigns', 'wpk' ),
				'parent_item_colon'  => __( 'Parent Campaigns:', 'wpk' ),
				'not_found'          => __( 'No Campaigns found.', 'wpk' ),
				'not_found_in_trash' => __( 'No Campaigns found in Trash.', 'wpk' ),
			],
			'description'        => __( 'Description.', 'wpk' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => [ 'slug' => Core::CAMPAIGN ],
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => [ 'title', 'editor', 'thumbnail', 'custom-fields', 'author' ],
		] );

		$postType->addTaxonomy( Core::CAMPAIGN . '_category', [
			'labels' => [
				'hierarchical'      => true,
				'name'              => _x( 'Categories', 'taxonomy general name', 'wpk' ),
				'singular_name'     => _x( 'Category', 'taxonomy singular name', 'wpk' ),
				'search_items'      => __( 'Search Categories', 'wpk' ),
				'all_items'         => __( 'All Categories', 'wpk' ),
				'parent_item'       => __( 'Parent Category', 'wpk' ),
				'parent_item_colon' => __( 'Parent Category:', 'wpk' ),
				'edit_item'         => __( 'Edit Category', 'wpk' ),
				'update_item'       => __( 'Update Category', 'wpk' ),
				'add_new_item'      => __( 'Add New Category', 'wpk' ),
				'new_item_name'     => __( 'New Category Name', 'wpk' ),
				'menu_name'         => __( 'Category', 'wpk' ),
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => [ 'slug' => Core::CAMPAIGN . '_category' ],
			],
		] )->addTaxonomy( Core::CAMPAIGN . '_tags', [
			'labels' => [
				'hierarchical'      => false,
				'name'              => _x( 'Tags', 'taxonomy general name', 'wpk' ),
				'singular_name'     => _x( 'Tag', 'taxonomy singular name', 'wpk' ),
				'search_items'      => __( 'Search Tags', 'wpk' ),
				'all_items'         => __( 'All Tags', 'wpk' ),
				'parent_item'       => __( 'Parent Tag', 'wpk' ),
				'parent_item_colon' => __( 'Parent Tag:', 'wpk' ),
				'edit_item'         => __( 'Edit Tag', 'wpk' ),
				'update_item'       => __( 'Update Tag', 'wpk' ),
				'add_new_item'      => __( 'Add New Tag', 'wpk' ),
				'new_item_name'     => __( 'New Tag Name', 'wpk' ),
				'menu_name'         => __( 'Tag', 'wpk' ),
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => [ 'slug' => Core::CAMPAIGN . '_tags' ],
			],
		] );;

	}

	/**
	 * Register campaign invitation post type
	 *
	 * @return void
	 */
	protected static function registerInvitations() {

		$postType = new PostType( Core::CAMPAIGN_INVITATION, [
			'labels'             => [
				'name'               => _x( 'Campaign invitations', 'post type general name', 'wpk' ),
				'singular_name'      => _x( 'Campaign invitation ', 'post type singular name', 'wpk' ),
				'menu_name'          => _x( 'Campaign invitations', 'admin menu', 'wpk' ),
				'name_admin_bar'     => _x( 'Campaign invitation ', 'add new on admin bar', 'wpk' ),
				'add_new'            => _x( 'Add New', 'Campaign invitation ', 'wpk' ),
				'add_new_item'       => __( 'Add New Campaign invitation ', 'wpk' ),
				'new_item'           => __( 'New Campaign invitation ', 'wpk' ),
				'edit_item'          => __( 'Edit Campaign invitation ', 'wpk' ),
				'view_item'          => __( 'View Campaign invitation ', 'wpk' ),
				'all_items'          => __( 'All Campaign invitations', 'wpk' ),
				'search_items'       => __( 'Search Campaign invitations', 'wpk' ),
				'parent_item_colon'  => __( 'Parent Campaign invitations:', 'wpk' ),
				'not_found'          => __( 'No Campaign invitations found.', 'wpk' ),
				'not_found_in_trash' => __( 'No Campaign invitations found in Trash.', 'wpk' ),
			],
			'description'        => __( 'Description.', 'wpk' ),
			'public'             => true,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => [ 'slug' => Core::CAMPAIGN_INVITATION ],
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => [ 'editor', 'thumbnail', 'custom-fields', 'author' ],
		] );

		$postType
			->addAdminColumn( 'author', esc_html__( 'Author', 'wpk' ), function ( $postID ) {
				$invite = CampaignInvitation::find( $postID );

				echo $invite->getAuthor()->user_login;
			} )
			->addAdminColumn( 'receiver', esc_html__( 'Receiver', 'wpk' ), function ( $postID ) {
				$invite = CampaignInvitation::find( $postID );

				echo $invite->getReceiver()->user_login;
			} )
			->addAdminColumn( 'expires', esc_html__( 'Expires', 'wpk' ), function ( $postID ) {
				echo CampaignInvitation::find( $postID )->expires( true )->format( DATE_FORMAT );
			} );

	}

}