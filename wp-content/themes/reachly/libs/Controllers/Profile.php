<?php


namespace Wpk\Controllers;

use Wpk\Controllers\Middleware\Upload;
use function Wpk\Core as CoreFc;
use Wpk\Helpers\Response;
use Wpk\Models\Campaign;
use Wpk\Models\User;
use Wpk\Traits\Request;
use Wpk\Utility;

/**
 * Handles profile related actions
 *
 * @author Przemysław Żydek
 */
class Profile extends Controller {

	protected $middleware = [
		'upload' => Upload::class,
	];

	/**
	 * Profile constructor.
	 */
	public function __construct() {

		parent::__construct();

		add_action( 'wp_ajax_wpk_user_profile', [ $this, 'handleForm' ] );
		add_action( 'wp_ajax_wpk_avatar_upload', [ $this, 'handleAvatarUpload' ] );
		add_action( 'wp_ajax_wpk_delete_avatar', [ $this, 'deleteAvatar' ] );
		add_action( 'wpk/userProfilePage', [ $this, 'handle' ] );

	}

	/**
	 * @return void
	 */
	public function handleAvatarUpload() {

		$response = $this->response;

		/** @var Upload $upload */
		$upload = $this->middleware( 'upload' );

		if ( ! $upload->checkNonce() ) {
			$response->addError( __( 'Security error', 'wpk' ), 'popup', true );
		}

		$file = $this->getFile( 'file' );
		$user = $this->user;

		if ( $file ) {

			$attachmentID = $upload->createAttachment( $file );
			$url          = wp_get_attachment_url( $attachmentID );

			$user->updateMeta( 'logo', $attachmentID );

			$response->setResult( [ $url ] );

		}

		$response->sendJson();

	}

	/**
	 * @return void
	 */
	public function deleteAvatar() {

		/** @var Upload $upload */
		$upload = $this->middleware( 'upload' );


		if ( ! $upload->checkNonce() ) {
			$this->response->addError( __( 'Security error', 'wpk' ), 'popup', true );
		}

		$user = $this->user;

		wp_delete_attachment( $user->meta( 'logo' ) );

		$user->deleteMeta( 'logo' );

		$this->response->setResult( true )->sendJson();

	}

	/**
	 * @return void
	 */
	public function handle() {

		if ( ! is_user_logged_in() ) {
			wp_redirect( home_url() );
		}

		$user = $this->user;
		$data = [
			'user'    => $user,
			'type'    => $user->meta( 'type' ),
			'sidebar' => 'profile.sidebar',
		];

		if ( $user->meta( 'type' ) === 'influencer' ) {
			$data[ 'categories' ] = Campaign::getCategories();
			$data[ 'interests' ]  = $user->getInterests();
		}

		$this->response->render( 'profile.profile', $data );

	}

	/**
	 * @return void
	 */
	public function handleForm() {

		$response = $this->response;

		$response->checkNonce( 'wpk_profile', 'wpk_nonce' );

		$user           = $this->user;
		$type           = $user->meta( 'type' );
		$requiredFields = [];

		if ( $type === 'company' ) {
			$requiredFields = [
				'email',
				'phone',
				'first_name',
				'last_name',
				'company',
				'city',
				'zip',
				'address',
				'company',
			];
		} else if ( $type === 'influencer' ) {
			$requiredFields = [
				'email',
				'phone',
				'first_name',
				'last_name',
				'city',
				'zip',
				'address',
			];
		}

		//Check if required fields have been filled
		foreach ( $requiredFields as $requiredField ) {

			$value = $this->getPostParam( $requiredField );

			if ( empty( $value ) ) {
				$label = Utility::formatInputKey( $requiredField );
				$response->addError( sprintf( __( '%s is required', 'wpk' ), $label ), "#{$requiredField}" );
			}

		}

		if ( $response->hasErrors() ) {
			$response->sendJson();
		}

		//Validate email
		$email = $this->getPostParam( 'email' );

		if ( ! Utility::validateEmail( $email ) ) {
			$response->addError( __( 'Invalid e-mail', 'wpk' ), '#email', true );
		}

		$email     = $this->getPostParam( 'email' );
		$firstName = $this->getPostParam( 'first_name' );
		$lastName  = $this->getPostParam( 'last_name' );

		//Update basic info
		$user
			->email( $email )
			->firstName( $firstName )
			->lastName( $lastName )
			->update();

		if ( is_wp_error( $user ) ) {
			$response->handleWpError( $user )->sendJson();
		}

		//Update meta
		$meta = [
			'address'                => $this->getPostParam( 'address' ),
			'city'                   => $this->getPostParam( 'city' ),
			'zip'                    => $this->getPostParam( 'zip' ),
			'phone'                  => $this->getPostParam( 'phone' ),
			'bio'                    => $this->getPostParam( 'bio' ),
			'notification_email'     => ! empty( $this->getPostParam( 'notification_email' ) ),
			'notification_dashboard' => ! empty( $this->getPostParam( 'notification_dashboard' ) ),
		];

		if ( $type === 'company' ) {
			$meta[ 'company' ] = $this->getPostParam( 'company' );
		} else if ( $type === 'influencer' ) {
			$meta[ 'interests' ] = $this->getPostParam( 'interests', [] );
		}

		$user->updateMetas( $meta );

		$newPassword = $this->getPostParam( 'new_password' );
		if ( ! empty( $newPassword ) ) {

			$passwordRepeat = $this->getPostParam( 'new_password_repeat' );

			if ( empty( $passwordRepeat ) ) {
				$response->addError( __( 'Retype your password', 'wpk' ), '#new_password_repeat', true );
			}

			if ( md5( $newPassword ) !== md5( $passwordRepeat ) ) {
				$response->addError( __( 'Provided passwords don\'t match.', 'wpk' ), '#new_password_repeat', true );
			}

			$user->password( $newPassword );

		}

		$response
			->addMessage( __( '<i class="material-icons">done</i><span>Saved!</span>', 'wpk' ), 'popup', Response::SUCCESS )
			->sendJson();

	}

}