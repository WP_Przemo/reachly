<?php


namespace Wpk\Controllers\Campaign\Creation;

use Wpk\Controllers\Middleware\Upload;
use Wpk\Helpers\Response;
use Wpk\Models\Campaign;
use Wpk\Models\User;
use Wpk\Utility;

use function Wpk\Core;


class ThirdStep extends Step {

	const STEP = 'third_step';

	protected $middleware = [
		'upload' => Upload::class,
	];

	/**
	 * ThirdStep constructor.
	 */
	public function __construct() {

		parent::__construct();

		add_action( 'wp_ajax_wpk_campaign_thumbnail_upload', [ $this, 'handleThumbUpload' ] );
		add_action( 'wp_ajax_wpk_campaign_logo_upload', [ $this, 'handleLogoUpload' ] );
		add_action( 'wp_ajax_wpk_delete_campaign_logo', [ $this, 'handleLogoRemoval' ] );
		add_action( 'wp_ajax_wpk_delete_campaign_thumbnail', [ $this, 'handleThumbnailRemoval' ] );

	}

	/**
	 * @return void
	 */
	public function handleLogoUpload() {
		$this->handleImageUpload( 'logo' );
	}

	/**
	 * void
	 */
	public function handleThumbUpload() {
		$this->handleImageUpload( 'thumbnail' );
	}

	/**
	 * @return void
	 */
	public function handleLogoRemoval() {
		$this->deleteImage( 'logo' );
	}

	/**
	 * @return void
	 */
	public function handleThumbnailRemoval() {
		$this->deleteImage( 'thumbnail' );
	}

	/**
	 * Handle campaign image upload via ajax
	 *
	 * @param string $metaKey
	 *
	 * @return void
	 */
	protected function handleImageUpload( string $metaKey ) {

		$response   = $this->response;
		$campaignID = (int) $this->getPostParam( 'id' );

		/** @var Upload $upload */
		$upload = $this->middleware( 'upload' );

		if ( empty( $campaignID ) ) {
			$response->addError( 'Campaign not found', 'popup', true );
		}

		$user     = $this->user;
		$campaign = Campaign::find( $campaignID );

		$file = $this->getFile( 'file' );

		if ( ! $upload->checkNonce() || $user->ID != $campaign->post_author ) {
			$response->addError( 'Security error', 'popup', true );
		}

		$attachmentID = $upload->createAttachment( $file );

		if ( $attachmentID ) {

			if ( $metaKey === 'thumbnail' ) {
				$campaign->thumnbail( $attachmentID );
			} else {
				$campaign->updateMeta( $metaKey, $attachmentID );
			}

			$response->setResult( [ wp_get_attachment_url( $attachmentID ) ] );
		}

		$response->sendJson();

	}

	/**
	 * @param string $metaKey
	 *
	 * @return void
	 */
	protected function deleteImage( string $metaKey ) {

		$response   = $this->response;
		$campaignID = (int) $this->getPostParam( 'id' );

		if ( empty( $campaignID ) ) {
			$response->addError( 'Campaign not found', 'popup', true );
		}

		/** @var Upload $upload */
		$upload = $this->middleware( 'upload' );

		$user     = $this->user;
		$campaign = Campaign::find( $campaignID );


		if ( ! $upload->checkNonce() || $user->ID != $campaign->post_author ) {
			$response->addError( 'Security error', 'popup', true );
		}

		if ( $metaKey === 'thumbnail' ) {
			$campaign->deleteThumbnail();
		} else {
			wp_delete_attachment( $campaign->meta( $metaKey ) );

			$campaign->deleteMeta( $metaKey );
		}

		$response->setResult( true )->sendJson();

	}

	/**
	 * @return void
	 */
	public function handle() {

		$response = $this->response;

		$response->checkNonce( 'wpk_campaign_nonce', 'wpk_campaign_nonce' );

		$requiredFields = [ 'title', 'description', 'start_date', 'tags' ];

		foreach ( $requiredFields as $requiredField ) {
			if ( empty( $_POST[ $requiredField ] ) ) {

				$label = Utility::formatInputKey( $requiredField );

				if ( $label === 'Tags' ) {
					$response->addError( sprintf( '%s are required', $label ), '#' . $requiredField );
				} else {
					$response->addError( sprintf( '%s is required', $label ), '#' . $requiredField );
				}
			}
		}

		if ( $response->hasErrors() ) {
			$response->sendJson();
		}

		$campaignID = (int) $this->getPostParam( 'campaign_id' );
		$user       = $this->user;

		if ( empty( $campaignID ) ) {
			$response->addError( __( 'No campaign found', 'wpk' ), 'popup', true );
		}

		$campaign = Campaign::find( $campaignID );

		if ( $user->ID != $campaign->post_author ) {
			$response->addError( __( 'Permission issue', 'wpk' ), 'popup', true );
		}

		if ( $campaign->creationStep() < Campaign::DETAILS_STEP ) {
			$response->setRedirectUrl( $campaign->creationUrl() )->sendJson();
		}

		try {
			$startDate = new \DateTime( $this->getPostParam( 'start_date' ) );
		} catch ( \Exception $e ) {
			$response->addError( __( 'Invalid date format', 'wpk' ), '#start_date', true );
		}

		$sevenDays = Utility::addWorkDays( 7 )->getTimestamp();

		if ( $sevenDays > $startDate->getTimestamp() ) {
			$response->addError( __( 'The campaign should start at least 7 business days from today.', 'wpk' ), '#start_date', true );
		}

		$campaign
			->updateMetas( [
				'items'         => $this->getPostParam( 'items' ),
				'start_date'    => $startDate->format( MYSQL_DATETIME_FORMAT ),
				'renewed_date'  => $startDate->format( MYSQL_DATETIME_FORMAT ),
				'website'       => $this->getPostParam( 'website' ),
				'creation_step' => 4,
			] )->setTerms(
				explode( ',', $this->getPostParam( 'categories' ) ),
				'campaign_category'
			)
			->setTerms(
				explode( ',', $this->getPostParam( 'tags' ) ),
				'campaign_tags'
			)
			->title( $this->getPostParam( 'title' ) )
			->content( $this->getPostParam( 'description' ) )
			->update();

		$response
			->setResult( true )
			->setRedirectUrl( $campaign->creationUrl() )
			->sendJson();

	}

	/**
	 * @return void
	 */
	function view() {

		$campaignID = (int) $this->getQueryParam( 'campaign_id' );
		$user       = $this->user;

		if ( empty( $campaignID ) || ! is_user_logged_in() ) {
			$this->response->render404();

			return;
		}

		$campaign = Campaign::find( $campaignID );

		if ( $campaign->post_author != $user->ID ) {
			$this->response->render404();

			return;
		}

		$data = [];

		$data[ 'campaign' ]     = $campaign;
		$data[ 'sidebar' ]      = 'campaign.creation.steps.third-step-sidebar';
		$data[ 'stepTemplate' ] = 'campaign.creation.steps.third-step';
		$data[ 'user' ]         = $user;
		$data[ 'items' ]        = $campaign->meta( 'items' );
		$data[ 'step' ]         = 3;

		$data[ 'categories' ] = Campaign::getCategories();
		$data[ 'tags' ]       = Campaign::getTags();

		$data[ 'campaignCategories' ] = $campaign->terms( 'campaign_category' );
		$data[ 'campaignTags' ]       = $campaign->terms( 'campaign_tags' );


		$this->response->render( 'campaign.creation.creation', $data );

	}
}