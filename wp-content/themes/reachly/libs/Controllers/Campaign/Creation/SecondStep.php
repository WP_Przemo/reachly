<?php


namespace Wpk\Controllers\Campaign\Creation;

use Wpk\Controllers\Campaign\Middleware\Payment;
use Wpk\Models\Campaign;
use Wpk\Models\User;
use Wpk\Pages;
use Wpk\Utility;

use function Wpk\Core;

class SecondStep extends Step {

	const STEP = 'second_step';

	protected $middleware = [
		'payment' => Payment::class,
	];

	/**
	 * @return void
	 */
	public function handle() {
	}

	/**
	 * @return void
	 */
	function view() {

		$campaignID = (int) $this->getQueryParam( 'campaign_id' );
		$core       = Core();
		$imagesPath = $core->enqueue->getAssetsPath( 'images' );

		if ( empty( $campaignID ) ) {
			wp_redirect( home_url() );
		}

		$campaign = Campaign::find( $campaignID );

		if ( $campaign->creationStep() > 2 ) {
			wp_redirect( $campaign->creationUrl() );
		}

		try {
			$this->middleware( 'payment' )->addCampaignToCart( $campaign );
		} catch ( \Exception $e ) {
			Utility::log( $e->getMessage(), 'CAMPAIGN_ADD_TO_CART_ERROR' );

			return;
		}

		$data                   = [];
		$data[ 'campaign' ]     = $campaign;
		$data[ 'profileUrl' ]   = Pages::getProfileUrl();
		$data[ 'icons' ]        = [
			'paypal_active'  => "$imagesPath/paypal-active.png",
			'paypal_default' => "$imagesPath/paypal-default.png",
			'stripe_default' => "$imagesPath/stripe-default.png",
			'stripe_active'  => "$imagesPath/stripe-active.png",
		];
		$data[ 'loginMessage' ] = apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
		$data[ 'checkoutUrl' ]  = esc_url( wc_get_checkout_url() );
		$data[ 'checkout' ]     = \WC_Checkout::instance();
		$data[ 'user' ]         = User::current();
		$data[ 'sidebar' ]      = 'campaign.creation.steps.first-step-sidebar';
		$data[ 'stepTemplate' ] = 'campaign.checkout';


		$this->response->render( 'campaign.creation.creation', $data );

	}
}