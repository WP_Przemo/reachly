<?php


namespace Wpk\Controllers\Campaign\Creation;

use Wpk\Models\Campaign;
use Wpk\Models\User;
use Wpk\Utility;


/**
 * @author Przemysław Żydek
 */
class FourthStep extends Step {

	protected $middleware = [
		'filter' => \Wpk\Controllers\Campaign\Middleware\Filter::class,
	];

	/**
	 * @return void
	 */
	public function handle() {
		return;
	}

	/**
	 * @return void
	 */
	public function view() {

		$campaignID = $this->getQueryParam( 'campaign_id' );

		if ( empty( $campaignID ) || ! is_user_logged_in() ) {
			$this->response->render404();

			return;
		}

		$campaign = Campaign::find( $campaignID );
		$user     = User::current();

		if ( $campaign->post_author != $user->ID || $campaign->fullyCreated() ) {
			$this->response->render404();

			return;
		}

		if ( $campaign->creationStep() < Campaign::INFLUENCERS_STEP ) {
			$this->response->render404();

			return;
		}

		$plan = $campaign->getPlan();

		$amount = $plan->getImpressions();
		if ( $amount >= 250000 ) {
			$recommend = __( 'around 30', 'wpk' );
		} else if ( $amount >= 350000 ) {
			$recommend = __( 'around 55', 'wpk' );
		} else {
			$recommend = __( 'between 10-15', 'wpk' );
		}

		$page = $this->getQueryParam( 'page', 1 );


		$influencers = User::init()
		                   ->influencers()
		                   ->perPage( 10 )
		                   ->exclude( $campaign->skippedInfluencers() + $campaign->getDeclinedInfluencers() )
		                   ->hasMetaValue( 'registered', '1' )
		                   ->page( $page );

		if ( $this->getPostParam( 'wpk_filter' ) ) {
			$this->middleware( 'filter' )->handle( $influencers, $campaign );
		}

		$data = [
			'campaign'           => $campaign,
			'influencers'        => $influencers->get(),
			'totalPages'         => $influencers->attributes[ 'total_pages' ],
			'step'               => 4,
			'stepTemplate'       => 'campaign.creation.steps.fourth-step',
			'sidebar'            => 'campaign.influencers-filter.filter',
			'ref'                => $this->getQueryParam( 'ref', wp_get_referer() ),
			'order'              => $campaign->wcOrder(),
			'plan'               => $campaign->getPlan(),
			'recommend'          => sprintf( __( 'We recommend %s instagrames', 'wpk' ), $recommend ),
			'initialImpressions' => $campaign->getInitialImpressions(),
			'currentImpressions' => $campaign->getImpressions(),
			'page'               => $page,
			'template'           => Utility::getPageTemplate(),
			'categories'         => Campaign::getCategories(),

		];

		$this->response->render( 'campaign.creation.creation', $data );

	}
}