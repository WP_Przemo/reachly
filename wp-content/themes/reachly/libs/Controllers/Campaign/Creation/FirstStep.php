<?php


namespace Wpk\Controllers\Campaign\Creation;

use Wpk\Helpers\Response;
use Wpk\Models\Campaign;
use Wpk\Models\CampaignPlan;
use Wpk\Models\User;
use Wpk\Pages;
use Wpk\Traits\Request;
use Wpk\Utility;

use function Wpk\Core;

/**
 * Handles first step of campaign creation
 *
 * @author Przemysław Żydek
 */
class FirstStep extends Step {

	const STEP = 'first_step';

	/**
	 * @return void
	 */
	public function view() {

		$data       = [];
		$campaignID = (int) $this->getQueryParam( 'campaign_id' );
		$campaign   = null;

		$data[ 'profileUrl' ] = Pages::getProfileUrl();
		$data[ 'plans' ]      = CampaignPlan::init()->perPage( - 1 )->order( 'title', 'DESC' )->get()->all();

		if ( ! empty( $campaignID ) ) {
			$campaign = Campaign::find( $campaignID );

			if ( $campaign->creationStep() > 2 ) {
				wp_redirect( $campaign->creationUrl() );
			} else {
				$data[ 'campaign' ] = $campaign;
			}

		}

		$data[ 'stepTemplate' ] = 'campaign.creation.steps.first-step';
		$data[ 'step' ]         = 1;
		$data[ 'sidebar' ]      = 'campaign.creation.steps.first-step-sidebar';
		$data[ 'user' ]         = $this->user;

		$this->response->render( 'campaign.creation.creation', $data );

	}

	/**
	 * @return void
	 */
	public function handle() {

		Utility::filterPost( $_POST );

		$response = $this->response;
		$planID   = (int) $this->getPostParam( 'plan_id' );

		if ( empty( $this->getPostParam( 'campaign_length' ) ) ) {
			$response->addError( __( 'Please choose campaign length.', 'wpk' ), 'popup', true );
		}

		if ( empty( $planID ) ) {
			$response->addError( __( 'Unable to determine plan. Please reload page and try again', 'wpk' ), 'popup', true );
		}

		$length = $this->getPostParam( 'campaign_length' );
		$plan   = CampaignPlan::find( $planID );

		$prices = $plan->prices();

		if ( ! isset( $prices[ $length ] ) ) {
			$response->addError( __( 'Unable to determine plan price. Please reload page and try again', 'wpk' ), 'popup', true );
		}

		$price       = $prices[ $length ];
		$impressions = $plan->getImpressions();

		//No edition, create fresh campaign
		if ( empty( $this->getPostParam( 'campaign_id' ) ) ) {

			$campaign = Campaign::init()->title( esc_html__( 'Unnamed campaign', 'wpk' ) )
			         ->status( 'draft' )
			         ->addMetas(
				         [
					         'plan'                => $planID,
					         'price'               => $price,
					         'length'              => $length,
					         'impressions'         => $impressions,
					         'initial_impressions' => $impressions,
					         'creation_step'       => 2,
					         'fully_created'       => false,
					         'social_media'        => 'instagram',
				         ]
			         );

			$campaign = $campaign->create();

		} else {

			$campaignID = (int) $this->getPostParam( 'campaign_id' );
			$campaign   = Campaign::find( $campaignID );

			if ( ! $campaign->fullyCreated() && $campaign->creationStep() <= Campaign::PAYMENT_STEP ) {

				$campaign->updateMetas( [
					'plan'                => $planID,
					'price'               => $price,
					'length'              => $length,
					'impressions'         => $impressions,
					'initial_impressions' => $impressions,
				] );
			}

		}

		if ( ! is_wp_error( $campaign->ID ) ) {
			$response->setRedirectUrl( $campaign->creationUrl() )->setResult( $campaign->ID );

		}

		$response->sendJson();

	}

}