<?php


namespace Wpk\Controllers\Campaign\Creation;

use Wpk\Controllers\Controller;

/**
 * Template class used for campaign creation steps
 *
 * @author Przemysław Żydek
 */
abstract class Step extends Controller {

	/** @var string */
	const STEP = '';

	/**
	 * Step constructor.
	 */
	public function __construct() {
		add_action( sprintf( 'wp_ajax_wpk_campaign_creation_%s', static::STEP ), [ $this, 'handle' ] );

		parent::__construct();
	}

	/**
	 * @return void
	 */
	abstract public function handle();

	/**
	 * @return void
	 */
	abstract function view();

}