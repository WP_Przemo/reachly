<?php


namespace Wpk\Controllers\Campaign;

use Wpk\Controllers\Controller;
use function Wpk\Core;
use Wpk\Models\Campaign;

/**
 * @author Przemysław Żydek
 */
class Preview extends Controller {

	/**
	 * Preview constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wp_ajax_wpk_campaign_preview', [ $this, 'handle' ] );
	}

	/**
	 * Handles campaign preview
	 *
	 * @return void
	 */
	public function handle() {

		$response = $this->response;

		$campaignID = (int) $this->getPostParam( 'campaign_id' );
		$user       = $this->user;

		if ( empty( $campaignID ) ) {
			$response->addError( esc_html__( 'No campaign provided', 'wpk' ), 'popup', true );
		}

		$campaign = Campaign::find( $campaignID );

		$response->validateAuthor( $user, $campaign );

		$startDate = $this->getPostParam( 'start_date' );

		try {
			$startDate = new \DateTime( $startDate );
		} catch ( \Exception $e ) {
			$startDate = new \DateTime();
		}

		$length = $campaign->getLength();

		$endDate = clone $startDate;
		$endDate->modify( "$length months" );

		$data = [
			'isPreview'     => true,
			'campaign'      => $campaign,
			'isAuthor'      => true,
			'canBeRenewed'  => false,
			'canBeModified' => true,
			'hasEnded'      => false,
			'hasStarted'    => false,
			'length'        => $campaign->getLength(),
			'startDate'     => $startDate,
			'endDate'       => $endDate,
			'renewedDate'   => $startDate,
			'userType'      => $user->meta( 'type' ),
			'logo'          => $campaign->getLogoUrl(),
			'thumbnail'     => $campaign->getThumbnailUrl(),
			'website'       => $this->getPostParam( 'website' ),
			'items'         => (array) $this->getPostParam( 'items', [] ),
			'user'          => $user,
			'title'         => $this->getPostParam( 'title' ),
			'content'       => apply_filters( 'the_content', $this->getPostParam( 'description' ) ),
		];

		$html = Core()->view->render( 'campaign.single.single', $data );

		$response->setResult( $html )->sendJson();

	}

}