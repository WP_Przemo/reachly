<?php


namespace Wpk\Controllers\Campaign;

use Wpk\Controllers\Campaign\Middleware\SingleActions;
use Wpk\Controllers\Controller;
use Wpk\Core;
use Wpk\Models\Campaign;
use Wpk\Models\User;

use function Wpk\Core as CoreFc;

/**
 * Handles campaign single view
 */
class Single extends Controller {

	/**
	 * Single constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wpk/singleCampaign', [ $this, 'view' ] );
		add_action( 'pre_get_posts', [ $this, 'displayDrafts' ] );
	}

	/**
	 * @param int $campaignID
	 *
	 * @return void
	 */
	public function view( int $campaignID ) {

		if ( ! is_user_logged_in() ) {
			echo CoreFc()->view->render( '404' );

			return;
		}

		$campaign = Campaign::find( $campaignID );
		$user     = $this->user;

		$data = [
			'isPreview'     => false,
			'campaign'      => $campaign,
			'hasEnded'      => $campaign->hasEnded(),
			'hasStarted'    => $campaign->hasStarted(),
			'isAuthor'      => $campaign->post_author == $user->ID,
			'canBeRenewed'  => $campaign->canBeRenewed(),
			'canBeModified' => $campaign->canBeModified( 'renewed_date' ),
			'categories'    => $campaign->terms( 'campaign_category' ),
			'tags'          => $campaign->terms( 'campaign_tags' ),
			'length'        => $campaign->getLength(),
			'startDate'     => $campaign->startDate( true ),
			'endDate'       => $campaign->endDate( true ),
			'renewedDate'   => $campaign->renewedDate( true ),
			'userType'      => $user->meta( 'type' ),
			'nextPost'      => get_next_post(),
			'prevPost'      => get_previous_post(),
			'logo'          => $campaign->getLogoUrl(),
			'thumbnail'     => $campaign->getThumbnailUrl(),
			'website'       => $campaign->meta( 'website' ),
			'items'         => (array) $campaign->meta( 'items' ),
			'user'          => $user,
			'title'         => $campaign->post_title,
			'content'       => apply_filters( 'the_content', $campaign->post_content ),
		];

		if ( $user->meta( 'type' ) === 'influencer' ) {
			$data[ 'actions' ] = $this->getActions( $campaign, $user );
		}

		$this->response->render( 'campaign.single.single', $data );

	}

	/**
	 * Also display drafts
	 *
	 * @param \WP_Query $query
	 *
	 * @return void
	 */
	public function displayDrafts( \WP_Query $query ) {

		$type = $query->get( 'post_type' );

		if ( is_single() && $type === Core::CAMPAIGN ) {
			$query->set( 'post_status', [ 'publish', 'draft', 'started', 'stopped' ] );
		}

	}

	/**
	 * Get actions for single campaign
	 *
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return array
	 */
	public function getActions( Campaign $campaign, User $influencer ): array {

		$actions = [];

		//No actions for skipped influencers
		if ( $campaign->isSkipped( $influencer ) ) {
			return $actions;
		}

		if ( $campaign->hasDeclined( $influencer ) ) {

			$actions[] = [
				'action'   => 'declined',
				'text'     => esc_html__( 'You have declined invitation to this campaign', 'wpk' ),
				'disabled' => true,
			];

			return $actions;

		}

		if ( $campaign->haveSendCollaboration( $influencer ) ) {
			$actions[] = [
				'action'   => 'collaboration_sent',
				'text'     => esc_html__( 'Collaboration offer sent', 'wpk' ),
				'disabled' => true,
			];

			return $actions;
		}

		if ( $campaign->isInvited( $influencer ) ) {

			$actions[] = [
				'action'  => 'influencer_accept',
				'text'    => esc_html__( 'Accept invite', 'wpk' ),
				'tooltip' => sprintf( esc_html__( 'You will take part in this campaign from %s', 'wpk' ), $campaign->renewedDate() ),
			];

			$actions[] = [
				'action' => 'influencer_decline',
				'text'   => esc_html__( 'Decline invite', 'wpk' ),
			];


		} else if ( $campaign->haveInfluencer( $influencer ) ) {

			$actions[] = [
				'action'   => 'in_campaign',
				'text'     => sprintf( esc_html__( 'In campaign from %s', 'wpk' ), $campaign->renewedDate() ),
				'disabled' => true,
			];

		} else if ( $campaign->willBeAdded( $influencer ) ) {

			$date = $campaign->hasStarted() ? $campaign->renewalDate() : $campaign->startDate();

			$actions[] = [
				'action'   => 'in_campaign',
				'text'     => sprintf( esc_html__( 'You will take part in campaign from %s', 'wpk' ), $date ),
				'disabled' => true,
			];

		} else if ( $campaign->canBeModified() || $campaign->canBeModified( 'renewed_date' ) ) {

			$actions[] = [
				'action'  => 'collaborate',
				'text'    => esc_html__( 'Collaborate', 'wpk' ),
				'tooltip' => sprintf( esc_html__( 'You will take part in this campaign from %s', 'wpk' ), $campaign->renewedDate() ),
			];

		}

		return $actions;

	}

}