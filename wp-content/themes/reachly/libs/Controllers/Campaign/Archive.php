<?php


namespace Wpk\Controllers\Campaign;

use Wpk\Controllers\Controller;
use Wpk\Models\Campaign;
use Wpk\Models\User;

/**
 * @author Przemysław Żydek
 */
class Archive extends Controller {

	/**
	 * Archive constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wpk/campaignsArchive', [ $this, 'view' ] );
	}

	/**
	 * @return void
	 */
	public function view() {

		$response = $this->response;
		$user     = $this->user;

		if ( ! is_user_logged_in() || $user->meta( 'type' ) !== 'influencer' ) {
			$response->render404();

			return;
		}

		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

		$campaigns = Campaign::init()
		                     ->status( [ 'draft', 'publish', 'started' ] )
		                     ->hasMetaValue( 'creation_step', 3, '>=' )
		                     ->perPage( 10 )
		                     ->paged( $paged );

		$author = (int) $this->getQueryParam( 'author' );

		if ( ! empty( $author ) ) {
			$campaigns->author( $author );
		}

		$campaigns = $campaigns->get();

		$data = [
			'campaigns'  => $campaigns,
			'totalPages' => $campaigns->query->max_num_pages,
			'user'       => $user,
			'author'     => empty( $author ) ? false : User::find( $author ),

		];

		$response->render( 'campaign.archive', $data );

	}

}