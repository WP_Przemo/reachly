<?php


namespace Wpk\Controllers\Campaign;

use Wpk\Controllers\Controller;
use Wpk\Helpers\Response;
use Wpk\Models\Campaign;
use Wpk\Models\User;
use Wpk\Pages;
use Wpk\Utility;

/**
 * @author Przemysław Żydek
 */
class Publish extends Controller {

	/**
	 * Publish constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wp_ajax_wpk_publish_campaign', [ $this, 'handle' ] );
	}

	/**
	 * @return void
	 */
	public function handle() {

		$campaignID = (int) $this->getPostParam( 'campaign_id' );
		$response   = $this->response;

		$response->checkNonce( 'wpk_publish_campaign', 'wpk_nonce' );

		if ( empty( $campaignID ) ) {
			$response->addError( esc_html__( 'No campaign provided', 'wpk' ), 'popup', true );
		}

		$campaign = Campaign::find( $campaignID );

		$response->validateAuthor( User::current(), $campaign );

		$startDate = $campaign->startDate( true )->getTimestamp();

		$sevenDays = Utility::addWorkDays( 7 )->getTimestamp();

		if ( $sevenDays >= $startDate ) {
			$response->addError( __( 'The campaign should start at least 7 business days from today.', 'wpk' ), 'popup' )
			         ->setRedirectUrl( Pages::getCampaignCreationUrl( Campaign::DETAILS_STEP, $campaignID ) )
			         ->sendJson();
		}

		if ( ! $campaign->haveReachedImpressions() ) {
			$response->addError( esc_html__( 'Your campaign have not reached required impressions', 'wpk' ), 'popup', true );
		}

		$campaign->updateMeta( 'fully_created', true )->status( 'publish' )->update();

		do_action( 'wpk/campaign/published', $campaign );

		$response->setRedirectUrl( $campaign->getPermalink() )->sendJson();


	}

}