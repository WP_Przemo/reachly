<?php


namespace Wpk\Controllers\Campaign\Middleware;

use Wpk\Helpers\Response;
use Wpk\Models\Campaign;
use Wpk\Models\User;

/**
 * Handles influencers bookmarking
 *
 * @author Przemysław Żydek
 */
class Bookmark {

	/**
	 * @param Response $response
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return void
	 */
	public function handle( Response $response, Campaign $campaign, User $influencer ) {

		$response->validateAuthor( User::current(), $campaign );

		$campaign->bookmark( $influencer );

		$response
			->addMessage( sprintf( __( 'Bookmarked <strong>%s</strong>!', 'wpk' ), $influencer->campaignSocialMeta( $campaign, 'username' ) ), 'popup', Response::SUCCESS )
			->sendJson();

	}

	/**
	 * @param Response $response
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return void
	 */
	public function undo( Response $response, Campaign $campaign, User $influencer ) {

		$response->validateAuthor( User::current(), $campaign );

		$campaign->removeBookmark( $influencer );

		$response
			->addMessage( sprintf( __( 'Removed bookmark for <strong>%s</strong>!', 'wpk' ), $influencer->campaignSocialMeta( $campaign, 'username' ) ), 'popup', Response::SUCCESS )
			->sendJson();

	}

}