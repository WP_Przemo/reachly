<?php


namespace Wpk\Controllers\Campaign\Middleware;

use Wpk\Controllers\Middleware\Middleware;
use function Wpk\Core;
use Wpk\Helpers\Response;
use Wpk\Models\Campaign;
use Wpk\Models\CampaignInvitation;
use Wpk\Models\User;

/**
 * Middleware for invitation actions
 *
 * @author Przemysław Żydek
 */
class Invite extends Middleware {

	/**
	 * @param Response $response
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return void
	 */
	public function handle( Response $response, Campaign $campaign, User $influencer ) {

		$influencerImpressions = $influencer->getImpressions( $campaign );

		$response->validateAuthor( User::current(), $campaign );

		if ( $campaign->isInvited( $influencer ) ) {
			$response->addError( esc_html__( 'This influencer is already invited', 'wpk' ), 'popup', true );
		}

		//Company wants to interact with influencer, but campaign cannot be renewed yet
		if ( $campaign->haveInfluencer( $influencer ) && ! $campaign->canBeModified( 'renewed_date' ) || $campaign->willBeAdded( $influencer ) ) {
			$response->addError( esc_html__( 'You can\'t do that because this influencer is already in this campaign.', 'wpk' ), 'popup', true );
		}

		//There are less than seven days before start date
		if ( ! $campaign->canBeModified( 'start_date' ) ) {
			$response->addError( esc_html__( 'You can\'t do that', 'wpk' ), 'popup', true );
		}

		//Influencer has previously declined invitation to this campaign
		if ( $campaign->hasDeclined( $influencer ) ) {
			$response->addError( esc_html__( 'You can\'t do that because this influencer has declined invitation.', 'wpk' ), 'popup', true );
		}

		//Influencer have already sent collaboration offer
		if ( $campaign->haveSendCollaboration( $influencer ) ) {
			$response->addError( esc_html__( 'This influencer has sent collaboration offer.', 'wpk' ), 'popup', true );
		}

		//Company cant afford this influencer
		if ( $influencerImpressions > $campaign->getImpressions() ) {
			$response->addError( esc_html__( 'You don\'t have enough impressions to invite this influencer', 'wpk' ), 'popup', true );
		}

		//If influencer is skipped, unskip him first
		$campaign->unskipInfluencer( $influencer );

		//Woohoo, passed validation, let's create the invite!
		$result = CampaignInvitation::createInvite( $campaign, $influencer );

		if ( ! $result ) {
			$response->addError( esc_html__( 'Error while creating invitation', 'wpk' ), 'popup', true );
		}

		$username = $influencer->campaignSocialMeta( $campaign, 'username' );

		$response
			//Send current campaign impressions in response
			->setResult( $campaign->getImpressions() )
			->addMessage(
				sprintf( __( 'Successfully invited <strong>%s</strong>!' ),
					$username ),
				'popup',
				Response::SUCCESS )
			->sendJson();

	}

	/**
	 * @param Response $response
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return void
	 */
	public function cancel( Response $response, Campaign $campaign, User $influencer ) {

		$response->validateAuthor( User::current(), $campaign );

		/** @var CampaignInvitation $invite */
		$invite = CampaignInvitation::getInvite( $campaign, $influencer )->first();

		if ( $invite ) {

			$inviteImpressions   = $invite->getImpressions();
			$campaignImpressions = $campaign->getImpressions();

			//Set current campaign impressions as response result
			$response->setResult( $inviteImpressions + $campaignImpressions );

			$invite->delete();

			$response->addMessage(
				sprintf( __( 'Canceled invitation for <strong>%s</strong>', 'wpk' ), $influencer->campaignSocialMeta( $campaign, 'username' ) ),
				'popup',
				Response::SUCCESS
			);
		}

	}

	/**
	 * Handles invitation acceptation
	 *
	 * @param Response $response
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return void
	 */
	public function influencerAccept( Response $response, Campaign $campaign, User $influencer ) {

		/** @var CampaignInvitation $invite */
		$invite = CampaignInvitation::getInvite( $campaign, $influencer )->first();

		if ( empty( $invite ) ) {
			$response->addError( esc_html__( 'No invitation found', 'wpk' ), 'popup', true );
		}

		$result = $invite->accept();

		if ( ! $result ) {
			$response->addError( esc_html__( 'Error while accepting invitation', 'wpk' ), 'popup', true );
		}

		$html = Core()->view->render( 'campaign-invitation.invite-accepted' );

		$response->setResult( $html );

	}

	/**
	 * @param Response $response
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return void
	 */
	public function influencerDecline( Response $response, Campaign $campaign, User $influencer ) {

		/** @var CampaignInvitation $invite */
		$invite = CampaignInvitation::getInvite( $campaign, $influencer )->first();

		if ( empty( $invite ) ) {
			$response->addError( esc_html__( 'No invitation found', 'wpk' ), 'popup', true );
		}

		$result = $invite->decline();

		if ( ! $result ) {
			$response->addError( esc_html__( 'Error while declining invitation', 'wpk' ), 'popup', true );
		}

		$html = Core()->view->render( 'campaign-invitation.invite-declined' );

		$response->setResult( $html );

	}

}