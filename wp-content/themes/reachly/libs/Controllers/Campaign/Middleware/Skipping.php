<?php


namespace Wpk\Controllers\Campaign\Middleware;

use Wpk\Controllers\Middleware\Middleware;
use Wpk\Helpers\Response;
use Wpk\Models\Campaign;
use Wpk\Models\CampaignInvitation;
use Wpk\Models\User;

/**
 * Middleware for skipping influencers in campaign
 *
 * @author Przemysław Żydek
 */
class Skipping extends Middleware {

	/**
	 * @param Response $response
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return void
	 */
	public function cancel( Response $response, Campaign $campaign, User $influencer ) {

		$response->validateAuthor( User::current(), $campaign );

		if ( $campaign->unskipInfluencer( $influencer ) ) {

			$response->addMessage(
				sprintf( __( '<strong>%s</strong> is no longer skipped', 'wpk' ), $influencer->campaignSocialMeta( $campaign, 'username' ) ),
				'popup',
				Response::SUCCESS
			);

		}

		$response->sendJson();

	}

	/**
	 * @param Response $response
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return void
	 */
	public function handle( Response $response, Campaign $campaign, User $influencer ) {

		$response->validateAuthor( User::current(), $campaign );

		//Company wants to interact with influencer, but campaign cannot be renewed yet
		if ( $campaign->haveInfluencer( $influencer ) && ! $campaign->canBeModified( 'renewed_date' ) ) {
			$response->addError( esc_html__( 'You can\'t do that because this influencer is already in this campaign.', 'wpk' ), 'popup', true );
		}

		if ( $campaign->skipInfluencer( $influencer ) ) {

			/** @var CampaignInvitation $invite */
			$invite = CampaignInvitation::getInvite( $campaign, $influencer )->first();

			if ( $invite ) {

				$inviteImpressions   = $invite->getImpressions();
				$campaignImpressions = $campaign->getImpressions();

				//Set current campaign impressions as response result
				$response->setResult( $inviteImpressions + $campaignImpressions );

				$invite->delete();
			}

			$response->addMessage(
				sprintf( __( 'Skipped <strong>%s</strong>', 'wpk' ), $influencer->campaignSocialMeta( $campaign, 'username' ) ),
				'popup',
				Response::SUCCESS
			);

		}

		$response->sendJson();

	}

}