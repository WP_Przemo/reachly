<?php


namespace Wpk\Controllers\Campaign\Middleware;

use Wpk\Controllers\Middleware\Middleware;
use Wpk\Models\Campaign;
use Wpk\Models\CampaignInvitation;
use Wpk\Models\User;

/**
 * Handles influencers filter
 *
 * @author Przemysław Żydek
 */
class Filter extends Middleware {

	/**
	 * Handle filtering
	 *
	 * @param User     $influencers User model to filter through
	 * @param Campaign $campaign
	 *
	 * @return void
	 */
	public function handle( User $influencers, Campaign $campaign ) {

		$interests = $this->getPostParam( 'interests', [] );

		if ( ! empty( $interests ) ) {
			$this->interests( $interests, $influencers );
		}

		$address = $this->getPostParam( 'address' );

		if ( ! empty( $address ) ) {
			$this->address( $address, $influencers );
		}

		$age = $this->getPostParam( 'age' );

		if ( ! empty( $age ) ) {
			$this->age( $age, $influencers );
		}

		$gender = $this->getPostParam( 'gender' );

		if ( ! empty( $gender ) ) {
			$this->gender( $gender, $influencers );
		}

		$statuses = $this->getPostParam( 'statuses' );

		if ( ! empty( $statuses ) ) {
			$this->statuses( $statuses, $influencers, $campaign );
		}


	}

	/**
	 *
	 * @param  array $interests
	 * @param User   $influencers
	 *
	 * @return void
	 */
	protected function interests( array $interests, User $influencers ) {

		$interests = array_map( function ( $item ) {
			return [ $item, 'LIKE' ];
		}, $interests );

		$influencers->hasMetaValueArray( $interests, 'interests', 'AND' );

	}

	/**
	 * @param string $address
	 * @param User   $influencers
	 *
	 * @return void
	 */
	protected function address( string $address, User $influencers ) {

		$influencers->hasMetaRelation( [
			'zip'     => [ $address, 'LIKE' ],
			'city'    => [ $address, 'LIKE' ],
			'address' => [ $address, 'LIKE' ],
		] );

	}

	/**
	 * @param array $ages
	 * @param User  $influencers
	 *
	 * @return void
	 */
	protected function age( array $ages, User $influencers ) {

		$ages[ 0 ] = [ (int) $ages[ 0 ], '>' ];
		$ages[ 1 ] = [ (int) $ages[ 1 ], '<' ];

		$influencers->hasMetaValueArray( $ages, 'age', 'AND' );

	}

	/**
	 * @param string $gender
	 * @param User   $influencers
	 *
	 * @return void
	 */
	protected function gender( string $gender, User $influencers ) {

		$influencers->hasMetaValue( 'gender', $gender );

	}

	/**
	 * @param array    $statuses
	 * @param User     $influencers
	 * @param Campaign $campaign
	 *
	 * @return void
	 */
	protected function statuses( array $statuses, User $influencers, Campaign $campaign ) {

		//Show skipped influencers
		if ( in_array( 'skipped', $statuses ) ) {

			$skipped = $campaign->skippedInfluencers();

			//Remove the skipped influencers from "exclude" list, so that they are properly displayed
			$excluded = &$influencers->attributes[ 'exclude' ];

			foreach ( $skipped as $id ) {
				$index = array_search( $id, $excluded );

				unset( $excluded[ $index ] );
			}


			$influencers->include( $skipped );

		}

		$gatherInvitesInfluencersIds = function () use ( $campaign, $influencers ) {

			$inviteInfluencerIds = [];
			$invites             = CampaignInvitation::getInvite( $campaign );

			if ( ! $invites->empty() ) {

				/** @var CampaignInvitation $invite */
				foreach ( $invites->all() as $invite ) {
					$inviteInfluencerIds[] = $invite->getReceiver()->ID;
				}

			}

			return $inviteInfluencerIds;

		};

		//Show influencers invited by company
		if ( in_array( 'invited_by_company', $statuses ) ) {

			$inviteInfluencerIds = $gatherInvitesInfluencersIds();

			$influencers->include( $inviteInfluencerIds );


		}

		//Show bookmarked influencers
		if ( in_array( 'bookmarked', $statuses ) ) {

			$bookmarked = $campaign->getBookmarked();

			$influencers->include( $bookmarked );

		}

		//Show influencers that have accepted invitation
		if ( in_array( 'accepted_by_influencer', $statuses ) ) {

			$acceptedByInfluencer = ( $campaign->getInfluencers( 'influencer' ) + $campaign->getToAddInfluencers( 'influencer' ) );

			$influencers->include( array_keys( $acceptedByInfluencer ) );

		}

		//Show influencers that have declined invitation
		if ( in_array( 'declined_influencers', $statuses ) ) {

			$declined = $campaign->getDeclinedInfluencers();


			//Remove the declined influencers from "exclude" list, so that they are properly displayed
			$excluded = &$influencers->attributes[ 'exclude' ];

			foreach ( $declined as $id ) {
				$index = array_search( $id, $excluded );

				unset( $excluded[ $index ] );
			}

			$influencers->include( $declined );

		}

		//This is tough one
		if ( in_array( 'new', $statuses ) ) {

			if ( ! isset( $skipped ) ) {
				$skipped = $campaign->skippedInfluencers();
			}

			if ( ! isset( $inviteInfluencerIds ) ) {
				$inviteInfluencerIds = $gatherInvitesInfluencersIds();
			}

			if ( ! isset( $bookmarked ) ) {
				$bookmarked = $campaign->getBookmarked();
			}

			if ( ! isset( $acceptedByInfluencer ) ) {
				$acceptedByInfluencer = array_keys( $campaign->getInfluencers( 'influencer' ) + $campaign->getToAddInfluencers( 'influencer' ) );
			}

			$exclude = array_merge( $skipped, $inviteInfluencerIds, $bookmarked, $acceptedByInfluencer );

			$influencers->exclude( $exclude );

		} else if ( empty( $influencers->attributes[ 'include' ] ) && empty( $influencers->attributes[ 'exclude' ] ) ) {

			//Add dummy meta to display empty results, since there are no influencers matching the cryteria
			$influencers->hasMetaValue( 'wpk_dummy', true );

		}


	}

}