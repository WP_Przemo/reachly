<?php


namespace Wpk\Controllers\Campaign\Middleware;

use Wpk\Controllers\Middleware\Middleware;
use Wpk\Models\Campaign;
use Wpk\Models\User;
use Wpk\Pages;

/**
 * Handles Campaign Payment
 */
class Payment extends Middleware {

	/**@var int|bool ID of template product used for purschases */
	public static $productID;

	/**
	 * Campaign_Payment constructor.
	 */
	public function __construct() {

		self::$productID = $this->getTemplateProduct();
		$this->setupHooks();

	}

	/**
	 * Setup class hooks
	 */
	protected function setupHooks() {

		add_action( 'woocommerce_before_calculate_totals', [ $this, 'setCampaignPrice' ] );
		add_action( 'woocommerce_checkout_create_order_line_item', [ $this, 'orderCreated' ], 10, 4 );
		add_action( 'woocommerce_new_order', [ $this, 'assignCampaignToOrder' ] );
		add_action( 'woocommerce_after_checkout_validation', [ $this, 'validateCheckout' ], 10, 2 );
		add_action( 'wp_ajax_wpk_campaign_renew_payment', [ $this, 'renewPayment' ] );
		add_action( 'init', [ $this, 'redirect' ] );

		add_filter( 'woocommerce_checkout_fields', [ $this, 'wcCheckoutFields' ], 99 );


	}

	/**
	 * Performs redirect to campaign after purschase
	 *
	 * @return void
	 */
	public function redirect() {

		$key = $this->getQueryParam( 'key' );

		if ( ! empty( $key ) ) {

			$orderID = wc_get_order_id_by_order_key( $_GET[ 'key' ] );
			$order   = wc_get_order( $orderID );

			$campaignsPage = Pages::getMyCampaignsUrl();

			if ( $order->get_item_count() > 1 ) {
				wp_redirect( $campaignsPage );

				//For some reason the redirect doesn't work without "dying" :/
				die();
			} else {

				$campaignID = array_shift( $order->get_meta( '_wpk_campaign_ids', true ) );
				$campaign   = Campaign::find( $campaignID );

				wp_redirect( $campaign->creationUrl() );

				//Same
				die();
			}
		}

	}

	/**
	 * Validate WC Checkout, checks if there is already order for this campaign
	 *
	 * @param array     $data
	 * @param \WP_Error $errors
	 */
	public function validateCheckout( array $data, $errors ) {

		$cart = WC()->cart->get_cart();

		foreach ( $cart as $item_key => $item ) {

			$campaignID = $item[ '_wpk_campaign_id' ];
			$campaign   = Campaign::find( $campaignID );

			if ( ! empty( $campaign->meta( 'order_id' ) ) ) {
				wc_add_notice( 'error', __( 'You have already ordered this campaign.', 'wpk' ) );
			}
		}

	}

	/**
	 * Remove unnecessary fields from checkout
	 *
	 * @param array $fields
	 *
	 * @return array
	 */
	public function wcCheckoutFields( array $fields ): array {

		return [];

	}

	/**
	 * Saves order ID in campaign meta data. Set billing values based on these set in profile
	 *
	 * @param int $orderID
	 *
	 * @throws \WC_Data_Exception
	 */
	public function assignCampaignToOrder( int $orderID ) {

		$order = wc_get_order( $orderID );
		$user  = User::current();

		$campaignIDS = $order->get_meta( '_wpk_campaign_ids' );

		$order->set_billing_address_1( $user->meta( 'address' ) );
		$order->set_billing_city( $user->meta( 'city' ) );
		$order->set_billing_postcode( $user->meta( 'zip' ) );
		$order->set_billing_company( $user->meta( 'company' ) );
		$order->set_billing_phone( $user->meta( 'phone' ) );
		$order->save();

		foreach ( $campaignIDS as $campaignID ) {

			Campaign::find( $campaignID )
			        ->updateMeta( 'order_id', $orderID )
			        ->updateMeta( 'creation_step', 3 );

		}
	}

	/**
	 * Saves campaign ID in order meta data
	 *
	 * @param \WC_Order_Item_Product $item
	 * @param string                 $cart_item_key
	 * @param array                  $values
	 * @param \WC_order              $order
	 */
	public function orderCreated( \WC_Order_Item_Product $item, $cart_item_key, $values, \WC_Order $order ) {

		$campaignID = $values[ '_wpk_campaign_id' ];
		$ids        = $order->get_meta( '_wpk_campaign_ids' );

		if ( ! is_array( $ids ) ) {
			$ids = [];
		}

		$ids[] = $campaignID;
		$order->update_meta_data( '_wpk_campaign_ids', $ids );
		$order->save_meta_data();

	}

	/**
	 * Set campaign price in cart based on it's plan
	 *
	 * @param \WC_Cart $cart
	 */
	public function setCampaignPrice( \WC_Cart $cart ) {

		$cartContent = $cart->get_cart();

		if ( ! empty( $cartContent ) ) {

			foreach ( $cartContent as $cart_key => $item ) {

				if ( isset( $item[ '_wpk_campaign_id' ] ) ) {

					$campaignID = $item[ '_wpk_campaign_id' ];
					$campaign   = Campaign::find( $campaignID );

					$campaignPlan = $campaign->getPlan();
					$price        = $campaign->meta( 'price' );

					/** @var \WC_Product $product */
					$product  = $item[ 'data' ];
					$duration = $campaign->meta( 'length' );

					$name = sprintf( __( 'Campaign - %s - %s months', 'wpk' ), $campaignPlan->post_title, $duration );
					$product->set_price( $price );
					$product->set_name( $name );

				}

			}
		}
	}

	/**
	 * Populate checkout fields with company details
	 *
	 * @param string $value
	 * @param string $field
	 *
	 * @return mixed
	 */
	public function populateCheckoutFields( $value, $field ) {

		$user = User::current();

		switch ( $field ) {

			case 'billing_country':
			case 'shipping_country':
				return $user->meta( 'country' );
				break;
			case 'billing_first_name':
			case 'shipping_first_name':
				return $user->first_name;
				break;
			case 'billing_last_name':
			case 'shipping_last_name':
				return $user->last_name;
				break;
			case 'billing_company':
			case 'shipping_company':
				return $user->meta( 'company' );
				break;
			case 'billing_address_1':
			case 'shipping_address_1':
				return $user->meta( 'address' );
				break;
			case 'billing_city':
			case 'shipping_city':
				return $user->meta( 'city' );
				break;
			case 'billing_state':
			case 'shipping_state':
				return $user->meta( 'state' );
				break;
			case 'billing_postcode':
			case 'shipping_postcode':
				return $user->meta( 'zip' );
				break;
			case 'billing_phone':
			case 'shipping_phone':
				return $user->meta( 'phone' );
				break;
		}

		return $value;
	}

	/**
	 * Add new template product
	 *
	 * @return bool|int|\WP_Error
	 */
	private function addNewProduct() {

		$args = [
			'post_title'   => 'Campaign plan',
			'post_content' => '',
			'post_status'  => 'publish',
			'post_type'    => "product",
		];

		$productID = wp_insert_post( $args );

		if ( ! is_wp_error( $productID ) ) {
			wp_set_object_terms( $productID, 'simple', 'product_type' );
			update_post_meta( $productID, '_regular_price', '0' );
			update_post_meta( $productID, '_price', '0' );
			update_post_meta( $productID, '_wpk_product', '1' );

			return $productID;
		}

		return false;


	}

	/**
	 * Get template product ID, or create new if it's doesn't exists
	 *
	 * @return bool|int|\WP_Error
	 */
	private function getTemplateProduct() {

		$args  = [
			'post_type'   => 'product',
			'post_status' => 'publish',
			'meta_query'  => [
				[
					'key'   => '_wpk_product',
					'value' => '1',
				],
			],
		];
		$query = new \WP_Query( $args );
		if ( $query->have_posts() ) {
			$products = $query->get_posts();
			$products = array_shift( $products );

			return $products->ID;
		} else {
			return $this->addNewProduct();
		}

	}

	/**
	 * Add campaign to cart
	 *
	 * @param Campaign $campaign
	 *
	 * @return bool|string
	 *
	 * @throws \Exception
	 */
	public function addCampaignToCart( Campaign $campaign ) {

		if ( ! empty( $campaign->meta( 'cart_key' ) ) ) {
			return false;
		}

		$cart = WC()->cart;

		if ( ! empty( $campaign->meta( 'order_id' ) ) ) {
			wc_add_notice( 'error', __( 'This campaign have been already ordered', 'wpk' ) );

			return false;
		}

		$userID = get_current_user_id();

		if ( $campaign->post_author != $userID ) {
			wc_add_notice( __( 'Permission issue', 'wpk' ), 'error' );

			return false;
		}

		//Empty cart before adding campaign to it
		$cart->empty_cart();

		$cartKey = $cart->add_to_cart( self::$productID, 1, 0, [], [
			'_wpk_campaign_id' => $campaign->ID,
		] );

		$campaign->meta( 'cart_key', $cartKey );

		return $cartKey;

	}

}