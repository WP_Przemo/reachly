<?php


namespace Wpk\Controllers\Campaign\Middleware;

use Wpk\Controllers\Middleware\Middleware;
use function Wpk\Core;
use Wpk\Helpers\Response;
use Wpk\Models\Campaign;
use Wpk\Models\CampaignInvitation;
use Wpk\Models\User;

/**
 * Middleware for collaboration actions
 *
 * @author Przemysław Żydek
 */
class Collaborate extends Middleware {

	/**
	 * @param Response $response
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return void
	 */
	public function handle( Response $response, Campaign $campaign, User $influencer ) {

		$influencerImpressions = $influencer->getImpressions( $campaign );

		if ( $campaign->isInvited( $influencer ) ) {
			$response->addError( esc_html__( 'You are already invited to this campaign', 'wpk' ), 'popup', true );
		}

		//Company wants to interact with influencer, but campaign cannot be renewed yet
		if ( $campaign->haveInfluencer( $influencer ) && ! $campaign->canBeModified( 'renewed_date' ) || $campaign->willBeAdded( $influencer ) ) {
			$response->addError( esc_html__( 'You can\'t do that because you are already in this campaign.', 'wpk' ), 'popup', true );
		}

		//There are less than seven days before start date
		if ( ! $campaign->canBeModified( 'start_date' ) || $campaign->isSkipped( $influencer ) ) {
			$response->addError( esc_html__( 'You can\'t do that', 'wpk' ), 'popup', true );
		}

		//Influencer has previously declined invitation to this campaign
		if ( $campaign->hasDeclined( $influencer ) ) {
			$response->addError( esc_html__( 'You can\'t do that because ou have declined invitation to this campaign.', 'wpk' ), 'popup', true );
		}

		//Influencer have already sent collaboration offer
		if ( $campaign->haveSendCollaboration( $influencer ) ) {
			$response->addError( esc_html__( 'You have already sent collaboration offer for this campaign.', 'wpk' ), 'popup', true );
		}


		//Woohoo, passed validation, let's create the invite!
		$result = CampaignInvitation::createCollaboration( $campaign, $influencer );

		if ( ! $result ) {
			$response->addError( esc_html__( 'Error while creating invitation', 'wpk' ), 'popup', true );
		}

		$html = Core()->view->render( 'campaign-invitation.collaborate-thankyou' );

		$response->setResult( $html )->sendJson();

	}

	/**
	 * Handles invitation acceptation
	 *
	 * @param Response $response
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return void
	 */
	public function accept( Response $response, Campaign $campaign, User $influencer ) {

		/** @var CampaignInvitation $invite */
		$invite = CampaignInvitation::getCollaboration( $campaign, $influencer )->first();

		if ( empty( $invite ) ) {
			$response->addError( esc_html__( 'No invitation found', 'wpk' ), 'popup', true );
		}

		$result = $invite->accept();

		if ( ! $result ) {
			$response->addError( esc_html__( 'Error while accepting invitation', 'wpk' ), 'popup', true );
		}

		$response
			->addMessage( sprintf( __( 'Accepted offer from <strong>%s</strong>!', 'wpk' ), $influencer->campaignSocialMeta( $campaign, 'username' ) ), 'popup', Response::SUCCESS )
			->setResult( $campaign->getImpressions() )
			->sendJson();

	}

}