<?php


namespace Wpk\Controllers\Campaign;

use Wpk\Controllers\Controller;
use Wpk\Models\Campaign;

/**
 * Handles moving influencers from "to_add" list into campaign
 *
 * @author Przemysław Żydek
 */
class AddInfluencers extends Controller {

	/**
	 * AddInfluencers constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wpk/campaign/started', [ $this, 'handle' ], 1 );
		add_action( 'wpk/campaign/renewed', [ $this, 'handle' ], 1 );
	}

	/**
	 * @param Campaign $campaign
	 *
	 * @return void
	 */
	public function handle( Campaign $campaign ) {
		$campaign->addToAddInfluencers();
	}

}