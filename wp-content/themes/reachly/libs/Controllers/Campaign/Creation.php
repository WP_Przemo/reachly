<?php


namespace Wpk\Controllers\Campaign;

use Wpk\Controllers\Controller;

/**
 * Controller for creating new campaign
 *
 * @author Przemysław Żydek
 */
class Creation extends Controller {

	/** @var array Array of steps and their callbacks */
	protected $steps = [];

	protected $middleware = [
		1 => Creation\FirstStep::class,
		2 => Creation\SecondStep::class,
		3 => Creation\ThirdStep::class,
		4 => Creation\FourthStep::class,
	];

	/**
	 * Creation constructor.
	 */
	public function __construct() {
		add_action( 'wpk/campaignCreationPage', [ $this, 'handle' ] );

		parent::__construct();
	}

	/**
	 * @return void
	 */
	public function handle() {

		$step = (int) $this->getQueryParam( 'step', 1 );

		call_user_func( [ $this->middleware( $step ), 'view' ] );

	}

}