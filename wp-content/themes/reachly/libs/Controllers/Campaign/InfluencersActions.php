<?php


namespace Wpk\Controllers\Campaign;

use Wpk\Controllers\Campaign\Middleware\Bookmark;
use Wpk\Controllers\Campaign\Middleware\Collaborate;
use Wpk\Controllers\Campaign\Middleware\Invite;
use Wpk\Controllers\Campaign\Middleware\Skipping;
use Wpk\Controllers\Controller;
use Wpk\Models\Campaign;
use Wpk\Models\User;

/**
 * Handle influencers related stuff
 *
 * @author Przemysław Żydek
 */
class InfluencersActions extends Controller {

	protected $middleware = [
		'invite'      => Invite::class,
		'skipping'    => Skipping::class,
		'collaborate' => Collaborate::class,
		'bookmark'    => Bookmark::class,
	];

	/**
	 * Influencers constructor.
	 */
	public function __construct() {
		add_action( 'wp_ajax_wpk_influencer_action', [ $this, 'handleActions' ] );

		parent::__construct();
	}

	/**
	 * Handle influencers actions
	 *
	 * @return void
	 */
	public function handleActions() {

		$response = $this->response;

		$influencerAction = $this->getPostParam( 'influencer_action' );
		$campaignID       = (int) $this->getPostParam( 'campaign_id' );
		$influencerID     = (int) $this->getPostParam( 'influencer_id' );

		if ( empty( $influencerAction ) || empty( $campaignID ) ) {
			$response->addError( esc_html__( 'Invalid parameters', 'wpk' ), 'popup', true );
		}

		$response->checkNonce( 'wpk_influencer_action', 'wpk_nonce' );

		$influencer = empty( $influencerID ) ? User::current() : User::find( $influencerID );
		$campaign   = Campaign::find( $campaignID );

		switch ( $influencerAction ) {

			//Invite to campaign, sent by company
			case 'invite':

				$this->middleware( 'invite' )->handle( $response, $campaign, $influencer );
				break;

			//Company wants to skip influencer
			case 'skip':

				$this->middleware( 'skipping' )->handle( $response, $campaign, $influencer );
				break;

			//Company wants to unskip influencer
			case 'unskip':

				$this->middleware( 'skipping' )->cancel( $response, $campaign, $influencer );
				break;

			//Company wants to cancel invitation
			case 'uninvite':

				$this->middleware( 'invite' )->cancel( $response, $campaign, $influencer );
				break;

			//Influencer wants to accept invitation
			case 'influencer_accept':
				$this->middleware( 'invite' )->influencerAccept( $response, $campaign, $influencer );
				break;

			//Influencer wants to decline invitation
			case 'influencer_decline':
				$this->middleware( 'invite' )->influencerDecline( $response, $campaign, $influencer );
				break;

			case 'collaborate':
				$this->middleware( 'collaborate' )->handle( $response, $campaign, $influencer );
				break;

			case 'accept':
				$this->middleware( 'collaborate' )->accept( $response, $campaign, $influencer );
				break;

			case 'bookmark':
				$this->middleware( 'bookmark' )->handle( $response, $campaign, $influencer );
				break;

			case 'unbookmark':
				$this->middleware( 'bookmark' )->undo( $response, $campaign, $influencer );
				break;

		}

		$response->sendJson();

	}

}