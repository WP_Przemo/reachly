<?php


namespace Wpk\Controllers\Admin;

use Wpk\Controllers\Controller;
use function Wpk\Core;
use Wpk\Models\Campaign;
use Wpk\Models\User;
use Wpk\Utility;

/**
 * @author Przemysław Żydek
 */
class CampaignNotification extends Controller {

	/**
	 * CampaignNotification constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wpk/campaign/published', [ $this, 'send' ] );
	}

	/**
	 * On campaign publish or renewal send e-mail to administrator with list of all influencers and their address
	 *
	 * @param Campaign $campaign
	 *
	 * @return void
	 */
	public function send( Campaign $campaign ) {

		$email = get_option( 'admin_email' );

		$influencers = $campaign->getToAddInfluencers();

		array_walk( $influencers, function ( &$item, $index ) {
			$item = User::find( $index );
		}
		);

		$data = [
			'campaign'    => $campaign,
			'influencers' => $influencers,
			'author'      => $campaign->getAuthor(),
		];

		$message = Core()->view->render( 'admin.campaign-email', $data );

		$title = sprintf( esc_html__( 'Campaign %s have been published!', 'wpk' ), $campaign->post_title );

		wp_mail( $email, $title, $message, Utility::getEmailHeaders() );

	}

}