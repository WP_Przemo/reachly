<?php


namespace Wpk\Controllers\CampaignPlan;

use Wpk\Controllers\Controller;
use Wpk\Models\CampaignPlan;

/**
 * Populates acf values for Campaign plan
 *
 * @author Przemysław Żydek
 */
class Acf extends Controller {

	/**
	 * Acf constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_filter( 'acf/load_field/name=wpk_plan', [ $this, 'planSelect' ] );
	}

	/**
	 * Populates acf field "wpk_plan" with campaign plans
	 *
	 * @param array $field
	 *
	 * @return array
	 */
	public function planSelect( array $field ): array {

		$field[ 'choices' ] = [];

		$plan = new CampaignPlan( [
			'post_status' => 'publish',
		] );

		foreach ( $plan->get() as $post ) {
			$field[ 'choices' ][ $post->ID ] = $post->post_title;
		}

		return $field;

	}

}