<?php

namespace Wpk\Controllers;

use Wpk\Core;
use Wpk\Models\User;
use Wpk\Pages;
use Wpk\Utility;

use function Wpk\Core as CoreFc;

/**
 * Manages dynamic content for menus
 */
class Menu extends Controller {

	/**
	 * Menu constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_filter( 'wp_nav_menu_items', [ $this, 'filterMenu' ], 9999 );
	}

	/**
	 * Filters menu items based on context
	 *
	 * @param string $items
	 *
	 * @return string
	 */
	public function filterMenu( string $items ): string {

		if ( ! is_user_logged_in() ) {
			return $items;
		}

		$user      = $this->user;
		$type      = $user->meta( 'type' );
		$campaigns = Pages::getMyCampaignsUrl();
		$messages  = get_post_type_archive_link( Core::CAMPAIGN_CONVERSATION );
		$template  = Utility::getPageTemplate();

		if ( $type === 'company' ) {

			$stats = Pages::getStatisticsPageUrl();

			if ( ! empty( $campaigns ) ) {
				$items .= $this->menuItem( __( 'Campaigns', 'wpk' ), $campaigns, $template === 'my_campaigns.php' );
			}

			if ( ! empty( $messages ) ) {
				$items .= $this->menuItem( __( 'Messages', 'wpk' ), $messages, is_post_type_archive( 'messages' ) || get_post_type() === 'messages' );

			}
			if ( ! empty( $stats ) ) {
				$items .= $this->menuItem( __( 'Statistics', 'wpk' ), $stats, $template === 'statictics.php' );
			}

		} else /*if ( $type === 'influencer' )*/ {

			$brands = Pages::getBrandsPageUrl();

			if ( ! empty( $brands ) ) {
				$items .= $this->menuItem( __( 'Brands', 'wpk' ), $brands, $template === 'brands.php' );
			}

			if ( ! empty( $campaigns ) ) {
				$items .= $this->menuItem( __( 'My Campaigns', 'wpk' ), $campaigns, $template === 'my_campaigns.php' );
			}

			if ( ! empty( $messages ) ) {
				$items .= $this->menuItem( __( 'Messages', 'wpk' ), $messages, is_post_type_archive( 'messages' ) || get_post_type() === 'messages' );
			}
		}

		return $items;

	}

	/**
	 * @param string $content
	 * @param string $url
	 * @param bool   $active
	 *
	 * @return string
	 */
	protected function menuItem( string $content, string $url, bool $active = false ) {

		return CoreFc()->view->render( 'menu.item', [
			'content' => $content,
			'url'     => $url,
			'active'  => $active,
		] );

	}

}