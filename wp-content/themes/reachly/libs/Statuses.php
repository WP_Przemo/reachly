<?php


namespace Wpk;

/**
 * Registers new statuses
 *
 * @author Przemysław Żydek
 */
class Statuses {

	/**
	 * @return void
	 */
	public static function register() {

		register_post_status( 'started', [
			'label'                     => __( 'Started' ),
			'public'                    => true,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Started <span class="count">(%s)</span>', 'Started <span class="count">(%s)</span>' ),
		] );

		register_post_status( 'online', [
			'label'                     => __( 'Online' ),
			'public'                    => true,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Online <span class="count">(%s)</span>', 'Online <span class="count">(%s)</span>' ),
		] );

		register_post_status( 'stopped', [
			'label'                     => __( 'Stopped' ),
			'public'                    => true,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Stopped <span class="count">(%s)</span>', 'Stopped <span class="count">(%s)</span>' ),
		] );

		register_post_status( 'ended', [
			'label'                     => __( 'Ended' ),
			'public'                    => true,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop( 'Ended <span class="count">(%s)</span>', 'Ended <span class="count">(%s)</span>' ),
		] );

	}

}