<?php


namespace Wpk\Api;

use Wpk\Api\Helpers\Insight;

/**
 * Interface used for getting insights values from API
 *
 * @author Przemysław Żydek
 */
interface Insights {

	/**
	 * @param string $insight
	 *
	 * @return mixed
	 */
	public static function getInsight( string $insight, $token ): Insight;

	public static function getImpressions( $token ): Insight;

	public static function getReach( $token ): Insight;

	public static function getEngagment( $token ): Insight;

	/**
	 * @param array $insight
	 *
	 * @return array
	 */
	public static function parseInsight( array $insight ): array;

}