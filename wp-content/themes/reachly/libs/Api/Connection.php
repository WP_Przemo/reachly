<?php


namespace Wpk\Api;

/**
 * @author Przemysław Żydek
 */
abstract class Connection {

	/** @var mixed Stores connection with API */
	protected static $connection;

	/**
	 * Connect with API
	 *
	 * @return void
	 */
	abstract public static function connect();

	/**
	 * @param string $endpoint
	 * @param mixed  $token
	 * @param array  $params
	 *
	 * @return mixed
	 */
	abstract public static function get( string $endpoint, $token, array $params );



}