<?php


namespace Wpk\Api;

use Wpk\Templates\Schedule;

/**
 * Caches endpoints, so that they won't be called twice in the same session
 *
 * @author Przemysław Żydek
 */
class Cache extends Schedule {

	/** @var string */
	const SLUG = 'wpk_api_cache';

	/** @var bool Decides if cache is active or not. Better disable while development */
	const ENABLED = true;

	protected $hook = 'wpk_api_cache_hook';

	protected $callbacks = [ 'removeExpiredItems' ];

	protected $recurrence = 'daily';

	/** @var string */
	protected $api;

	/** @var array */
	protected $values = [];

	/**
	 * Cache constructor.
	 *
	 * @param string $api API name
	 *
	 */
	public function __construct( string $api ) {

		$this->api    = $api;
		$this->values = $this->getOption();

		parent::__construct();
	}

	/**
	 * Remove cache items that have expired
	 *
	 * @return void
	 */
	public function removeExpiredItems() {

		foreach ( $this->values as $api => $apis ) {
			foreach ( $apis as $token => $endpoints ) {
				foreach ( $endpoints as $endpoint => $item ) {

					$expires = (int) $item[ 'expires' ];

					if ( time() > $expires ) {
						unset( $this->values[ $api ][ $token ][ $endpoint ] );
					}

				}
			}
		}

		$this->updateOption();

	}

	/**
	 * @return array
	 */
	protected function getOption(): array {
		return get_option( "wpk_{$this->api}", [] );
	}

	/**
	 * @return void
	 */
	protected function updateOption() {
		update_option( "wpk_{$this->api}", $this->values );
	}


	/**
	 * @param string $endpoint
	 * @param mixed  $token Access token value
	 * @param mixed  $value
	 */
	public function add( string $endpoint, $token, $value ) {

		if ( ! self::ENABLED || empty( $value ) ) {
			return;
		}

		$this->values[ $this->api ][ $token ][ $endpoint ] = [
			'value'   => $value,
			'expires' => strtotime( '+3 hours' ),
		];

		$this->updateOption();

	}

	/**
	 * @param string $endpoint
	 * @param mixed  $token Token value
	 *
	 * @return bool|mixed
	 */
	public function get( string $endpoint, $token ) {

		if ( ! self::ENABLED ) {
			return false;
		}

		if ( empty( $this->values[ $this->api ][ $token ][ $endpoint ] ) ) {
			return false;
		}

		$item = $this->values[ $this->api ][ $token ][ $endpoint ];

		//Item expired, remove it from cache
		if ( time() > (int) $item[ 'expires' ] ) {
			unset( $this->values[ $this->api ][ $token ][ $endpoint ] );

			$this->updateOption();

			return false;
		}

		return $item[ 'value' ];

	}

	/**
	 * Clear cache
	 *
	 * @return $this
	 */
	public function clear(): self {
		delete_option( "wpk_{$this->api}" );

		return $this;
	}

}