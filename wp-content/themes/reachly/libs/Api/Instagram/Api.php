<?php

namespace Wpk\Api\Instagram;

use Facebook\Authentication\AccessToken;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Wpk\Api\Helpers\Insight;
use Wpk\Models\User;
use Wpk\Utility;

/**
 * Main facebook API class
 */
class Api extends \Wpk\Api\Facebook\Api {

	/** @var Facebook */
	public static $connection;

	/**
	 * Connect with API
	 *
	 * @return void
	 */
	public static function connect() {

		if ( ! session_id() ) {
			session_start();
		}

		self::$connection = parent::$connection;

	}

	/**
	 * Get ids of instagram accounts
	 *
	 * @param AccessToken $token
	 *
	 * @return array
	 */
	public static function getInstagramAccounts( AccessToken $token ): array {

		$accounts = self::getFacebookAccounts( $token );
		$result   = [];

		if ( $accounts ) {
			foreach ( $accounts as $account ) {
				$id = $account[ 'id' ];


				$data = self::get( $id, $token, [ 'fields' => 'instagram_business_account' ] );

				if ( ! $data ) {
					return $result;
				}

				$data = $data->getGraphNode()->asArray();

				if ( isset( $data[ 'instagram_business_account' ] ) ) {
					$result[] = $data[ 'instagram_business_account' ][ 'id' ];
				}


			}
		}

		return $result;


	}

	/**
	 * Get instagram account data
	 *
	 * @param string|int  $id
	 * @param AccessToken $token
	 * @param array       $fields
	 *
	 * @return array|bool
	 */
	public static function getAccountData(
		$id, AccessToken $token, $fields = [
		'id',
		'followers_count',
		'follows_count',
		'media_count',
		'profile_picture_url',
		'username',
		'name',
		'biography',
	]
	) {

		if ( is_array( $fields ) ) {
			$fields = implode( ',', $fields );
		}

		try {
			$data = self::get( $id, $token, [ 'fields' => $fields ] )
			            ->getGraphNode()
			            ->asArray();

			return $data;
		} catch ( FacebookSDKException $e ) {
			Utility::log( $e->getMessage() );

			return false;
		}


	}

	/**
	 * Get account impressions or reach
	 *
	 * @param string|int  $id
	 * @param AccessToken $token
	 * @param array       $data Date for insights, contains period, optional since and until values as timestamp
	 *
	 * @return bool|Insight
	 */
	public static function getInstagramImpressionsOrReach( $id, AccessToken $token, array $data = [] ) {

		$data = wp_parse_args( $data, [
			'metric' => '',
		] );

		/** @var string $period */
		/** @var int $since */
		/** @var int $until */
		extract( $data );

		$data[ 'period' ] = $period !== 'lifetime' ? $period : 'days_28';


		$response = self::get( "$id/insights", $token, $data );

		if ( $response ) {
			return new Insight( $response->getGraphEdge()->asArray()[ 0 ] );
		}


		return false;

	}


	/**
	 * Get account insights
	 *
	 * @param string|int  $id
	 * @param AccessToken $token
	 * @param array       $metric
	 * @param array       $date Date for insights, contains period, optional since and until values as timestamp
	 *
	 * @return bool|array
	 */
	public static function getInstagramInsights(
		$id, AccessToken $token, array $metric = [
		'audience_city',
		'audience_country',
		'audience_gender_age',
		'audience_locale',
		'impressions',
		'reach',
	],
		array $date = []
	) {

		$insights = [];

		$date = wp_parse_args( $date, [
			'period' => 'lifetime',
		] );

		if ( in_array( 'impressions', $metric ) ) {

			$insights[ 'impressions' ] = self::getInstagramImpressionsOrReach( $id, $token, [ 'metric' => 'impressions', 'period' => $date['period'] ] );

			$key = array_search( 'impressions', $metric );
			unset( $metric[ $key ] );

		}

		if ( in_array( 'reach', $metric ) ) {

			$insights[ 'reach' ] = self::getInstagramImpressionsOrReach( $id, $token, [ 'metric' => 'reach', 'period' => $date['period']  ] );

			$key = array_search( 'reach', $metric );
			unset( $metric[ $key ] );

		}

		$date[ 'metric' ] = implode( ',', $metric );


		$data = self::get( "$id/insights", $token, $date );

		if ( $data ) {

			$data = $data->getGraphEdge()->asArray();

			foreach ( $data as $item ) {
				if ( $item ) {
					$name              = $item[ 'name' ];
					$insights[ $name ] = new Insight( $item );
				}
			}

		}

		return $insights;


	}

	/**
	 * @param User $user
	 * @param int|bool  $limit
	 *
	 * @return bool|array
	 */
	public static function getUserMedia( User $user, $limit = 10 ) {

		$accountID = $user->meta( 'instagram_id' );
		$token     = $user->getApiToken( 'instagram' );
		$result    = [];

		if ( empty( $accountID ) || empty( $token ) ) {
			return false;
		}

		$medias = self::get( "$accountID/media", $token );

		if ( $medias ) {
			$medias = $medias->getGraphEdge()->asArray();

			foreach ( $medias as $media ) {
				if ( count( $result ) <= $limit || $limit === false ) {
					$result[] = new Media( $media[ 'id' ], $user );
				}
			}

		}

		return $result;

	}

}