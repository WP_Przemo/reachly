<?php

namespace Wpk\Api\Instagram;

use Facebook\Authentication\AccessToken;
use Wpk\Api\Helpers\Insight;
use Wpk\Models\User;

class Media extends \Wpk\Api\Media {

	/**
	 * Media constructor.
	 *
	 * @param      $id
	 * @param User $user
	 */
	public function __construct( $id, User $user ) {

		parent::__construct( $id, $user );

		$this->parseTags();


	}

	/**
	 * Parse tags from media description
	 *
	 * @return self
	 */
	protected function parseTags(): self {

		preg_match_all( "/(#\w+)/", $this->caption, $matches );

		if ( ! empty( $matches ) ) {

			$matches = $matches[ 0 ];

			array_walk( $matches, function ( &$value ) {
				$value = str_replace( '#', '', $value );
			} );

			$this->tags = $matches;

		}

		return $this;

	}

	/**
	 * Get API data for media
	 *
	 * @return object|false
	 */
	public function getApiData() {

		$fields = implode( ',', [
			'caption',
			'comments',
			'comments_count',
			'like_count',
			'media_url',
			'permalink',
			'timestamp',
		] );

		$id   = $this->id;
		$user = $this->author;

		$data = Api::get( "$id", $user->getApiToken( 'instagram' ), [ 'fields' => $fields ] );

		if ( ! $data ) {
			return false;
		}

		$data = (object) $data->getGraphNode()->asArray();

		return $data;


	}

	/**
	 * Get insights data for media
	 *
	 * @param array|string $metric
	 *
	 * @return self
	 */
	public function getInsights( array $metric = [ 'impressions', 'engagement', 'reach' ] ): self {

		$id    = $this->id;
		$token = $this->author->getApiToken( 'instagram' );

		$response = Api::getInstagramInsights( $id, $token, $metric );

		foreach ( $response as $insight ) {

			if ( $insight ) {
				$key                    = $insight->name;
				$this->insights[ $key ] = $insight;
			}

		}

		return $this;

	}

	/**
	 * Get insight value
	 *
	 * @param string $insight
	 *
	 * @return array|bool|int
	 */
	public function getInsightValue( $insight ) {

		if ( isset( $this->insights[ $insight ] ) ) {

			/** @var Insight $ins */
			$ins = $this->insights[ $insight ];

			return $ins->value;

		}

		return false;

	}

	/**
	 * Get media image
	 *
	 * @param string $size
	 * @param bool   $onlyUrl
	 *
	 * @return bool|mixed
	 */
	public function getImage( string $size, bool $onlyUrl = true ) {

		$images = $this->images;
		if ( isset( $images[ $size ] ) ) {
			$target = $images[ $size ];

			return $onlyUrl ? $target[ 'url' ] : $target;
		}

		return false;

	}

}