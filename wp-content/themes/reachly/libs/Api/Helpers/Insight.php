<?php


namespace Wpk\Api\Helpers;

use Wpk\Utility;

class Insight extends Entity {

	/** @var int|array Stores insight value */
	public $value;

	/** @var string Insight name */
	public $name;

	/** @var string Insight period */
	public $period;

	/** @var string */
	public $title;

	/** @var string Insight ID */
	public $id;

	/** @var string Insight description */
	public $description;

	/**
	 * Insight constructor.
	 *
	 * @param array $data
	 */
	public function __construct( $data ) {

		parent::__construct( $data );

		foreach ( $this->data as $key => $value ) {
			if ( $key !== 'values' ) {
				$this->$key = $value;
			}
		}

		$this->value = isset( $data[ 'values' ][ 0 ][ 'value' ] ) ? $data[ 'values' ][ 0 ][ 'value' ] : 0;
		$this->parseValues();

	}

	/**
	 * Parses insight values
	 */
	protected function parseValues() {

		switch ( $this->name ) {

			case 'audience_country':
				$values = [];
				foreach ( $this->value as $country_code => $item ) {

					$country            = \Locale::getDisplayRegion( "-{$country_code}", get_locale() );
					$values[ $country ] = $item;

				}
				$this->value = $values;

				break;

		}

	}

	/**
	 * Parse age range for insight
	 *
	 * @param array $values Values from insight
	 *
	 * @return array
	 */
	public static function parseAgeRange( array $values = [] ): array {

		$ages = [
			'men'   => [],
			'women' => [],
			'all'    => [],
		];

		foreach ( $values as $key => $value ) {

			//Ages are displayed like M.55-64 so we remove gender prefix from it
			$age = preg_replace( '/[A-z]./', '', $key );

			if ( Utility::contains( $key, 'F' ) ) {
				$ages[ 'female' ][ $age ] = $value;
			} else if ( Utility::contains( $key, 'M' ) ) {
				$ages[ 'male' ][ $age ] = $value;
			}

			if ( ! isset( $ages[ 'all' ][ $age ] ) ) {
				$ages[ 'all' ][ $age ] = 0;
			}

			$ages[ 'all' ][ $age ] += $value;

		}

		return $ages;

	}

}