<?php


namespace Wpk\Api\Helpers;

/**
 * Abstract class for other entities
 */
abstract class Entity {

	/** @var array Raw data for entity */
	protected $data = [];

	/**
	 * Entity constructor.
	 *
	 * @param array $data
	 */
	public function __construct( $data = [] ) {

		$this->setData( $data );

	}

	/**
	 * Helper for setting entity data
	 *
	 * @param  string|int $key
	 * @param  string     $default
	 *
	 * @return mixed
	 */
	public function getData( $key, $default = '' ) {

		return isset( $this->data[ $key ] ) ? $this->data[ $key ] : $default;

	}

	/**
	 * Set entity data
	 *
	 * @param $data
	 */
	protected function setData( $data ) {

		$this->data = (array) $data;

	}

}