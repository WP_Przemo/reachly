<?php


namespace Wpk\Api;

use Wpk\Api\Helpers\Entity;
use Wpk\Models\User;

/**
 * Helper class for managing instagram media
 */
abstract class Media extends Entity {

	/** @var string Media ID */
	public $id = '';

	/**@var int Timestamp with date when media was uploaded to instagram */
	public $createdTime = '';

	/**@var array Array with uploaded images */
	public $images = [];

	/**@var array Array with caption data about image */
	public $caption = [];

	/** @var User Array with author data */
	public $author = [];

	/** @var array Array with media tags */
	public $tags = [];

	/** @var int Amount of likes */
	public $likes = 0;

	/** @var string URL to media (not direct link to image) */
	public $link = '';

	/** @var array Array with comments content */
	public $comments = [];

	/** @var int Amount of comments */
	public $commentsCount = 0;

	/** @var int Amount o media impressions */
	public $impressions = 0;

	/** @var int Amount of media reach */
	public $reach = 0;

	/** @var string URL to media */
	public $mediaUrl;

	/** @var array Stores raw insights data */
	public $insights;


	/**
	 * Media constructor.
	 *
	 * @param mixed id Media ID
	 * @param User $user Media author
	 *
	 */
	public function __construct( $id, User $user ) {

		$this->id     = $id;
		$this->author = $user;
		$data         = $this->getApiData();

		parent::__construct( $data );

		$this->createdTime = $this->getData( 'timestamp', false );
		$this->mediaUrl    = $this->getData( 'media_url' );
		$this->caption     = $this->getData( 'caption' );

		$this->likes         = $this->getData( 'like_count', 0 );
		$this->link          = $this->getData( 'permalink' );
		$this->comments      = $this->getData( 'comments', [] );
		$this->commentsCount = intval( $this->getData( 'comments_count', 0 ) );
	}

	/**
	 * Get API data for media
	 *
	 * @return object|false
	 */
	abstract public function getApiData();

	/**
	 * Get insight value
	 *
	 * @param string $insight
	 *
	 * @return array|bool|int
	 */
	abstract public function getInsightValue( $insight );

	/**
	 * Get creation date of image. Return timestamp if $format is set to false
	 *
	 * @param string|bool $format
	 *
	 * @return \DateTime|string
	 */
	public function getCreationDate( $format = DATE_FORMAT ) {

		$date = new \DateTime( $this->createdTime );

		return $format ? $date->format( $format ) : $date;

	}

	/**
	 * Get media image
	 *
	 * @param string $size
	 * @param bool   $onlyUrl
	 *
	 * @return bool|mixed
	 */
	abstract public function getImage( string $size, bool $onlyUrl = true );

}