<?php

namespace Wpk\Api\Facebook;

use Facebook\Authentication\AccessToken;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\FacebookResponse;
use Wpk\Api\Cache;
use Wpk\Api\Connection;
use Wpk\Utility;

/**
 * Main facebook API class
 */
class Api extends Connection {

	/** @var Facebook */
	protected static $connection;

	/** @var Cache $cache */
	protected static $cache;

	/**
	 * Connect with API
	 *
	 * @return void
	 */
	public static function connect() {

		if ( empty( self::$connection ) ) {

			if ( ! session_id() ) {
				session_start();
			}

			self::$connection = new Facebook( [
				'app_id'                => FACEBOOK_APP_ID,
				'app_secret'            => FACEBOOK_APP_SECRET,
				'default_graph_version' => 'v3.0',
			] );

			self::$cache = new Cache( 'facebook' );

		}

	}

	/**
	 * @param string      $endpoint
	 * @param AccessToken $token
	 * @param array       $params
	 *
	 * @return FacebookResponse|bool
	 */
	public static function get( string $endpoint, $token, array $params = [] ) {

		$endpoint = add_query_arg( $params, $endpoint );

		$cache = self::$cache->get( $endpoint, $token->getValue() );

		if ( ! empty( $cache ) ) {
			return $cache;
		}

		try {
			$data = self::$connection->get( $endpoint, $token );

			self::$cache->add( $endpoint, $token->getValue(), $data );

		} catch ( FacebookSDKException $e ) {
			Utility::log( $e->getMessage(), 'FACEBOOK_SDK_ERROR' );

			return false;
		}

		return $data;
	}

	/**
	 * @return void
	 */
	public static function clearCache() {
		self::$cache->clear();
	}

	/**
	 * Helper function for getting long lived token
	 *
	 * @param AccessToken $token
	 *
	 * @return bool|AccessToken
	 */
	public static function getLongLivedToken( AccessToken $token ) {

		$connection = self::$connection;
		$client     = $connection->getOAuth2Client();
		if ( $token ) {

			try {
				$token = $client->getLongLivedAccessToken( $token );
			} catch ( FacebookSDKException $e ) {
				Utility::log( $e->getMessage() );

				return false;
			}

		}

		return $token;

	}

	/**
	 * @param AccessToken $token
	 *
	 * @return array|bool
	 */
	public static function getFacebookAccounts( AccessToken $token ) {

		try {
			$data = self::$connection->get( 'me/accounts', $token );

			return $data->getGraphEdge()->asArray();
		} catch ( FacebookSDKException $e ) {
			Utility::log( $e->getMessage(), 'FB Error' );

			return false;
		}

	}


}