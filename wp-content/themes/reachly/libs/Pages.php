<?php


namespace Wpk;

/**
 * Helper class for getting pages via their templates
 *
 * @package wpk
 */
class Pages {

	/**
	 * Pages constructor.
	 */
	public function __construct() {
		$this->setupHooks();
	}

	/**
	 * @return void
	 */
	protected function setupHooks() {
		add_filter( 'pre_get_document_title', [ $this, 'setPageTitle' ] );
	}

	/**
	 * Set page title based on context
	 *
	 * @param string $title
	 *
	 * @return string
	 */
	public function setPageTitle( string $title ): string {

		$template = Utility::getPageTemplate();

		switch ( $template ) {

			case 'edit_campaign.php':
				$post_title = get_the_title( $_GET[ 'id' ] );

				return sprintf( __( 'Edit campaign %s', 'wpk' ), $post_title );
				break;

		}

		return $title;

	}

	/**
	 * @param string $template
	 *
	 * @return string
	 */
	protected static function getPageUrl( string $template ): string {
		return get_permalink( Utility::getPageByTemplate( $template ) );
	}

	/**
	 * Get company register page
	 *
	 * @return string
	 */
	public static function getCompanyRegisterPage(): string {

		return self::getPageUrl( 'company_register.php' );

	}

	/**
	 * Get "My Campaigns" page
	 *
	 * @return string
	 */
	public static function getMyCampaignsUrl(): string {

		return self::getPageUrl( 'my_campaigns.php' );

	}

	/**
	 *  Get "Add new campaign" page
	 *
	 * @return string
	 */
	public static function getNewCampaignsUrl(): string {

		return self::getPageUrl( 'new_campaign.php' );

	}

	/**
	 * Get link to campaign creation page. If no campaign page exists, returns home url
	 *
	 * @param int $step Optional, step into which user will be redirected
	 * @param int $campaignID Optional, campaign id which data will be loaded
	 *
	 * @return string
	 */
	public static function getCampaignCreationUrl( int $step = 1, int $campaignID = null ): string {

		$page = Utility::getPageByTemplate( 'new_campaign.php' );

		if ( ! empty( $page ) ) {
			$url  = get_permalink( $page->ID );
			$args = [
				'step' => $step,
			];

			if ( ! empty( $campaignID ) ) {
				$args[ 'campaign_id' ] = $campaignID;
			}
			$query = add_query_arg( $args, $url );

			return $query;
		}

		return home_url();

	}

	/**
	 * Get "My Profile" page url
	 *
	 * @return string
	 */
	public static function getProfileUrl(): string {
		return self::getPageUrl( 'user_profile.php' );
	}

	/**
	 * Get "Campaign preview" page
	 *
	 * @return string
	 */
	public static function getCampaignPreviewPageUrl(): string {

		return self::getPageUrl( 'campaign_preview.php' );

	}

	/**
	 * Get "Statistics" page
	 *
	 * @return string
	 */
	public static function getStatisticsPageUrl(): string {

		return self::getPageUrl( 'statictics.php' );

	}

	/**
	 * Get "Messages" page
	 *
	 * @return array|false|\WP_Post
	 */
	public static function getMessagesPage() {

		return Utility::getPageByTemplate( 'messages.php' );

	}

	/**
	 * Get "edit campaign" page
	 *
	 * @return string
	 */
	public static function getEditCampaignPageUrl(): string {

		return self::getPageUrl( 'edit_campaign.php' );

	}

	/**
	 * Get "brands" page
	 *
	 * @return string
	 */
	public static function getBrandsPageUrl(): string {

		return self::getPageUrl( 'brands.php' );

	}

	/**
	 * Get "Register" page
	 *
	 * @return string
	 */
	public static function getRegisterPageUrl(): string {

		return self::getPageUrl( 'register.php' );

	}

	/**
	 * @return string
	 */
	public static function getLoginPageUrl(): string {
		return self::getPageUrl( 'login.php' );
	}

}