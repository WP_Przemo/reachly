<?php


namespace Wpk\Helpers;

/***
 * @author Przemysław Żydek
 */
class Chart {

	/**
	 * Helper function for formatting data for Highchart
	 *
	 * @param array $values
	 *
	 * @return string
	 */
	public static function preparePieValues( array $values = [] ): string {

		$result = [];

		foreach ( $values as $index => $value ) {

			$result[] = [ $index, $value ];

		}

		usort( $result, function ( $a, $b ) {
			if ( is_numeric( $a[ 1 ] ) && is_numeric( $b[ 1 ] ) ) {
				return $a[ 1 ] < $b[ 1 ];
			}

			return true;
		} );

		return ( json_encode( $result ) );

	}

}