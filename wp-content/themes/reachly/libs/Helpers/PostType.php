<?php


namespace Wpk\Helpers;

use Wpk\Utility;

/**
 * Helper function for creating new post types
 *
 * @author Przemysław Żydek
 */
class PostType {

	/** @var array Post type args */
	protected $args = [];

	/** @var string Post type slug */
	protected $slug = [];

	/**
	 * PostType constructor.
	 *
	 * @param string $slug
	 * @param array $args
	 */
	public function __construct( string $slug, array $args = [] ) {
		$this->slug = $slug;
		$this->args = $args;

		add_action( 'init', [ $this, 'register' ] );
	}

	/**
	 * Register post type. Hooked into init
	 *
	 * @return void
	 */
	public function register() {
		register_post_type( $this->slug, $this->args );
	}

	/**
	 * Add new column to admin view
	 *
	 * @param string $columnSlug
	 * @param string $columnName
	 * @param \Closure Callback for column content
	 *
	 * @return self
	 */
	public function addAdminColumn( string $columnSlug, string $columnName, \Closure $callback ): self {

		add_filter( "manage_{$this->slug}_posts_columns", function ( $columns = [] ) use ( $columnSlug, $columnName ) {
			$columns[ $columnSlug ] = $columnName;

			return $columns;
		} );

		add_action( "manage_{$this->slug}_posts_custom_column", function ( $column, $postId ) use ( $callback, $columnSlug ) {
			if ( $column === $columnSlug ) {
				$callback( $postId );
			}
		}, 10, 2 );

		return $this;
	}

	/**
	 * @param string $taxonomy
	 * @param array $args
	 *
	 * @return self
	 */
	public function addTaxonomy( string $taxonomy, $args ) {

		$taxonomy = register_taxonomy( $taxonomy, $this->slug, $args );

		if ( is_wp_error( $taxonomy ) ) {
			Utility::log( $taxonomy->get_error_messages(), 'add_tax_error' );
		}

		return $this;

	}

}