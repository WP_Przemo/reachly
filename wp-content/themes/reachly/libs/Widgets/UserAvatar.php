<?php


namespace Wpk\Widgets;

use function Wpk\Core;
use Wpk\Models\User;
use Wpk\Pages;
use Wpk\Utility;

class UserAvatar extends \WP_Widget {

	/** @var array Widget options */
	private $options = [
		'classname'   => 'wpk_header_avatar',
		'description' => 'Displays user avatar in header',
	];

	public function __construct() {
		parent::__construct( 'wpk_header_user_avatar', 'WPK Header User Avatar', $this->options );
	}

	public function widget( $args, $instance ) {

		$notifications = [];
		$user          = User::current();


		$data = [
			'profileUrl'         => Pages::getProfileUrl(),
			'user'               => $user,
			'userName'           => $user->user_login,
			/*TODO Notifications*/
			'notifications'      => $notifications,
			'notificationsCount' => count( $notifications ),
			'counterClass'       => empty( $notifications ) ? '' : 'wpk-has-notifications',
			'settingsActive'     => Utility::getPageTemplate() === 'user_profile.php' ? 'wpk-active' : '',
			'logoutUrl'          => wp_logout_url(),
			'avatar'             => $user->getAvatar(),
		];

		echo Core()->view->render( 'widgets.user-avatar', $data );
	}


}