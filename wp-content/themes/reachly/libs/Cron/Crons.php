<?php


namespace Wpk\Cron;

/**
 * Loads cron modules
 *
 * @author Przemysław Żydek
 */
class Crons {

	/** @var array */
	protected $crons = [
		CampaignInvitation\Cron::class,
		Campaign\Start::class,
		Campaign\Renew::class,
		User\Instagram::class,
	];

	/**
	 * Cron constructor.
	 */
	public function __construct() {

		foreach ( $this->crons as $cron ) {
			new $cron();
		}

	}

}