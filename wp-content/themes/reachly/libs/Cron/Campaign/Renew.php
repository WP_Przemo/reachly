<?php


namespace Wpk\Cron\Campaign;

use Wpk\Cron\Cron;
use Wpk\Models\Campaign;

/**
 * @author Przemysław Żydek
 */
class Renew extends Cron {

	const HOOK = 'wpk_campaign_renew';

	/**
	 * @param array $params
	 *
	 * @return void
	 */
	public function handle( array $params = [] ) {

		$campaign = Campaign::find( $params[ 'campaign_id' ] );

	}

}