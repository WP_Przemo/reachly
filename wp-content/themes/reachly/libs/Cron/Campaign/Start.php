<?php


namespace Wpk\Cron\Campaign;

use Wpk\Core;
use Wpk\Cron\Cron;
use Wpk\Models\Campaign;

/**
 * Cron class for campaign start
 *
 * @author Przemysław Żydek
 */
class Start extends Cron {

	const HOOK = 'wpk_start_campaign';

	/**
	 * Start constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wpk/campaign/published', [ $this, 'createTask' ] );
		add_action( 'before_delete_post', [ $this, 'deleteTask' ] );
	}

	/**
	 * Delete cron task on campaign removal
	 *
	 * @param int $postID
	 *
	 * @return void
	 */
	public function deleteTask( int $postID ) {

		if ( get_post_type( $postID ) !== Core::CAMPAIGN ) {
			return;
		}

		$args = [
			'campaign_id' => $postID,
		];

		wp_clear_scheduled_hook( self::HOOK, [ $args ] );

	}

	/**
	 * Create cron task on campaign publish
	 *
	 * @param Campaign $campaign
	 *
	 * @return void
	 */
	public function createTask( Campaign $campaign ) {

		$startDate = $campaign->startDate( true );

		$args = [
			'campaign_id' => $campaign->ID,
		];

		wp_schedule_single_event( $startDate->getTimestamp(), self::HOOK, [ $args ] );

	}

	/**
	 * @param array $params
	 *
	 * @return void
	 */
	public function handle( array $params = [] ) {

		/** @var Campaign $campaign */
		$campaign = Campaign::find( $params[ 'campaign_id' ] );

		if ( ! $campaign->haveReachedImpressions() || $campaign->post_status !== 'publish' ) {
			do_action( 'wpk/campaign/startFailed', $campaign );

			return;
		}

		$campaign->addToAddInfluencers()->status( 'started' )->update();

		do_action( 'wpk/campaign/started', $campaign );

	}

}