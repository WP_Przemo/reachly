<?php


namespace Wpk\Cron\User;

use Wpk\Cron\Cron;
use Wpk\Models\User;

/**
 * Cron task for updating instagram data
 *
 * @author Przemysław Żydek
 */
class Instagram extends Cron {

	/** @var string Hook used for cron task */
	const HOOK = 'wpk_instagram_schedule';

	/**@var string */
	const DATE = '+6 hours';

	/**
	 * Instagram constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wpk/user/instagramDataUpdated', [ $this, 'create' ] );
	}

	/**
	 * @param User $user
	 *
	 * @return void
	 */
	public function create( User $user ) {

		$args = [
			'user_id' => $user->ID,
		];

		wp_schedule_single_event( strtotime( self::DATE ), self::HOOK, $args );

	}


	/**
	 * @param array $params
	 *
	 * @return void
	 */
	public function handle( array $params = [] ) {

		$userID = $params[ 'user_id' ];
		$user   = User::find( $userID );

		if ( ! $user->updateInstagramData( true ) ) {
			$user->removeApiToken( 'instagram' );
		}

	}
}