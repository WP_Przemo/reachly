<?php

namespace Wpk\Cron\CampaignInvitation;

use Wpk\Core;
use Wpk\Models\CampaignInvitation;

/**
 * Manages cron tasks for invitations
 *
 * @author Przemysław Żydek
 */
class Cron {

	/** @var string */
	const INVITE_HOOK = 'wpk_campaign_invitation_expire';

	/** @var string */
	const COLLABORATE_HOOK = 'wpk_campaign_collaborate_expire';

	/**
	 * Cron constructor.
	 */
	public function __construct() {
		add_action( 'wpk/campaign/invitationCreated', [ $this, 'createTaskForInvite' ] );
		add_action( 'wpk/campaign/collaborationCreated', [ $this, 'createTaskForInvite' ] );
		add_action( 'before_delete_post', [ $this, 'removeTask' ] );

		add_action( self::INVITE_HOOK, [ $this, 'removeExpiredInvite' ] );
		add_action( self::COLLABORATE_HOOK, [ $this, 'removeExpiredCollaboration' ] );
	}

	/**
	 * On invite creation creates cron task to remove the invite on expire date
	 *
	 * @param CampaignInvitation $invite
	 *
	 * @return void
	 */
	public function createTaskForInvite( CampaignInvitation $invite ) {
		wp_schedule_single_event( $invite->meta( 'expires' ), self::INVITE_HOOK, [ $invite->ID ] );
	}

	/**
	 * On collaboration creation creates cron task to remove the invite on expire date
	 *
	 * @param CampaignInvitation $invite
	 *
	 * @return void
	 */
	public function createTaskForCollaboration( CampaignInvitation $invite ) {
		wp_schedule_single_event( $invite->meta( 'expires' ), self::COLLABORATE_HOOK, [ $invite->ID ] );
	}

	/**
	 * Unschedule cron task if invite was removed manually
	 *
	 * @param int $postID
	 */
	public function removeTask( int $postID ) {

		if ( get_post_type( $postID ) === Core::CAMPAIGN_INVITATION ) {

			$invite  = CampaignInvitation::find( $postID );
			$expires = $invite->meta( 'expires' );

			wp_unschedule_event( $expires, self::INVITE_HOOK, [ $invite->ID ] );

		}

	}

	/**
	 * Remove expired invitation
	 *
	 * @param int $inviteID
	 *
	 * @return void
	 */
	public function removeExpiredInvite( int $inviteID ) {

		$invite = CampaignInvitation::find( $inviteID );

		do_action( 'wpk/campaign/invitationExpired', $invite );

		$invite->delete();

	}


	/**
	 * Remove expired collaboration offer
	 *
	 * @param int $inviteID
	 *
	 * @return void
	 */
	public function removeExpiredCollaboration( int $inviteID ) {

		$invite     = CampaignInvitation::find( $inviteID );
		$influencer = $invite->getAuthor();
		$campaign   = $invite->getCampaign();

		$campaign->skipInfluencer($influencer);

		do_action( 'wpk/campaign/collaborationExpired', $invite );

		$invite->delete();

	}

}