<?php


namespace Wpk\Cron;

/**
 * @author Przemysław Żydek
 */
abstract class Cron {

	/** @var string Hook used for cron task */
	const HOOK = '';

	/**
	 * Cron constructor.
	 */
	public function __construct() {
		add_action( static::HOOK, [ $this, 'handle' ] );
	}

	abstract public function handle( array $params = [] );

}