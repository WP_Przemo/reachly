<?php


namespace Wpk\Templates;

/**
 * Abstract class for creating new cron schedules.
 *
 * @author Przemysław Żydek
 */
abstract class Schedule {

	/** @var string Name of hook used for this schedule */
	protected $hook = '';

	/** @var array Array of methods uses as callbacks for this schedule */
	protected $callbacks = [];

	/** @var string Determines how often schedule should run (hourly, twicedaily, daily) */
	protected $recurrence = 'hourly';

	/** @var array Args for schedule callbacks */
	protected $args = [];


	/**
	 * Schedule constructor.
	 *
	 */
	public function __construct() {

		if ( empty( $this->hook ) ) {
			return;
		}

		$this->setupActivationHooks();
		$this->setupHooks();

	}

	/**
	 * Setups schedule activation hooks
	 */
	protected function setupActivationHooks() {

		add_action( 'after_switch_theme', [ $this, 'createSchedule' ] );
		add_action( 'switch_theme', [ $this, 'removeSchedule' ] );


	}

	/**
	 * Setups schedule hooks
	 */
	protected function setupHooks() {

		foreach ( $this->callbacks as $callback ) {
			add_action( $this->hook, [ $this, $callback ] );
		}

	}

	/**
	 * Creates schedule after theme activation
	 *
	 * @return void
	 */
	public function createSchedule() {

		/* TODO Configurable timestamp? */
		wp_schedule_event( time(), $this->recurrence, $this->hook, $this->args );

	}

	/**
	 * Clears schedule after changing theme
	 *
	 * @return void
	 */
	public function removeSchedule() {

		wp_clear_scheduled_hook( $this->hook, $this->args );

	}

}