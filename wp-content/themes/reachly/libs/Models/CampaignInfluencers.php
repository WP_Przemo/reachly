<?php


namespace Wpk\Models;

use Wpk\Models\Traits;

/**
 * @autro Przemysław Żydek
 */
class CampaignInfluencers extends Entity {

	use Traits\Sql;

	protected $table = 'influencers';

	/**
	 * CampaignInfluencers constructor.
	 *
	 * @param null $attributes
	 */
	public function __construct( $attributes = null ) {

		if ( is_numeric( $attributes ) ) {
			$this->getById( $attributes );
		} else {
			parent::__construct( $attributes );
		}
	}

	/**
	 * @param int $id
	 *
	 * @return void
	 */
	protected function getById( int $id ) {

		global $wpdb;

		$query = "SELECT * FROM {$this->getTable()} WHERE id = $id";

		$data         = $wpdb->get_results( $query );
		$this->entity = $data;

	}

	/**
	 * @return bool|static
	 */
	public function create() {

		global $wpdb;

		if ( ! $wpdb->insert( $this->getTable(), $this->attributes[ 'values' ] ) ) {
			return false;
		}

		return static::find( $wpdb->insert_id );

	}

	/**
	 * Execute query
	 *
	 * @return Collection
	 */
	public function get(): Collection {

		global $wpdb;

		$query = $this->buildResultsQuery();

		$results = $wpdb->get_results( $query, ARRAY_A );

		return Collection::make( $results );

	}

	/**
	 * @return self
	 */
	public function update() {
		// TODO: Implement update() method.
	}

	/**
	 * @return bool
	 */
	public function delete(): bool {
		// TODO: Implement delete() method.
	}
}