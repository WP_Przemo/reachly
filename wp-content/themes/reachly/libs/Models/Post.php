<?php

namespace Wpk\Models;

use Wpk\Models\Traits;
use Wpk\Utility;

/**
 * Model class for WordPress posts
 *
 * @author Przemysław Żydek
 */
class Post extends Entity {

	use Traits\Post\HandleTaxonomy,
		Traits\HandleMetaFields;

	protected $entityClass = \WP_Post::class;

	protected $entityInstanceMethod = 'get_instance';

	protected $queryClass = \WP_Query::class;

	protected $queryResultsMethod = 'get_posts';

	protected $getMetaFunction = 'get_post_meta';

	protected $updateMetaFunction = 'update_post_meta';

	protected $addMetaFunction = 'add_post_meta';

	protected $deleteMetaFunction = 'delete_post_meta';

	/**
	 * @return string
	 */
	public function __toString(): string {
		return $this->hasEntity() ? $this->post_content : '';
	}

	/**
	 * Rename certain attributes so that they will fit in @see wp_insert_post()
	 *
	 * @return self
	 */
	protected function parseCreationAttributes(): self {

		if ( ! empty( $this->attributes[ 'author' ] ) ) {
			$this->attributes[ 'post_author' ] = $this->attributes[ 'author' ];
		}

		return $this;

	}

	/**
	 * @return bool|static
	 */
	public function create() {

		if ( empty( $this->attributes ) ) {
			return false;
		}

		$taxInput = [];

		if ( ! empty( $this->attributes[ 'tax_input' ] ) ) {
			$taxInput = $this->attributes[ 'tax_input' ];
			unset( $this->attributes[ 'tax_input' ] );
		}

		$this->parseCreationAttributes();

		$post = wp_insert_post( $this->attributes );

		/*
		 * Manually add new taxonomies.
		 * The reason for that is "tax_input" in wp_insert_post must be passed as terms id, and sometimes we want to use slug or name instead :>
		 * */
		if ( $post ) {

			$post = new static( $post );

			if ( ! empty( $taxInput ) ) {
				foreach ( $taxInput as $taxonomy => $terms ) {
					$post->setTerms( $terms, $taxonomy );
				}
			}

		}


		return $post;

	}

	/**
	 * Add post status to query
	 *
	 * @param string|array $status
	 *
	 * @return self
	 */
	public function status( $status ): self {

		$this->attributes[ 'post_status' ] = $status;

		return $this;

	}

	/**
	 * @param string|array $type
	 *
	 * @return self
	 */
	public function type( string $type ): self {

		$this->attributes[ 'post_type' ] = $type;

		return $this;

	}

	/**
	 *
	 * @param string $title
	 *
	 * @return self
	 */
	public function title( string $title ): self {

		$this->attributes[ 'post_title' ] = $title;

		return $this;

	}

	/**
	 * @param string $content
	 *
	 * @return Post
	 */
	public function content( string $content ): self {

		$this->attributes[ 'post_content' ] = $content;

		return $this;

	}

	/**
	 * @param string $fields
	 *
	 * @return self
	 */
	public function fields( string $fields ): self {

		$this->attributes[ 'fields' ] = $fields;

		return $this;

	}

	/**
	 * @param int $amount
	 *
	 * @return self
	 */
	public function perPage( int $amount ): self {

		$this->attributes[ 'posts_per_page' ] = $amount;

		return $this;

	}

	/**
	 * @param int $page
	 *
	 * @return self
	 */
	public function paged( int $page ): self {

		$this->attributes[ 'paged' ] = $page;

		return $this;

	}

	/**
	 * Set order of posts. Must be called before @see Entity::get()
	 *
	 * @param array|string $by
	 * @param string       $how
	 * @param string|null  $metaKey Optional, if order should be set by meta key
	 *
	 * @return self
	 */
	public function order( $by, string $how = 'ASC', string $metaKey = null ): self {

		$this->attributes[ 'order' ]   = $how;
		$this->attributes[ 'orderby' ] = $by;

		if ( ! empty( $metaKey ) ) {
			$this->attributes[ 'meta_key' ] = $metaKey;
		}

		return $this;

	}

	/**
	 * @param int $authorID
	 *
	 * @return self
	 */
	public function author( int $authorID ): self {

		$this->attributes[ 'author' ] = $authorID;

		return $this;

	}

	/**
	 * @param mixed $authors
	 *
	 * @return self
	 */
	public function authorNotIn( $authors ): self {

		if ( ! is_array( $authors ) ) {
			$authors = [ $authors ];
		}

		$notIn = &$this->attributes[ 'author__not_in' ];

		if ( ! is_array( $notIn ) ) {
			$notIn = [];
		}

		$notIn = array_merge( $authors, $notIn );

		return $this;

	}

	/**
	 * @return self
	 */
	public function deleteThumbnail(): self {
		delete_post_thumbnail( $this->ID );

		return $this;
	}

	/**
	 * @return string
	 */
	public function getThumbnailUrl(): string {
		return get_the_post_thumbnail_url( $this->ID );
	}

	/**
	 * Set post thumbnail
	 *
	 * @param int $attachmentID
	 *
	 * @return bool
	 */
	public function thumnbail( int $attachmentID ): bool {
		return set_post_thumbnail( $this->ID, $attachmentID );
	}

	/**
	 * Execute query
	 *
	 * @return Collection
	 */
	public function get(): Collection {

		$result = [];

		$query = new \WP_Query( $this->attributes );

		foreach ( $query->get_posts() as $post ) {
			$result[] = new static( $post );
		}

		return Collection::make( $result, $query );

	}

	/**
	 * @return self
	 */
	public function update(): self {

		$this
			->parseCreationAttributes()
			->attributes[ 'ID' ] = $this->ID;

		$result = wp_update_post( $this->attributes );

		if ( is_wp_error( $result ) ) {
			Utility::log( $result->get_error_messages(), 'POST_UPDATE_ERROR' );

			return $this;
		}

		return new static( $this->ID );

	}

	/**
	 * @return bool
	 */
	public function delete(): bool {
		return (bool) wp_delete_post( $this->ID, true );
	}

	/**
	 * @return string
	 */
	public function getPermalink(): string {
		return get_permalink( $this->ID );
	}

	/**
	 * @return User
	 */
	public function getAuthor(): User {
		return User::find( $this->post_author );
	}

}