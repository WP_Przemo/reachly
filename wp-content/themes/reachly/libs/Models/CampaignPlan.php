<?php


namespace Wpk\Models;

use Wpk\Core;

/**
 * Campaign plan model
 *
 * @author Przemysław Żydek
 */
class CampaignPlan extends Post {

	/**
	 * Get plan prices in array (length = > value)
	 *
	 * @return array
	 */
	public function prices(): array {

		return [
			'1'  => (int) $this->meta( 'month_price' ),
			'3'  => (int) $this->meta( 'three_months_price' ),
			'6'  => (int) $this->meta( 'six_months_price' ),
			'12' => (int) $this->meta( 'twelve_months_price' ),
		];

	}

	/**
	 * @return int
	 */
	public function getImpressions(): int {
		return (int) $this->meta( 'impressions' );
	}

	/**
	 * @return int
	 */
	public function getRequiredImpressions(): int {
		return (int) $this->meta( 'required_impressions' );
	}

	/**
	 * @return bool|self
	 */
	public function create() {
		$this->attributes[ 'post_type' ] = Core::CAMPAIGN_PLAN;

		return parent::create();
	}

	/**
	 * @return Collection
	 */
	public function get(): Collection {
		$this->attributes[ 'post_type' ] = Core::CAMPAIGN_PLAN;

		return parent::get();
	}

}