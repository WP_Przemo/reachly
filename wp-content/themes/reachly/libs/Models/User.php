<?php

namespace Wpk\Models;

use Facebook\Authentication\AccessToken;
use Wpk\Api\Helpers\Insight;
use Wpk\Api\Instagram\Api;
use Wpk\Api\Instagram\Media;
use Wpk\Cron\User\Instagram;
use Wpk\Models\Traits;
use Wpk\Utility;

/**
 * Model class for WordPress users
 *
 * @author Przemysław Żydek
 */
class User extends Entity {

	use Traits\HandleMetaFields;

	/** @var array */
	const TYPES = [
		'influencer',
		'company',
	];

	/** @var \WP_User */
	public $entity;

	public $attributes = [];

	protected $entityClass = \WP_User::class;

	protected $entityInstanceMethod = 'get_data_by';

	protected $getMetaFunction = 'get_user_meta';

	protected $updateMetaFunction = 'update_user_meta';

	protected $addMetaFunction = 'add_user_meta';

	protected $deleteMetaFunction = 'delete_user_meta';

	/** @var array Contains all available genders */
	public static $genders = [
		'male'   => 'Men',
		'female' => 'Women',
	];

	/**
	 * User constructor.
	 *
	 * @param mixed $attributes
	 */
	public function __construct( $attributes = null ) {

		if ( is_numeric( $attributes ) ) {
			$this->entity = new \WP_User( $attributes );
		} else {
			parent::__construct( $attributes );
		}

	}

	/**
	 * Get current user ID
	 *
	 * @return int
	 */
	public static function id(): int {
		return get_current_user_id();
	}

	/**
	 * @param string $firstName
	 *
	 * @return self
	 */
	public function firstName( string $firstName ): self {

		$this->attributes[ 'first_name' ] = $firstName;

		return $this;

	}

	/**
	 * @param string $lastName
	 *
	 * @return self
	 */
	public function lastName( string $lastName ): self {

		$this->attributes[ 'last_name' ] = $lastName;

		return $this;

	}

	/**
	 * @param string $login
	 *
	 * @return self
	 */
	public function login( string $login ): self {

		$this->attributes[ 'login' ] = $login;

		return $this;

	}

	/**
	 * @param string $email
	 *
	 * @return self
	 */
	public function email( string $email ): self {

		$this->attributes[ 'user_email' ] = $email;

		return $this;

	}

	/**
	 * Searches for possible string matches on columns
	 *
	 * @param string $value
	 *
	 * @return User
	 */
	public function search( string $value ): self {
		$this->attributes[ 'search' ] = $value;

		return $this;
	}

	/**
	 * @param string $password
	 *
	 * @return self
	 */
	public function password( string $password ): self {

		if ( $this->hasEntity() ) {

			wp_password_change_notification( $this->entity );
			wp_set_password( $password, $this->ID );
			wp_set_auth_cookie( $this->ID, true );

		} else {

			$this->attributes[ 'password' ] = $password;

		}

		return $this;

	}

	/**
	 * @param string|array $cap
	 *
	 * @return self
	 */
	public function cap( $cap ): self {

		if ( is_array( $cap ) ) {
			$this->attributes[ 'cap' ] = array_merge( $this->attributes[ 'cap' ], $cap );
		} else {

			$this->attributes[ 'cap' ][] = $cap;

		}

		return $this;

	}

	/**
	 * @param array $items
	 *
	 * @return self
	 */
	public function exclude( array $items ): self {

		if ( ! is_array( $this->attributes[ 'exclude' ] ) ) {
			$this->attributes[ 'exclude' ] = [];
		}

		$this->attributes[ 'exclude' ] = array_merge( $items, $this->attributes[ 'exclude' ] );

		return $this;
	}

	/**
	 * @param array $items
	 *
	 * @return self
	 */
	public function include( array $items ): self {

		if ( ! is_array( $this->attributes[ 'include' ] ) ) {
			$this->attributes[ 'include' ] = [];
		}

		$this->attributes[ 'include' ] = array_merge( $items, $this->attributes[ 'include' ] );

		return $this;
	}

	/**
	 * @param string|array $role
	 *
	 * @return self
	 */
	public function role( $role ): self {

		if ( is_array( $role ) ) {
			$this->attributes[ 'role' ] = array_merge( $this->attributes[ 'role' ], $role );
		} else {

			$this->attributes[ 'role' ][] = $role;

		}

		return $this;

	}

	/**
	 * @return bool|static
	 */
	public function create() {

		$attributes = $this->attributes;

		if ( empty( $attributes[ 'password' ] ) ) {
			$attributes[ 'password' ] = wp_generate_password();
		}

		$user = wp_create_user( $attributes[ 'login' ], $attributes[ 'password' ], $attributes[ 'email' ] );

		if ( is_wp_error( $user ) ) {
			Utility::log( $user->get_error_messages(), 'USER_CREATE_ERROR' );

			return false;
		}

		$user = new static( $user );

		foreach ( $this->attributes[ 'role' ] as $role ) {
			$user->entity->add_role( $role );
		}

		foreach ( $this->attributes[ 'cap' ] as $cap ) {
			$user->entity->add_cap( $cap );
		}

		$user->addMetas( $this->attributes[ 'meta_input' ] );

		return $user;

	}

	/**
	 * @param string $api
	 *
	 * @return mixed
	 */
	public function getApiToken( string $api ) {

		//They use the same token
		if ( $api === 'instagram' ) {
			$api = 'facebook';
		}

		return $this->meta( "{$api}_token" );
	}

	/**
	 * @return array
	 */
	public function getInterests(): array {
		return (array) $this->meta( 'interests' );
	}

	/**
	 * @param string $api
	 * @param mixed  $token
	 *
	 * @return self
	 */
	public function apiToken( string $api, $token ): self {

		//They use the same token
		if ( $api === 'instagram' ) {
			$api = 'facebook';
		}

		return $this->updateMeta( "{$api}_token", $token );
	}

	/**
	 * @param string $api
	 *
	 * @return self
	 */
	public function removeApiToken( string $api ): self {

		if ( $api === 'instagram' ) {
			$api = 'facebook';
		}

		return $this->deleteMeta( "{$api}_token" );
	}

	/**
	 * @return string
	 */
	public function getAvatar(): string {

		$avatar = $this->meta( 'avatar' );

		if ( Utility::contains( $avatar, 'http' ) ) {
			return $avatar;
		}

		return wp_get_attachment_url( $avatar );
	}

	/**
	 * @return bool
	 */
	public function isBillingFilled(): bool {

		$address  = $this->meta( 'address' );
		$postcode = $this->meta( 'zip' );
		$city     = $this->meta( 'city' );
		$company  = $this->meta( 'company' );

		return ! empty( $address ) && ! empty( $postcode ) && ! empty( $city ) && ! empty( $company );

	}

	/**
	 * @param int $amount
	 *
	 * @return self
	 */
	public function perPage( int $amount ): self {
		$this->attributes[ 'per_page' ] = $amount;

		return $this;
	}

	/**
	 * @param int $page
	 *
	 * @return self
	 */
	public function page( int $page ): self {
		$this->attributes[ 'page' ] = $page;

		return $this;
	}

	/**
	 * Execute query
	 *
	 * @return Collection
	 */
	public function get(): Collection {

		$result = [];

		$query   = new \WP_User_Query( $this->attributes );
		$results = $query->get_results();

		//If we have pagination params, build main query with them
		if ( ! empty( $this->attributes[ 'per_page' ] ) ) {

			$totalUsers = count( $results );

			$page    = empty( $this->attributes[ 'page' ] ) ? 1 : $this->attributes[ 'page' ];
			$perPage = $this->attributes[ 'per_page' ];

			$offset     = $perPage * ( $page - 1 );
			$totalPages = ceil( $totalUsers / $perPage );

			$this->attributes[ 'number' ]      = $perPage;
			$this->attributes[ 'total_pages' ] = $totalPages;
			$this->attributes[ 'offset' ]      = $offset;

			$results = ( new \WP_User_Query( $this->attributes ) )->get_results();

		}

		/*$hasExclude     = isset( $this->attributes[ 'exclude' ] );
		$isArrayExclude = $hasExclude ? is_array( $this->attributes[ 'exclude' ] ) : false;*/

		foreach ( $results as $post ) {

			/*
			if ( $hasExclude && $isArrayExclude && in_array( $post->ID, $this->attributes[ 'exclude' ] ) ) {
			}*/

			$result[] = new static( $post );
		}


		return Collection::make( $result );

	}

	/**
	 * @return self
	 */
	public static function current(): self {
		return self::find( get_current_user_id() );
	}

	/**
	 * @return self|\WP_Error
	 */
	public function update() {

		$this->attributes[ 'ID' ] = $this->ID;

		$update = wp_update_user( $this->attributes );

		if ( is_wp_error( $update ) ) {
			Utility::log( $update->get_error_messages(), 'UPDATE_USER_ERROR' );

			return $update;
		}

		//Return updated model
		return new static( $this->ID );

	}

	/**
	 * @return bool
	 */
	public function delete(): bool {
		return (bool) wp_delete_user( $this->ID );
	}

	/**
	 * Perform update from instagram api
	 *
	 * @param bool $includeMedia
	 *
	 * @return bool
	 */
	public function updateInstagramData( bool $includeMedia = true ): bool {

		$accountID = $this->meta( 'instagram_id' );
		$token     = $this->getApiToken( 'instagram' );

		$data     = Api::getAccountData( $accountID, $token );
		$insights = Api::getInstagramInsights( $accountID, $token );

		if ( empty( $data ) || empty( $insights ) ) {
			$this->removeApiToken( 'instagram' );

			return false;
		}

		$meta = [];

		$meta[ 'avatar' ]             = $data[ 'profile_picture_url' ];
		$meta[ 'instagram_insights' ] = $insights;

		//Add instagram prefix to meta keys
		foreach ( $data as $key => $value ) {

			$key          = "instagram_${key}";
			$meta[ $key ] = $value;

		}

		//Save insights values for filtering
		$cities = get_option( 'wpk_cities', [] );

		/** @var Insight $insight */
		foreach ( $insights as $key => $insight ) {

			$value = $insight->value;

			//Sometimes insight values are also array, save them separately for filtering
			if ( is_array( $value ) ) {
				foreach ( $value as $index => $item ) {

					$_index = str_replace( [ ',', ' ' ], [ '_' ], strtolower( $index ) );

					$metakey          = "instagram_{$key}_{$_index}";
					$meta[ $metakey ] = $item;

					//Save instagram city in taxonomy as well
					if ( $key === 'audience_city' ) {
						$cities[ $_index ] = $index;
					}

				}
			} else {
				$meta[ "instagram_{$key}" ] = $value;
			}

		}

		update_option( 'wpk_cities', $cities );

		$this->updateMetas( $meta );

		if ( $includeMedia ) {
			$media = Api::getUserMedia( $this, false );

			if ( ! $media ) {
				return false;
			}

			$this->updateSocialMeta( 'instagram', 'media', $media )->calculateAverages( $media, 'instagram' );
		}

		do_action( 'wpk/user/instagramDataUpdated', $this );

		return true;

	}

	/**
	 * Calculates average values (likes per post, comments per post)
	 *
	 * @param array  $media Array of uploaded medias
	 * @param string $socialMedia
	 */
	protected function calculateAverages( array $media, string $socialMedia ) {

		$totalLikes      = 0;
		$totalComments   = 0;
		$averageLikes    = 0;
		$averageComments = 0;

		//Variables for counting engagment rate
		$engagmentRate = 0;
		$followers     = (int) $this->socialMeta( $socialMedia, 'followers_count' );

		if ( ! empty( $media ) ) {

			/** @var Media $item */
			foreach ( $media as $item ) {
				$likes    = $item->likes;
				$comments = $item->commentsCount;

				if ( $likes > 0 ) {
					$totalLikes ++;
					$averageLikes = $averageLikes + $likes;
				}

				if ( $comments > 0 ) {
					$totalComments ++;
					$averageComments = $averageComments + $likes;
				}

				$likesAndComments = $likes + $comments;
				$engagmentRate    += $likesAndComments / $followers;

			}
		}
		$comments      = $averageComments > 0 ? $averageComments / $totalComments : $averageComments;
		$likes         = $averageComments > 0 ? $averageLikes / $totalLikes : $likes;
		$engagmentRate = ( $engagmentRate / count( $media ) ) * 100;

		$this->updateSocialMeta( 'instagram', 'engagment_rate', floor( $engagmentRate ) );
		$this->updateSocialMeta( 'instagram', 'comments_number', floor( $comments ) );
		$this->updateSocialMeta( 'instagram', 'likes_per_post', floor( $likes ) );

	}

	/**
	 * Search for influencers
	 *
	 * @return self
	 */
	public function influencers(): self {
		return $this->hasMetaValue( 'type', 'influencer' );
	}

	/**
	 * Get social media associated with campaign
	 *
	 * @param Campaign $campaign
	 * @param mixed    $metaKey
	 *
	 * @return mixed
	 */
	public function campaignSocialMeta( Campaign $campaign, $metaKey ) {

		$social = $campaign->meta( 'social_media' );

		return $this->socialMeta( $social, $metaKey );

	}

	/**
	 * @param string $social
	 * @param mixed  $key
	 *
	 * @return mixed
	 */
	public function socialMeta( string $social, $key ) {
		return $this->meta( "{$social}_{$key}" );
	}

	/**
	 * @param string $social
	 * @param mixed  $key
	 * @param mixed  $value
	 *
	 * @return self
	 */
	public function updateSocialMeta( string $social, $key, $value ): self {
		return $this->updateMeta( "{$social}_{$key}", $value );
	}

	/**
	 * @param Campaign $campaign Campaign from which social impressions should be get
	 *
	 * @return int
	 */
	public function getImpressions( Campaign $campaign ): int {
		return (int) $this->campaignSocialMeta( $campaign, 'impressions' );
	}

	/**
	 * Get instagram profile url
	 *
	 * @return string
	 */
	public function getInstagramProfileUrl(): string {
		$name = $this->socialMeta( 'instagram', 'username' );

		return "https://www.instagram.com/$name";
	}

	/**
	 * Checks if user is verified
	 *
	 * TODO Add support for other social medias
	 *
	 * @return bool
	 */
	public function hasValidToken(): bool {

		/** @var AccessToken $token */
		$token       = $this->getApiToken( 'facebook' );
		$instagramID = $this->socialMeta( 'instagram', 'id' );

		//Make test api call to check if token is valid
		$accountData = Api::getAccountData( $instagramID, $token, [ 'id' ] );

		return (bool) $accountData;

	}

	/**
	 * Get company logo
	 *
	 * @return string
	 */
	public function getLogo(): string {
		return wp_get_attachment_url( $this->meta( 'logo' ) );
	}

}