<?php


namespace Wpk\Models;

use Wpk\Core;
use Wpk\Pages;
use Wpk\Utility;


class Campaign extends Post {

	/** @var int */
	const PLAN_STEP = 1;

	/** @var int */
	const PAYMENT_STEP = 2;

	/** @var int */
	const DETAILS_STEP = 3;

	/** @var int */
	const INFLUENCERS_STEP = 4;

	/** @var string */
	const DATE_FORMAT = 'd-m-y';


	/**
	 * Checks if campaigns is fully created
	 *
	 * @return bool
	 */
	public function fullyCreated(): bool {
		return $this->meta( 'fully_created' );
	}

	/**
	 * Get current creation step for this campaign
	 *
	 * @return int
	 */
	public function creationStep(): int {
		return (int) $this->meta( 'creation_step' );
	}

	/**
	 * @param bool $asDateTime
	 *
	 * @return string|\DateTime
	 */
	public function startDate( bool $asDateTime = false ) {

		$startDate = $this->meta( 'start_date' );

		if ( empty( $startDate ) ) {
			return '';
		}

		$date = new \DateTime( $startDate );

		return $asDateTime ? $date : $date->format( DATE_FORMAT );

	}

	/**
	 * @return CampaignPlan
	 */
	public function getPlan(): CampaignPlan {
		return CampaignPlan::find( $this->meta( 'plan' ) );
	}

	public function creationUrl(): string {
		return Pages::getCampaignCreationUrl( $this->creationStep(), $this->ID );
	}

	/**
	 * @return bool|self
	 */
	public function create() {
		$this->attributes[ 'post_type' ] = Core::CAMPAIGN;

		return parent::create();
	}

	/**
	 * @return Collection
	 */
	public function get(): Collection {
		$this->attributes[ 'post_type' ] = Core::CAMPAIGN;

		return parent::get();
	}

	/**
	 * @return string
	 */
	public function getLogoUrl(): string {
		return wp_get_attachment_url( $this->meta( 'logo' ) );
	}

	/**
	 * @param array $args
	 *
	 * @return array
	 */
	public static function getCategories( array $args = [] ): array {

		$args = array_merge( [
			'taxonomy'   => 'campaign_category',
			'hide_empty' => false,
		], $args );

		return get_terms( $args );

	}

	/**
	 * Get campaign tags
	 *
	 * @param array $args
	 *
	 * @return array
	 */
	public static function getTags( array $args = [] ): array {

		$args = array_merge( [
			'taxonomy'   => 'campaign_tags',
			'hide_empty' => false,
		], $args );

		return get_terms( $args );

	}

	/**
	 * @return bool|\WC_Order|
	 */
	public function wcOrder() {

		$orderID = $this->meta( 'order_id' );

		return empty( $orderID ) ? false : wc_get_order( $orderID );

	}

	/**
	 * @return bool
	 */
	public function hasEnded(): bool {
		return $this->endDate( true )->getTimestamp() <= time();
	}

	/**
	 * @return bool
	 */
	public function hasStarted(): bool {
		return $this->startDate( true )->getTimestamp() <= time() && $this->post_status === 'started';
	}

	/**
	 * Get campaign initial impressions
	 *
	 * @return int
	 */
	public function getInitialImpressions(): int {
		return (int) $this->meta( 'initial_impressions' );
	}

	/**
	 * @return int
	 */
	public function getImpressions(): int {
		return (int) $this->meta( 'impressions' );
	}

	/**
	 * @param int $impressions
	 *
	 * @return self
	 */
	public function setImpressions( int $impressions ): self {
		return $this->updateMeta( 'impressions', $impressions );
	}

	/**
	 * @param int $amount
	 *
	 * @return self
	 */
	public function subtractImpressions( int $amount ): self {

		$impressions = $this->getImpressions();
		$impressions -= $amount;

		return $this->setImpressions( $impressions );

	}

	/**
	 * @param int $amount
	 *
	 * @return self
	 */
	public function addImpressions( int $amount ): self {

		$impressions = $this->getImpressions();
		$impressions += $amount;

		return $this->setImpressions( $impressions );

	}

	/**
	 * @return int
	 */
	public function getLength(): int {
		return (int) $this->meta( 'length' );
	}

	/**
	 * Get campaign URL based on its state
	 *
	 * @return string
	 */
	public function getUrl(): string {
		return $this->fullyCreated() ? get_permalink( $this->ID ) : $this->creationUrl();
	}

	/**
	 * @param string $ref
	 *
	 * @return string
	 */
	public function getEditUrl( string $ref = '' ): string {

		if ( ! $this->fullyCreated() ) {
			return $this->creationUrl();
		}

		$url     = Pages::getEditCampaignPageUrl();
		$referer = ! empty( $ref ) ? $_GET[ 'ref' ] : Utility::getCurrentUrl();

		return add_query_arg( [
			'id'     => $this->ID,
			'action' => 'edit',
			'ref'    => urlencode( $referer ),
		], $url );

	}

	/**
	 * @param string $ref
	 *
	 * @return string
	 */
	public function getInfluencersEditUrl( string $ref = '' ): string {

		$url     = get_permalink( Pages::getEditCampaignPage() );
		$referer = ! empty( $ref ) ? $ref : Utility::getCurrentUrl();

		return add_query_arg( [
			'id'     => $this->ID,
			'action' => 'influencers',
			'ref'    => urlencode( $referer ),
		], $url );

	}

	/**
	 * @param bool $asDateTime
	 *
	 * @return \DateTime|string
	 */
	public function endDate( bool $asDateTime = false ) {

		$start  = $this->startDate( true );
		$length = $this->getLength();

		$start->modify( "+$length months" );

		return $asDateTime ? $start : $start->format( DATE_FORMAT );

	}

	/**
	 * @param bool $asDateTime
	 *
	 * @return \DateTime|string
	 */
	public function renewedDate( bool $asDateTime = false ) {
		$date = new \DateTime( $this->meta( 'renewed_date' ) );

		return $asDateTime ? $date : $date->format( DATE_FORMAT );
	}

	/**
	 * Get renewal date of campaign (renewed date + 3 months).
	 *
	 * @param bool $asDateTime Whenever return it as DateTime object
	 *
	 * @return string|\DateTime
	 */
	public function renewalDate( bool $asDateTime = false ) {

		$date = $this->renewedDate( true );

		$length = $this->getLength();

		if ( $length < 3 ) {
			$date->modify( "+$length months" );
		} else {
			$date->modify( '+3 months' );
		}

		return $asDateTime ? $date : $date->format( DATE_FORMAT );

	}

	/**
	 * Check if current campaign can be modified (there are more than 14 days before start date)
	 *
	 * @param string $type Type of date to compare to (start_date, or renewed_date)
	 *
	 * @return bool
	 */
	public function canBeModified( string $type = 'start_date' ): bool {

		$date      = $this->meta( $type );
		$startDate = strtotime( $date );
		$length    = $this->getLength();

		if ( $type === 'start_date' ) {
			$days = Utility::subtractWorkDays( 7 )->getTimestamp();

			return $days <= $startDate;
		} else if ( $type === 'renewed_date' ) {

			$today = time();

			$renewal_date = $this->renewalDate( true );

			if ( $length <= 3 || in_array( $this->post_status, [ 'draft', 'stopped', 'ended' ] ) ) {
				return false;
			}

			$renewal          = Utility::subtractWorkDays( 14, clone $renewal_date );
			$renewalSevenDays = Utility::subtractWorkDays( 7, clone $renewal_date );


			return $today >= $renewal->getTimestamp() && $today <= $renewalSevenDays->getTimestamp() ||
			       date( DATE_FORMAT ) == $renewal->format( DATE_FORMAT );

		} else {
			return false;
		}

	}

	/**
	 * Checks if campaign can be renewed.
	 * If there are less than seven days before renewal date, return true.
	 * Otherwise returns false.
	 *
	 * @return bool
	 */
	public function canBeRenewed(): bool {

		$renewalDate = $this->renewalDate( true );

		if ( empty( $renewalDate ) ) {
			return false;
		}
		$now = time();

		//Renewal date have passed
		if ( $now > $renewalDate->getTimestamp() ) {
			return false;
		}

		$renewalDate->modify( '-7 days' );

		return $now > $renewalDate->getTimestamp();

	}

	/**
	 * Get skipped influencers, they are stored in simple array with IDS (for example [1, 2, 3] )
	 *
	 * @return array
	 */
	public function skippedInfluencers(): array {
		return array_filter( (array) $this->meta( 'skipped_influencers' ) );
	}

	/**
	 * Update skipped influencers, they are stored in simple array with IDS (for example [1, 2, 3] )
	 *
	 * @param array $data
	 *
	 * @return self
	 */
	public function updateSkippedInfluencers( array $data ): self {
		return $this->updateMeta( 'skipped_influencers', $data );
	}

	/**
	 * Get array of bookmarked influencers
	 *
	 * @return array
	 */
	public function getBookmarked(): array {
		return array_filter( (array) $this->meta( 'bookmarked' ) );
	}

	/**
	 * @param array $bookmarked
	 *
	 * @return self
	 */
	public function updateBookmarked( array $bookmarked ): self {
		return $this->updateMeta( 'bookmarked', $bookmarked );
	}

	/**
	 * @param string $acceptedBy Optional, get users accepted by provided value
	 *
	 * @return array
	 */
	public function getInfluencers( string $acceptedBy = '' ): array {

		$meta = array_filter( (array) $this->meta( 'influencers' ) );

		if ( empty( $acceptedBy ) ) {
			return $meta;
		}

		return array_filter( $meta, function ( $item ) use ( $acceptedBy ) {
			return $item[ 'accepted_by' ] === $acceptedBy;
		} );

	}

	/**
	 * @param array $influencers
	 *
	 * @return self
	 */
	public function updateInfluencers( array $influencers ): self {
		return $this->updateMeta( 'influencers', $influencers );
	}

	/**
	 * @param string $acceptedBy Optional, get users accepted by provided value
	 *
	 * @return array
	 */
	public function getToAddInfluencers( string $acceptedBy = '' ): array {

		$meta = array_filter( (array) $this->meta( 'to_add_influencers' ) );;

		if ( empty( $acceptedBy ) ) {
			return $meta;
		}

		return array_filter( $meta, function ( $item ) use ( $acceptedBy ) {
			return $item[ 'accepted_by' ] === $acceptedBy;
		} );
	}

	/**
	 * @param array $influencers
	 *
	 * @return self
	 */
	public function updateToAddInfluencers( array $influencers ): self {
		return $this->updateMeta( 'to_add_influencers', $influencers );
	}

	/**
	 * @return array
	 */
	public function getToRemoveInfluencers(): array {
		return (array) $this->meta( 'to_remove_influencers' );
	}

	/**
	 * @param array $influencers
	 *
	 * @return self
	 */
	public function updateToRemoveInfluencers( array $influencers ): self {
		return $this->updateMeta( 'to_remove_influencers', $influencers );
	}

	/**
	 * @return array
	 */
	public function getDeclinedInfluencers(): array {
		return array_filter( (array) $this->meta( 'declined_influencers' ) );
	}

	/**
	 * @param array $influencers
	 *
	 * @return self
	 */
	public function updateDeclinedInfluencers( array $influencers ): self {
		return $this->updateMeta( 'declined_influencers', $influencers );
	}

	/**
	 * Check if influencer have sent collaboration offer to this campaign
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function haveSendCollaboration( User $influencer ): bool {
		return ! CampaignInvitation::getCollaboration( $this, $influencer )->empty();
	}

	/**
	 * Check if influencer is invited to this campaign
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function isInvited( User $influencer ): bool {
		return ! CampaignInvitation::getInvite( $this, $influencer )->empty();
	}

	/**
	 * Check if provided influencer have declined invitation to campaign
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function hasDeclined( User $influencer ): bool {
		return in_array( $influencer->ID, $this->getDeclinedInfluencers() );
	}

	/**
	 * Add influencer to declined list. These influencers cannot be invited again
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function addDeclinedInfluencer( User $influencer ): bool {

		$declined = $this->getDeclinedInfluencers();

		if ( in_array( $influencer->ID, $declined ) ) {
			return false;
		}

		$declined[] = $influencer->ID;

		$this->updateDeclinedInfluencers( $declined );

		return true;

	}

	/**
	 * Invitation have been accepted by influencer
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function acceptInvite( User $influencer ): bool {

		$invite     = CampaignInvitation::getInvite( $this, $influencer )->first();
		$influencer = $invite->getReceiver();

		$args = [
			'impressions' => $invite->getImpressions(),
			'accepted_by' => 'influencer',
		];

		$this->addToAddInfluencer( $influencer->ID, $args );

		return true;

	}

	/**
	 * Influencer offer have been accepted by company
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function acceptCollaborationOffer( User $influencer ): bool {

		$invite = CampaignInvitation::getCollaboration( $this, $influencer )->first();

		$influencer  = $invite->getAuthor();
		$impressions = $invite->getImpressions();

		$campaign = $invite->getCampaign();

		$args = [
			'impressions' => $impressions,
			'accepted_by' => 'company',
		];

		//Campaign can't afford this influencer
		if ( $this->getImpressions() < $impressions ) {
			return false;
		}

		$this->subtractImpressions( $impressions )->addToAddInfluencer( $campaign, $influencer->ID, $args );

		return true;

	}

	/**
	 * Add influencer into "to add" list. These influencers are added to campaign on renewal or start date
	 *
	 * @param int   $influencerID
	 * @param array $args Following args are required [impressions => int, accepted_by => (influencer, company)]
	 *
	 * @return void
	 */
	public function addToAddInfluencer( int $influencerID, array $args ) {

		$toAdd = $this->getToAddInfluencers();

		$toAdd[ $influencerID ] = $args;

		$this->updateToAddInfluencers( $toAdd );

	}

	/**
	 * Add influencer into campaign
	 *
	 * @param int   $influencerID
	 * @param array $args Following args are required [impressions => int, accepted_by => (influencer, company)]
	 *
	 * @return void
	 */
	public function addInfluencer( int $influencerID, array $args ) {

		$influencers = $this->getInfluencers();

		$influencers[ $influencerID ] = $args;

		$this->updateInfluencers( $influencers );

	}

	/**
	 * Checks if provided user is in campaign
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function inCampaign( User $influencer ): bool {
		return array_key_exists( $influencer->ID, $this->getInfluencers() );
	}

	/**
	 * Check if on renewal date user will be added to campaign
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function willBeAdded( User $influencer ): bool {
		return array_key_exists( $influencer->ID, $this->getToAddInfluencers() );
	}

	/**
	 * Check if on renewal date user will be removed from campaign
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function willBeRemoved( User $influencer ): bool {
		return array_key_exists( $influencer->ID, $this->getToRemoveInfluencers() );
	}

	/**
	 * Remove influencer from "skipped" list
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function unskipInfluencer( User $influencer ): bool {

		$skipped = $this->skippedInfluencers();

		$index = array_search( $influencer->ID, $skipped );

		if ( $index !== false ) {
			unset( $skipped[ $index ] );

			$this->updateSkippedInfluencers( $skipped );

			return true;
		} else {
			return false;
		}

	}

	/**
	 * Mark influencer as "skipped" for this campaign
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function skipInfluencer( User $influencer ): bool {

		$skipped = $this->skippedInfluencers();

		//Check if influencer haven't been skipped to avoid duplicates
		$index = array_search( $influencer->ID, $skipped );
		if ( $index !== false ) {
			return false;
		}

		$skipped[] = $influencer->ID;

		$this->updateSkippedInfluencers( $skipped );

		return true;

	}

	/**
	 * Check if influencer is skipped in campaign
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function isSkipped( User $influencer ) {

		$skipped = $this->skippedInfluencers();

		return array_search( $influencer->ID, $skipped ) !== false;

	}

	/**
	 * Check if influencer is marked as bookmarked
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function isBookmarked( User $influencer ): bool {
		$bookmarked = $this->getBookmarked();

		return in_array( $influencer->ID, $bookmarked );
	}

	/**
	 * @param User $influencer
	 *
	 * @return void
	 */
	public function bookmark( User $influencer ) {

		$bookmarked = $this->getBookmarked();

		if ( ! in_array( $influencer->ID, $bookmarked ) ) {
			$bookmarked[] = $influencer->ID;
		}

		$this->updateBookmarked( $bookmarked );

		do_action( 'wpk/campaign/influencerBookmarked', $this );

	}

	/**
	 * @param User $influencer
	 *
	 * @return void
	 */
	public function removeBookmark( User $influencer ) {

		$bookmarked = $this->getBookmarked();
		$index      = array_search( $influencer->ID, $bookmarked );

		if ( $index !== false ) {
			unset( $bookmarked[ $index ] );
		}

		$this->updateBookmarked( $bookmarked );

		do_action( 'wpk/campaign/influencerUnbookmarked', $this );

	}

	/**
	 * Check if provided campaign have this active influencer
	 *
	 * @param User $influencer
	 *
	 * @return bool
	 */
	public function haveInfluencer( User $influencer ): bool {

		$influencers = $this->getInfluencers();

		return isset( $influencers[ $influencer->ID ] );
	}

	/**
	 * Check if campaigns have reached required impressions in order to be started or renewed
	 *
	 * @return bool
	 */
	public function haveReachedImpressions(): bool {

		$requiredImpressions = $this->getPlan()->getRequiredImpressions();
		//We will add impressions from invitations to this value
		$totalImpressions = $this->getImpressions();
		$invites          = CampaignInvitation::getInvite( $this );

		foreach ( $invites->all() as $invite ) {
			$totalImpressions += $invite->getImpressions();
		}

		return $totalImpressions <= $requiredImpressions;

	}

	/**
	 * Add influencers from "to add" list
	 *
	 * @return self
	 */
	public function addToAddInfluencers(): self {

		$toAdd      = $this->getToAddInfluencers();
		$inCampaign = $this->getInfluencers();

		$this
			->updateInfluencers( array_filter( $toAdd + $inCampaign ) )
			->updateToAddInfluencers( [] );

		return $this;

	}

}