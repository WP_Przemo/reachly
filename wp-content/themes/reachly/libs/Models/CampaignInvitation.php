<?php


namespace Wpk\Models;

use Wpk\Core;

/**
 * Campaign invitation model
 *
 * @author Przemysław Żydek
 */
class CampaignInvitation extends Post {

	/** @var string After what time invitation expires */
	const EXPIRES_IN = '+48 hours';

	/**
	 * @return int
	 */
	public function getImpressions(): int {
		return (int) $this->meta( 'impressions' );
	}

	/**
	 * @return User
	 */
	public function getAuthor(): User {
		return User::find( $this->post_author );
	}

	/**
	 * @return User
	 */
	public function getReceiver(): User {
		$receiver = (int) $this->meta( 'receiver' );

		return User::find( $receiver );
	}

	/**
	 * Get invitation expire date
	 *
	 * @param bool $asDatetime
	 *
	 * @return \DateTime|string
	 */
	public function expires( bool $asDatetime = false ) {
		$expires = ( new \DateTime() )->setTimestamp( $this->meta( 'expires' ) );

		return $asDatetime ? $expires : $expires->format( DATE_FORMAT );
	}

	/**
	 * @return Campaign
	 */
	public function getCampaign(): Campaign {
		$campaignID = (int) $this->meta( 'campaign_id' );

		return Campaign::find( $campaignID );
	}

	/**
	 * Get invite to campaign
	 *
	 * @param Campaign $campaign
	 * @param User     $influencer - if empty will return all invites for this campaign
	 *
	 * @return bool|Collection
	 */
	public static function getInvite( Campaign $campaign, User $influencer = null ) {

		$meta = [
			'campaign_id' => $campaign->ID,
			'type'        => 'invite',
		];

		if ( ! empty( $influencer ) ) {
			$meta[ 'receiver' ] = $influencer->ID;
		}

		$invite = CampaignInvitation::init()->hasMetaRelation( $meta )->status( 'any' )->get();

		return $invite;

	}

	/**
	 * @param Campaign $campaign
	 *
	 * @return Collection
	 */
	public static function getInvitesAndCollaborations( Campaign $campaign ): Collection {
		return CampaignInvitation::init()->hasMetaValue( 'campaign_id', $campaign->ID )->status( 'any' )->get();
	}

	/**
	 * Get collaboration offers from provided influencer to provided campaign
	 *
	 * @param Campaign $campaign
	 * @param User     $influencer If empty - will return all collaboration offers for this campaign
	 *
	 * @return bool|Collection
	 */
	public static function getCollaboration( Campaign $campaign, User $influencer = null ) {

		$meta = [
			'campaign_id' => $campaign->ID,
			'receiver'    => $campaign->post_author,
			'type'        => 'collaboration',
		];

		$invite = CampaignInvitation::init()
		                            ->hasMetaRelation( $meta )
		                            ->status( 'any' );

		if ( ! empty( $influencer ) ) {
			$invite->author( $influencer->ID );
		}

		return $invite->get();

	}

	/**
	 * @return bool|self
	 */
	public function create() {
		$this->attributes[ 'post_type' ] = Core::CAMPAIGN_INVITATION;

		return parent::create();
	}

	/**
	 * @return Collection
	 */
	public function get(): Collection {
		$this->attributes[ 'post_type' ] = Core::CAMPAIGN_INVITATION;

		return parent::get();
	}

	/**
	 * Accept invitation or collaboration offer
	 *
	 * @return bool
	 */
	public function accept(): bool {

		$type        = $this->meta( 'type' );
		$campaign    = $this->getCampaign();
		$impressions = $this->getImpressions();

		$args = [
			'impressions' => $impressions,
		];

		if ( ! $campaign->canBeModified() && ! $campaign->canBeModified( 'renewed_date' ) ) {
			return false;
		}

		if ( $type === 'invite' ) {

			$influencer            = $this->getReceiver();
			$args[ 'accepted_by' ] = 'influencer';

			do_action( 'wpk/campaign/inviteAccepted', $this, $influencer, $campaign );

		} else if ( $type === 'collaboration' ) {

			//Company can't afford adding this influencer
			if ( $impressions > $campaign->getImpressions() ) {
				return false;
			}

			$influencer            = $this->getAuthor();
			$args[ 'accepted_by' ] = 'company';

			$campaign->subtractImpressions( $impressions );

			do_action( 'wpk/campaign/collaborationAccepted', $this, $influencer, $campaign );

		} else {
			return false;
		}

		$campaign->addToAddInfluencer( $influencer->ID, $args );

		//Delete the invitation
		$this->updateMeta( 'status', 'accepted' )->delete();

		return true;

	}

	/**
	 * Decline invitation or collaboarion offer
	 *
	 * @return bool
	 */
	public function decline(): bool {

		$type     = $this->meta( 'type' );
		$campaign = $this->getCampaign();

		if ( ! $campaign->canBeModified() && ! $campaign->canBeModified( 'renewed_date' ) ) {
			return false;
		}

		if ( $type === 'invite' ) {

			$influencer = $this->getReceiver();

			$campaign->addDeclinedInfluencer( $influencer );

			do_action( 'wpk/campaign/inviteDeclined', $this, $influencer, $campaign );


		} else if ( $type === 'collaboration' ) {

			$influencer = $this->getAuthor();

			$campaign->skipInfluencer( $influencer );

			do_action( 'wpk/campaign/collaborationDeclined', $this, $influencer, $campaign );

		} else {
			return false;
		}

		return $this->delete();

	}

	/**
	 * Creates invitation to campaign (invite is send by company)
	 *
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return static|false;
	 */
	public static function createInvite( Campaign $campaign, User $influencer ) {

		$influencerImpressions = $influencer->getImpressions( $campaign );

		if ( $influencerImpressions > $campaign->getImpressions() || $campaign->isInvited( $influencer ) ) {
			return false;
		}

		$author   = (int) $campaign->post_author;
		$receiver = $influencer->ID;

		$title = sprintf( esc_html__( '%s - invite for %s', 'wpk' ), $campaign->post_title, $influencer->user_login );

		$meta = [
			'receiver'    => $receiver,
			'type'        => 'invite',
			'campaign_id' => $campaign->ID,
			'impressions' => $influencerImpressions,
			//Not related to WP post status, this is actual state of invitation
			'status'      => 'pending',
			'expires'     => strtotime( self::EXPIRES_IN ),
		];

		$invite = self::init()
		              ->addMetas( $meta )
		              ->title( $title )
		              ->author( $author )
		              ->status( 'publish' )
		              ->create();

		if ( $invite ) {
			$campaign->subtractImpressions( $influencerImpressions );

			do_action( 'wpk/campaign/invitationCreated', $invite );
		}

		return $invite;

	}

	/**
	 * Creates invitation to campaign (invite is send by company)
	 *
	 * @param Campaign $campaign
	 * @param User     $influencer
	 *
	 * @return static|false;
	 */
	public static function createCollaboration( Campaign $campaign, User $influencer ) {

		$influencerImpressions = $influencer->getImpressions( $campaign );

		$receiver = $campaign->getAuthor();

		$title = sprintf( esc_html__( '%s - collaboration offer from ', 'wpk' ), $campaign->post_title, $influencer->user_login );

		$meta = [
			'receiver'    => $receiver->ID,
			'type'        => 'collaboration',
			'campaign_id' => $campaign->ID,
			'impressions' => $influencerImpressions,
			//Not related to WP post status, this is actual state of invitation
			'status'      => 'pending',
			'expires'     => strtotime( self::EXPIRES_IN ),
		];

		$invite = self::init()
		              ->addMetas( $meta )
		              ->title( $title )
		              ->author( $influencer->ID )
		              ->status( 'publish' )
		              ->create();

		if ( $invite ) {
			do_action( 'wpk/campaign/invitationCreated', $invite );
		}

		return $invite;

	}

}