<?php


namespace Wpk\Models;

/**
 * @author Przemysław Żydek
 */
class Collection {

	/** @var mixed */
	public $query;

	/** @var array */
	protected $items = [];

	/**
	 * Collection constructor.
	 *
	 * @param mixed $items
	 * @param mixed $query Optional query parameter
	 */
	public function __construct( $items = [], $query = [] ) {
		$this->items = $this->getArrayItems( $items );
		$this->query = $query;
	}

	/**
	 * Add new items to collection
	 *
	 * @param mixed|Collection $items
	 *
	 * @return Collection
	 */
	public function add( $items ): self {

		if ( $items instanceof self ) {
			$this->items = array_merge( $items->all(), $this->items );
		} else {
			$items = $this->getArrayItems( $items );

			$this->items = array_merge( $items, $this->items );
		}

		$this->items = array_unique( $this->items );

		return $this;

	}

	/**
	 * Parses items to array
	 *
	 * @param mixed $items
	 *
	 * @return array
	 */
	protected function getArrayItems( $items ) {
		return (array) $items;
	}

	/**
	 * @param mixed $items
	 * @param mixed $query
	 *
	 * @return static
	 */
	public static function make( $items = [], $query = [] ) {
		return new static( $items, $query );
	}

	/**
	 * @return array
	 */
	public function all() {
		return $this->items;
	}

	/**
	 * @return int
	 */
	public function count() {
		return count( $this->items );
	}

	/**
	 * @return bool
	 */
	public function empty() {
		return empty( $this->items );
	}

	/**
	 * Check if items contain selected value by key
	 *
	 * @param mixed $key
	 * @param mixed $value
	 * @param bool  $strict
	 *
	 * @return bool
	 */
	public function contains( $key, $value, bool $strict = false ): bool {

		foreach ( $this->all() as $item ) {

			if ( $strict && $item->$key === $value ) {
				return true;
			} else if ( $item->$key == $value ) {
				return true;
			}

		}

		return false;

	}

	/**
	 * @param      $key
	 * @param      $value
	 * @param bool $strict
	 *
	 * @return static
	 */
	public function find( $key, $value, bool $strict = false ): self {

		$results = [];

		foreach ( $this->all() as $item ) {

			if ( $strict && $item->$key === $value ) {
				$results[] = $item;
			} else if ( $item->$key == $value ) {
				$results[] = $item;
			}

		}

		return new static( $results );

	}

	/**
	 * Execute a callback over each item.
	 *
	 * @param callable $callback
	 *
	 * @return self
	 */
	public function each( callable $callback ): self {

		foreach ( $this->items as $key => $item ) {
			if ( $callback( $item, $key ) === false ) {
				break;
			}
		}

		return $this;

	}

	/**
	 * Perform filtering of collections
	 *
	 * @param callable $callback Callback function for filtering
	 *
	 * @return self
	 */
	public function filter( callable $callback ): self {
		$this->items = array_filter( $this->items, $callback );

		return $this;
	}

	/**
	 * Get first item from collection
	 *
	 * @return bool|mixed
	 */
	public function first() {
		return isset( $this->items[ 0 ] ) ? $this->items[ 0 ] : false;
	}

}