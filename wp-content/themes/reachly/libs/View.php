<?php


namespace Wpk;

use Jenssegers\Blade\Blade;

/**
 * View wrapper class
 *
 * @author Przemysław Żydek
 */
class View {

	/** @var string Path to views directory */
	protected $path;

	/** @var Blade Blade instance */
	protected $blade;

	/**
	 * View constructor.
	 *
	 * @param string|array $path Paths to views directory
	 * @param string       $cachePath Path to cache directory
	 */
	public function __construct( string $path, string $cachePath = null ) {

		$this->path = $path;

		if ( empty( $cachePath ) ) {
			$cachePath = "$path/cache";
		}

		$this->blade = new Blade( $this->path, $cachePath );

	}

	/**
	 * Render view
	 *
	 * @param string $file
	 * @param array  $data
	 *
	 * @return string
	 */
	public function render( string $file, array $data = [] ): string {

		return $this->blade->render( $file, $data );

	}

}