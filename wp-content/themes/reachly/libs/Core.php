<?php

namespace Wpk;

use Wpk\Controllers\PostTypes;
use Wpk\Cron\Crons;
use Wpk\Helpers\PostType;
use Wpk\Models\CampaignInvitation;
use Wpk\Models\CampaignPlan;

/**
 * Core class
 *
 * @author Przemysław Żydek
 */
final class Core {

	/** @var string Slug used for translations */
	const SLUG = 'wpk';

	/** @var string Campaign plan slug */
	const CAMPAIGN_PLAN = 'campaign_plan';

	/** @var string Campaign slug */
	const CAMPAIGN = 'campaign';

	/** @var string */
	const CAMPAIGN_INVITATION = 'campaign_invitation';

	/** @var string */
	const CAMPAIGN_CONVERSATION = 'campaign_conversation';

	/** @var string Stores path to this theme directory */
	public $dir;

	/** @var string Stores url to this theme directory */
	public $url;

	/** @var Loader Stores loader instance */
	public $loader;

	/** @var Utility */
	public $utility;

	/** @var Enqueue */
	public $enqueue;

	/** @var View */
	public $view;

	/** @var Controllers\Controllers */
	public $controllers;

	/** @var array */
	public $widgets = [];

	/** @var Crons */
	public $cron;

	/** @var Core Stores class instance */
	private static $instance;

	/**
	 * Core constructor.
	 */
	private function __construct() {
	}

	private function __clone() {
	}

	private function __wakeup() {
	}

	/**
	 * Core init.
	 *
	 * @param string $namespace Namespace of core class
	 *
	 * @return Core
	 */
	public function init( string $namespace = null ) {

		$this->url = get_stylesheet_directory_uri();
		$this->dir = get_stylesheet_directory();

		$namespace = empty( $namespace ) ? __NAMESPACE__ : $namespace;

		$this->loader  = new Loader( "{$this->dir}/libs", $namespace );
		$this->utility = new Utility();
		$this->enqueue = new Enqueue();
		$this->view    = new View( "{$this->dir}/views" );
		$this->cron    = new Crons();

		//Load controllers
		Controllers\Controllers::load();

		//Register statuses
		Statuses::register();

		//Register post types
		PostTypes::register();

		$this->setupHooks()
		     ->loadApi()
		     ->loadWidgets();

		return $this;

	}

	/**
	 * @return self
	 */
	protected function setupHooks(): self {

		add_action( 'after_switch_theme', [ $this, 'createInfluencersTable' ] );

		return $this;

	}

	/**
	 * @return self
	 */
	protected function loadApi(): self {

		Api\Facebook\Api::connect();
		Api\Instagram\Api::connect();

		return $this;

	}

	/**
	 * @return self
	 */
	public function loadWidgets(): self {
		register_widget( 'Wpk\\Widgets\\UserAvatar' );

		return $this;
	}

	/**
	 * Get Core instance.
	 *
	 * @return self
	 */
	public static function getInstance(): self {

		if ( empty( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;

	}
}
