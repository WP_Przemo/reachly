<?php

namespace Wpk;

use Wpk\Api\Instagram\Api;
use Wpk\Models\Campaign;
use Wpk\Models\User;
use Wpk\Traits;

/**
 * Utility class.
 *
 * @author Przemysław Żydek
 */
class Utility {

	use Traits\Request;

	/**@var string Debug option slug. */
	private static $name = 'wpk_debug';

	/**@var mixed Stores debug option. */
	private static $debug;


	/**
	 * Utility constructor
	 */
	public function __construct() {

		self::$debug = get_option( self::$name, [] );
		add_action( 'admin_menu', [ $this, 'addMenu' ] );

	}

	/**
	 * Update debug option
	 *
	 * @param mixed $value
	 */
	public static function updateDebug( $value ) {

		self::$debug = $value;
		update_option( self::$name, self::$debug );

	}

	/**
	 * Helper function for logging stuff
	 *
	 * @param mixed  $log
	 * @param string $title
	 */
	public static function log( $log, $title = null ) {

		if ( ! empty( $title ) ) {
			error_log( $title );
		}
		if ( is_array( $log ) || is_object( $log ) ) {
			error_log( print_r( $log, true ) );
		} else {
			error_log( $log );
		}


	}

	/**
	 * Get debug option value
	 *
	 * @return mixed
	 */
	public static function getDebug() {

		return self::$debug;

	}

	/**
	 * Add debug menu.
	 */
	public function addMenu() {

		add_menu_page( 'WPK Debug', 'WPK Debug', 'manage_options', 'wpk_debug', [ $this, 'debugMenu' ] );

	}

	/**
	 * Debug menu callback function.
	 */
	public function debugMenu() {

		$campaign = Campaign::find(1376);

		var_dump($campaign->getInfluencers());

	}

	/**
	 * Display user first and last name, or login
	 *
	 * @param \WP_User $user
	 *
	 * @return string User name
	 */
	public static function getUserName( \WP_User $user ) {

		if ( empty( $user->first_name ) || empty( $user->last_name ) ) {
			return $user->user_login;
		}

		return $user->first_name . ' ' . $user->last_name;

	}

	/**
	 * Get date range from provided dates and return them in selected format.
	 *
	 * @param \DateTime     $from
	 * @param \DateTime     $to
	 * @param \DateInterval $interval
	 * @param string        $format
	 *
	 * @return array
	 */
	public static function getDateRange( \DateTime $from, \DateTime $to, \DateInterval $interval, $format = 'Y-m-d' ) {

		$range  = new \DatePeriod( $from, $interval, $to );
		$result = [];


		foreach ( $range as $item ) {
			$result[] = $item->format( $format );
		}
		try {
			$last = new \DateTime( end( $result ) );
			$last->modify( 'tomorrow' );
		} catch ( \Exception $e ) {
			$current_year = date( 'Y' );
			$last         = new \DateTime( $current_year . '-' . end( $result ) );
			$last->modify( 'tomorrow' );
		}
		$result[] = $last->format( $format );


		return $result;

	}

	/**
	 * Check if element is iframe.
	 *
	 * @param $string
	 *
	 * @return bool
	 */
	public static function isIframe( $string ) {

		return strpos( $string, '<iframe' ) !== false;

	}

	/**
	 *
	 *
	 * @param int $value
	 *
	 * @return float|string
	 */
	public static function round( $value ) {

		$value    = floatval( $value );
		$exploded = str_split( (string) $value );
		if ( $value >= 1000000 ) {
			return $exploded[ 0 ] . 'm';
		} else if ( $value >= 100000 ) {
			return $exploded[ 0 ] . $exploded[ 1 ] . $exploded[ 2 ] . 'k';
		} else if ( $value >= 10000 ) {
			return $exploded[ 0 ] . $exploded[ 1 ] . 'k';
		} else if ( $value > 999 ) {
			return $exploded[ 0 ] . 'k';
		}

		return $value;

	}

	/**
	 * Validate e-mail address
	 *
	 * @param string $email
	 *
	 * @return bool
	 */
	public static function validateEmail( $email ) {

		$isValid = true;
		$atIndex = strrpos( $email, "@" );

		if ( is_bool( $atIndex ) && ! $atIndex ) {
			$isValid = false;
		} else {
			$domain    = substr( $email, $atIndex + 1 );
			$local     = substr( $email, 0, $atIndex );
			$localLen  = strlen( $local );
			$domainLen = strlen( $domain );

			if ( $localLen < 1 || $localLen > 64 ) {
				// local part length exceeded
				$isValid = false;
			} else if ( $domainLen < 1 || $domainLen > 255 ) {
				// domain part length exceeded
				$isValid = false;
			} else if ( $local[ 0 ] == '.' || $local[ $localLen - 1 ] == '.' ) {
				// local part starts or ends with '.'
				$isValid = false;
			} else if ( preg_match( '/\\.\\./', $local ) ) {
				// local part has two consecutive dots
				$isValid = false;
			} else if ( ! preg_match( '/^[A-Za-z0-9\\-\\.]+$/', $domain ) ) {
				// character not valid in domain part
				$isValid = false;
			} else if ( preg_match( '/\\.\\./', $domain ) ) {
				// domain part has two consecutive dots
				$isValid = false;
			} else if ( ! preg_match( '/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace( "\\\\", "", $local ) ) ) {
				// character not valid in local part unless
				// local part is quoted
				if ( ! preg_match( '/^"(\\\\"|[^"])+"$/',
					str_replace( "\\\\", "", $local ) ) ) {
					$isValid = false;
				}
			}

			if ( $isValid && function_exists( "checkdnsrr" ) && ! ( checkdnsrr( $domain, "MX" ) || checkdnsrr( $domain, "A" ) ) ) {
				// domain not found in DNS
				$isValid = false;
			}
		}

		return $isValid;

	}

	/**
	 * Check if string contains provided substring
	 *
	 * @param string       $string
	 * @param string|array $substring
	 *
	 * @return bool
	 */
	public static function contains( $string, $substring ) {

		if ( is_array( $substring ) ) {
			foreach ( $substring as $item ) {
				if ( strpos( $string, $item ) === false ) {
					return false;
				}
			}

			return true;
		} else {
			return strpos( $string, $substring ) !== false;
		}

	}

	/**
	 * Helper function for getting templates.
	 *
	 * @param string $path Path to template (relative to /templates)
	 * @param bool   $return Whenever return template content or just load it
	 *
	 * @return bool|string
	 */
	public static function getTemplate( $path, $return = false ) {

		$core         = Core();
		$templatePath = $core->dir . '/templates/';
		$path         = $templatePath . $path;

		if ( ! file_exists( $path ) ) {
			return false;
		}

		if ( $return ) {
			ob_start();
			require_once $path;

			return ob_get_clean();
		} else {
			require_once $path;

			return true;
		}

	}

	/**
	 * Perform console log with provided value
	 *
	 * @param mixed $value
	 */
	public static function consoleLog( $value ) {

		$debug = debug_backtrace();
		$debug = $debug[ 0 ][ 'file' ] . ' Line ' . $debug[ 0 ][ 'line' ];

		$hook = is_admin() ? 'admin_footer' : 'wp_footer';

		add_action( $hook, function () use ( $debug, $value ) {
			?>
			<script>
				'use strict';
				console.log(<?php echo json_encode( $debug ) ?>);
				console.log(<?php echo json_encode( $value ) ?>);
			</script>
			<?php
		} );

	}

	/**
	 * Get page by provideded template name
	 *
	 * @param string $template Template name (for example page.php)
	 * @param bool   $single Whenever return only first result, or array with all results.
	 *
	 * @return array|\WP_Post|false
	 */
	public static function getPageByTemplate( $template, $single = true ) {

		$args  = [
			'meta_key'   => '_wp_page_template',
			'meta_value' => $template,
		];
		$pages = get_pages( $args );

		if ( ! empty( $pages ) ) {
			if ( $single ) {
				return array_shift( $pages );
			}

			return $pages;
		}

		return false;

	}

	/**
	 * Get page template file name
	 *
	 * @return string
	 */
	public static function getPageTemplate(): string {
		return basename( get_page_template() );
	}

	/**
	 * Filters $_POST values
	 *
	 * @param array $post $_POST array passed as reference
	 *
	 */
	public static function filterPost( &$post ) {

		foreach ( $post as $key => $item ) {

			if ( is_array( $item ) ) {
				Utility::filterPost( $item );

				return;
			}
			$item         = apply_filters( 'wpk/filter_post_value', filter_var( $item ), $key );
			$post[ $key ] = $item;

		}

	}

	/**
	 * Format input key for display on front end
	 *
	 * @param string $key
	 *
	 * @return string
	 */
	public static function formatInputKey( string $key ): string {

		$key = str_replace( [ 'billing_', 'wpk_' ], [ '' ], $key );
		$key = str_replace( [ '_', '-' ], [ ' ' ], $key );


		return ucfirst( $key );

	}

	/**
	 * Subtract workdays from provided date
	 *
	 * @param int       $days How many workdays add to date
	 * @param \DateTime $date
	 *
	 * @return \DateTime
	 */
	public static function subtractWorkDays( $days, \DateTime $date = null ) {

		if ( empty( $date ) ) {
			$date = new \DateTime();
		}

		if ( $days == 0 ) {
			return $date;
		}

		$date->modify( '-1 day' );

		if ( ! in_array( $date->format( 'N' ), [ '6', '7' ] ) ) {
			$days --;
		}

		return self::subtractWorkDays( $days, $date );

	}

	/**
	 * Add workdays to provided date
	 *
	 * @param int       $days How many workdays add to date
	 * @param \DateTime $date
	 *
	 * @return \DateTime
	 */
	public static function addWorkDays( $days, \DateTime $date = null ) {

		if ( empty( $date ) ) {
			$date = new \DateTime();
		}

		if ( $days == 0 ) {
			return $date;
		}

		try {
			$date->add( new \DateInterval( 'P1D' ) );
		} catch ( \Exception $e ) {
			Utility::log( $e->getMessage() );

			return $date;
		}

		if ( ! in_array( $date->format( 'N' ), [ '6', '7' ] ) ) {
			$days --;
		}

		return self::addWorkDays( $days, $date );

	}

	/**
	 * Check if provided user has admin role
	 *
	 * @param User|null $user
	 *
	 * @return bool
	 */
	public static function isAdmin( User $user = null ): bool {

		if ( empty( $user ) ) {
			$user = User::current();
		}

		return in_array( 'administrator', $user->roles );

	}

	/**
	 * Get current page url
	 *
	 * @return string
	 */
	public static function getCurrentUrl(): string {

		global $wp;

		return add_query_arg( [], home_url( $wp->request ) );

	}

	/**
	 * Get headers for email
	 *
	 * @return array
	 */
	public static function getEmailHeaders(): array {

		$headers = apply_filters( 'wpk/utility/email_headers', [
			"MIME-Version: 1.0",
			"Content-Type: text/html;charset=UTF-8;",
		] );

		return $headers;

	}

}