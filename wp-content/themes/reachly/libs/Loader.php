<?php


namespace Wpk;

/**
 * Autoloader for plugin classes
 *
 * @author Przemysław Żydek
 */
class Loader {

	/** @var string Namespace of module */
	protected $namespace;

	/** @var string Root directory with modules */
	public $dir;

	/**
	 * Loader init
	 *
	 * @param string $dir Root directory with modules
	 * @param string $namespace Namespace of core class
	 */
	public function __construct( $dir = null, $namespace = null ) {

		$this->namespace = empty( $namespace ) ? __NAMESPACE__ : $namespace;
		$this->dir       = rtrim( $dir, '/' ) . '/';

		spl_autoload_register( [ $this, 'loader' ], true, true );

	}

	/**
	 * Auto require plugin class
	 *
	 * @param string $class
	 *
	 * @return void
	 */
	public function loader( string $class ) {

		if ( strpos( $class, $this->namespace ) !== false ) {
			$class = str_replace( $this->namespace . '\\', '', ltrim( $class, '\\' ) );
			$class = str_replace( '\\', '/', $class );


			if ( $class === 'Helpers/Insight' ) {
				$class = 'Api/Helpers/Insight';
			}

			$file = $this->dir . $class . '.php';

			require_once $file;
		}

	}

}