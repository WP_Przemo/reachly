<?php
/*
Plugin Name: WP Kraken [#wpk_job_id]
Plugin URI: https://wpkraken.io/job/wpk_job_id
Description:
Author: WP Kraken [Przemo]
Author URI: https://wpkraken.io/
Version: 1.0
Text Domain: wpk_job_id
*/

namespace Wpk;

//Require necessary files
require_once 'includes/wpk_functions.php';
require_once 'libs/Loader.php';
require_once 'libs/Core.php';
require_once 'vendor/autoload.php';

/**
 * Helper function for accessing Core.
 *
 * @return Core
 *
 * @author Przemysław Żydek
 */
function Core() {

	return Core::getInstance();

}

$core = Core();

/* Release the Kraken! */
$core->init();
