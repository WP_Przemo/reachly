<?php
/**
 * View for user avatar widget
 */
?>

@if(is_user_logged_in())
    <div class="wpk-notification-wrap">
        <div class="wpk-notification wpk-sub">
            <i class="material-icons">notifications</i>
            <div class="wpk-notification-counter {{ $counterClass }}">
                <span>{{ $notificationsCount }}</span>
            </div>
        </div>
        <ul class="wpk-submenu wpk-notifications">

            <li style="display: <?php echo empty( $notifications ) ? 'block' : 'none' ?>" class="wpk-hover-ignore wpk-no-notifications">

                <span>{{  __( 'You don\'t have any notifications.', 'wpk' ) }}</span>
            </li>
            @if(!empty($notifications))
                {!! $notifications !!}
            @endif
        </ul>
    </div>
    <div class="wpk-separator">|</div>
    <div class="wpk-header-avatar-inner wpk-sub">
        <div class="wpk-username wpk-sub">
            <a class="" href="{{ $profileUrl }}">
                <span>{{ $userName }}</span>
            </a>
        </div>
        <div class="wpk-avatar-outer wpk-sub">
            <ul class="wpk-submenu">
                <li class="{{ $settingsActive }}">
                    <a href="{{ $profileUrl }}">
                        <span>{{ __( 'Settings', 'wpk' ) }}</span>
                    </a>
                </li>
                <li class="wpk-separator"></li>
                <li>
                    <a href="<?php echo home_url() ?>">
                        {{ __( 'Visit our support', 'wpk' )  }}
                    </a>
                </li>
                <li class="wpk-separator"></li>
                <li>
                    <a href="{!! $logoutUrl !!}">
                        {{ __( 'Logout', 'wpk' )  }}
                    </a>
                </li>
            </ul>
            <a href="{{ $profileUrl }}">
                <div class="wpk-avatar">
                    @if( ! empty($avatar) )
                        <img src="{{ $avatar }}" alt="">
                    @endif
                </div>
            </a>

        </div>

    </div>
    <div class="wpk-mobile-profile">
        <div class="wpk-modal-inner">
            <div class="wpk-avatar">
                @if ( ! empty( $avatar ) )
                    <img src="{{ $avatar }}" alt="">
                @endif
            </div>
            <div class="wpk-username">
                {{ sprintf( __( 'Hello %s ;)', 'wpk' ), $userName )  }}
            </div>
            <ul class="wpk-modal-menu">
                <li class="{{ $settingsActive }}">
                    <a href="{{ $profileUrl }}">
                        <span>{{ __( 'Settings', 'wpk' ) }}</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo home_url() ?>">
						<?php __( 'Visit our support', 'wpk' ) ?>
                    </a>
                </li>
                <li>
                    <a href="{!! $logoutUrl !!}">
                        {{ __( 'Logout', 'wpk' )  }}
                    </a>
                </li>
            </ul>
        </div>
    </div>
@endif