<div class="wpk-input-container wpk-file-input wpk-image-upload" data-action="{{ $action }}" data-remove-action="{{ $removeAction }}" data-images="{{ json_encode($images) }}" @if(!empty($id)) data-id="{{ $id }}"@endif>
    <div class="wpk-input-container-inner">
        <div class="wpk-input-label">
            <span>{{ $label }}</span>
        </div>
        <div class="wpk-file-input-target wpk-float-left">
            <div class="wpk-upload-progress wpk-disabled">
                <div class="wpk-progress"></div>
            </div>
            <div class="wpk-add">
                <i class="material-icons">add</i>
            </div>
            @if ( ! empty( $image ) )
                <img src="{{ $image }}" alt="">
            @endif
        </div>
        <div class="wpk-image-actions wpk-float-left wpk-flex">
            <span data-action="remove">{{ __( 'Remove', 'wpk' ) }}</span>
        </div>
    </div>
    <input type="hidden" name="wpk_file_nonce" class="wpk-file-nonce" value="{{ wp_create_nonce('wpk_file_nonce') }}">
    <input type="file">
</div>