<?php
/**
 * @var \Wpk\Models\User $influencer
 */
?>

<div class="wpk-influencer wpk-element col-xl-4 col-md-6 col-xs-12"
     data-influencer-id="{{ $influencer->ID  }}"
     data-impressions="{{ $influencer->getImpressions($campaign) }}"
     data-campaign-id="{{ $campaign->ID }}">
    <div class="wpk-influencer-inner wpk-element-inner">
        <div class="wpk-influencer-image wpk-element-image">
            <img src="{{ $influencer->getAvatar() }}" alt="">
        </div>
        <div class="wpk-influencer-data wpk-element-data">
            <div class="wpk-influencer-title wpk-element-title">
                <span class="wpk-instagram-username">{{ $influencer->campaignSocialMeta($campaign, 'username') }}</span>
            </div>
            <div class="wpk-element-separator wpk-influencer-separator"></div>
            <div class="wpk-element-metas wpk-influencer-metas">
                <div class="wpk-element-meta-row wpk-influencer-meta-row">
                    <span class="wpk-element-meta">
                        {{
                        sprintf(__('%s likes per post', 'wpk'), \Wpk\Utility::round(
                            $influencer->campaignSocialMeta($campaign, 'likes_per_post')
                        ))
                        }}
                       </span>
                    <span class="wpk-element-meta">
                        {{
                        sprintf(__('Followed by %s', 'wpk'), \Wpk\Utility::round(
                            $influencer->campaignSocialMeta($campaign, 'followers_count')
                        ))
                        }}
                       </span>
                </div>
                <div class="wpk-element-meta-row wpk-influencer-meta-row">
                    {{
                    sprintf(__('Follows %s', 'wpk'), \Wpk\Utility::round(
                        $influencer->campaignSocialMeta($campaign, 'follows_count')
                    ))
                    }}

                </div>
            </div>
            @include('influencer.element.campaign-data')
        </div>
    </div>
</div>

