<?php
/**
 * @var \Wpk\Models\User $influencer
 */
?>

@php
    $actions = \Wpk\Campaign\GridActions::influencerActions($influencer, $campaign);
@endphp


<div class="wpk-element-actions wpk-influencer-actions">
    @foreach($actions as $action)
        <div class="wpk-element-action wpk-influencer-action {{ isset($action['disabled']) && $action['disabled'] ? 'wpk-action-disabled' : '' }}"
             data-action="{{ $action['action'] }}"
             data-influencer-id="{{ $influencer->ID }}"
             data-campaign-id="{{ $campaign->ID }}"
             data-username="{{ $influencer->campaignSocialMeta($campaign, 'username') }}">
            <span class="wpk-action-icon"><i class="material-icons"></i></span>
            <span class="wpk-action-label">{{ $action['label'] }}</span>
        </div>
    @endforeach
</div>
