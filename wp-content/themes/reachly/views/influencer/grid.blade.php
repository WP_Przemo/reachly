<?php
/**
 * @var \Wpk\Models\Collection $influencers
 */
?>
<div class="wpk-influencers-grid-outer">
    <div class="wpk-influencers-grid row" data-status="{{ $campaign->post_status }}">
        @if ( !$influencers->empty() )
            @foreach($influencers->all() as $influencer)
                @include('influencer.element.element', ['influencer' => $influencer])
            @endforeach

            <div class="wpk-pagination col-xl-12" data-container=".wpk-influencers-grid" data-el=".wpk-influencer">
                {!!
                  paginate_links( array(
                    'format'    => '?page=%#%',
                    'prev_text' => '<i class="material-icons">keyboard_arrow_left</i>',
                    'next_text' => '<i class="material-icons">keyboard_arrow_right</i>',
                    'total'     => $totalPages,
                    'current'   => $page,
                    'end_size'  => 1,
                    'mid_size'  => 5,
                ) )
                 !!}
            </div>
        @else
            <div class="wpk-error-message">
                <span>{{ __( 'No influencers found.', 'wpk' )  }}</span>
            </div>

        @endif

    </div>
</div>