<div class="wpk-media-grid wpk-instagram-media-grid row">

    @foreach($medias as $media)
        <div class="wpk-media wpk-instagram-media col-md-3 col-xs-1">
            <a target="_blank" href="{{ $media->link }}">
                <img src="{{ $media->mediaUrl }}">
            </a>
        </div>
    @endforeach

</div>