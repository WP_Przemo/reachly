<div class="wpk-influencer-actions wpk-item-actions">
    @if(count($actions) === 1)

        <div class="wpk-border-round-button"
             data-action="{{ $actions[0]['action'] }}"
             data-influencer-id="{{ $influencer->ID }}"
             data-campaign-id="{{ $campaign->ID }}"
             data-username="<?php echo $influencer->socialMeta( 'instagram', 'username' ) ?>">
            <i class="material-icons"></i>
            <span>{{ $actions[0]['text'] }}</span>
        </div>

    @else

        @foreach($actions as $action)

            <div class="wpk-influencer-action wpk-border-round-button wpk-tooltip-target"
                 data-action="{{ $action['action'] }}"
                 data-influencer-id="{{ $influencer->ID }}"
                 data-campaign-id="{{ $campaign->ID }}">
                <i class="material-icons"></i>
                <div class="wpk-tooltip ">
                    <div class="wpk-tooltip-text">
                        {{ $action['text'] }}
                    </div>
                </div>
            </div>

        @endforeach

    @endif
</div>