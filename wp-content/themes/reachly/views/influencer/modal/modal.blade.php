<div class="wpk-influencer-modal" data-influencer-id="{{ $influencer->ID }}" data-campaign-id="{{ $campaign->ID }}">
    <div class="wpk-influencer-modal-actions wpk-navigation-actions">
        <div class="wpk-navigation">
            <div class="wpk-border-round-button wpk-close-modal wpk-tooltip-target">
                <i class="material-icons">keyboard_arrow_left</i>
                <div class="wpk-tooltip ">
                    <div class="wpk-tooltip-text">
						<?php _e( 'Close', 'wpk' ) ?>
                    </div>
                </div>
            </div>
        </div>
        @isset($campaign)
            @include('influencer.modal.actions')
        @endisset
    </div>

    @include('influencer.modal.influencer')
</div>
