<?php
/** @var \Wpk\Models\User $influencer */
?>
<article class="wpk-single-influencer" id="{{ $influencer->ID }}">
    <div class="wpk-influencer-details">
        <div class="wpk-influencer-detail">
            <div class="wpk-influencer-avatar">
                <img src="{{ $influencer->getAvatar() }}" alt="">
            </div>
            <div class="wpk-influencer-personal-info">
                <a target="_blank" href="{{ $influencer->getInstagramProfileUrl()  }}" class="wpk-influencer-name">
                    <h2>{{ $influencer->socialMeta('instagram', 'username') }}</h2>
                </a>
                <div class="wpk-influencer-metas">
                    <div class="wpk-influencer-meta">
                        <span>{{ ucfirst( $influencer->meta( 'gender' ) ) }}</span>
                    </div>
                    <div class="wpk-influencer-meta">
                        <span>{{ sprintf( __( '%s years old', 'wpk' ), $influencer->meta('age') )  }}</span>
                    </div>

                    @foreach($influencer->getInterests() as $interest)
                        <div class="wpk-influencer-meta">
                            <span>{{ \Wpk\Utility::formatInputKey( $interest) }}</span>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="wpk-influencer-detail wpk-influencer-stats">
            <div class="wpk-influencer-tab wpk-border-round-button">
                <h3>{{ $followersCount }}</h3>
                <span>{{ __( 'followers', 'wpk' )  }}</span>
            </div>
            <div class="wpk-influencer-tab wpk-border-round-button">
                <h3>{{ $likesPerPost }}</h3>
                <span>{{ __( 'likes per post', 'wpk' ) }}</span>
            </div>
            <div class="wpk-influencer-tab wpk-border-round-button">
                <h3>{{ $reach }}</h3>
                <span>{{ __( 'reach', 'wpk' )  }}</span>
            </div>
            <div class="wpk-influencer-tab wpk-border-round-button">
                <h3>{{ $impressions }}</h3>
                <span>{{ __( 'impressions', 'wpk' ) }}</span>
            </div>
            <div class="wpk-influencer-tab wpk-border-round-button">
                <h3>{{ $engagmentRate }}%</h3>
                <span>{{ __( 'engagment rate', 'wpk' )  }}</span>
            </div>
        </div>
    </div>
    <h5 class="wpk-uppercase">{{ __( 'Audience insights', 'wpk' ) }}</h5>
    <div class="wpk-influencer-insights">
        <div class="wpk-influencer-insight">
            <div class="wpk-insight-title">
                <i class="material-icons">
                    person_outline
                </i>
                <h4>{{ __( 'Gender', 'wpk' )  }}</h4>
            </div>

            <div class="wpk-pie-legend">

            </div>
            <div class="wpk-pie wpk-hide-label"
                 data-columns="{{  json_encode( [
				     'string' => __( 'Gender', 'wpk' ),
				     'number' => '',
			     ] )  }}"
                 data-values="{{ $genderRange }}"
                 data-title="{{ __( 'Genders', 'wpk' )  }}"
                 data-name="{{ __( 'Gender', 'wpk ' ) }}"></div>
        </div>
        <div class="wpk-influencer-insight wpk-bar-container">
            <div class="wpk-insight-title">
                <i class="material-icons">star_border</i>
                <h4>{{ __( 'Age range', 'wpk' )  }}</h4>
            </div>
            <div class="wpk-insight-filters">
                <button data-values="{{ $ages['all'] }}" class="wpk-button wpk-bar-value wpk-checkbox-button wpk-active">
                    {{ __( 'All', 'wpk' ) }}
                </button>
                <button data-values="{{ $ages['male'] }}" class="wpk-button wpk-bar-value wpk-checkbox-button">
                    {{ __('Men', 'wpk') }}
                </button>
                <button data-values="<?php echo htmlentities( json_encode( $ages[ 'female' ] ) ) ?>" class="wpk-button wpk-bar-value wpk-checkbox-button">
                    {{ __('Women', 'wpk') }}
                </button>
            </div>
            <div class="wpk-bar"
                 data-values="{{ $ages['all'] }}"
                 data-columns="{{ json_encode( [ 'Age range', 'Number of people' ] )  }}"
            ></div>
        </div>
        <div class="wpk-influencer-insight wpk-bar-container">
            <div class="wpk-insight-title">
                <i class="material-icons">location_searching</i>
                <h4>{{ __( 'Locations', 'wpk' ) }}</h4>
            </div>
            <div class="wpk-insight-filters">
                <button data-values="{{ $audienceCity }}" class="wpk-button wpk-bar-value wpk-checkbox-button wpk-active">
                    {{ __( 'Cities', 'wpk' )  }}
                </button>
                <button data-values="{{ $audienceCountry }}" class="wpk-button wpk-bar-value wpk-checkbox-button">
                    {{ __( 'Countries', 'wpk' )  }}
                </button>
            </div>
            <div class="wpk-bar"
                 data-values="{{ $audienceCity }}"
                 data-columns="{{  json_encode( [ 'Location', 'Number of people' ] ) }}"
            ></div>
        </div>
    </div>
    <div class="wpk-influencer-bio">
        {{ $influencer->meta('bio') }}
    </div>

    @include('influencer.modal.media')

</article>