<div class="wpk-tab-container">
    <form action="#" id="wpk_register_form" class="wpk-form" method="post">
        <div class="wpk-tab" id="general">
            <div class="wpk-tab-title">
                <h2>{{ __( 'Complete form', 'wpk' ) }}</h2>
            </div>
            <div class="wpk-tab-inner">
                <div class="wpk-form-wrap">
                    <div class="wpk-form-inner wpk-full-inputs wpk-float-label">
                        <div class="wpk-input-container wpk-two-inputs">
                            <label>
                                <input type="text" id="first_name" name="first_name" value="{{  $user->first_name }}">
                                <span class="wpk-label">{{ __( 'First name*', 'wpk' ) }}</span>
                            </label>
                            <label>
                                <input type="text" id="last_name" name="last_name" value="{{  $user->last_name }}">
                                <span class="wpk-label">{{ __( 'Last name*', 'wpk' ) }}</span>
                            </label>
                        </div>
                        <label class="wpk-input-container">
                            <input type="text" id="phone" name="phone" value="{{  $user->meta( 'phone' ) }}">
                            <span class="wpk-label">{{ __( 'Phone*', 'wpk' ) }}</span>
                        </label>
                        <label class="wpk-input-container">
                            <input type="text" id="email" name="email" value="{{  $user->user_email }}">
                            <span class="wpk-label">{{ __( 'Email*', 'wpk' ) }}</span>
                        </label>
                        <label class="wpk-input-container">
                            <input class="wpk-google-autocomplete" type="text" id="address" name="address" value="{{  $user->meta( 'address' ) }}">
                            <span class="wpk-label">{{ __( 'Address*', 'wpk' ) }}</span>
                        </label>

                        <div class="wpk-input-container wpk-two-inputs">
                            <label>
                                <input type="text" id="city" name="city" value="{{  $user->meta( 'city' ) }}">
                                <span class="wpk-label">{{ __( 'City*', 'wpk' ) }}</span>
                            </label>
                            <label>
                                <input type="text" id="zip" name="zip" value="{{  $user->meta( 'zip' ) }}">
                                <span class="wpk-label">{{ __( 'Zip*', 'wpk' ) }}</span>
                            </label>
                        </div>
                        <div class="wpk-input-container wpk-two-inputs">
                            <label>
                                <input autocomplete="off" data-orientation="center" data-allow_past="yes" class="wpk-date-picker" type="text" id="birth_date" name="birth_date" value="{{  $user->meta( 'birth_date' ) }}">
                                <span class="wpk-label">{{ __( 'Birth date*', 'wpk' ) }}</span>
                            </label>


                            <div id="gender_inputs" class="wpk-input-container">
				        	<span class="wpk-input-label wpk-float-left">
				            	{{ __( 'Gender :', 'wpk' ) }}
				            </span>

                                @foreach($genders as $key => $gender)
                                    @php
                                        $selected = $key === $userGender ? 'checked' : '';
                                        $active = $selected === 'checked' ? 'wpk-active' : '';
                                    @endphp

                                    <div class="wpk-button wpk-checkbox-button wpk-float-left {{ $active }}>">{{ $gender }}</div>
                                    <input name="gender" class="wpk-gender" type="radio" {{ $selected }} value="{{ $key }}">
                                @endforeach
                            </div>
                        </div>
                        <div id="birth_date_calendar" class="wpk-calendar-container wpk-no-dropdown"></div>
                        <div class="wpk-input-container wpk-button-inputs wpk-category-checkboxes">
						<span class="wpk-input-label wpk-float-left">
					        {{ __( 'Categories you want to work in :', 'wpk' ) }}
                        </span>
                            @foreach($categories as $category)
                                @php
                                    $icon = get_field( 'category_icon', $category );
                                    $icon = isset( $icon[ 'sizes' ][ 'thumbnail' ] ) ? $icon[ 'sizes' ][ 'thumbnail' ] : null;
                                @endphp

                                <div class="wpk-border-round-button wpk-icon-button wpk-checkbox-button" data-allow-multiple="1">
                                    <img src="{{ $icon }}" alt="">
                                    <span>{{  $category->name  }}</span>
                                </div>
                                <input type="checkbox" value="{{ $category->slug }}" name="interests[]" class="wpk-interests wpk-multiple-input">

                            @endforeach
                        </div>
                        <div class="wpk-input-container wpk-full-width">
					<span class="wpk-input-label">
						{{ __( 'Write about yourself: ', 'wpk' ) }}
					</span>
                            <textarea name="bio" id="bio" cols="30" rows="10" placeholder="{{ __( 'Start typing here...', 'wpk' ) }}">{{  $user->meta('bio') }}</textarea>

                        </div>
                    </div>
                </div>
            </div>
            <div class="wpk-input-container">
                <div class="wpk-checkbox wpk-accept-terms">
                    <input type="checkbox" name="accept_terms" id="accept_terms" value="yes">
                    <label for="accept_terms"><span>
                            {!! sprintf( __( 'I accept Reachly <a target="_blank" href="%s">terms of influencers</a> and <a target="_blank" href="%s">privacy policy</a>', 'wpk' ), '#', '#' )  !!}
                        </span>
                    </label>
                </div>
            </div>
            <button class="wpk-button wpk-submit">
                <input type="hidden" name="wpk_nonce" id="wpk_nonce" value="{{ wp_create_nonce('wpk_register') }}">
                {{ __( 'Save & Continue' ) }}
            </button>
        </div>

    </form>
</div>