<div class="wpk-my-campaigns-sidebar">
    <div class="row wpk-sidebar-section">
        <?php if(isset($campaign)): ?>
            <div class="wpk-campaign-title">
                <?php echo e($campaign->post_title); ?>

            </div>
        <?php else: ?>
            <div>
                <?php echo e(__( 'Unnamed campaign', 'wpk' )); ?>

            </div>
        <?php endif; ?>
    </div>
    <div class="wpk-sidebar-separator"></div>
</div>