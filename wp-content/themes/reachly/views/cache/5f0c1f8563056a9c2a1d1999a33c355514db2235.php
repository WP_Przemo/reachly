<?php
/** @var \Wpk\Models\CampaignPlan $plan */
?>
<div class="wpk-campaign-plan-tab" data-length="1" data-plan-id="<?php echo e($plan->ID); ?>"
     data-prices="<?php echo e(json_encode($plan->prices())); ?>">
    <div class="wpk-plan-title wpk-uppercase">
        <span><?php echo e($plan->post_title); ?></span>
    </div>
    <div class="wpk-plan-price">
        <span class="wpk-currency"><?php echo get_woocommerce_currency_symbol(); ?></span>
        <span class="wpk-price"><?php echo e($plan->prices()['1']); ?></span>
    </div>
    <div class="wpk-plan-separator"></div>
    <div class="wpk-plan-impressions">
        <?php echo sprintf( __( '<strong>%s</strong> impressions', 'wpk' ), $plan->meta('impressions') ); ?>

    </div>
    <div class="wpk-plan-buy">
        <div class="wpk-button wpk-plan-buy-button">
            <span><?php echo e(__( 'Pay now', 'wpk' )); ?></span>
        </div>
    </div>
</div>
