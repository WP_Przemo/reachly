<div class="wpk-wrap row wpk-my-campaigns">
    <?php if( false ): ?>

        <div class="col-xl-12 col-md-12 wpk-wrap-inner">
            <a class="wpk-circle-icon wpk-has-message wpk-full-icon" href="<?php echo e($profileUrl); ?>">
                <div class="wpk-circle">
                    <div class="wpk-circle-inner">
                        <i class="material-icons">check</i>
                    </div>
                </div>
                <div class="wpk-message">
					<?php _e( 'Verify your account', 'wpk' ) ?>
                </div>
            </a>
        </div>

    <?php else: ?>

        <?php echo $__env->make('sidebar', ['template' => $sidebar], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="col-sm-12 col-xl-10 col-md-8 wpk-wrap-inner">
            <?php echo $header; ?>

            <div class="wpk-step-inner wpk-step-<?php echo e($step); ?>" data-step="<?php echo e($step); ?>">
                <?php echo $__env->make($stepTemplate, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>