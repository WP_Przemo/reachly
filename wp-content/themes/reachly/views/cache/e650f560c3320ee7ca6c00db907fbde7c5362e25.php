<form class="wpk-new-campaign-form wpk-form-step-1" action="#" method="post">
    <input type="hidden" name="campaign_id" id="campaign_id"
           value=" <?php if(isset($campaign)): ?> <?php echo e($campaign->ID); ?> <?php endif; ?> ">
    <div class="wpk-campaign-creation-section">
        <div class="wpk-campaign-creation-inner wpk-campaign-duration">
            <div class="wpk-step">
                <span>1</span>
            </div>
            <div class="wpk-title wpk-uppercase">
                <?php echo e(__( 'Choose duration :', 'wpk' )); ?>

            </div>
            <input type="hidden" name="campaign_length" id="campaign_length" value="">
            <div class="wpk-durations">
                <div class="wpk-button" data-length="1">
                    <span><?php echo e(__( '1 month', 'wpk' )); ?></span>
                </div>
                <div class="wpk-button" data-length="3">
                    <span><?php echo e(__( '3 months', 'wpk' )); ?></span>
                </div>
                <div class="wpk-button" data-length="6">
                    <span><?php echo e(__( '6 month', 'wpk' )); ?></span>
                </div>
                <div class="wpk-button" data-length="12">
                    <span><?php echo e(__( '1 year', 'wpk' )); ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="wpk-campaign-creation-section">
        <div class="wpk-campaign-creation-inner">
            <div class="wpk-step">
                <span>2</span>
            </div>
            <div class="wpk-title wpk-uppercase">
                <span><?php echo e(__( 'Choose plan:', 'wpk' )); ?></span>
            </div>
            <div class="wpk-campaign-plans wpk-tab" id="one_month">

                <?php if(isset($campaign)): ?>
                    <input type="hidden" id="campaign_price" value="<?php echo e($campaign->meta('price')); ?>">
                    <input type="hidden" id="campaign_duration" value="<?php echo e($campaign->meta('length')); ?>">
                <?php endif; ?>

                <?php $__currentLoopData = $plans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php echo $__env->make('campaign-plan.tab', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
        </div>
    </div>
</form>