<?php
/**
 * @var \Wpk\Models\Collection $companies
 */
?>
<div class="row wpk-upper-form wpk-companies">
    <div class=" wpk-input-container wpk-search-icon wpk-icon-right">
        <input class="wpk-autocomplete" type="text" name="search" id="search" placeholder="{{ __( 'Search by brands', 'wpk' ) }}">
        <input type="hidden" class="wpk-autocomplete-nonce" value="{{ wp_create_nonce( 'wpk_company_filter_autocomplete' ) }}">
        <ul class="wpk-suggestions"></ul>
    </div>
</div>
<div class="wpk-elements-grid wpk-company-grid row">

    @if(!$companies->empty())
        @foreach($companies->all() as $company)
            @include('company.element')
        @endforeach

            <div class="wpk-pagination col-xl-12" data-container=".wpk-company-grid" data-el=".wpk-company">
                {!!
                  paginate_links( array(
                    'format'    => '?page=%#%',
                    'prev_text' => '<i class="material-icons">keyboard_arrow_left</i>',
                    'next_text' => '<i class="material-icons">keyboard_arrow_right</i>',
                    'total'     => $totalPages,
                    'current'   => $page,
                    'end_size'  => 1,
                    'mid_size'  => 5,
                ) )
                 !!}
            </div>
    @else
        <div class="wpk-error-message">
            <span>{{ __( 'No brands found.' ) }}</span>
        </div>
    @endif
</div>