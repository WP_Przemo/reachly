<?php
/**
 * @var \Wpk\Models\User $company
 */
?>
<div class="wpk-company wpk-element col-xl-3 col-md-6 col-xs-12" data-company-id="{{ $company->ID }}">
    <div class="wpk-company-inner wpk-element-inner">
        <div class="wpk-company-image wpk-element-image">
            <img src="{{ $company->getAvatar() }}" alt="">
        </div>
        <div class="wpk-company-data wpk-element-data">
            @if(!empty($company->getLogo()))
                <div class="wpk-element-logo wpk-company-logo">
                    <img src="{{ $company->getLogo() }}" alt="">
                </div>
            @else
                <div class="wpk-company-title wpk-element-title">
                    <span class="">{{ $company->meta('company') }}</span>
                </div>
        @endif
        <!--<div class="wpk-element-separator wpk-company-separator"></div>-->
            <div class="wpk-element-actions wpk-company-actions">
                <a href="{!! add_query_arg('author',  $company->ID, $archiveLink) !!}" class="wpk-element-action wpk-company-action">
                    <span class="wpk-action-label">{{ __( 'View Campaigns', 'wpk' ) }}</span>
                </a>
            </div>
        </div>
    </div>
</div>