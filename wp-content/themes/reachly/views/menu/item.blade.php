<li class="menu-item menu-item-type-post_type menu-item-object-page mkd-menu-narrow mkd-wide-background">
    <a href="{{ $url }}">
        <span class="mkd-item-outer">
            <span class="mkd-item-inner">
                <span class="mkd-item-text">
                    {!! $content !!}
                </span>
            </span>
            <span class="plus"></span></span>
    </a>
</li>
