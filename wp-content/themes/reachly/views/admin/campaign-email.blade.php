<p> {{ sprintf(__('Campaign <strong>%s</strong> have been published by company<strong>%s</strong>', 'wpk'), $campaign->post_title,$author->meta('company')) }} </p>
<h2>{{ __('Company details') }}</h2>

<table>
    <thead>
    <tr>
        <td>
            {{ __('First and last name', 'wpk') }}
        </td>
        <td>
            {{ __('Name', 'wpk') }}
        </td>
        <td>
            {{ __('Email', 'wpk') }}
        </td>
        <td>
            {{ __('Address', 'wpk') }}
        </td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            {{ $author->first_name }} {{ $author->last_name }}
        </td>
        <td>
            {{ $author->meta('company') }}
        </td>
        <td>
            {{ $author->user_email }}
        </td>
        <td>
            <p>{{ $author->meta('address') }}</p>
            <p>{{ $author->meta('city') }} {{ $author->meta('zip') }}</p>
        </td>
    </tr>
    </tbody>
</table>

<h2>{{ __('Influencers', 'wpk') }}</h2>
<table>
    <thead>
    <tr>
        <td>
            {{ __('Login', 'wpk') }}
        </td>
        <td>
            {{ __('Address', 'wpk') }}
        </td>
    </tr>
    </thead>
    <tbody>
    @foreach($influencers as $influencer)
        <tr>
            <td>{{ $influencer->user_login }}</td>
            <td>
                <p>{{ $influencer->user_email }}</p>
                <p>{{ $influencer->first_name }} {{ $influencer->last_name }}</p>
                <p>{{ $influencer->meta('address') }}</p>
                <p>{{ $influencer->meta('city') }} {{ $influencer->meta('zip') }}</p>
            </td>
        </tr>

    @endforeach
    </tbody>
</table>