<div class="wpk-wrap">
    <div class="wpk-wrap-inner">
        <span>
            {{ __('Something went wrong.', 'wpk') }}
        </span>
        <div class="wpk-return">
            <a class="wpk-link" href="{{ home_url() }}">{{ __('Return home', 'wpk') }}</a>
        </div>
    </div>
</div>