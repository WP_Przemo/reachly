<?php
/** @var \Wpk\Models\CampaignPlan $plan */
?>
<div class="wpk-campaign-plan-tab" data-length="1" data-plan-id="{{ $plan->ID }}"
     data-prices="{{ json_encode($plan->prices()) }}">
    <div class="wpk-plan-title wpk-uppercase">
        <span>{{ $plan->post_title }}</span>
    </div>
    <div class="wpk-plan-price">
        <span class="wpk-currency">{!! get_woocommerce_currency_symbol() !!}</span>
        <span class="wpk-price">{{ $plan->prices()['1'] }}</span>
    </div>
    <div class="wpk-plan-separator"></div>
    <div class="wpk-plan-impressions">
        {!! sprintf( __( '<strong>%s</strong> impressions', 'wpk' ), $plan->meta('impressions') ) !!}
    </div>
    <div class="wpk-plan-buy">
        <div class="wpk-button wpk-plan-buy-button">
            <span>{{ __( 'Pay now', 'wpk' )  }}</span>
        </div>
    </div>
</div>
