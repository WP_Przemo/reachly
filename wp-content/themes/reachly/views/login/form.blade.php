<div class="wpk-wrap row wpk-login">
    <div class="wpk-wrap-inner">
        <form action="" id="wpk_login" class="wpk-form wpk-float-label">
            <label class="wpk-input-container wpk-login">
                <input type="text" id="login" name="login">
                <span class="wpk-label">{{ __( 'Login', 'wpk' ) }}</span>
            </label>
            <label class="wpk-input-container wpk-password">
                <input type="password" id="password" name="password">
                <span class="wpk-label">{{ __( 'Password', 'wpk' ) }}</span>
            </label>

            <div class="wpk-submit-container">
                <input type="hidden" name="wpk_nonce" id="wpk_nonce" value="{{ wp_create_nonce( 'wpk_login' ) }}">
                <button class="wpk-button wpk-submit wpk-float-left">
                    {{ __( 'Login', 'wpk' )  }}
                </button>
                <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" scope="public_profile,email,instagram_basic,instagram_manage_insights,manage_pages"
                     onlogin="checkLoginState();"></div>
            </div>
        </form>

        <div class="wpk-links">
            <a href="{{ add_query_arg('action', 'lostpassword', wp_login_url() )}}">{{ __('Lost your password?', 'wpk') }}</a>
            <a href="{{ home_url() }}">←{{ __('Back to reachly', 'wpk') }}</a>
        </div>
    </div>
</div>