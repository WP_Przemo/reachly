<h3 class="wpk-ig-title">{{ __('Select instagram account', 'wpk')  }}</h3>

<ul class="wpk-ig-accounts">
    @foreach($accounts as $account)
        <li class="wpk-ig-account" data-id="{{ $account['id'] }}">
            <span class="wpk-ig-name">{{ $account['username'] }}</span>
            <div class="wpk-ig-avatar">
                <img src="{{ $account['profile_picture_url'] }}" alt="">
            </div>
        </li>
    @endforeach
</ul>