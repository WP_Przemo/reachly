<div class="wpk-collaborate-thankyou">
    <i class="linear-icon icon-smile"></i>
    <h2>{{ __( 'You have accepted invite to the campaign.', 'wpk' ) }}</h2>
    <h3>{{ __( 'Thanks for your collaboration!', 'wpk' ) }}</h3>

    <a href="{{ \Wpk\Pages::getBrandsPageUrl() }}">
        <span>{{ __( 'Go back to campaigns', 'wpk' )  }}</span>
        <i class="material-icons">keyboard_arrow_right</i>
    </a>
</div>