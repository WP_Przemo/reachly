<div class="wpk-collaborate-thankyou">
	<i class="linear-icon icon-smile"></i>
	<h2><?php _e( 'Your message has been sent.', 'wpk' ) ?></h2>
	<h3><?php _e( 'Thanks for your collaboration!', 'wpk' ) ?></h3>

	<a href="<?php echo get_permalink( \Wpk\Pages::getBrandsPageUrl() ) ?>">
		<span><?php _e( 'Go back to campaigns', 'wpk' ) ?></span>
		<i class="material-icons">keyboard_arrow_right</i>
	</a>
</div>