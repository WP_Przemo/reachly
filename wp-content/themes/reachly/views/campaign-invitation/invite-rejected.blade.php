<div class="wpk-collaborate-thankyou">
    <i class="linear-icon icon-sad"></i>
    <h2>{{ __( 'You have rejected invitation to this campaign.', 'wpk' )}}</h2>
    <h3>{{ __( 'You won\'t be able to be invited to it again.', 'wpk' ) }}</h3>

    <a href="{{ \Wpk\Pages::getBrandsPageUrl() }}">
        <span>{{ __( 'Go back to campaigns', 'wpk' )  }}</span>
        <i class="material-icons">keyboard_arrow_right</i>
    </a>
</div>