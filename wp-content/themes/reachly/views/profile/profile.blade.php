<div class="wpk-wrap row" id="wpk_user_profile">
    <div class="wpk-page-title">
        <h2>{{ __('Profile Settings', 'wpk')  }}</h2>
    </div>
    @include('sidebar', ['template' => $sidebar])
    <div class="col-xl-10 col-md-12 wpk-wrap-inner">
        <div class="wpk-tab-container">
            <form action="#" id="wpk_profile_form" class="wpk-form" method="post">
                @if($type === 'influencer' || $type === 'company')
                    @include("profile.$type.general")
                    @include("profile.$type.change-password")
                    @include("profile.$type.notifications")
                @endif
                <div class="wpk-tab" id="notifications"></div>
                <div class="wpk-submit-container">
                    <input type="hidden" name="wpk_nonce" id="wpk_nonce" value="{{ wp_create_nonce( 'wpk_profile' ) }}">
                    <button class="wpk-button wpk-submit wpk-float-left">
                        {{ __( 'Save Information' )  }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>