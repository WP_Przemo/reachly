<div class="wpk-tab" id="notifications">
    <div class="wpk-tab-title">
        <h2>{{ __( 'Notification settings', 'wpk' ) }}</h2>
    </div>
    <div class="wpk-tab-inner">
        <div class="wpk-form-wrap">
            <div class="wpk-form-inner wpk-full-inputs">
                <div class="wpk-input-container wpk-checkboxes">
                    <div class="wpk-input-container-inner">
                        <div class="wpk-checkbox">
                            <input {{ checked(true, $user->meta( 'notification_email' ), false) }} type="checkbox" name="notification_email" id="notification_email" value="on">
                            <label for="notification_email"><span>{{  __( 'Send notifications to my email', 'wpk' ) }}</span></label>
                        </div>
                        <div class="wpk-checkbox">
                            <input {{ checked(true, $user->meta('notification_dashboard'), false) }} type="checkbox" name="notification_dashboard" id="notification_dashboard" value="on">
                            <label for="notification_dashboard"><span>{{ __( 'Send notifications to my dashboard', 'wpk' )  }}</span></label>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>