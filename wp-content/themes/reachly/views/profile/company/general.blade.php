<div class="wpk-tab" id="general">
    <div class="wpk-tab-title">
        <h2>{{ __( 'General Information', 'wpk' ) }}</h2>
    </div>
    <div class="wpk-tab-inner">
        <div class="wpk-form-wrap">
            <div class="wpk-form-inner wpk-full-inputs wpk-float-label">
                <div class="wpk-input-container wpk-two-inputs">
                    <label>
                        <input type="text" id="first_name" name="first_name" value="{{ $user->first_name }}">
                        <span class="wpk-label">{{ __( 'First name*', 'wpk' )  }}</span>
                    </label>
                    <label>
                        <input type="text" id="last_name" name="last_name" value="{{ $user->last_name }}">
                        <span class="wpk-label">{{ __( 'Last name*', 'wpk' ) }}</span>
                    </label>
                </div>
                <label class="wpk-input-container">
                    <input type="text" id="phone" name="phone" value="{{ $user->meta( 'phone' ) }}">
                    <span class="wpk-label">{{ __( 'Phone*', 'wpk' ) }}</span>
                </label>
                <label class="wpk-input-container">
                    <input type="text" id="company" name="company" value="{{ $user->meta('company') }}">
                    <span class="wpk-label">{{ __( 'Company name*', 'wpk' ) }}</span>
                </label>
                <label class="wpk-input-container">
                    <input type="text" id="website" name="website" value="{{ $user->meta( 'website' ) }}">
                    <span class="wpk-label">{{ __( 'Website', 'wpk' ) }}</span>
                </label>
                <label class="wpk-input-container">
                    <input type="text" id="email" name="email" value="{{ $user->user_email }}">
                    <span class="wpk-label">{{ __( 'Email*', 'wpk' ) }}</span>
                </label>
                <label class="wpk-input-container">
                    <input class="wpk-google-autocomplete" type="text" id="address" name="address" value="{{ $user->meta( 'address' ) }}">
                    <span class="wpk-label">{{ __( 'Address*', 'wpk' ) }}</span>
                </label>

                <div class="wpk-input-container wpk-two-inputs">
                    <label>
                        <input type="text" id="city" name="city" value="{{ $user->meta( 'city' ) }}">
                        <span class="wpk-label">{{ __( 'City*', 'wpk' ) }}</span>
                    </label>
                    <label>
                        <input type="text" id="zip" name="zip" value="{{ $user->meta( 'zip' ) }}">
                        <span class="wpk-label">{{ __( 'Zip*', 'wpk' ) }}</span>
                    </label>
                </div>
                <div id="birth_date_calendar" class="wpk-calendar-container wpk-no-dropdown"></div>

                @include('partials.form.file-input', [
                'label' => __( 'Logo', 'wpk' ),
                'images' => [$user->getAvatar()],
                'action' => 'wpk_avatar_upload',
                'removeAction' => 'wpk_delete_avatar'
                ])

                <div class="wpk-input-container wpk-full-width">
					<span class="wpk-input-label">
						{{ __( 'Write about yourself: ', 'wpk' ) }}
					</span>
                    <textarea name="bio" id="bio" cols="30" rows="10" placeholder="{{ __( 'Start typing here...', 'wpk' )  }}">{{ $user->meta('bio') }}</textarea>

                </div>

            </div>
        </div>
    </div>
</div>