<div class="wpk-tab" id="change_password">
    <div class="wpk-tab-title">
        <h2>{{ __( 'Change password', 'wpk' ) }}</h2>
    </div>
    <div class="wpk-tab-inner">
        <div class="wpk-form-wrap">
            <div class="wpk-form-inner wpk-full-inputs wpk-float-label">
                <div class="wpk-input-container wpk-two-inputs">
                    <label class="wpk-input-container">
                        <input type="password" id="new_password" name="new_password">
                        <span class="wpk-label">{{ __( 'New password', 'wpk' ) }}</span>
                    </label>
                    <label class="wpk-input-container">
                        <input type="password" id="new_password_repeat" name="new_password_repeat">
                        <span class="wpk-label">{{ __( 'Re-enter new password', 'wpk' ) }}</span>
                    </label>
                </div>
            </div>
        </div>
    </div>

</div>
