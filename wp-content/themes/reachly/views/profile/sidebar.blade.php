<?php
/**
 * Sidebar view for profile page
 */
?>

<div class="wpk-user-profile-sidebar">
    <div class="wpk-horizontal-sections">
        <div class="wpk-section" data-title="{{ __( 'General Info', 'wpk' ) }}" data-target="#general">
        </div>
        <div class="wpk-section-separator"></div>
        <div class="wpk-section" data-title="{{ __( 'Password', 'wpk' ) }}" data-target="#change_password">

        </div>
        <div class="wpk-section-separator"></div>
        <div class="wpk-section" data-title="{{ __( 'Notifications', 'wpk' ) }}" data-target="#notifications">

        </div>
    </div>
</div>