<div class="wpk-sidebar-form wpk-filter-container wpk-sidebar-section wpk-form row">
    <div class="wpk-input-container wpk-location-icon">
        <div class="wpk-input-label">
            <span><?php _e( 'Address:', 'wpk' ) ?></span>
        </div>
        <input type="text" name="address" id="address" class="wpk-location wpk-filter-input">
    </div>
</div>
