<div class="wpk-sidebar-form wpk-filter-container wpk-range-slider wpk-sidebar-section wpk-form row" data-min="18" data-max="100">
    <div class="wpk-input-container wpk-checkboxes">
        <div class="wpk-input-container-inner">
            <div class="wpk-input-label">
                <span>{{ __( 'Statuses', 'wpk' ) }}</span>
            </div>
            <div class="wpk-checkbox">
                <input type="checkbox" class="wpk-filter-input" value="new" name="statuses[]" id="wpk_new">
                <label for="wpk_new"><span>{{ __( 'New (without correlation with that campaign)' ) }}</span></label>
            </div>
            <div class="wpk-checkbox">
                <input type="checkbox" class="wpk-filter-input" value="bookmarked" name="statuses[]" id="wpk_bookmarked">
                <label for="wpk_bookmarked"><span>{{ __( 'Saved' ) }}</span></label>
            </div>
            <div class="wpk-checkbox">
                <input type="checkbox" class="wpk-filter-input" name="statuses[]" value="accepted_by_influencer" id="wpk_accepted_by_influencer">
                <label for="wpk_accepted_by_influencer"><span>{{ __( 'Accepted' ) }}</span></label>
            </div>
            @if ( $template === 'edit_campaign.php' )
                <div class="wpk-checkbox">
                    <input type="checkbox" class="wpk-filter-input" name="statuses[]" value="accepted_by_influencer_before" id="wpk_accepted_by_influencer_before">
                    <label for="wpk_accepted_by_influencer_before"><span>{{ __( 'Accepted before' ) }}</span></label>
                </div>
            @endif
            <div class="wpk-checkbox">
                <input type="checkbox" class="wpk-filter-input" value="declined_influencers" name="statuses[]" id="wpk_declined_influencers">
                <label for="wpk_declined_influencers"><span>{{ __( 'Declined' ) }}</span></label>
            </div>
            <div class="wpk-checkbox">
                <input type="checkbox" class="wpk-filter-input" value="accepted_by_company" name="statuses[]" id="wpk_accepted_by_company">
                <label for="wpk_accepted_by_company"><span>{{ __( 'Accepted by company' ) }}</span></label>
            </div>
            @if ( $template === 'edit_campaign.php' )
                <div class="wpk-checkbox">
                    <input type="checkbox" class="wpk-filter-input" value="accepted_by_company_before" name="statuses[]" id="wpk_accepted_by_company_before">
                    <label for="wpk_accepted_by_company_before"><span>{{ __( 'Accepted by company before' ) }}</span></label>
                </div>
            @endif
            <div class="wpk-checkbox">
                <input type="checkbox" class="wpk-filter-input" value="invited_by_company" name="statuses[]" id="wpk_invited_by_company">
                <label for="wpk_invited_by_company"><span>{{ __( 'Invited by company' ) }}</span></label>
            </div>
            <div class="wpk-checkbox">
                <input type="checkbox" class="wpk-filter-input" value="want_to_collaborate" name="statuses[]" id="wpk_want_to_collaborate">
                <label for="wpk_want_to_collaborate"><span>{{ __( 'Want to collaborate' ) }}</span></label>
            </div>
            <div class="wpk-checkbox">
                <input type="checkbox" class="wpk-filter-input" value="skipped" name="statuses[]" id="wpk_skipped_influencers">
                <label for="wpk_skipped_influencers"><span>{{ __( 'Skipped' ) }}</span></label>
            </div>
        </div>
    </div>
</div>