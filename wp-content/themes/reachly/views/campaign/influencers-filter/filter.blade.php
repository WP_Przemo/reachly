<form class="wpk-influencers-filters">
    <div class="row-wpk-sidebar-section wpk-mobile-section wpk-sidebar-form wpk-filter-container wpk-form">
        <div class="wpk-filter-mobile-activator">
            <i class="material-icons wpk-open">menu</i>
            <span class="wpk-uppercase">{{ __('Filters', 'wpk') }}</span>
        </div>
    </div>
    <div class="wpk-sidebar-separator"></div>
    <div class="wpk-influencers-filter-inner">
        @if ( $template === 'edit_campaign.php')
            <div class="row wpk-sidebar-section wpk-return-link wpk-uppercase">
                <a href="{{ $ref }}">
                    <i class="material-icons">keyboard_arrow_left</i>
                    <span>{{ __( 'Go back', 'wpk' )  }}</span>
                </a>
            </div>
        @else
            {{--TODO Campaign edit url--}}
            <div class="row wpk-sidebar-section wpk-return-link wpk-uppercase">
                <a href="{{ \Wpk\Pages::getCampaignCreationUrl( 3, $campaign->ID ) }}">
                    <i class="material-icons">keyboard_arrow_left</i>
                    <span>{{ __( 'Edit details', 'wpk' ) }}</span>
                </a>
            </div>
        @endif

        @include('campaign.influencers-filter.categories')
        <div class="wpk-sidebar-separator"></div>
        @include('campaign.influencers-filter.address')
        <div class="wpk-sidebar-separator"></div>
        @include('campaign.influencers-filter.age')
        <div class="wpk-sidebar-separator"></div>
        @include('campaign.influencers-filter.gender')
        <div class="wpk-sidebar-separator"></div>
        @include('campaign.influencers-filter.statuses')

        {{--  @include('campaign.influencers-filter.instagram-insights')--}}


    </div>
    <input type="hidden" name="wpk_nonce" id="wpk_nonce" value="{{ wp_create_nonce('wpk_influencers_filter') }}">
    <input type="hidden" name="wpk_filter" id="wpk_filter" value="1">
</form>