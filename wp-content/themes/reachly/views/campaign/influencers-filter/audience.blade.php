<div class="wpk-sidebar-form wpk-filter-container wpk-sidebar-section wpk-form row wpk-has-next-section">
    <div class="wpk-input-container wpk-selection-under">
        <div class="wpk-input-container-inner wpk-select2-container">
            <div class="wpk-input-label">
                <span><?php _e( 'Audience place of residence:', 'wpk' ) ?></span>
            </div>
        </div>
    </div>
</div>
<div class="wpk-sidebar-form wpk-filter-container wpk-sidebar-section wpk-form row wpk-has-next-section">
    <div class="wpk-input-container wpk-selection-under">
        <div class="wpk-input-container-inner wpk-select2-container">
            <label for=""><?php _e( 'Select country' ) ?></label>
            <select id="categories" data-type="meta" data-key="instagram_audience_country_" data-value_type="array" data-target="user" data-compare="EXISTS" class="wpk-select2-under-save wpk-float-left wpk-filter-input" multiple="multiple" placeholder="<?php _e( 'Choose country...' ) ?>">

				<?php /** @var WP_Term $category */
				foreach ( \Wpk\Utility::getAllCountries() as $country ) : ?>
                <option value="<?php echo strtolower( $country ) ?>"><?php echo $country ?></option>
				<?php endforeach; ?>

            </select>
            <div class="wpk-selections">
            </div>
        </div>
    </div>
</div>
<div class="wpk-sidebar-form wpk-filter-container wpk-sidebar-section wpk-form row">
    <div class="wpk-input-container wpk-selection-under">
        <div class="wpk-input-container-inner wpk-select2-container">
            <label for=""><?php _e( 'Select city:', 'wpk' ) ?></label>
            <select id="instagram_audience_city_" data-type="meta" data-key="instagram_audience_city_" data-value_type="array" data-target="user" data-compare="EXISTS" class="wpk-select2-under-save wpk-float-left wpk-filter-input" multiple="multiple" placeholder="<?php _e( 'Choose city...' ) ?>">

				<?php /** @var WP_Term $category */
				foreach ( get_option( 'wpk_cities', [] ) as $slug => $city ) : ?>
                <option value="<?php echo $slug ?>"><?php echo $city ?></option>
				<?php endforeach; ?>

            </select>
            <div class="wpk-selections">
            </div>
        </div>
    </div>
</div>
<div class="wpk-sidebar-separator"></div>