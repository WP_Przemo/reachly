<div class="wpk-sidebar-form wpk-filter-container wpk-sidebar-section wpk-form row">
    <div class="wpk-input-container wpk-gender-filter">
        <div class="wpk-input-container-inner">
            <div class="wpk-input-label">
                <span>{{ __( 'Filter by gender:', 'wpk' ) }}</span>
            </div>

            @foreach ( \Wpk\Models\User::$genders as $key => $gender )
                <div class="wpk-button wpk-checkbox-button wpk-float-left">{{ $gender }}</div>
                <input name="gender" class="wpk-gender wpk-filter-input" type="checkbox" value="{{ $key }}">
            @endforeach
        </div>
    </div>
</div>
<div class="wpk-sidebar-separator"></div>