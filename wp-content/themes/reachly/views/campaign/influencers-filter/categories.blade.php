<div class="wpk-sidebar-form wpk-filter-container wpk-sidebar-section wpk-form row">
    <div class="wpk-input-container wpk-selection-under">
        <div class="wpk-input-container-inner wpk-search-icon wpk-select2-container">
            <div class="wpk-input-label">
                <span>{{ __( 'Categories:', 'wpk' ) }}</span>
            </div>
            <select id="categories" name="interests[]" class="wpk-select2-under wpk-float-left wpk-filter-input" multiple="multiple" placeholder="{{ __( 'Choose categories...', 'wpk' ) }}">
                @foreach ( $categories as $category )
                    <option value="{{ $category->slug  }}">{{ $category->name  }}</option>
                @endforeach
            </select>
            <div class="wpk-selections">
            </div>
        </div>
    </div>
</div>