<div class="wpk-sidebar-form wpk-filter-container wpk-range-slider wpk-sidebar-section wpk-form row" data-min="18" data-max="100">
    <div class="wpk-input-container">
        <div class="wpk-input-container-inner">
            <div class="wpk-input-label">
                <span>{{ __( 'Filter by age:', 'wpk' ) }}</span>
            </div>
            <input type="hidden" name="age[]" class="wpk-filter-input wpk-range-input">
            <input type="hidden" name="age[]" class="wpk-filter-input wpk-range-input">
            <div class="wpk-range-container"></div>
            <div class="wpk-range-value">
                <span class="wpk-value-target">18</span> {{ __('age', 'wpk') }} -
                <span class="wpk-value-target">100</span> {{ __('age') }}
            </div>
        </div>
    </div>
</div>