<div class="wpk-sidebar-form wpk-range-slider wpk-filter-container wpk-sidebar-section wpk-form row" data-min="0" data-max="5000000">
    <div class="wpk-input-container">
        <div class="wpk-input-container-inner">
            <div class="wpk-input-label">
                <span><?php _e( 'Filter by followers:', 'wpk' ) ?></span>
            </div>
            <input type="hidden" data-type="meta" data-key="instagram_followers_count" data-value_type="NUMERIC" data-target="user" data-compare=">=" class="wpk-filter-input wpk-range-input">
            <input type="hidden" data-type="meta" data-key="instagram_followers_count" data-value_type="NUMERIC" data-target="user" data-compare="<=" class="wpk-filter-input wpk-range-input">
            <div class="wpk-range-container"></div>
            <div class="wpk-range-value">
				<?php _e( '<span class="wpk-value-target">0</span> followers - <span class="wpk-value-target">5000000</span> followers' ) ?>
            </div>
        </div>
    </div>
</div>
<div class="wpk-sidebar-separator"></div>
<div class="wpk-sidebar-form wpk-filter-container wpk-range-slider wpk-sidebar-section wpk-form row" data-min="0" data-max="5000000">
    <div class="wpk-input-container">
        <div class="wpk-input-container-inner">
            <div class="wpk-input-label">
                <span><?php _e( 'Filter by likes per post:', 'wpk' ) ?></span>
            </div>
            <input type="hidden" data-type="meta" data-key="instagram_likes_per_post" data-value_type="NUMERIC" data-target="user" data-compare=">=" class="wpk-filter-input wpk-range-input">
            <input type="hidden" data-type="meta" data-key="instagram_likes_per_post" data-value_type="NUMERIC" data-target="user" data-compare="<=" class="wpk-filter-input wpk-range-input">
            <div class="wpk-range-container"></div>
            <div class="wpk-range-value">

				<?php _e( '<span class="wpk-value-target">0</span> likes - <span class="wpk-value-target">5000000</span> likes' ) ?>

            </div>
        </div>
    </div>
</div>
<div class="wpk-sidebar-separator"></div>
<div class="wpk-sidebar-form wpk-filter-container wpk-range-slider wpk-sidebar-section wpk-form row" data-min="0" data-max="5000000">
    <div class="wpk-input-container">
        <div class="wpk-input-container-inner">
            <div class="wpk-input-label">
                <span><?php _e( 'Filter by number of comments:', 'wpk' ) ?></span>
            </div>
            <input type="hidden" data-type="meta" data-key="instagram_comments_number" data-value_type="NUMERIC" data-target="user" data-compare=">=" class="wpk-filter-input wpk-range-input">
            <input type="hidden" data-type="meta" data-key="instagram_comments_number" data-value_type="NUMERIC" data-target="user" data-compare="<=" class="wpk-filter-input wpk-range-input">
            <div class="wpk-range-container"></div>
            <div class="wpk-range-value">

				<?php _e( '<span class="wpk-value-target">0</span> comments - <span class="wpk-value-target">5000000</span> comments' ) ?>

            </div>
        </div>
    </div>
</div>
<div class="wpk-sidebar-separator"></div>