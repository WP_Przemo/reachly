<?php
/** @var \Wpk\Models\Campaign $campaign */
?>

<div class="wpk-single-campaign-sidebar">
    <div class="row wpk-sidebar-section wpk-return-link wpk-uppercase">
        <a href="{{ \Wpk\Pages::getMyCampaignsUrl() }}">
            <i class="material-icons">keyboard_arrow_left</i>
            <span>{{ __( 'Go back to campaigns', 'wpk' ) }}</span>
        </a>
    </div>
    <div class="row wpk-sidebar-section">
        <div class="wpk-section-title">
            {{ __( 'Time duration:', 'wpk' ) }}
        </div>
        <div class="wpk-date-range-tabs">
            <div class="wpk-date-tab">
                <div class="wpk-tab-icon">
                    <i class="linear-icon icon-calendar-check"></i>
                </div>
                <div class="wpk-tab-thin">
                    {{ $startDate->format( 'l' )  }}
                </div>
                <div class="wpk-tab-bold">
                    {{ $startDate->format( 'd M' ) }}
                </div>
            </div>
            <div class="wpk-date-tab-separator">
                <i class="material-icons">keyboard_arrow_right</i>
            </div>
            <div class="wpk-date-tab">
                <div class="wpk-tab-icon">
                    <i class="linear-icon icon-calendar-cross"></i>
                </div>
                <div class="wpk-tab-thin">
                    {{ $endDate->format( 'l' )  }}
                </div>
                <div class="wpk-tab-bold">
                    {{ $endDate->format( 'd M' ) }}
                </div>
            </div>
        </div>
    </div>
    <div class="wpk-sidebar-separator"></div>

    @if(!empty($categories))
        <div class="row wpk-sidebar-section wpk-selection-under wpk-selection-disabled">
            <div class="wpk-section-title">
                {{ __( 'Product category:', 'wpk' )  }}
            </div>
            <div class="wpk-selections">

                @foreach($categories as $category)

                    <div class="wpk-selection">
                        {{ $category->name }}
                    </div>

                @endforeach

            </div>
        </div>
    @endif

    <div class="wpk-sidebar-separator"></div>
    <div class="row wpk-sidebar-section">
        <div class="wpk-section-title">
            {{ __( 'Edit campaign:', 'wpk' )  }}
        </div>
        <div class="wpk-sidebar-buttons">

            <a href="{{ $campaign->getEditUrl() }}" class="wpk-button">
                {{ __('Edit details', 'wpk') }}
            </a>

            @if($campaign->canBeModified('renewed_date'))

                <a href="{{ $campaign->getInfluencersEditUrl() }}" class="wpk-button">
                    {{ __( 'Edit influencers', 'wpk' ) }}
                </a>

            @endif

        </div>
    </div>
</div>
