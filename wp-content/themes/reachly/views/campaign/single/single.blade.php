<?php
/**
 * @var \Wpk\Models\Campaign $campaign
 */
?>

<div class="wpk-wrap row wpk-my-campaigns">

    @if($isAuthor && !$isPreview)
        @include('sidebar', ['template' => 'campaign.single.sidebar'])
    @endif

    <div class="{{ $isAuthor && !$isPreview ? 'col-xl-10' : 'col-xl-12' }} col-md-12 wpk-wrap-inner">
        <div class="wpk-single-campaign wpk-single-campaign-{{ $userType }}">

            @if($userType === 'influencer')
                @include('campaign.single.actions.influencer')
            @elseif($isPreview)
                @include('campaign.single.actions.preview')
            @endif

            <article class="wpk-single-campaign-inner" id="{{ $campaign->ID }}">

                @if(!empty($logo))
                    <div class="wpk-campaign-logo">
                        <img src="{{ $logo }}" alt="">
                    </div>
                @endif

                @if(!empty($website))
                    <div class="wpk-campaign-website">
                        <a href="{!! $website !!}">{!! $website !!}</a>
                    </div>
                @endif

                <div class="wpk-campaign-thumbnail">

                    @if(!empty($thumbnail))
                        <img src="{{ $thumbnail }}" alt="">
                    @endif

                    <div class="wpk-campaign-date">
                        <i class="material-icons">date_range</i>

                        @if($hasEnded)

                            <span>{{ __('Campaign has ended: ', 'wpk') }}</span>
                            <span>{{ $startDate->format('F d') }}</span>

                        @elseif($hasStarted)

                            <span>{{ __('Campaign has started: ', 'wpk') }}</span>
                            <span>{{ $startDate->format('F d')  }}</span>

                        @else

                            <span>{{ __('Campaign starts: ', 'wpk') }}</span>
                            <span>{{ $startDate->format('F d')  }}</span>

                        @endif

                    </div>

                </div>
                <div class="wpk-campaign-title wpk-section">
                    <div class="wpk-section-inner">
                        {{ $title }}
                    </div>
                </div>
                <div class="wpk-campaign-description wpk-section">
                    <div class="wpk-section-title">
                        {{ __( 'What we\'re looking for?', 'wpk' ) }}
                    </div>
                    <div class="wpk-section-inner">
                        {!! $content !!}
                    </div>
                </div>
                @if(!empty($tags))

                    <div class="wpk-campaign-tags wpk-section">

                        <div class="wpk-section-title">
                            {{ __('Preffered tags', 'wpk') }}
                        </div>

                        <div class="wpk-section-inner">
                            @foreach($tags as $tag)
                                <div class="wpk-tag">
                                    #{{ $tag->name }}
                                </div>
                            @endforeach
                        </div>
                    </div>

                @endif

                @if(!empty($items))

                    <div class="wpk-item-section wpk-section">
                        <div class="wpk-section-title">
                            {{ __( 'What you get?', 'wpk' ) }}
                        </div>
                        <div class="wpk-section-inner">

                            @foreach($items as $item)
                                <div class="wpk-checkbox">
                                    <input type="checkbox" checked>
                                    <label>{{ $item }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                @endif

            </article>

        </div>
    </div>
</div>
