<div class="wpk-navigation">
    <div class="wpk-border-round-button wpk-close-modal wpk-tooltip-target">
        <i class="material-icons">keyboard_arrow_left</i>
        <div class="wpk-tooltip ">
            <div class="wpk-tooltip-text">
                {{ __('Close preview', 'wpk') }}
            </div>
        </div>
    </div>
</div>