<?php
/** @var \Wpk\Models\User $user */
?>
<div class="wpk-navigation-actions">
    <div class="wpk-item-actions">

        @if($user->hasValidToken())

            @foreach($actions as $action)

                <div data-action="{{ $action['action'] }}" class="wpk-influencer-action wpk-button wpk-tooltip-target {{ !empty($action['disabled']) ? 'wpk-action-disabled' : '' }}" data-campaign-id="{{ $campaign->ID }}">
                    <i class="material-icons"></i>
                    <span>{{ $action['text'] }}</span>
                    @isset($action['tooltip'])
                        <div class="wpk-tooltip">
                            <div class="wpk-tooltip-message">{{ $action['tooltip'] }}</div>
                        </div>
                    @endisset
                </div>

            @endforeach

        @endif

    </div>
</div>