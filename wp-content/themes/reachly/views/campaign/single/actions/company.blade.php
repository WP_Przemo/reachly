<div class="wpk-navgiation-actions">

    @if(!empty($logo))
        <div class="wpk-campaign-logo">
            <img src="{{ $logo }}" alt="">
        </div>
    @endif

    @if(!empty($website))
        <div class="wpk-campaign-website">
            <a href="{!! $website !!}">{!! $website !!}</a>
        </div>
    @endif
</div>