<?php
/**
 * Campaign checkout view
 *
 * @var \Wpk\Models\User     $user
 * @var \Wpk\Models\Campaign $campaign
 */
?>

@php
    wc_print_notices();
@endphp

@if(!$user->isBillingFilled())

    <a class="wpk-circle-icon wpk-has-message wpk-full-icon" href="{{ $profileUrl }}">
        <div class="wpk-circle">
            <div class="wpk-circle-inner">
                <i class="material-icons">shopping_cart</i>
            </div>
        </div>
        <div class="wpk-message">
            {{ __( 'You need to fill all billing details on your profile.', 'wpk' ) }}
        </div>
    </a>

@else

    <div class="woocommerce">
        <h3 class="wpk-uppercase"><?php _e( 'Payment method', 'woocommerce' ) ?></h3>
        <div class="wpk-payment-methods wpk-radio-activators">
            <div class="wpk-button wpk-paypal-button" data-target="#payment_method_paypal">
                <img class="wpk-active-image" src="{{ $icons['paypal_active'] }}" alt="">
                <img class="wpk-default-image" src="{{ $icons['paypal_default'] }}" alt="">
            </div>
            <div class="wpk-button" data-target="#payment_method_stripe">
                <img class="wpk-active-image" src="{{ $icons['stripe_active'] }}" alt="">
                <img class="wpk-default-image" src="{{ $icons['stripe_default'] }}" alt="">
            </div>
            <div class="wpk-button" data-target="#payment_method_cheque">
                Cheque
            </div>
        </div>

        @if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() )

            {{ $loginMessage }}

        @else

            <form name="checkout" method="post" class="checkout woocommerce-checkout" action="{{ esc_url( wc_get_checkout_url() ) }}" enctype="multipart/form-data">

                @if ( $checkout->get_checkout_fields() )

                    @php do_action( 'woocommerce_checkout_before_customer_details' ) @endphp

                    <div class="row" id="customer_details" style>
                        <div class="col-md-12">
                            @php do_action( 'woocommerce_checkout_billing' ); @endphp
                        </div>

                        <div class="col-md-12">
                        </div>
                    </div>

                    @php do_action( 'woocommerce_checkout_after_customer_details' ); @endphp

                @endif

                <h3 class="wpk-uppercase">{{ __( 'Your order', 'woocommerce' ) }}</h3>

                @php do_action( 'woocommerce_checkout_before_order_review' ); @endphp

                <div id="order_review" class="woocommerce-checkout-review-order">
                    @php do_action( 'woocommerce_checkout_order_review' ); @endphp
                </div>

                @php do_action( 'woocommerce_checkout_after_order_review' ); @endphp

            </form>

            @php do_action( 'woocommerce_after_checkout_form', $checkout ); @endphp

        @endif

    </div>
@endif