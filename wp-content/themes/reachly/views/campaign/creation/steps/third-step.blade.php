<?php
/**
 * Third step of campaign creation view
 *
 * @var \Wpk\Models\Campaign $campaign
 */
?>

<div class="wpk-form-wrap">
    <form id="wpk_form" class="wpk-form wpk-new-campaign-form wpk-form-step-3" action="#" method="post" data-campaign="{{ $campaign->ID }}">
        <div class="wpk-form-inner">
            <div class="wpk-input-container wpk-campaign-title">
                <div class="wpk-input-container-inner">
                    <input autocomplete="off" type="text" id="title" name="title" placeholder="{{ __( 'Add campaign title...', 'wpk' ) }}" value="{{ $campaign->post_title }}">
                </div>
            </div>
            <div class="wpk-input-container wpk-textarea-container wpk-campaign-description">
                <div class="wpk-input-container-inner">
                    <div class="wpk-input-label">
                        <span>{{ __('Describe what you search for', 'wpk') }}</span>
                        <span class="wpk-quotation-mark">?</span>

                    </div>
                    @php
                        wp_editor( $campaign, 'description', [
						'editor_class'  => 'wpk_editor',
						'media_buttons' => false,
						'tinymce'       => [
							'statusbar' => false,
							'toolbar1'  => 'bold italic underline | forecolor blockquote bullist numlist | media',
						],
					] ) @endphp
                </div>
            </div>
            <div class="wpk-input-container">
                <div class="wpk-input-container-inner">
                    <div class="wpk-input-label">
                        <span>{{ __( 'What influencer will get: ', 'wpk' ) }}</span>
                    </div>
                    <div class="wpk-list-item">
                        <input type="text" name="items[]" class="wpk-campaign-items wpk-float-left" value="@php echo empty( $items ) ? '' : $items[ 0 ] @endphp">
                        <div class="wpk-button wpk-add-more wpk-float-left">{{ __( 'Add more', 'wpk' ) }}</div>
                    </div>
                    @if ( ! empty( $items ) && count( $items ) > 1 )
                        @php $counter = 0 @endphp
                        @foreach ( $items as $item )
                            @if ( $counter > 0 )
                                <div class="wpk-list-item">
                                    <input type="text" name="items[]" class="wpk-campaign-items wpk-float-left" value="{{ $item }}">
                                    <div class="wpk-button wpk-remove-item wpk-float-left">-</div>
                                </div>
                            @endif
                            @php $counter ++ @endphp
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="wpk-input-container">
                <div class="wpk-input-container-inner">
                    <div class="wpk-input-label">
                        <span>{{ __( 'Company website address', 'wpk' ) }}</span>
                    </div>
                    <input type="text" name="website" id="website" value="{{ $campaign->meta('website') }}">
                </div>
            </div>

            @include('partials.form.file-input', [
              'label' => __( 'Featured photo', 'wpk' ),
              'images' => [$campaign->getThumbnailUrl()],
              'action' => 'wpk_campaign_thumbnail_upload',
              'removeAction' => 'wpk_delete_campaign_thumbnail',
              'id' => $campaign->ID
              ])

            <div class="wpk-submit-container wpk-submit-actions">
                <input type="hidden" name="wpk_campaign_nonce" id="wpk_campaign_nonce" value="{{ wp_create_nonce('wpk_campaign_nonce') }}">
                <input type="hidden" name="campaign_id" id="campaign_id" value="{{ $campaign->ID }}">
                <a href="{{ \Wpk\Pages::getCampaignPreviewPageUrl()  }}" class="wpk-hidden wpk-preview-link" target="_blank">{{ __('Preview', 'wpk') }} </a>
                <button class="wpk-button wpk-campaign-preview">
                    <span>{{ __( 'Preview Campaign' )  }}</span>
                </button>
                <button class="wpk-button wpk-submit" type="submit">
                    <span>{{ __( 'Save & go to next step', 'wpk' )  }}</span>
                </button>
            </div>
        </div>
    </form>
</div>