<?php
/**
 * View for third step sidebar
 *
 * @var \Wpk\Models\Campaign $campaign
 */
?>

<div class="wpk-sidebar-form wpk-sidebar-section wpk-form row">
    <div class="wpk-input-container">
        <div class="wpk-input-container-inner wpk-date-picker-input">
            <div class="wpk-input-label">
                <span>{{ __( 'Pick starting date', 'wpk' ) }}</span>
            </div>
            <input autocomplete="off" data-length="{{ $campaign->meta('length')  }}" type="text" name="start_date" id="start_date" value="{{ $campaign->startDate() }}" class="wpk-date-picker wpk-campaign-date">
            <div id="start_date_calendar" class="wpk-calendar-container"></div>
        </div>
    </div>
</div>
<div class="wpk-sidebar-separator"></div>
<div class="wpk-sidebar-form wpk-sidebar-section wpk-form row">

    @include('partials.form.file-input', [
            'label' => __( 'Campaign logo', 'wpk' ),
            'images' => [$campaign->getLogoUrl()],
            'action' => 'wpk_campaign_logo_upload',
            'removeAction' => 'wpk_delete_campaign_logo',
             'id' => $campaign->ID
            ])

</div>

<div class="wpk-sidebar-separator"></div>
<div class="wpk-sidebar-form wpk-sidebar-section wpk-form row">
    <div class="wpk-input-container wpk-select-container wpk-selection-under">
        <div class="wpk-input-container-inner wpk-input-selector wpk-categories">
            <div class="wpk-input-label">
                <span>{{ __( 'Product categories', 'wpk' )  }}</span>
            </div>
            <select name="categories" id="categories" class="wpk-select2-under wpk-float-left" multiple="multiple" placeholder="{{ __( 'Choose categories...', 'wpk' ) }}">

                @foreach($categories as $category)
                    <option value="{{ $category->slug }}">{{ $category->name }}</option>
                @endforeach
            </select>

            <div class="wpk-selections">
                @foreach($campaignCategories as $campaignCategory)
                    <div class="wpk-selection" data-value="{{ $campaignCategory->slug }}">{{ $campaignCategory->name }}</div>
                @endforeach
            </div>

        </div>
    </div>
</div>
<div class="wpk-sidebar-separator"></div>
<div class="wpk-sidebar-form wpk-sidebar-section wpk-form row">
    <div class="wpk-input-container wpk-select-container wpk-selection-under">
        <div class="wpk-input-container-inner wpk-input-selector wpk-tags">
            <div class="wpk-input-label">
                <span>{{ __( 'Tags', 'wpk' ) }}</span>
            </div>
            @php
                $selected = false;
            @endphp
            <select data-noresults="{{ __( 'Enter tags', 'wpk' ) }}" data-allow-new="1" name="tags" id="tags" class="wpk-select2-under wpk-float-left" multiple="multiple" placeholder="{{ __( 'Choose tags...' )  }}">

            </select>
            <div class="wpk-selections">
                @foreach($campaignTags as $campaignTag)
                    <div class="wpk-selection" data-value="{{ $campaignTag->slug }}">#{{ $campaignTag->name }}</div>
                @endforeach
            </div>

        </div>
    </div>
</div>