<div class="wpk-my-campaigns-sidebar">
    <div class="row wpk-sidebar-section">
        @isset($campaign)
            <div class="wpk-campaign-title">
                {{ $campaign->post_title }}
            </div>
        @else
            <div>
                {{ __( 'Unnamed campaign', 'wpk' ) }}
            </div>
        @endif
    </div>
    <div class="wpk-sidebar-separator"></div>
</div>