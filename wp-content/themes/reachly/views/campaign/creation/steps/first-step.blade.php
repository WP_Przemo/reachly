<form class="wpk-new-campaign-form wpk-form-step-1" action="#" method="post">
    <input type="hidden" name="campaign_id" id="campaign_id"
           value=" @isset($campaign) {{ $campaign->ID  }} @endisset ">
    <div class="wpk-campaign-creation-section">
        <div class="wpk-campaign-creation-inner wpk-campaign-duration">
            <div class="wpk-step">
                <span>1</span>
            </div>
            <div class="wpk-title wpk-uppercase">
                {{ __( 'Choose duration :', 'wpk' ) }}
            </div>
            <input type="hidden" name="campaign_length" id="campaign_length" value="">
            <div class="wpk-durations">
                <div class="wpk-button" data-length="1">
                    <span>{{ __( '1 month', 'wpk' )  }}</span>
                </div>
                <div class="wpk-button" data-length="3">
                    <span>{{ __( '3 months', 'wpk' )  }}</span>
                </div>
                <div class="wpk-button" data-length="6">
                    <span>{{ __( '6 month', 'wpk' )  }}</span>
                </div>
                <div class="wpk-button" data-length="12">
                    <span>{{ __( '1 year', 'wpk' )  }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="wpk-campaign-creation-section">
        <div class="wpk-campaign-creation-inner">
            <div class="wpk-step">
                <span>2</span>
            </div>
            <div class="wpk-title wpk-uppercase">
                <span>{{ __( 'Choose plan:', 'wpk' )  }}</span>
            </div>
            <div class="wpk-campaign-plans wpk-tab" id="one_month">

                @isset($campaign)
                    <input type="hidden" id="campaign_price" value="{{ $campaign->meta('price') }}">
                    <input type="hidden" id="campaign_duration" value="{{ $campaign->meta('length') }}">
                @endisset

                @foreach($plans as $plan)
                    @include('campaign-plan.tab')
                @endforeach

            </div>
        </div>
    </div>
</form>