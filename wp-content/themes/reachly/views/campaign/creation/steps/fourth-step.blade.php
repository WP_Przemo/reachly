<?php
/**
 * @var \Wpk\Models\Campaign $campaign
 */
?>

<div class="wpk-form-wrap">
    @if ( $order->needs_payment() )
        <div class="wpk-notice">
            <span>{{ __( 'Your payment haven\'t been received yet. You can start inviting influencers after we receive payment.', 'wpk' )  }}</span>
        </div>
    @endif

    <div class="wpk-form-step-4 wpk-filter-target">
        <div class="wpk-campaign-notice">
            {!! $recommend !!}
            {{--<div class="wpk-notice-close wpk-float-right"><i class="material-icons">close</i></div>--}}
        </div>
    </div>

    @include('campaign.impressions-header-big')
    @include('influencer.grid')

</div>