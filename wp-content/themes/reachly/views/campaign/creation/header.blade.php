<?php

use Wpk\Utility;

?>

@php
    $steps = [
                1 => __( 'Choosing Plan', 'wpk' ),
                2 => __( 'Payment Options', 'wpk' ),
                3 => __( 'Description', 'wpk' ),
                4 => __( 'Influencers', 'wpk' ),
            ];

            $counter = 1;
            $total   = count( $steps );
@endphp
<div class="wpk-steps-wrapper">
    @foreach ( $steps as $index => $item )
        @php
            if ( empty( $campaign ) ) {
                if ( $index < $step ) {
                    $active = 'wpk-was-active';
                } else if ( $step == $index ) {
                    $active = 'wpk-active';
                } else {
                    $active = '';
                }
                $url = $active === 'wpk-was-active' ? add_query_arg( [ 'step' => $index ] ) : '';
            } else {
                $current_step = $campaign->creationStep();
                //Campaign already ordered
                if ( $current_step > 2 ) {
                    if ( $index < $current_step ) {
                        if ( $current_step == 4 && $index == 3 ) {
                            $active = 'wpk-was-active';
                        } else {
                            $active = 'wpk-was-active wpk-disabled';
                        }
                    } else if ( $current_step == $index ) {
                        $active = 'wpk-active';
                    } else {
                        $active = '';
                    }
                } else {
                    if ( $index < $current_step ) {
                        $active = 'wpk-was-active';
                    } else if ( $current_step == $index ) {
                        $active = 'wpk-active';
                    } else {
                        $active = '';
                    }
                }
                $url = $active === 'wpk-was-active' || $active === 'wpk-active' ? add_query_arg( [ 'step' => $index ] ) : '';
            }


        @endphp
        @if ( ! empty( $url ) )
            <a href="{{ $url }}" class="wpk-step wpk-tooltip-target {{ $active }}" data-step="{{ $item }}">
                @if ( ! empty( $active ) && Utility::contains( $active, 'wpk-was-active' ) )
                    <span class="material-icons">check</span>
                @endif
                <div class="wpk-tooltip wpk-tooltip-top">
                    <div class="wpk-tooltip-text">{{ $item }}</div>
                </div>
            </a>
        @else
            <div class="wpk-step wpk-tooltip-target @php echo $active @endphp" data-step="@php echo $item @endphp">
                @if ( ! empty( $active ) && Utility::contains( $active, 'wpk-was-active' ) )
                    <span class="material-icons">check</span>
                @endif
                <div class="wpk-tooltip wpk-tooltip-top">
                    <div class="wpk-tooltip-text">@php echo $item @endphp</div>
                </div>
            </div>
        @endif

        @if ( $counter < $total )
            <div class="wpk-step-separator"></div>
        @endif
        @php $counter ++; @endphp
    @endforeach
</div>