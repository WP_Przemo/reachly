<?php

use Wpk\Post_types\Campaign\Post;

global $campaign, $step;
$published = Post::getCampaignsByStatus( 'publish' );
$drafts = Post::getCampaignsByStatus( 'draft' );
$online = Post::getCampaignsByStatus( 'online' );
$started = Post::getCampaignsByStatus( 'started' );
$stopped = Post::getCampaignsByStatus( 'stopped' );
$ended = Post::getCampaignsByStatus( 'ended' );
$order_id = empty( $campaign ) ? false : $campaign->getOrderId();
$order = empty( $order_id ) ? false : wc_get_order( $order_id );
?>
<div class="wpk-my-campaigns-sidebar">
    <div class="row wpk-sidebar-section">
		<?php if ( ! isset( $_GET[ 'step' ] ) ) : ?>
        <a class="wpk-new-campaign-link" href="<?php echo \Wpk\Pages::getCampaignCreationUrl() ?>">
            <div class="wpk-circle-icon wpk-float-left">
                <div class="wpk-circle material-icons">
                    <span></span>
                </div>
            </div>
            <div class="wpk-message wpk-uppercase wpk-float-right"><?php _e( 'Create new campaign', 'wpk' ) ?></div>
        </a>
		<?php else: ?>
		<?php if ( ! \Wpk\Post_types\Campaign\Creation::isCampaignCreated() ) : ?>
        <div>
			<?php _e( 'Unnamed campaign', 'wpk' ) ?>
        </div>
		<?php else: ?>
        <div class="wpk-campaign-title">
			<?php echo $campaign->post->post_title ?>
        </div>

		<?php endif; ?>
		<?php endif; ?>
    </div>
    <div class="wpk-sidebar-separator"></div>
	<?php if ( $step == 3 ) : ?>
		<?php get_template_part( 'templates/company/sidebar/campaign-creation-sidebar' ) ?>
	<?php elseif ( $step == 4 && $order && ! $order->needs_payment() ) : ?>
		<?php get_template_part( 'templates/company/sidebar/campaign-influencers-filter' ) ?>
	<?php elseif(\Wpk\Utility::getPageTemplate() === 'my_campaigns.php'): ?>

    <div class="wpk-accordion">
        <div class="wpk-accordion-title wpk-sidebar-section">
            <span>- <?php printf( __( 'Drafts (%s)', 'wpk' ), $drafts->post_count ) ?></span>
            <ul class="wpk-accordion-list">
				<?php if ( $drafts->have_posts() ) {
					while ( $drafts->have_posts() ) {
						$drafts->the_post();
						get_template_part( 'templates/post-types/campaign/campaign-list' );
					}
				} ?>
            </ul>
        </div>
        <div class="wpk-sidebar-separator"></div>
        <div class="wpk-accordion-title wpk-sidebar-section">
            <span>- <?php printf( __( 'Published (%s)', 'wpk' ), $published->post_count ) ?></span>
            <ul class="wpk-accordion-list">
				<?php if ( $published->have_posts() ) {
					while ( $published->have_posts() ) {
						$published->the_post();
						get_template_part( 'templates/post-types/campaign/campaign-list' );
					}
				} ?>
            </ul>
        </div>
        <div class="wpk-sidebar-separator"></div>
        <div class="wpk-accordion-title wpk-sidebar-section">
            <span>- <?php printf( __( 'Started (%s)', 'wpk' ), $started->post_count ) ?></span>
            <ul class="wpk-accordion-list">
				<?php if ( $started->have_posts() ) {
					while ( $started->have_posts() ) {
						$started->the_post();
						get_template_part( 'templates/post-types/campaign/campaign-list' );
					}
				} ?>
            </ul>
        </div>
        <div class="wpk-sidebar-separator"></div>
        <div class="wpk-accordion-title wpk-sidebar-section">
            <span>- <?php printf( __( 'Stopped (%s)', 'wpk' ), $stopped->post_count ) ?></span>
            <ul class="wpk-accordion-list">
				<?php if ( $stopped->have_posts() ) {
					while ( $stopped->have_posts() ) {
						$stopped->the_post();
						get_template_part( 'templates/post-types/campaign/campaign-list' );
					}
				} ?>
            </ul>
        </div>
        <div class="wpk-sidebar-separator"></div>
        <div class="wpk-accordion-title wpk-sidebar-section">
            <span>- <?php printf( __( 'Ended (%s)', 'wpk' ), $ended->post_count ) ?></span>
            <ul class="wpk-accordion-list">
				<?php if ( $ended->have_posts() ) {
					while ( $ended->have_posts() ) {
						$ended->the_post();
						get_template_part( 'templates/post-types/campaign/campaign-list' );
					}
				} ?>
            </ul>
        </div>
        <div class="wpk-sidebar-separator"></div>
    </div>

	<?php endif; ?>
</div>