<div class="wpk-wrap wpk-flex row wpk-my-campaigns">
    @if( false )

        <div class="col-xl-12 col-md-12 wpk-wrap-inner">
            <a class="wpk-circle-icon wpk-has-message wpk-full-icon" href="{{ $profileUrl }}">
                <div class="wpk-circle">
                    <div class="wpk-circle-inner">
                        <i class="material-icons">check</i>
                    </div>
                </div>
                <div class="wpk-message">
					<?php _e( 'Verify your account', 'wpk' ) ?>
                </div>
            </a>
        </div>

    @else

        @include('sidebar', ['template' => $sidebar])

        <div class="col-sm-12 col-xl-10 col-md-8 wpk-wrap-inner">

            @include('campaign.creation.header')

            <div class="wpk-step-inner wpk-step-{{ $step }}" data-step="{{ $step }}">
                @include($stepTemplate)
            </div>
        </div>

    @endif
</div>