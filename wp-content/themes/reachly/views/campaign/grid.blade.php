<?php
/**
 * @var \Wpk\Models\Collection $campaigns
 * @var \Wpk\Models\User       $author
 */
?>
<div class="wpk-wrap row wpk-my-campaigns">

    @if($type === 'company')
        @include('sidebar', ['template' => 'campaign.grid.sidebar'])
    @endif
    <div class="<?php echo $type === 'company' ? 'col-xl-10' : 'col-xl-12' ?> {{ $type === 'company' ? 'col-xl-10' : 'col-xl-12' }} col-md-12 wpk-wrap-inner">

        @if($campaigns->empty() && $type === 'company')

            <div class="wpk-new-campaign">
                <a href="{{ \Wpk\Pages::getNewCampaignsUrl() }}">
                    <div class="wpk-circle-icon">
                        <div class="wpk-circle">
                            <span>+</span>
                        </div>
                    </div>
                    <div class="wpk-message">
                        {{ __( 'Create your first campaign ;)', 'wpk' )  }}
                    </div>
                </a>
            </div>

        @else

            @if( !empty( $author ) )
                <div class="wpk-navigation">
                    <a href="{{ \Wpk\Pages::getBrandsPageUrl() }}" class="wpk-border-round-button wpk-prev">
                        <i class="material-icons">keyboard_arrow_left</i>
                    </a>
                </div>
                <div class="wpk-company-logo">
                    <img src="{{ $author->getLogo() }}" alt="">
                </div>
                <div class="wpk-company-website">
                    @if( !empty($user->meta('website')) )
                        <a href="{!! $user->meta('website') !!}">{{ $user->meta('website') }}</a>
                    @endif
                </div>

            @endif

            <div class="wpk-campaigns-grid row">
                @foreach($campaigns->all() as $campaign)
                    @include("campaign.grid.element")
                @endforeach
            </div>

            <div class="wpk-pagination col-xl-12" data-container=".wpk-campaigns-grid" data-el=".wpk-campaign">
                {!!
                 paginate_links( array(
                     'prev_text' => '<i class="material-icons">keyboard_arrow_left</i>',
                     'next_text' => '<i class="material-icons">keyboard_arrow_right</i>',
                     'current'   => $paged,
                     'end_size'  => 1,
                     'mid_size'  => 5,
                     'total'        => $campaigns->query->max_num_pages,
                 ) );
                 !!}
            </div>

        @endif
    </div>
</div>