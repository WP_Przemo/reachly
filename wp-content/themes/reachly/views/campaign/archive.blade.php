<div class="wpk-wrap">
    <div class="wpk-wrap-inner">
        @if(!$campaigns->empty())

            @include('campaign.grid')

        @else

            <div class="wpk-has-message">
                <div class="wpk-message">
                    <span>{{ __( 'No campaigns found.', 'wpk' ) }}</span>
                    <a href="{{ \Wpk\Pages::getBrandsPageUrl() }}" class="wpk-link">{{ __('Return', 'wpk') }}</a>
                </div>
            </div>

        @endif
    </div>
</div>