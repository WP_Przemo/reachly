<div class="wpk-my-campaigns-sidebar">
    <div class="row wpk-sidebar-section">
        <a class="wpk-new-campaign-link" href="{{ \Wpk\Pages::getCampaignCreationUrl() }}">
            <div class="wpk-circle-icon wpk-float-left">
                <div class="wpk-circle material-icons">
                    <span></span>
                </div>
            </div>
            <div class="wpk-message wpk-uppercase wpk-float-right">
                {{ __( 'Create new campaign', 'wpk' ) }}
            </div>
        </a>
    </div>
    <div class="wpk-sidebar-separator"></div>

    <div class="wpk-accordion">

        @include('campaign.grid.accordion', ['status' => __('Drafts', 'wpk'), 'campaigns' => $drafts])
        <div class="wpk-sidebar-separator"></div>
        @include('campaign.grid.accordion', ['status' => __('Published', 'wpk'), 'campaigns' => $published])
        <div class="wpk-sidebar-separator"></div>
        @include('campaign.grid.accordion', ['status' => __('Started', 'wpk'), 'campaigns' => $started])
        <div class="wpk-sidebar-separator"></div>
        @include('campaign.grid.accordion', ['status' => __('Ended', 'wpk'), 'campaigns' => $ended])
        <div class="wpk-sidebar-separator"></div>

    </div>
</div>