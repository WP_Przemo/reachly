<?php
/**
 * @var \Wpk\Models\Campaign $campaign
 */
?>

@php
    $actions = \Wpk\Campaign\GridActions::campaignElement($campaign, $user);
    $isAuthor = $campaign->post_author == $user->ID;
@endphp

<div class="wpk-campaign col-lg-6 col-xl-4 col-md-6 col-xs-12 wpk-element">
    <div class="wpk-campaign-inner wpk-element-inner">
        <a href="{{ $isAuthor ? $campaign->getUrl() : $campaign->getPermalink() }}" class="wpk-campaign-image wpk-element-image">

            <img src="{{ get_the_post_thumbnail_url( $campaign->ID ) }}" alt="">
            @if ( ! empty( $campaign->startDate() ) )
                <div class="wpk-campaign-duration">
                    <span>{{ sprintf( '%s - %s', $campaign->startDate(), $campaign->endDate() )  }}</span>
                </div>
            @endif
        </a>
        <div class="wpk-campaign-data">
            <div class="wpk-campaign-post">
                <div class="wpk-campaign-title">
                    <span>{{ $campaign->post_title }}</span>
                </div>
                <div class="wpk-campaign-status">

                    <span>
                    @if($isAuthor)
                            {{ ucfirst( $campaign->post_status ) }}
                        @else
                            {{ $campaign->post_status === 'draft' ? __('Publish',  'wpk') : ucfirst($campaign->post_status ) }}
                        @endif
                    </span>

                </div>
            </div>
            <div class="wpk-element-actions">
                <a href="{{ $actions[0]['link'] }}" class="wpk-element-action wpk-{{ $actions[0]['action'] }}">
                    <span class="wpk-action-icon"><i class="material-icons"></i></span>
                    <span>{{ $actions[0]['label'] }}</span>
                </a>
                @isset($actions[1])

                    <a href="{{ $actions[1]['link'] }}" class="wpk-element-action wpk-{{ $actions[1]['action'] }}">
                        <span class="wpk-action-icon"><i class="material-icons"></i></span>
                        <span>{{ $actions[1]['label'] }}</span>
                    </a>

                @endisset
            </div>
        </div>
    </div>
</div>
