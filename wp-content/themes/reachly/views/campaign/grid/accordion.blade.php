<?php
/**
 * @var \Wpk\Models\Campaign $campaign
 */
?>

<div class="wpk-accordion-title wpk-sidebar-section">
    <span>- {{ $status }} ({{ $campaigns->count() }})</span>
    <ul class="wpk-accordion-list">
        @foreach($campaigns->all() as $campaign)
            <li>
                <a href="{{ $campaign->getUrl() }}">
                    <div class="wpk-list-thumbnail">
                        <img src="{{ $campaign->getLogoUrl() }}" alt="">
                    </div>
                    <div class="wpk-list-title">
                        <span>{{ $campaign->post_title }}</span>
                    </div>
                </a>
            </li>
        @endforeach
    </ul>
</div>