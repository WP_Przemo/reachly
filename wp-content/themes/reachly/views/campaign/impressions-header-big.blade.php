<div class="wpk-campaign-impressions wpk-campaign-impressions-big wpk-fixable">

    <div class="wpk-title wpk-float-left">
        <h2>{{ __( 'Impressions left:', 'wpk' ) }}</h2>
    </div>
    <div class="wpk-impressions-slider-outer wpk-float-left">
        <div class="wpk-impressions-slider" data-min="0" data-max="{{ $initialImpressions }}" data-value="{{ $currentImpressions }}"></div>
        <div class="wpk-impressions-amount">
                <span>
                    <strong class="wpk-impressions-value">{{ $currentImpressions }}</strong> {{ __('impressions left', 'wpk') }}
                </span>
            @if ( \Wpk\Utility::getPageTemplate() === 'new_campaign.php' && ! $campaign->haveReachedImpressions() )
                @php
                    $target = $campaign->getPlan()->getRequiredImpressions();
                @endphp
                <div class="wpk-tooltip-target wpk-notice">
                    <i class="material-icons">warning</i>
                    <div class="wpk-tooltip">
                        <div class="wpk-tooltip-text">
                            {{ sprintf( __( 'In order to publish this campaign you need to have at least %s remaining impressions. Pending invites are not taken into account.', 'wpk' ), $target ) }}
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @if ( ! $campaign->fullyCreated() )
        <div class="wpk-button wpk-publish-campaign wpk-float-right {{ $campaign->haveReachedImpressions() ? '' : 'wpk-disabled' }}" data-campaign-id="{{ $campaign->ID }}">
            <span>{{ __( 'Publish', 'wpk' ) }}</span>
        </div>
        <input type="hidden" name="wpk_nonce" id="wpk_nonce" value="{{ wp_create_nonce('wpk_publish_campaign') }}">
    @endif
    <a href="{{ \Wpk\Pages::getMyCampaignsUrl() }}" class="wpk-button wpk-save-campaign wpk-float-right">
        <span>{{ __( 'Back to campaigns', 'wpk' ) }}</span>
    </a>

</div>