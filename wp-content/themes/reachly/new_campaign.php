<?php
/**
 * Template Name: New campaign
 *
 * @package wpk
 */

get_header();

do_action('wpk/campaignCreationPage');

get_footer();